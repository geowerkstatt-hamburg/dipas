import {fileURLToPath, URL} from "node:url";
import {defineConfig} from "vite";
import vue from "@vitejs/plugin-vue";
import legacy from "@vitejs/plugin-legacy";
import * as path from "path";

/**
 * Variable stores the host (e.g. lindenallee.localhost:8081),
 * see server.proxy.configure, used in server.proxy.rewrite
 */
let proxyReqHost;

export default defineConfig({
  plugins: [
    // In the vitetest.config.js vue() is expected to be the first element
    // in the array. If this is changed the vitest.config.js has to be adjusted
    // accordingly. If any parameters are added to vue() the vitest.config.js may
    // have to be adjusted, too.
    vue(),
    legacy({
      targets: [
        "> 1%",
        "last 2 versions",
        "not dead",
        "not IE 11",
      ]
    })
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "./src/scss/_global.scss";
        `
      }
    }
  },
  server: {
    port: 8080,
    strictPort: true,
    host: true,
    https: false,
    proxy: {
      /**
       * Proxy the API requests in order to reach the DIPAS backend to avoid CORS problems.
       */
      "^/drupal": {
        target: "http://glue:8081",
        changeOrigin: true,
        /**
         * Store host (e.g. "lindenallee.localhost:8081") to global variable. Use it in rewrite function.
         */
        configure: (proxy) => {
          proxy.on("proxyReq", (proxyReq, req) => {
            proxyReqHost = req.headers.host;
          });
        },
        /**
         * Extract the subdomain from proxyReqHost, e.g. lindenallee
         * Rewrite urlPath "/drupal/dipas/contributionmap" to "/drupal/dipas/lindenallee/contributionmap"
         */
        rewrite: (urlPath) => {
          const subdomain = proxyReqHost?.split(".")[0];

          return urlPath.replace("/drupal/dipas", `/drupal/dipas/${subdomain}`);
        }
      }
    }
  },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      "~bootstrap": path.resolve(__dirname, "node_modules/bootstrap"),
    }
  },
  optimizeDeps: {
    exclude: ["customParseFormat"]
  }
});
