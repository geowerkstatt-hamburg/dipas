/**
 * Stringifies an ordered version of the given jsObject.
 *
 * @private
 * @param {Object} jsObject
 * @returns {*|string}
 */
// eslint-disable-next-line func-style
export const $serializeObject = function (jsObject) {
  if (Array.isArray(jsObject)) {
    return JSON.stringify(
      jsObject.map(item => $serializeObject(item))
    );
  }
  else if (typeof jsObject === "object" && jsObject !== null) {
    return Object.keys(jsObject)
      .sort()
      .map(key => `${key}:${$serializeObject(jsObject[key])}`)
      .join("|");
  }

  return jsObject;
};

/**
 * Generates a simplified "non-cryptographic/non-secure" checksum.
 * Examples: N322374350, P870797008
 *
 * @private
 * @param {string} str
 * @returns {string}
 */
// eslint-disable-next-line func-style
export const $createSimpleChecksum = function (str) {
  let hash = 0;

  for (let i = 0, len = str.length; i < len; i++) {
    hash = (hash << 5) - hash + str.charCodeAt(i);
    hash |= 0; // Convert to 32bit integer
  }

  hash = hash.toString();

  // Prefix string with P (positive) or N (negative)
  let signPrefix = "P";

  if (hash < 0) {
    signPrefix = "N";
    hash = hash.substring(1, hash.length);
  }

  return `${signPrefix}${hash}`;
};

/**
 * Generates a very simplified "non-cryptographic/unsecure" checksum for a given object,
 * regardless of the order of the data in the object.
 *
 * Example 1 generates the same checksum as example 2.
 *
 * Example 1:
 * jsObject:        {item1: {subItemB: 2, subItemA: undefined}, item2: [{keyC: 1}, {keyE: 3, keyD: 2}]}
 * serializeObject: "item1:subItemA:undefined|subItemB:2|item2:[\"keyC:1\",\"keyD:2|keyE:3\"]"
 * getChecksum:     P451897847
 *
 * Example 2:
 * jsObject:        {item2: [{keyC: 1}, {keyD: 2, keyE: 3}], item1: {subItemA: undefined, subItemB: 2}}
 * serializeObject: "item1:subItemA:undefined|subItemB:2|item2:[\"keyC:1\",\"keyD:2|keyE:3\"]"
 * getChecksum:     P451897847
 */

/**
 * Generates the checksum of an object, regardless of the order of the data in the object.
 * @param {Object} jsObject
 * @returns {string}
 */
// eslint-disable-next-line func-style
export const $generateObjectChecksum = function (jsObject) {
  const serializedObject = $serializeObject(jsObject);
  const simpleChecksum = $createSimpleChecksum(serializedObject);

  return simpleChecksum;
};

export default {
  install: (app) => {
    // Inject for Vuex, access with this.$store.getters.generateObjectChecksum(obj1);
    app.config.globalProperties.$store.getters.$generateObjectChecksum = $generateObjectChecksum;
    app.config.globalProperties.$store.getters.$serializeObject = $serializeObject;
    app.config.globalProperties.$store.getters.$createSimpleChecksum = $createSimpleChecksum;

    // Inject for direct access in components, e.g. this.generateObjectChecksum(obj1);
    app.mixin({
      methods: {
        $generateObjectChecksum,
        $serializeObject,
        $createSimpleChecksum,
      }
    });
  }
};
