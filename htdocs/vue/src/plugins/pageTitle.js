export default {
  install: (app) => {
    app.mixin({
      computed: {
        /**
         * This is necessary, because the init-endpoint takes too long to answer after a cache rebuild in drupal.
         * Without this computed property combined with the watcher below, the page title might look like "Beiträge | ",
         * missing the proceeding title.
         */
        proceedingTitle () {
          return this.$store.getters["proceeding/proceedingTitle"];
        }
      },
      watch: {
        htmlPageTitle (title) {
          this.setHtmlPageTitle(title);
        },
        proceedingTitle () {
          this.setHtmlPageTitle(this.htmlPageTitle);
        }
      },
      mounted () {
        this.setHtmlPageTitle(this.htmlPageTitle);
      },
      methods: {
        /**
         * Sets the HTML title tag. Shown in the browser tab and used as the label in bookmarks.
         *
         * In any page component, set the htmlPageTitle for the page inside a "computed" (if dynamic data is used)
         * or in "data" for static titles.
         *
         * Dynamic example (e.g. access other computed properties like contributionId):
         *
         * computed: {
         *   htmlPageTitle () {
         *     return this.$t("pages.ContributionDetails.htmlPageTitle", {
         *       contributionId: this.currentContributionId,
         *       proceedingTitle: this.proceedingTitle,
         *     });
         *   },
         *  }
         *
         * Static example:
         *
         * data () {
         *   return {
         *     htmlPageTitle: this.$t("pages.Contributions.htmlPageTitle"),
         *   };
         * },
         *
         * @param {string} title The title for the current page
         */
        setHtmlPageTitle (title) {
          if (title !== undefined) {
            document.title = title + (title.length ? " | " : "") + this.$store.getters["proceeding/proceedingTitle"];
          }
        }
      }
    });
  }
};
