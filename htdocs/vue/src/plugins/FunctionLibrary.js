export const FunctionLibrary = {
  // eslint-disable-next-line no-unused-vars
  install (app, options) {
    app.config.globalProperties.$generateUUID = function () {
      return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0,
          // eslint-disable-next-line no-mixed-operators
          v = c === "x" ? r : r & 0x3 | 0x8;

        return v.toString(16);
      });
    };

    app.config.globalProperties.$isObject = function (item) {
      return item && typeof item === "object" && !Array.isArray(item);
    };

    app.config.globalProperties.$mergeDeep = function (target, ...sources) {
      if (!sources.length) {
        return target;
      }
      const source = sources.shift();

      if (
        app.config.globalProperties.$isObject(target) &&
        app.config.globalProperties.$isObject(source)
      ) {
        for (const key in source) {
          if (app.config.globalProperties.$isObject(source[key])) {
            if (!target[key]) {
              Object.assign(target, {[key]: {}});
            }
            app.config.globalProperties.$mergeDeep(target[key], source[key]);
          }
          else {
            Object.assign(target, {[key]: source[key]});
          }
        }
      }

      return app.config.globalProperties.$mergeDeep(target, ...sources);
    };

    app.config.globalProperties.$hex2rgb = function (hex) {
      const result = (/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i).exec(hex);

      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    };

    app.config.globalProperties.$ucFirst = function (string, lowerCaseTail = false) {
      return string.substring(0, 1).toUpperCase() + (!lowerCaseTail ? string.substring(1) : string.substring(1).toLowerCase());
    };

    app.config.globalProperties.$shuffleArray = function (array) {
      let currentIndex = array.length,
        randomIndex;

      while (currentIndex > 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [
          array[randomIndex], array[currentIndex]];
      }

      return array;
    };

    app.config.globalProperties.$snakeCaseToPascalCase = function (string, delimiter = "_") {
      let stringElements = string.split(delimiter);

      for (const index in stringElements) {
        stringElements[index] = app.config.globalProperties.$ucFirst(stringElements[index], true);
      }

      return stringElements.join("");
    };
  }
};
