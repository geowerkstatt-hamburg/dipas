export default {
  // eslint-disable-next-line no-unused-vars
  install: (app, options) => {
    app.mixin({
      data () {
        return {
          bootstrapBreakpoints: {
            xs: 0,
            sm: 576,
            md: 768,
            lg: 992,
            xl: 1200,
            xxl: 1400
          },
          breakpointAssignments: {
            isMobile: ["xs", "sm"],
            isTablet: ["md", "lg"],
            isDesktop: ["xl", "xxl"]
          },
          activeBootstrapBreakpoint: undefined,
          isMobile: undefined,
          isTablet: undefined,
          isDesktop: undefined,
          orientation: ""
        };
      },
      mounted () {
        screen.orientation.addEventListener("change", this.$onResize);
        window.addEventListener("resize", this.$onResize);
        this.$onResize();
      },
      methods: {
        $onResize () {
          const breakpointArray = Object.keys(this.bootstrapBreakpoints).map(key => [key, this.bootstrapBreakpoints[key]]);
          const activeBreakpoint = breakpointArray.find(([, width], index) => {
            const nextBreakpoint = Object.entries(this.bootstrapBreakpoints)[index + 1];

            return innerWidth >= width && (nextBreakpoint !== undefined && innerWidth < nextBreakpoint[1] || nextBreakpoint === undefined);
          });

          this.activeBootstrapBreakpoint = activeBreakpoint[0];

          this.orientation = innerWidth >= innerHeight ? "isLandscape" : "isPortrait";

          Object.entries(this.breakpointAssignments).forEach(([screenSize, breakpoints]) => {
            this[screenSize] = breakpoints.indexOf(this.activeBootstrapBreakpoint) > -1;
          });

          let cssClasses = this.$el.className;

          if (typeof cssClasses !== "string") {
            cssClasses = "";
          }
          cssClasses = cssClasses.split(" ");
          cssClasses = cssClasses.filter(elem => {
            return Object.keys(this.bootstrapBreakpoints).indexOf(elem.replace(/^bootstrap-/, "")) === -1 &&
              ["isMobile", "isTablet", "isDesktop", "isLandscape", "isPortrait"].indexOf(elem) === -1;
          });
          cssClasses.push("bootstrap-" + this.activeBootstrapBreakpoint);
          ["isMobile", "isTablet", "isDesktop"].forEach(screenSize => {
            if (this[screenSize]) {
              cssClasses.push(screenSize);
            }
          });
          cssClasses.push(this.orientation);

          try {
            this.$el.className = cssClasses.join(" ").trim();
          }
          catch (e) {
            // comment prevents linter error
          }
        }
      }
    });
  }
};

