import {createStore} from "vuex";
import cookie from "vue-cookies";
import * as CryptoJS from "crypto-js";

// Store modules
import proceeding from "@/store/modules/proceeding.js";
import contributions from "@/store/modules/contributions.js";
import interactions from "@/store/modules/interactions.js";
import conceptions from "@/store/modules/conceptions.js";
import appointments from "@/store/modules/appointments.js";
import statistics from "@/store/modules/statistics.js";

export default createStore({
  state: {
    cacheTTL: 60 * 60 * 1000, // seconds
    securityToken: {
      passphrase: "NYZM6QccC8f7pZEoN7U37TxyExfPBxIP",
      initializingVector: undefined,
      token: undefined,
      decryptedToken: undefined,
      initializingTimestampBackend: undefined,
      initializingTimestampFrontend: undefined
    },
    cookiesAccepted: Boolean(Object.keys(cookie.get("dipas") ?? {}).length)
  },
  actions: {
    doRequest ({getters}, request) {
      const requestParams = Object.assign({
          basePath: "/drupal/dipas/",
          method: "GET"
        }, request),
        token = getters.requestToken();

      let queryString = "";

      if (requestParams.query) {
        queryString = "?" + Object.entries(requestParams.query)
          .map(([key, value]) => key + "=" + encodeURIComponent(value))
          .join("&");
      }

      if (token) {
        queryString += (queryString.length ? "&" : "?")
         + "token=" + token;
      }

      switch (requestParams.method.toUpperCase()) {
        case "POST":
          return new Promise((resolve, reject) => {
            fetch(
              requestParams.basePath + requestParams.endpoint + queryString,
              {
                method: "POST",
                body: JSON.stringify(requestParams.payload),
              }
            ).then(async response => {
              const responseData = await response.json();

              if (responseData.status === "success") {
                resolve(responseData);
              }
              else {
                reject(response);
              }
            }).catch(error => {
              reject(error);
            });
          });

        case "GET":
        default:
          return new Promise((resolve, reject) => {
            fetch(
              requestParams.basePath + requestParams.endpoint + queryString,
              {method: "GET"}
            ).then(async response => {
              const responseData = await response.json();

              if (responseData.status === "success") {
                resolve(responseData);
              }
              else {
                reject(response);
              }
            }).catch(error => {
              reject(error);
            });
          });
      }
    },
    confirmCookies ({dispatch, commit}) {
      dispatch(
        "doRequest",
        {
          endpoint: "confirmcookies",
          method: "POST",
          payload: {
            confirmCookies: true
          }
        }
      ).then((response) => {
        commit("setCookiesAccepted", response.status === "success");
      });
    },
    loadEndpoint ({dispatch, commit, getters, state}, endpoint) {
      const data = getters.endpoint(endpoint);

      if (!data.content || ((data.timestamp + state.cacheTTL) < new Date().getTime())) {
        dispatch(
          "doRequest", {
            endpoint: endpoint,
            method: "GET"
          }
        ).then(response => {
          commit("endpoint", {endpoint, content: response.content});
          return response.body;
        });
      }
    }
  },
  mutations: {
    handleError () {
      // TODO - TBD
    },
    setRequestToken (state, {token, initializingVector, initializingTimestamp}) {
      state.securityToken.token = token;
      state.securityToken.initializingVector = initializingVector;
      state.securityToken.initializingTimestampBackend = Number(initializingTimestamp);
      state.securityToken.initializingTimestampFrontend = new Date().getTime();

      let decrypted = CryptoJS.AES.decrypt(
        token,
        CryptoJS.enc.Utf8.parse(state.securityToken.passphrase),
        {
          iv: CryptoJS.enc.Utf8.parse(state.securityToken.initializingVector),
        }
      );

      state.securityToken.decryptedToken = decrypted.toString(CryptoJS.enc.Utf8);
    },
    setCookiesAccepted (state, status) {
      state.cookiesAccepted = status;
    },
    endpoint (state, payload) {
      state[payload.endpoint] = {
        timestamp: new Date().getTime(),
        content: payload.content
      };
    }
  },
  getters: {
    tokenData: (state) => (detail) => {
      return state.securityToken[detail] ?? false;
    },
    requestToken: (state, getters) => (now = new Date().getTime()) => {
      if (getters.tokenData("decryptedToken")) {
        const timeDifference = now - getters.tokenData("initializingTimestampFrontend"),
          requestTime = getters.tokenData("initializingTimestampBackend") + timeDifference,
          requestToken = getters.tokenData("decryptedToken") + ":|:" + requestTime;

        return CryptoJS.AES.encrypt(
          requestToken,
          CryptoJS.enc.Utf8.parse(getters.tokenData("passphrase")),
          {
            iv: CryptoJS.enc.Utf8.parse(getters.tokenData("initializingVector")),
          }
        ).toString().replace(/\+/g, "%2b");
      }

      return false;
    },
    cookieData: () => (cookieName = "dipas") => {
      return cookie.get(cookieName) ?? false;
    },
    cookiesAccepted (state) {
      return state.cookiesAccepted;
    },
    endpoint: (state) => (endpoint) => {
      return !state[endpoint]
        ? {}
        : state[endpoint];
    },
  },
  modules: {
    proceeding,
    contributions,
    interactions,
    conceptions,
    appointments,
    statistics
  }
});
