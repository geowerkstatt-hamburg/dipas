import {isProxy, toRaw} from "vue";

export default {
  namespaced: true,
  state: {
    cacheTTL: 60 * 60 * 1000, // seconds
    configuration: undefined,
    contributionMapExtent: undefined,
    contentPages: {}
  },
  actions: {
    async initializeProceeding ({dispatch, commit}) {
      await dispatch(
        "doRequest",
        {endpoint: "init"},
        {root: true}
      ).then(configuration => {
        commit(
          "setRequestToken",
          {
            token: configuration.checksum,
            initializingVector: configuration.signature,
            initializingTimestamp: configuration.timestamp
          },
          {
            root: true
          }
        );
        commit("setProceedingConfiguration", configuration);
      });

      return true;
    },
    async projectareaExtent ({dispatch, commit, state}) {
      if (state.contributionMapExtent) {
        return Promise.resolve();
      }

      return dispatch(
        "doRequest",
        {endpoint: "contributions"},
        {root: true}
      ).then(response => {
        commit("setContributionMapExtent", response.extent);
      });
    },
    loadEndpoint ({dispatch, commit, getters, state}, {endpoint, storeModule}) {
      const data = getters.endpoint(endpoint);

      if (!data.content || ((data.timestamp + state.cacheTTL) < new Date().getTime())) {
        dispatch(
          "doRequest", {
            endpoint,
            method: "GET"
          },
          {root: true}
        ).then(response => {
          commit(storeModule + "/endpoint", {endpoint, response}, {root: true});
        });
      }
    }
  },
  mutations: {
    setProceedingConfiguration (state, configuration) {
      state.configuration = configuration;
    },
    setContributionMapExtent (state, extent) {
      state.contributionMapExtent = extent;
    },
    endpoint (state, payload) {
      state.contentPages[payload.endpoint] = {
        timestamp: new Date().getTime(),
        content: payload.response
      };
    }
  },
  getters: {
    configurationValue: (state) => (valueID) => {
      let configurationPathElements = valueID.split("."),
        configPathElement,
        configuration;

      try {
        configuration = JSON.parse(JSON.stringify(state.configuration));

        while (
          configuration &&
          // eslint-disable-next-line no-cond-assign
          (configPathElement = configurationPathElements.shift())
        ) {
          configuration = configuration[configPathElement] ?? undefined;
        }

        return configuration;
      }
      catch (e) {
        return undefined;
      }
    },
    navigationLinks: (state) => (which) => {
      let linksRaw = state.configuration?.menus[which] ?? [],
        links = [];

      if (isProxy(linksRaw)) {
        linksRaw = toRaw(linksRaw);
      }

      Object.entries(linksRaw).forEach(([endpoint, linkdata]) => {
        const link = {
          endpoint,
          linktext: typeof linkdata === "object" ? linkdata.name : linkdata
        };

        if (linkdata.url) {
          link.url = linkdata.url;
        }

        links.push(link);
      });

      return links;
    },
    proceedingTitle (state) {
      return state.configuration?.projecttitle ?? "";
    },
    proceedingPhase (state) {
      return state.configuration?.projectphase ?? "";
    },
    proceedingActive (state, getters) {
      return ["phase1", "phasemix", "phase2"].indexOf(getters.proceedingPhase) > -1;
    },
    enabledPhase2 (state) {
      return state.configuration?.enabledPhase2;
    },
    takesNewContributions (state) {
      return state.configuration?.contributions.status === "open";
    },
    landingPageText (state) {
      return state.configuration?.landingpage?.text ?? "";
    },
    landingPageImage (state) {
      return state.configuration?.landingpage?.image ?? false;
    },
    landingPageButtons (state) {
      return state.configuration?.landingpage?.buttons ?? {button_default_behavior: 1};
    },
    hasProjectInfopage (state) {
      return state.configuration?.landingpage?.hasProjectInfopage ?? false;
    },
    projectOwner (state) {
      return state.configuration?.projectowner ?? false;
    },
    projectDownloads (state) {
      return state.configuration?.downloads ?? false;
    },
    hasMenuItem: (state) => (menu, item) => {
      return Boolean(state.configuration?.menus[menu][item]);
    },
    createUrlWithZoomToExtend: (state, getters) => (baseUrl) => {
      const urlQueryParams = {
        castToPoint: true,
        projection: "EPSG:4326",
        zoomToExtent: getters.contributionMapExtent.lon_min + "," +
          getters.contributionMapExtent.lat_min + "," +
          getters.contributionMapExtent.lon_max + "," +
          getters.contributionMapExtent.lat_max
      };

      return baseUrl + "?" +
        Object.entries(urlQueryParams).map(([key, value]) => {
          return key + "=" + value;
        }).join("&");
    },
    masterportalUrl: (state, getters) => (instanceID) => {
      switch (instanceID) {
        // URL for contribution wizard, auto zoom to extend
        case "createcontribution":
          return getters.createUrlWithZoomToExtend(state.configuration?.masterportal_instances.createcontribution.url);

        // URL for contribution wizard, if coordinates are injected via GET params. (see DIPAS table QR-Code tool)
        case "createcontributionPlainUrl":
          return state.configuration?.masterportal_instances.createcontribution.url;

        // URL for contribution map page, auto zoom to extend
        case "contributionmap":
          return getters.createUrlWithZoomToExtend(state.configuration?.masterportal_instances.contributionmap);

        case "contributionmapPlainUrl":
          return state.configuration?.masterportal_instances.contributionmap;

          // URL for contribution detail page
        case "singlecontribution":
          return state.configuration?.masterportal_instances.singlecontribution.url;

        default:
          return state.configuration?.masterportal_instances[instanceID];
      }
    },
    contributionMapExtent (state) {
      return state.contributionMapExtent ?? {};
    },
    taxonomyTerms: (state) => (vocab) => {
      return state.configuration?.taxonomy[vocab] ?? [];
    },
    taxonomyTermProperty: (state, getters) => (vocab, termID, property) => {
      let vocabTerms = getters.taxonomyTerms(vocab);

      return vocabTerms.length && termID
        ? getters.taxonomyTerms(vocab).find(elem => Number(elem.id) === Number(termID))[property] ?? ""
        : undefined;
    },
    typesIncluded (state) {
      return state.configuration?.contributionWizard.filter(type => {
        return type.id === "type";
      }).length > 0;
    },
    // Contributions options
    contributionsCommentsOpen: function (state) {
      return state.configuration?.contributions.comments.form === "open";
    },
    contributionsCommentsShow: function (state, getters) {
      return getters.contributionsCommentsOpen || getters.configurationValue("contributions.comments.display");
    },
    contributionsRatingsAllowed: function (state) {
      return state.configuration?.contributions.ratings ?? false;
    },
    contributionsRatingsShowExisting: function (state, getters) {
      return getters.contributionsRatingsAllowed || getters.configurationValue("contributions.display_existing_ratings");
    },
    contributionsCommentsMaxLength: function (state) {
      return state.configuration?.contributions.comments.maxlength ?? 0;
    },
    contributionsMaxLength: function (state) {
      return state.configuration?.contributions.maxlength ?? 0;
    },
    contributionGeometryType: function (state) {
      return state.configuration?.contributions.geometry ?? [];
    },
    // returns the key question for contributions, if it is configured in the backend. will be displayed on contributions list and contributions detail page
    contributionsPageSubheadline: function (state) {
      return state.configuration?.contributions.contributions_page_subheadline;
    },
    endpoint: (state) => (endpoint) => {
      return state.contentPages[endpoint]
        ? state.contentPages[endpoint]
        : {};
    }
  }
};
