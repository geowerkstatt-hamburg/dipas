export default {
  namespaced: true,
  state: {
    contentPages: {}
  },
  actions: {},
  mutations: {
    // This mutation is intentionally duplicated code in order to enable
    // for the redirection of vuex state data.
    endpoint (state, payload) {
      state.contentPages[payload.endpoint] = {
        timestamp: new Date().getTime(),
        content: payload.response
      };
    }
  },
  getters: {
    // This getter is intentionally duplicated code in order to enable
    // for the redirection of vuex state data.
    endpoint: (state) => (endpoint) => {
      return state.contentPages[endpoint]
        ? state.contentPages[endpoint]
        : {};
    },
    conceptionCommentsOpen (state, getters, rootState, rootGetters) {
      return rootGetters["proceeding/configurationValue"]("conception_comments_state") === "open";
    },
    displayConceptionComments (state, getters, rootState, rootGetters) {
      return rootGetters["proceeding/configurationValue"]("display_existing_conception_comments") ?? false;
    },
    allConceptions (state) {
      const conceptions = [];

      if (state.contentPages?.conceptionlist) {
        state.contentPages?.conceptionlist.content.content.forEach(content => {
          if (content.bundle === "division_in_planning_subareas") {
            content.field_content.forEach(subareaContent => {
              if (subareaContent.bundle === "planning_subarea") {
                subareaContent.field_content.forEach(conception => {
                  if (conception.bundle === "conception") {
                    const conceptionData = {
                      id: conception.field_conception.nid,
                      title: conception.field_conception.title,
                      subarea: subareaContent.field_name,
                      description: conception.field_conception.field_description,
                      imageUrl: conception.field_conception?.field_conception_media_image?.field_media_image?.url ??
                        conception.field_conception?.field_conception_media_image?.field_media_image,
                      imageAltText: conception.field_conception?.field_conception_media_image?.field_media_image?.alt ?? ""
                    };

                    conceptions.push(conceptionData);
                  }
                });
              }
            });
          }
        });
      }

      return conceptions;
    }
  }
};
