export default {
  namespaced: true,
  state: {
    cacheTTL: 60 * 5, // seconds
    schedule: {}
  },
  actions: {
    loadSchedule ({dispatch, commit, state}) {
      if (
        !state.schedule.appointments?.length ||
        state.schedule.timestamp + state.cacheTTL < new Date().getTime()
      ) {
        dispatch(
          "doRequest",
          {endpoint: "schedule"},
          {root: true}
        ).then(response => {
          commit("cacheSchedule", response.nodes);
        });
      }
    }
  },
  mutations: {
    cacheSchedule (state, scheduleData) {
      state.schedule = {
        timestamp: new Date().getTime(),
        appointments: scheduleData
      };
    }
  },
  getters: {
    schedule (state) {
      return state.schedule.appointments ?? [];
    }
  }
};
