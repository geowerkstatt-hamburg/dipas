import {toRaw} from "vue";
import {$generateObjectChecksum} from "@/plugins/jsObjectChecksum.js";

export default {
  namespaced: true,
  state: {
    cacheTTL: 300, // seconds
    requestRunning: false,
    contributionsPerPage: undefined,
    requestHash: undefined,
    contributions: {},
    contributionDetails: {},
    relatedContributions: {}
  },
  actions: {
    loadContributions ({dispatch, commit, getters, state}, {page, queryParams}) {
      const query = Object.assign({page}, queryParams),
        requestTime = Math.round(new Date().getTime() / 1000),
        requestHash = getters.contributionsRequestHash(query);

      if (
        state.contributions[requestHash] === undefined ||
        (getters.contributionsParameterizedRequestTime() + state.cacheTTL) < requestTime ||
        !getters.contributionsPage(page).length
      ) {
        state.requestRunning = true;

        dispatch(
          "doRequest",
          {
            endpoint: "contributionlist",
            query
          },
          {root: true}
        ).then(response => {
          commit("setContributionsPerPage", Number(response.pager.itemsPerPage));
          commit("cacheContributions", {requestTime, page, response, requestHash});

          state.requestRunning = false;
        });
      }
    },
    loadContributionDetails ({dispatch, commit, getters}, contributionID) {
      if (contributionID && !getters.contributionDetail(contributionID)) {
        dispatch(
          "doRequest",
          {endpoint: `contributiondetails/${contributionID}`},
          {root: true}
        ).then(contribution => {
          commit("cacheContributionDetails", contribution);
        });
      }
    },
    loadRelatedContributions ({dispatch, commit, getters}, contributionID) {
      if (!getters.relatedContributions(contributionID)) {
        dispatch(
          "doRequest",
          {endpoint: `relatedcontributions/${contributionID}`},
          {root: true}
        ).then(response => {
          commit("cacheRelatedContributions", {contributionID, related: response.related});
        });
      }
    },
    storeContribution ({dispatch, commit}, data) {
      return new Promise((resolve) => {
        dispatch(
          "doRequest",
          {
            endpoint: "addcontribution",
            method: "POST",
            payload: data,
          },
          {
            root: true,
          }
        ).then(response => {
          commit(
            "contributions/invalidateContributionsCache",
            {},
            {root: true}
          );

          commit("statistics/invalidateContributionStatistics",
            {},
            {root: true}
          );

          resolve(response.nid);
        });
      });
    }
  },
  mutations: {
    setContributionsPerPage (state, contributionsPerPage) {
      state.contributionsPerPage = contributionsPerPage;
    },
    cacheContributions (state, {requestTime, page, response, requestHash}) {
      const contributionData = JSON.parse(JSON.stringify(state.contributions));

      if (
        contributionData[requestHash] === undefined ||
        contributionData[requestHash].requestTime === undefined ||
        contributionData[requestHash].requestTime + state.cacheTTL < requestTime
      ) {
        // If no page with the hashed request configuration was cached before, the caches have
        // been invalidated or are stale, initialize a new caching object for this hash
        contributionData[requestHash] = {
          totalPages: Number(response.pager.totalPages),
          requestTime,
          pages: []
        };
      }

      contributionData[requestHash].pages[page - 1] = response.nodes;
      state.contributions = contributionData;
    },
    cacheContributionDetails (state, contribution) {
      state.contributionDetails[contribution.nid] = {
        timestamp: new Date().getTime(),
        contribution
      };
    },
    cacheRelatedContributions (state, {contributionID, related}) {
      state.relatedContributions[contributionID] = {
        timestamp: new Date().getTime(),
        related
      };
    },
    updateRatings (state, {entityID, ratings}) {
      state.contributionDetails[entityID].contribution.rating = Object.assign(
        state.contributionDetails[entityID].contribution.rating,
        ratings
      );
    },
    invalidateContributionsCache (state) {
      if (Object.keys(state.contributions).length) {
        Object.keys(state.contributions).forEach(cachedElementIndex => {
          state.contributions[cachedElementIndex].requestTime = undefined;
        });
      }
    }
  },
  getters: {
    requestRunning (state) {
      return state.requestRunning;
    },
    contributionsRequestHash: (state) => (queryParams = undefined) => {
      if (queryParams) {
        const hashBase = Object.assign({}, queryParams);

        delete hashBase.page;
        state.requestHash = $generateObjectChecksum(hashBase);
      }

      return state.requestHash;
    },
    contributionsPerPage (state) {
      return state.contributionsPerPage;
    },
    contributionPages (state, getters) {
      const totalPages = state.contributions[getters.contributionsRequestHash()]?.totalPages ?? 0,
        nodesFirstPage = state.contributions[getters.contributionsRequestHash()]?.pages[0].length;

      return totalPages === 1 && nodesFirstPage === 0
        ? 0
        : totalPages;
    },
    contributionsParameterizedRequestTime: (state, getters) => () => {
      return toRaw(state.contributions[getters.contributionsRequestHash()]?.requestTime ?? 0);
    },
    contributionsPage: (state, getters) => (page = 1) => {
      const requestHash = getters.contributionsRequestHash();

      return toRaw(state.contributions[requestHash]?.pages[page - 1] ?? []);
    },
    contributionDetail: (state) => (contributionID, property = undefined) => {
      const contributionCache = state.contributionDetails[contributionID];

      return contributionCache && contributionCache.timestamp + state.cacheTTL >= new Date().getTime()
        ? property ? contributionCache.contribution[property] : contributionCache.contribution
        : undefined;
    },
    relatedContributions: (state) => (contributionID) => {
      return state.relatedContributions[contributionID] && state.relatedContributions[contributionID].timestamp + state.cacheTTL >= new Date().getTime()
        ? state.relatedContributions[contributionID].related
        : undefined;
    }
  }
};
