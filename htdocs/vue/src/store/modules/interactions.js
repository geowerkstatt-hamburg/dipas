export default {
  namespaced: true,
  state: {
    cacheTTL: 300, // seconds
    runningRequests: {
      comments: {},
      ratings: {}
    },
    comments: {},
    cookieRefresh: window.performance.now()
  },
  actions: {
    loadComments ({dispatch, commit, rootGetters}, rootEntityId) {
      if (!rootGetters["interactions/comments"](rootEntityId).length) {
        dispatch(
          "doRequest",
          {endpoint: `getcomments/${rootEntityId}`},
          {root: true}
        ).then(response => {
          commit("cacheComments", {rootEntityId, comments: response.comments});
        });
      }
    },
    storeComment ({dispatch, commit, state}, data) {
      if (state.runningRequests.comments[data.id] === undefined) {
        state.runningRequests.comments[data.id] = true;
        return dispatch(
          "doRequest",
          {
            endpoint: "addcomment/",
            method: "POST",
            payload: data,
          },
          {
            root: true,
          }
        ).then(response => {
          commit("cacheComments", {rootEntityId: data.rootEntityID, comments: response.comments});
          commit("contributions/invalidateContributionsCache", null, {root: true});
          commit("statistics/invalidateConceptionStatistics", {}, {root: true});
          commit("statistics/invalidateContributionStatistics", {}, {root: true});
          delete state.runningRequests.comments[data.id];

          return new Promise((resolve) => resolve(response.insertedCommentID));
        });
      }
      return null;
    },
    storeRating ({dispatch, commit, state}, data) {
      if (state.runningRequests.ratings[data.id] === undefined) {
        state.runningRequests.ratings[data.id] = true;
        dispatch(
          "doRequest",
          {
            endpoint: `rate/${data.id}`,
            method: "POST",
            payload: data,
          },
          {
            root: true,
          }
        ).then(response => {
          commit(
            "contributions/updateRatings",
            {entityID: data.id, ratings: response.results},
            {root: true}
          );
          commit("contributions/invalidateContributionsCache", null, {root: true});
          commit("statistics/invalidateContributionStatistics", null, {root: true});
          delete state.runningRequests.ratings[data.id];
          state.cookieRefresh = window.performance.now();
        });
      }
    }
  },
  mutations: {
    cacheComments (state, {rootEntityId, comments}) {
      state.comments[rootEntityId] = {
        timestamp: new Date().getTime(),
        comments
      };
    }
  },
  getters: {
    comments: (state) => (rootEntityId) => {
      const commentsCache = state.comments[rootEntityId] ?? false;

      return commentsCache
        ? commentsCache.timestamp + state.cacheTTL >= new Date().getTime() ? commentsCache.comments : []
        : [];
    },
    commentsCount: (state, getters) => (rootEntityId) => {
      // eslint-disable-next-line func-style
      const recursiveCommentsCount = function (comments) {
        let total = 0;

        comments.forEach(comment => {
          total++;
          total += recursiveCommentsCount(comment.replies);
        });

        return total;
      };

      return recursiveCommentsCount(getters.comments(rootEntityId));
    },
    ratedEntities: (state, getters, rootState, rootGetters) => (entityType) => {
      const cookieData = Object.assign(
        {
          cookieRefresh: state.cookieRefresh,
          votes: []
        },
        rootGetters.cookieData("dipasv")
      );

      return cookieData.votes[entityType]
        ? cookieData.votes[entityType]
        : {};
    }
  },
};
