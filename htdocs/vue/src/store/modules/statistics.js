export default {
  namespaced: true,
  state: {
    cacheTTL: 10 * 60 * 1000,
    contributionStatistics: {},
    conceptionStatistics: {}
  },
  actions: {
    /**
     * Loads the statistical data and commits them to the vuex store
     * @returns {void}
     */
    loadContributionStatisticalData ({state, dispatch, commit}) {
      // Time to live for an already loaded list (in milliseconds)
      const ttl = state.TTL,
        list = state.contributionStatistics;

      if (!list.data || !list.timestamp || ((list.timestamp + ttl) < new Date().getTime())) {
        dispatch(
          "doRequest",
          {endpoint: "statistics"},
          {root: true}
        ).then(response => {
          commit("setContributionStatistics", response);
        });
      }
    },
    /**
     * Loads the conception statistical data and commits them to the vuex store
     * @returns {void}
     */
    loadConceptionStatisticalData ({state, dispatch, commit}) {
      // Time to live for an already loaded list (in milliseconds)
      const ttl = state.TTL,
        list = state.conceptionStatistics;

      if (!list.data || !list.timestamp || ((list.timestamp + ttl) < new Date().getTime())) {
        dispatch(
          "doRequest",
          {endpoint: "conceptionstatistics"},
          {root: true}
        ).then(response => {
          commit("setConceptionStatistics", response);
        });
      }
    }
  },
  mutations: {
    setContributionStatistics (state, payload) {
      state.contributionStatistics = {
        timestamp: new Date().getTime(),
        data: payload
      };
    },
    setConceptionStatistics (state, payload) {
      state.conceptionStatistics = {
        timestamp: new Date().getTime(),
        data: payload
      };
    },
    invalidateConceptionStatistics (state) {
      if (state.conceptionStatistics.timestamp) {
        state.conceptionStatistics.timestamp = undefined;
      }
    },
    invalidateContributionStatistics (state) {
      if (state.contributionStatistics.timestamp) {
        state.contributionStatistics.timestamp = undefined;
      }
    }
  },
  getters: {
    contributionStatistics (state) {
      return state.contributionStatistics;
    },
    conceptionStatistics (state) {
      return state.conceptionStatistics;
    }
  }
};
