export default {
  emits: ["contentLoaded"],
  mounted () {
    this.$emit("contentLoaded");
  }
};
