import colors from "@/scss/colors.module.scss";

const {
  color_accessible_type_1,
  color_accessible_type_2,
  color_accessible_type_3,
  color_accessible_type_4,
  color_accessible_type_5,
  color_accessible_type_6,
  color_accessible_type_7,
  color_accessible_type_8,
  color_accessible_type_9,
  color_accessible_type_10,
  color_accessible_type_11,
  color_accessible_type_12
} = colors;

export default {
  data () {
    return {
      accessibleChartColors: [
        color_accessible_type_1,
        color_accessible_type_2,
        color_accessible_type_3,
        color_accessible_type_4,
        color_accessible_type_5,
        color_accessible_type_6,
        color_accessible_type_7,
        color_accessible_type_8,
        color_accessible_type_9,
        color_accessible_type_10,
        color_accessible_type_11,
        color_accessible_type_12
      ],
      // dipasSettings is a globally defined constant imported from the Drupal backend
      // eslint-disable-next-line no-undef
      chartSelectionColor: dipasSettings.colors.focus
    };
  }
};

