import {createApp} from "vue";
import VueMatomo from 'vue-matomo';
import App from "./App.vue";
import {createI18n} from "vue-i18n";
import router from "./router";
import store from "./store";
import messages from "./locales";
import pageTitle from "./plugins/pageTitle";
import {FunctionLibrary} from "./plugins/FunctionLibrary.js";
import bootstrapBreakpoints from "./plugins/bootstrapBreakpoints";
import dayjsIntegration from "./plugins/dayjsIntegration.js";
import "bootstrap";
import DipasModal from "./components/DipasModal.vue";
import jsObjectChecksum from "@/plugins/jsObjectChecksum";
import vueCookies from "vue-cookies";

import "~bootstrap/dist/css/bootstrap.min.css";

const locale = import.meta.env.VITE_APP_LANG;

const i18n = createI18n({
  legacy: false,
  locale: locale,
  globalInjection: true,
  messages: messages
});

const app = createApp(App);

app.component("DipasModal", DipasModal);

app.use(router);
app.use(store);
app.use(i18n);
app.use(pageTitle);
app.use(jsObjectChecksum);
app.use(bootstrapBreakpoints);
app.use(FunctionLibrary);
app.use(dayjsIntegration);
app.use(vueCookies);

let trackingSettings = {};

try {
  trackingSettings = Object.assign({}, dipasStatisticsSettings);
}
catch (e) {}

if (
  trackingSettings.enableMatomoIntegration === 1 &&
  trackingSettings.enableProceedingTracking === 1
) {
  app.use(VueMatomo, {
    host: trackingSettings.trackingBackendUrl,
    siteId: trackingSettings.siteId,
  });

  window._paq.push(['trackPageView']);
}

app.mount("#app");
