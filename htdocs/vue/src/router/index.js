import {createRouter, createWebHashHistory} from "vue-router";

const routes = [
  {
    path: "/",
    name: "route_HomePage",
    component: () => import("@/pages/HomePage/HomePage.vue")
  },
  {
    path: "/contributions/map",
    name: "route_ContributionsMap",
    component: () => import("@/pages/Contributions/Contributions.vue"),
    props: {selectedTab: "map"}
  },
  {
    path: "/contributions/list",
    name: "route_ContributionsList",
    component: () => import("@/pages/Contributions/Contributions.vue"),
    props: {selectedTab: "list"}
  },
  {
    path: "/contributions",
    name: "route_Contributions",
    redirect: () => {
      return {name: "route_ContributionsMap"};
    }
  },
  // Route handle DIPAS table "QR-Code tool" link - Opens the "new contribution" wizard modal with selected coordinates.
  // Example URL: /#/contribution/new?lat=53.55412667211251&lon=9.992132915081676
  {
    path: "/contribution/new",
    name: "route_ContributionsOpenWizardModal",
    component: () => import("@/pages/Contributions/Contributions.vue"),
    props: {selectedTab: "map"}
  },
  {
    path: "/contribution/:id",
    name: "route_ContributionDetail",
    component: () => import("@/pages/ContributionDetail/ContributionDetail.vue")
  },
  {
    path: "/conceptionlist",
    name: "route_ConceptionList",
    component: () => import("@/pages/ContentPage/ContentPage.vue"),
    props: {endpoint: "conceptionlist", storeModule: "conceptions"}
  },
  {
    path: "/conception/:id",
    name: "route_ConceptionDetail",
    component: () => import("@/pages/ConceptionDetails/ConceptionDetails.vue"),
  },
  {
    path: "/projectinfo",
    name: "route_Projectinfo",
    redirect: () => {
      return {name: "route_HomePage", hash: "#projectinfo"};
    }
  },
  {
    path: "/statistics",
    name: "route_Statistics",
    component: () => import("@/pages/Statistics/Statistics.vue")
  },
  {
    path: "/schedule",
    name: "route_Schedule",
    component: () => import("@/pages/Schedule/Schedule.vue")
  },
  {
    path: "/:endpoint",
    name: "route_Contentpage",
    component: () => import("@/pages/ContentPage/ContentPage.vue"),
    props: route => ({endpoint: route.fullPath.substring(1)})
  },
  // Legacy endpoints not existing anymore. Redirecting to new pages instead
  {
    path: "/contributionmap",
    redirect: () => {
      return {name: "route_ContributionsMap"};
    }
  },
  {
    path: "/contributionlist",
    redirect: () => {
      return {name: "route_ContributionsList"};
    }
  }
];

if (import.meta.env.DEV) {
  routes.push({
    path: "/demo",
    name: "route_DevDemoPage",
    component: () => import("@/pages/DevDemoPage/DevDemoPage.vue")
  });
}

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  scrollBehavior () {
    return {
      top: 0,
    };
  },
  routes
});

export default router;
