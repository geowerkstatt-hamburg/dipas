import {fileURLToPath} from "node:url";
import {mergeConfig} from "vite";
import {configDefaults, defineConfig} from "vitest/config";
import viteConfig from "./vite.config";
import vue from "@vitejs/plugin-vue";

// assuming the first element in plugins in vite.config.js is vue()
viteConfig.plugins[0] = vue({
  template: {
    compilerOptions: {
      // ignorning the custom '<i18n-t>' tag to avoid warnings in tests
      isCustomElement: (tag) => ["i18n-t"].includes(tag)
    }
  }
});

export default mergeConfig(
  viteConfig,
  defineConfig({
    test: {
      environment: "jsdom",
      exclude: [...configDefaults.exclude, "e2e/*"],
      root: fileURLToPath(new URL("./", import.meta.url)),
      transformMode: {
        web: [/\.[jt]sx$/]
      }
    }
  })
);
