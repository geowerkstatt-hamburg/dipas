import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasCheckboxGroup from "@/components/DipasCheckboxGroup.vue";

describe("DipasCheckboxGroup", () => {
  const defaultProps = {
    fieldLabel: "Testgroup",
    options: [
      {
        label: "Label 1",
        value: "option1"
      },
      {
        label: "Label 2",
        value: "option2"
      },
      {
        label: "Label 3",
        value: "option3"
      }
    ]
  };

  it("Default rendering", () => {
    const wrapper = mount(
      DipasCheckboxGroup,
      {
        props: defaultProps,
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasCheckboxGroup");
    expect(wrapper.findAllComponents(".DipasCheckbox")).toHaveLength(3);

    wrapper.findAllComponents(".DipasCheckbox").forEach((checkbox, index) => {
      expect(checkbox.find("label").text()).toEqual(defaultProps.options[index].label);
      expect(checkbox.find("input").attributes().value).toEqual(defaultProps.options[index].value);
      expect(checkbox.find("input").attributes()).not.toHaveProperty("disabled");
    });
  });

  it("Disabled rendering", () => {
    const wrapper = mount(
      DipasCheckboxGroup,
      {
        props: {...defaultProps, disabled: true},
        global
      }
    );

    wrapper.findAllComponents(".DipasCheckbox").forEach((checkbox) => {
      expect(checkbox.find("input").attributes()).toHaveProperty("disabled");
    });
  });

  it("Click behavior", async () => {
    const wrapper = mount(
      DipasCheckboxGroup,
      {
        props: defaultProps,
        global
      }
    );

    wrapper.setProps({"onUpdate:modelValue": (newVal) => wrapper.setProps({modelValue: newVal})});

    await wrapper.find("label").trigger("click");

    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted()).toHaveProperty("update:modelValue");
  });
});
