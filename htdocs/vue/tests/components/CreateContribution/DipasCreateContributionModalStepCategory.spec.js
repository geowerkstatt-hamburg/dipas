import {mount} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import DipasCreateContributionModalStepCategory from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepCategory.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

global.mocks.$store.getters = Object.assign({}, global.mocks.$store.getters, {
  "proceeding/taxonomyTerms": () => {
    return [{
      "id": "23",
      "name": "Erwartungen",
      "description": "<p>HIer kann eine Beschreibung stehen!</p>\r\n",
      "weight": 0,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-5.png",
      "field_color": "#FF66B8"
    },
    {
      "id": "29",
      "name": "Einzelhandel und Gewerbe",
      "description": null,
      "weight": 1,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-3.png",
      "field_color": "#A67C52"
    },
    {
      "id": "25",
      "name": "Mobilität",
      "description": null,
      "weight": 2,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-1.png",
      "field_color": "#007EBD"
    },
    {
      "id": "31",
      "name": "Sonstiges",
      "description": null,
      "weight": 3,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-6.png",
      "field_color": "#919191"
    },
    {
      "id": "28",
      "name": "Soziales",
      "description": null,
      "weight": 4,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-4.png",
      "field_color": "#6200B3"
    },
    {
      "id": "30",
      "name": "Sport und Kultur",
      "description": null,
      "weight": 5,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-4_0.png",
      "field_color": "#0D016A"
    },
    {
      "id": "26",
      "name": "Stadtgrün",
      "description": null,
      "weight": 6,
      "field_category_icon": "//schulung.localhost:8080/drupal/sites/default/files/categoryicons/kategorie-2.png",
      "field_color": "#77A201"
    }];
  }
});

describe("DipasCreateContributionModalStepCategory", () => {
  it("loads all dom elements of this step in Desktop mode", () => {
    const wrapper = mount(DipasCreateContributionModalStepCategory, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepCategory").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.categoryList").exists()).toBe(true);
    expect(wrapper.findAll("div.DipasCreateContributionModalStepCategory div.categoryList div.categoryListColumn").length).toBe(3);
    expect(wrapper.findAll("div.categoryListColumn fieldset.DipasRadioGroup").length).toBe(2);
    expect(wrapper.findAll("div.categoryListColumn fieldset.DipasRadioGroup legend")[0].text()).toEqual("wizard.steps.category.stepTitle 1-6:");
    expect(wrapper.findAll("div.categoryListColumn fieldset.DipasRadioGroup legend")[1].text()).toEqual("wizard.steps.category.stepTitle 7:");
    expect(wrapper.find("img.wizardCategoryImage").exists()).toBe(true);
  });

  it("loads all dom elements of this step in Desktop mode for more than 18 categories", () => {
    global.mocks.$store.getters = Object.assign({}, global.mocks.$store.getters, {
      "proceeding/taxonomyTerms": () => {
        return [
          {
            "id": "23",
            "name": "Erwartungen",
            "field_color": "#FF66B8"
          },
          {
            "id": "29",
            "name": "Einzelhandel und Gewerbe",
            "field_color": "#A67C52"
          },
          {
            "id": "25",
            "name": "Mobilität",
            "field_color": "#007EBD"
          },
          {
            "id": "31",
            "name": "Sonstiges",
            "field_color": "#919191"
          },
          {
            "id": "28",
            "name": "Soziales",
            "field_color": "#6200B3"
          },
          {
            "id": "30",
            "name": "Sport und Kultur",
            "field_color": "#0D016A"
          },
          {
            "id": "26",
            "name": "Stadtgrün",
            "field_color": "#77A201"
          },
          {
            "id": "123",
            "name": "Erwartungen",
            "field_color": "#FF66B8"
          },
          {
            "id": "129",
            "name": "Einzelhandel und Gewerbe",
            "field_color": "#A67C52"
          },
          {
            "id": "125",
            "name": "Mobilität",
            "field_color": "#007EBD"
          },
          {
            "id": "131",
            "name": "Sonstiges",
            "field_color": "#919191"
          },
          {
            "id": "128",
            "name": "Soziales",
            "field_color": "#6200B3"
          },
          {
            "id": "130",
            "name": "Sport und Kultur",
            "field_color": "#0D016A"
          },
          {
            "id": "126",
            "name": "Stadtgrün",
            "field_color": "#77A201"
          },
          {
            "id": "223",
            "name": "Erwartungen",
            "field_color": "#FF66B8"
          },
          {
            "id": "229",
            "name": "Einzelhandel und Gewerbe",
            "field_color": "#A67C52"
          },
          {
            "id": "225",
            "name": "Mobilität",
            "field_color": "#007EBD"
          },
          {
            "id": "231",
            "name": "Sonstiges",
            "field_color": "#919191"
          },
          {
            "id": "228",
            "name": "Soziales",
            "field_color": "#6200B3"
          },
          {
            "id": "230",
            "name": "Sport und Kultur",
            "field_color": "#0D016A"
          },
          {
            "id": "226",
            "name": "Stadtgrün",
            "field_color": "#77A201"
          }
        ];
      }
    });

    const wrapper = mount(DipasCreateContributionModalStepCategory, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepCategory").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.categorySelect").exists()).toBe(true);
    expect(wrapper.find("div.categorySelect div.DipasSelect").exists()).toBe(true);
    expect(wrapper.find("img.wizardCategoryImage").exists()).toBe(true);
  });

  it("loads all dom elements of this step in Mobile mode", () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(DipasCreateContributionModalStepCategory, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepCategory").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.mobilecategoryList").exists()).toBe(true);
    expect(wrapper.findAll("div.mobilecategoryList fieldset.DipasRadioGroup").length).toBe(1);
    expect(wrapper.find("img.wizardCategoryImage").exists()).toBe(true);
  });
});
