import {mount} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import DipasCreateContributionModalStepType from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepType.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

global.mocks.$store.getters = Object.assign({}, global.mocks.$store.getters, {
  "proceeding/taxonomyTerms": () => {
    return [{
      "id": "32",
      "name": "Idee"
    },
    {
      "id": "33",
      "name": "Frage"
    },
    {
      "id": "34",
      "name": "Sonstiges"
    },
    {
      "id": "35",
      "name": "Kritik"
    },
    {
      "id": "756",
      "name": "Versuch"
    },
    {
      "id": "766",
      "name": "Versuch1"
    },
    {
      "id": "767",
      "name": "Versuch2"
    },
    {
      "id": "768",
      "name": "Versuch3"
    }];
  }
});

describe("DipasCreateContributionModalStepType", () => {
  it("loads all dom elements of this step in Desktop mode", () => {
    const wrapper = mount(DipasCreateContributionModalStepType, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepType").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.typesList").exists()).toBe(true);
    expect(wrapper.findAll("div.DipasCreateContributionModalStepType div.typesList div.typesListColumn").length).toBe(3);
    expect(wrapper.findAll("div.typesListColumn fieldset.DipasRadioGroup").length).toBe(2);
    expect(wrapper.findAll("div.typesListColumn fieldset.DipasRadioGroup legend")[0].text()).toEqual("wizard.steps.type.stepTitle 1-6:");
    expect(wrapper.findAll("div.typesListColumn fieldset.DipasRadioGroup legend")[1].text()).toEqual("wizard.steps.type.stepTitle 7-8:");
    expect(wrapper.find("img.wizardTypeImage").exists()).toBe(true);
  });

  it("loads all dom elements of this step in Desktop mode for more than 18 categories", () => {
    global.mocks.$store.getters = Object.assign({}, global.mocks.$store.getters, {
      "proceeding/taxonomyTerms": () => {
        return [
          {
            "id": "32",
            "name": "Idee"
          },
          {
            "id": "33",
            "name": "Frage"
          },
          {
            "id": "34",
            "name": "Sonstiges"
          },
          {
            "id": "35",
            "name": "Kritik"
          },
          {
            "id": "756",
            "name": "Versuch"
          },
          {
            "id": "766",
            "name": "Versuch1"
          },
          {
            "id": "767",
            "name": "Versuch1"
          },
          {
            "id": "132",
            "name": "Idee"
          },
          {
            "id": "133",
            "name": "Frage"
          },
          {
            "id": "134",
            "name": "Sonstiges"
          },
          {
            "id": "135",
            "name": "Kritik"
          },
          {
            "id": "1756",
            "name": "Versuch"
          },
          {
            "id": "1766",
            "name": "Versuch1"
          },
          {
            "id": "1767",
            "name": "Versuch1"
          },
          {
            "id": "232",
            "name": "Idee"
          },
          {
            "id": "233",
            "name": "Frage"
          },
          {
            "id": "234",
            "name": "Sonstiges"
          },
          {
            "id": "235",
            "name": "Kritik"
          },
          {
            "id": "2756",
            "name": "Versuch"
          }
        ];
      }
    });

    const wrapper = mount(DipasCreateContributionModalStepType, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepType").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.typesSelect").exists()).toBe(true);
    expect(wrapper.find("div.typesSelect div.DipasSelect").exists()).toBe(true);
    expect(wrapper.find("img.wizardTypeImage").exists()).toBe(true);
  });

  it("loads all dom elements of this step in Mobile mode", () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(DipasCreateContributionModalStepType, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepType").exists()).toBe(true);
    expect(wrapper.find("div.titleRow h2").exists()).toBe(true);
    expect(wrapper.find("div.mobiletypesList").exists()).toBe(true);
    expect(wrapper.findAll("div.mobiletypesList fieldset.DipasRadioGroup").length).toBe(1);
    expect(wrapper.find("img.wizardTypeImage").exists()).toBe(true);
  });
});
