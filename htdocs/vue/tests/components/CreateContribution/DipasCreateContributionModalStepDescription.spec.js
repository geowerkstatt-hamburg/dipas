import {mount} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import DipasCreateContributionModalStepDescription from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepDescription.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

describe("DipasCreateContributionModalStepDescription", () => {
  it("loads all dom elements of this step", () => {
    const wrapper = mount(DipasCreateContributionModalStepDescription, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepDescription").exists()).toBe(true);
    expect(wrapper.find("div.DipasTextfield").exists()).toBe(true);
    expect(wrapper.find("div.DipasTextfield div.wrapperLabel label div").classes()).toContain("required");
    expect(wrapper.find("div.DipasTextarea").exists()).toBe(true);
    expect(wrapper.find("div.DipasTextarea div.wrapperLabel label div").classes()).toContain("required");
    expect(wrapper.find("img.wizardTitleImage").exists()).toBe(true);
  });
});
