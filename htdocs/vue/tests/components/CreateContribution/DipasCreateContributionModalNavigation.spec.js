import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import CreateContribModalNavigation from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalNavigation.vue";
import {global} from "@/../tests/global.inc";

describe("CreateContribModal", () => {
  const defaultProps = {
    links: [
      {
        id: "title",
        name: "Beitrags-Beschreibung",
        hasBeenVisited: true
      },
      {
        id: "category",
        name: "Kategorie",
        hasBeenVisited: true
      },
      {
        id: "type",
        name: "Beitragstyp",
        hasBeenVisited: false
      },
      {
        id: "overview",
        name: "Zusammenfassung",
        hasBeenVisited: false
      }
    ],
    currentStep: 1
  };

  it("handels the basic features of the desktop navigation", () => {
    const wrapper = mount(CreateContribModalNavigation, {
      props: defaultProps,
      global
    });

    expect(wrapper.find("nav.DipasCreateContributionModalNavigation").exists()).toBe(true);
    expect(wrapper.findAll("nav.DipasCreateContributionModalNavigation ul li").length).toBe(4);
    expect(wrapper.findAll("nav.DipasCreateContributionModalNavigation ul li button").length).toBe(4);

    const buttons = wrapper.findAll("nav.DipasCreateContributionModalNavigation ul li button");

    expect(buttons[0].attributes().disabled).toBe(undefined);
    expect(buttons[1].attributes().disabled).toBe(undefined);
    expect(buttons[2].attributes().disabled).toBe("");
    expect(buttons[3].attributes().disabled).toBe("");
  });

  it("button click emits event", async () => {
    const wrapper = mount(CreateContribModalNavigation, {
        props: defaultProps,
        global
      }),
      buttons = wrapper.findAll("nav.DipasCreateContributionModalNavigation ul li button");

    await buttons[0].trigger("click");

    expect(wrapper.emitted("moveToStep"));
    expect(wrapper.emitted("moveToStep")[0]).toEqual([0]);
  });

  it("handels the basic features of the mobile navigation", () => {
    const wrapper = mount(CreateContribModalNavigation, {
      props: defaultProps,
      global: Object.assign({}, global, {
        mocks: {
          isMobile: true,
          isDesktop: false
        }
      })
    });

    expect(wrapper.find("nav.DipasCreateContributionModalNavigation").exists()).toBe(true);
    expect(wrapper.find("nav.DipasCreateContributionModalNavigation div.bottomSlideLine").exists()).toBe(true);
    expect(wrapper.findAll("nav.DipasCreateContributionModalNavigation div.bottomSlideLine span").length).toBe(4);

    const spans = wrapper.findAll("nav.DipasCreateContributionModalNavigation div.bottomSlideLine span");

    expect(spans[0].classes()).not.toContain("disabled");
    expect(spans[1].classes()).not.toContain("disabled");
    expect(spans[2].classes()).toContain("disabled");
    expect(spans[3].classes()).toContain("disabled");

    expect(spans[0].attributes().tabindex).toEqual("0");
    expect(spans[1].attributes().tabindex).toEqual("0");
    expect(spans[2].attributes()).not.toHaveProperty("tabindex");
    expect(spans[3].attributes()).not.toHaveProperty("tabindex");

    expect(spans[0].attributes().role).toEqual("button");
    expect(spans[1].attributes().role).toEqual("button");
    expect(spans[2].attributes()).not.toHaveProperty("role");
    expect(spans[3].attributes()).not.toHaveProperty("role");
  });

  it("circle click and keyup.enter emit event", async () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(CreateContribModalNavigation, {
        props: defaultProps,
        global
      }),
      spans = wrapper.findAll("nav.DipasCreateContributionModalNavigation div.bottomSlideLine span");

    await spans[0].trigger("click");

    expect(wrapper.emitted("moveToStep"));
    expect(wrapper.emitted("moveToStep")[0]).toEqual([0]);

    await spans[1].trigger("keyup.enter");

    expect(wrapper.emitted("moveToStep"));
    expect(wrapper.emitted("moveToStep")[1]).toEqual([1]);
  });
});
