import {mount, flushPromises} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import DipasCreateContributionModalStepLocation from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepLocation.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

describe("DipasCreateContributionModalStepLocation", () => {
  it("loads all dom elements of this step", async () => {
    const wrapper = mount(DipasCreateContributionModalStepLocation, {global});

    // mount uses async await, sync promise in order to run tests
    await flushPromises();

    // Trigger rendering on next tick (otherwise v-if="formData.selectedLocationType === 'geodata'" is not working in DipasCreateContributionModalStepLocation.vue!
    await wrapper.vm.$nextTick;

    expect(wrapper.find("div.DipasCreateContributionModalStepLocation").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeGeodata").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeGeodata div fieldset.DipasRadioGroup").exists()).toBe(true);
    expect(wrapper.findAll("div.DipasCreateContributionModalStepLocation div.locationTypeGeodata div fieldset.DipasRadioGroup div.DipasRadio").length).toBe(3);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.masterportalcontainer").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.masterportalcontainer button.linkDescribeLocation").exists()).toBe(true);
    expect(wrapper.find("img.wizardGeodataImage").exists()).toBe(true);
  });

  it("finds the correct hint-text and options", () => {
    const wrapper = mount(DipasCreateContributionModalStepLocation, {global}),
      options = wrapper.vm.options;

    expect(wrapper.vm.text).toEqual("wizard.steps.location.point.hint");
    expect(options.length).toBe(3);
    expect(options[0]).toHaveProperty("val");
    expect(options[0]).toHaveProperty("label");
    expect(options[0].label).toEqual("wizard.steps.location.point.label");
    expect(options[1].label).toEqual("wizard.steps.location.linestring.label");
    expect(options[2].label).toEqual("wizard.steps.location.polygon.label");
  });

  it("switch to text-input when clicking on button", async () => {
    const wrapper = mount(DipasCreateContributionModalStepLocation, {global});

    // mount uses async await, sync promise in order to run tests
    await flushPromises();

    // Trigger rendering on next tick (otherwise v-if="formData.selectedLocationType === 'geodata'" is not working in DipasCreateContributionModalStepLocation.vue!
    await wrapper.vm.$nextTick;

    wrapper.find("div.DipasCreateContributionModalStepLocation div.masterportalcontainer button.linkDescribeLocation").trigger("click");

    // Trigger rendering on next tick!
    await wrapper.vm.$nextTick;

    expect(wrapper.find("div.DipasCreateContributionModalStepLocation").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeGeodata").exists()).toBe(false);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeText").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeText button.DipasButton.linkBackGeodata").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepLocation div.locationTypeText div.DipasTextarea").exists()).toBe(true);
  });

  it("generates the correct default Masterportal URL", async () => {
    // This setup should generate the following Masterportal URL:
    // http://masterportal.url?projection=EPSG:4326&style=simple&postMessageUrl=http://localhost:3000

    const wrapper = mount(DipasCreateContributionModalStepLocation, {global});

    // mount uses async await, sync promise in order to run tests
    await flushPromises();

    await wrapper.vm.$nextTick;

    // Test the default geometryType (point)

    const initialMasterportalSrc = wrapper.find("iframe.masterportalIframe").attributes("src");

    expect(initialMasterportalSrc).contains("projection=EPSG:4326");
    expect(initialMasterportalSrc).contains("style=simple");
    expect(initialMasterportalSrc).contains("postMessageUrl=http://localhost:3000");

    expect(initialMasterportalSrc).not.contains("zoomlevel=");
    expect(initialMasterportalSrc).not.contains("center=");

    // Test, if user changes the geometryType (linestring). The MasterportalSrc must be the same.

    await wrapper.find(".DipasRadio input[value=\"linestring\"]").setChecked();

    expect(wrapper.find("iframe.masterportalIframe").attributes("src")).toBe(initialMasterportalSrc);
  });

  it("generates the correct Masterportal URL, if DIPAS table QR-Code tool parameters are given", async () => {
    // This setup should generate the following Masterportal URL:
    // http://masterportal.url?projection=EPSG:4326&style=simple&zoomlevel=6&center=9.992132915081676,53.55412667211251&postMessageUrl=http://localhost:3000
    global.mocks = {
      ...global.mocks,
      $route: {
        name: "route_ContributionsOpenWizardModal",
        query: {
          lat: 53.55412667211251,
          lon: 9.992132915081676
        },
      },
    };

    const wrapper = mount(DipasCreateContributionModalStepLocation, {global});

    // mount uses async await, sync promise in order to run tests
    await flushPromises();

    await wrapper.vm.$nextTick;

    // Test the default geometryType (point)

    const initialMasterportalSrc = wrapper.find("iframe.masterportalIframe").attributes("src");

    expect(initialMasterportalSrc).contains("projection=EPSG:4326");
    expect(initialMasterportalSrc).contains("style=simple");
    expect(initialMasterportalSrc).contains("postMessageUrl=http://localhost:3000");
    expect(initialMasterportalSrc).contains("zoomlevel=6");
    expect(initialMasterportalSrc).contains("center=9.992132915081676,53.55412667211251");

    // Test, if user changes the geometryType (linestring). The MasterportalSrc must be the same.

    await wrapper.find(".DipasRadio input[value=\"linestring\"]").setChecked();

    expect(wrapper.find("iframe.masterportalIframe").attributes("src")).toBe(initialMasterportalSrc);
  });
});
