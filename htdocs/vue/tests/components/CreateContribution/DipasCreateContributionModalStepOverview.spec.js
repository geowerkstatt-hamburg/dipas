import {mount} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import DipasCreateContributionModalStepOverview from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepOverview.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

describe("DipasCreateContributionModalStepOverview", () => {
  const defaultProps = {
    wizardSteps: [
      {
        "id": "description",
        "mandatory": true,
        "name": "Beschreibung",
        "hasBeenVisited": true,
        "component": "DipasCreateContributionModalStepDescription",
        "formData":
          {
            "title": "test-Beitrag",
            "description": "Mit ganz wenig Text"
          }
      },
      {
        "id": "category",
        "mandatory": true,
        "name": "Kategorie",
        "hasBeenVisited": true,
        "component": "DipasCreateContributionModalStepCategory",
        "formData":
          {
            "categories": "24"
          }
      },
      {
        "id": "type",
        "mandatory": true,
        "name": "Beitragstyp",
        "hasBeenVisited": true,
        "component": "DipasCreateContributionModalStepType",
        "formData":
          {
            "types": "33"
          }
      },
      {
        "id": "location",
        "mandatory": false,
        "name": "Position",
        "hasBeenVisited": true,
        "component": "DipasCreateContributionModalStepLocation",
        "formData":
          {
            "textdata": "Textliche Beschreibung",
            "selectedLocationType": "text"
          }
      },
      {
        "id": "overview",
        "name": "Zusammenfassung",
        "component": "DipasCreateContributionModalStepOverview",
        "hasBeenVisited": true,
        "formData": null,
        "mandatory": true
      }
    ]
  };

  it("loads all dom elements of this step", () => {
    const wrapper = mount(DipasCreateContributionModalStepOverview, {
      props: defaultProps,
      global
    });

    expect(wrapper.find("div.DipasCreateContributionModalStepOverview").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepOverview div").classes()).toContain("row");
    expect(wrapper.find("div.DipasCreateContributionModalStepOverview div div").classes()).toContain("col-6");
    expect(wrapper.find("div.DipasCreateContributionModalStepOverview div div div.DipasInfoButton button").exists()).toBe(true);
    expect(wrapper.findAll("div.DipasCreateContributionModalStepOverview div div.col-6").length).toBe(2);
    expect(wrapper.findAll("div.DipasCreateContributionModalStepOverview div div div.stepContainer").length).toBe(4);
  });

  it("jumps to correct step to edit", async () => {
    const wrapper = mount(DipasCreateContributionModalStepOverview, {
        props: defaultProps,
        global
      }),
      buttons = wrapper.findAll("div.DipasCreateContributionModalStepOverview div div div.stepContainer button.editDataButton");

    await buttons[0].trigger("click");

    expect(wrapper.emitted("moveToStep"));
    expect(wrapper.emitted("moveToStep")[0]).toEqual([0]);
  });

  it("extract the data from the formData properties", async () => {
    const wrapper = mount(DipasCreateContributionModalStepOverview, {
      props: defaultProps,
      global
    });

    expect(wrapper.vm.title).toEqual(defaultProps.wizardSteps[0].formData.title);
    expect(wrapper.vm.description).toEqual(defaultProps.wizardSteps[0].formData.description);
    expect(wrapper.vm.locationType).toEqual(defaultProps.wizardSteps[3].formData.selectedLocationType);
    expect(wrapper.vm.locationText).toEqual(defaultProps.wizardSteps[3].formData.textdata);
    expect(wrapper.vm.locationGeodata).toBe(null);
    expect(wrapper.vm.locationGeodataType).toEqual("point");
    // from global.inc.js
    expect(wrapper.vm.category).toEqual("Wert1");
    expect(wrapper.vm.type).toEqual("Wert2");

  });
});
