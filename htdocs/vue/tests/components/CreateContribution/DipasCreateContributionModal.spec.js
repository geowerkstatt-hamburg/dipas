import {mount} from "@vue/test-utils";
import {describe, it, expect, vi} from "vitest";
import CreateContribModal from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModal.vue";
import {global} from "@/../tests/global.inc";

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

// this part is neccessary, because we use DipasModal in our Component
Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("CreateContribModal", () => {
  it("handels the basic features of the wizard", () => {
    const wrapper = mount(CreateContribModal, {global});

    expect(wrapper.find("#DipasCreateContributionModal").exists()).toBe(true);
    expect(wrapper.find("div.modalStepContainer").exists()).toBe(true);
    expect(wrapper.findAll("div.footerButtonContainer").length).toBe(3);
  });

  it("handels the buttons correctly", async () => {
    const wrapper = mount(CreateContribModal, {global}),
      buttonList = wrapper.findAll("div.footerButtonContainer button.DipasButton");
    let stepValidationCalled = false;

    // mock function to return true; we do not want to check the correspondance to child-components here
    // but we want to check if this function has been called
    wrapper.vm.checkCurrentStepValidation = () => {
      stepValidationCalled = true;
      return true;
    };

    expect(buttonList.length).toBe(2);
    expect(buttonList[0].attributes().style).toEqual("display: none;");
    expect(wrapper.vm.wizardSteps[1].hasBeenVisited).toBe(false);

    await buttonList[1].trigger("click");

    expect(buttonList[0].attributes().style).toEqual("");
    expect(wrapper.vm.wizardSteps[1].hasBeenVisited).toBe(true);
    expect(stepValidationCalled).toBe(true);
    // since we do not want to call the step validation function when we step one step back
    stepValidationCalled = false;

    await buttonList[0].trigger("click");

    expect(buttonList[0].attributes().style).toEqual("display: none;");
    expect(wrapper.vm.wizardSteps[1].hasBeenVisited).toBe(true);
    expect(stepValidationCalled).toBe(false);
  });

  it("jump to last step", () => {
    const wrapper = mount(CreateContribModal, {global}),
      nextButtonTitle = wrapper.vm.nextButtonText;

    wrapper.vm.moveToStep(wrapper.vm.maxNumberOfSteps);

    expect(wrapper.vm.nextButtonText).not.toEqual(nextButtonTitle);
  });

  it("ask for cancel", async () => {
    const wrapper = mount(CreateContribModal, {global});

    wrapper.vm.askForCancel = true;

    // Trigger rendering on next tick!
    await wrapper.vm.$nextTick;

    expect(wrapper.find("nav.DipasCreateContributionModalNavigation").exists()).toBe(false);
    expect(wrapper.find("div.DipasCreateContributionModalStepCancel").exists()).toBe(true);
    expect(wrapper.find("button.DipasButton.secondary").exists()).toBe(true);

    const buttonList = wrapper.findAll("div.footerButtonContainer button.DipasButton");

    buttonList[2].trigger("click");

    expect(wrapper.emitted()).toHaveProperty("closeModal");
  });

  it("ask for cancel on mobile", async () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(CreateContribModal, {global});

    wrapper.vm.askForCancel = true;

    // Trigger rendering on next tick!
    await wrapper.vm.$nextTick;

    expect(wrapper.find("#DipasCancelCreateContributionModal").exists()).toBe(true);

    const buttonList = wrapper.findAll("#DipasCancelCreateContributionModal div.footerButtonContainer button.DipasButton");

    buttonList[1].trigger("click");

    expect(wrapper.emitted()).toHaveProperty("closeModal");
  });
});
