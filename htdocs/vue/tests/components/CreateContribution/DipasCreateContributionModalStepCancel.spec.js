import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import DipasCreateContributionModalStepCancel from "@/pages/Contributions/components/CreateContribution/DipasCreateContributionModalStepCancel.vue";
import {global} from "@/../tests/global.inc";

describe("DipasCreateContributionModalStepCancel", () => {
  it("loads all dom elements of this step", () => {
    const wrapper = mount(DipasCreateContributionModalStepCancel, {global});

    expect(wrapper.find("div.DipasCreateContributionModalStepCancel").exists()).toBe(true);
    expect(wrapper.find("div.DipasCreateContributionModalStepCancel div.titleRow").exists()).toBe(true);
    expect(wrapper.find("img.wizardCancelImage").exists()).toBe(true);
  });
});
