import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";

import DipasButton from "@/components/DipasButton.vue";
import DipasCardTeaser from "@/components/DipasCardTeaser.vue";

describe("DipasCardTeaser.vue", () => {

  it("renders the card component", () => {
    const defaultProps = {
      cardTitle: "Themenwahl Gesamtbereich G",
      cardText: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
      imageLink: "https://placekeanu.com/200/150",
      imageAltText: "Keanu Reeves",
      buttonLabel: "mehr erfahren"
    };
    const wrapper = mount(DipasCardTeaser, {props: defaultProps});

    expect(wrapper.exists()).toBe(true);
  });

  describe("DipasButton.vue", () => {
    const defaultProps = {
      buttonId: "DipasButtonID",
      label: "DipasButton Test",
      title: "DipasButton title string",
    };

    it("renders the button with the default label", () => {
      const wrapper = mount(DipasButton, {
        props: defaultProps
      });

      expect(wrapper.text()).toContain(defaultProps.label);
    });

    it("renders the button with a custom label", () => {
      const wrapper = mount(DipasButton, {
        props: defaultProps
      });

      expect(wrapper.text()).toContain(defaultProps.label);
    });

    it("emits a \"click\" event when the button is clicked", async () => {
      const wrapper = mount(DipasButton, {
        props: defaultProps
      });

      await wrapper.find("button").trigger("click");
      expect(wrapper.emitted("click")).toBeTruthy();
    });
  });
});
