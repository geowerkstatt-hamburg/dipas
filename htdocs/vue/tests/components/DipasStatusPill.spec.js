import {describe, it, expect} from "vitest";

import {mount} from "@vue/test-utils";
import DipasStatusPill from "@/components/DipasStatusPill.vue";
import {global} from "@/../tests/global.inc";

describe("DipasStatusPill", () => {
  const defaultProps = {};

  it("renders the component correctly", () => {
    const wrapper = mount(DipasStatusPill, {
      props: {
        ...defaultProps,
        proceedingActive: true
      },
      global,
    });

    expect(wrapper.classes()).toContain("DipasStatusPill");
    expect(wrapper.classes()).toContain("active");
    expect(wrapper.text()).toContain("statusPill.active");
  });
});
