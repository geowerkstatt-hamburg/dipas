import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasInfoButton from "@/components/DipasInfoButton.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("DipasInfoButton", () => {
  it("Default rendering", () => {
    const wrapper = mount(
      DipasInfoButton,
      {
        props: {
          tooltipTitle: "DipasInfoButton tooltip title",
          tooltipText: "DipasInfoButton tooltip text"
        },
        global
      }
    );

    wrapper.find("button").trigger("click");

    expect(wrapper.classes()).toContain("DipasInfoButton");
    expect(wrapper.emitted()).not.toHaveProperty("click");
    expect(wrapper.text()).toEqual("DipasInfoButton.label");
  });

  it("Disabled rendering", () => {
    const wrapper = mount(
      DipasInfoButton,
      {
        props: {
          tooltipTitle: "DipasInfoButton tooltip title",
          tooltipText: "DipasInfoButton tooltip text",
          disabled: true
        },
        global
      }
    );

    wrapper.find("button").trigger("click");

    expect(wrapper.classes()).toContain("DipasInfoButton");
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });
});
