import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import PageFooter from "@/components/PageFooter.vue";
import {global} from "../global.inc";

describe("PageFooter.vue", () => {

  it("renders DipasNavigation component", () => {
    const wrapper = mount(PageFooter, {
      global,
    });

    expect(wrapper.findComponent({name: "DipasNavigation"}).exists()).toBe(true);
  });

  it("renders the DIPAS logo with a link", () => {
    const wrapper = mount(PageFooter, {
      global,
    });

    const dipasLink = wrapper.find(".dipasLink");
    const dipasLogo = dipasLink.find(".dipasLink img");

    expect(dipasLink.exists()).toBe(true);
    expect(dipasLink.attributes("href")).toBe("https://www.dipas.org");
    expect(dipasLink.attributes("target")).toBe("_blank");
    expect(dipasLogo.exists()).toBe(true);
    expect(dipasLogo.attributes("alt")).toBe("DIPAS Logo");
    expect(dipasLogo.attributes("src")).toBe("/src/assets/images/DIPAS-Logo-neg.svg");
  });

  it("displays the \"participationBy\" text", () => {
    const wrapper = mount(PageFooter, {
      global,
    });

    const participationText = wrapper.text();

    expect(participationText).toContain("participationBy");
  });
});
