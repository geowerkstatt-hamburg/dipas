import {describe, it, expect} from "vitest";

import {mount} from "@vue/test-utils";
import DipasSelect from "../../src/components/DipasSelect.vue";
import {global} from "../global.inc";

describe("DipasSelect", () => {
  const defaultProps = {
    optionPlaceholder: "Placeholder for DipasSelect",
    options: [
      {
        value: "option-1",
        label: "Option 1",
      },
      {
        value: "option-2",
        label: "Option 2",
      },
      {
        value: "option-3",
        label: "Option 3",
      },
    ],
  };

  it("handles the basic attribute (optionPlaceholder, options)", () => {
    const wrapper = mount(DipasSelect, {
      props: {
        ...defaultProps,
      },
      global,
    });

    expect(wrapper.classes()).toContain("DipasSelect");
    expect(wrapper.find("span.multiselect__placeholder").text()).toBe(defaultProps.optionPlaceholder);
    expect(wrapper.findAll("li[role=\"option\"]").length).toBe(defaultProps.options.length);
  });

  it("handles multi select with values set", async () => {
    const wrapper = mount(DipasSelect, {
      props: {
        ...defaultProps,
        isMultiselect: true,
        modelValue: [
          {
            "value": "type-1",
            "label": "Beitragstyp 1"
          },
          {
            "value": "type-3",
            "label": "Beitragstyp 3"
          },
        ],
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global,
    });

    expect(wrapper.find("div.DipasSelect.isMultiselect").exists()).toBe(true);
    expect(wrapper.findAll("div.multiselect__tags span.multiselect__single").at(0).text()).toBe("Beitragstyp 1,");
    expect(wrapper.findAll("div.multiselect__tags span.multiselect__single").at(1).text()).toBe("Beitragstyp 3");
  });

  it("handles single select with value set", async () => {
    const wrapper = mount(DipasSelect, {
      props: {
        ...defaultProps,
        isMultiselect: false,
        modelValue: [
          {
            "value": "type-3",
            "label": "Beitragstyp 3"
          },
        ],
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global,
    });

    expect(wrapper.find("div.DipasSelect.isMultiselect").exists()).toBe(false);
    expect(wrapper.find("span.multiselect__single").text()).toBe("Beitragstyp 3");
  });

  it("handles disabled attribute", () => {
    const wrapper = mount(DipasSelect, {
      props: {
        ...defaultProps,
        disabled: true,
      },
      global,
    });

    expect(wrapper.find("div.multiselect").classes()).toContain("multiselect--disabled");
  });

  it("handles optionFieldId and optionFieldLabel attributes", () => {
    const wrapper = mount(DipasSelect, {
      props: {
        ...defaultProps,
        options: [
          {
            "id": "id-1",
            "name": "name 1",
          },
          {
            "id": "id-2",
            "name": "name 2",
          },
        ],
        optionFieldId: "id",
        optionFieldLabel: "name",
        modelValue: [
          {
            "id": "id-2",
            "name": "name 2"
          },
        ],
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global,
    });

    expect(wrapper.findAll("div.multiselect__tags span.multiselect__single").at(0).text()).toBe("name 2");
    expect(wrapper.findAll("li[role=\"option\"]").length).toBe(wrapper.props().options.length);
  });
});
