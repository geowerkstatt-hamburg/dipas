import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasModal from "@/components/DipasModal.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("DipasModal", () => {
  it("Default rendering", () => {
    const defaultProps = {},
      wrapper = mount(
        DipasModal,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.classes()).toContain("DipasModal");
    expect(wrapper.find("button.DipasButton.closeModalTop").exists()).toBe(true);
    expect(wrapper.find("button.DipasButton.closeModalFooter").exists()).toBe(true);
    expect(wrapper.find("h2").exists()).toBe(false);
  });

  it("No Close Buttons", () => {
    const testProps = {
        iconClose: false,
        useFooter: false
      },
      wrapper = mount(
        DipasModal,
        {
          props: testProps,
          global
        }
      );

    expect(wrapper.find("button.DipasButton.closeModalTop").exists()).toBe(false);
    expect(wrapper.find("button.DipasButton.closeModalFooter").exists()).toBe(false);
    expect(wrapper.find("h2").exists()).toBe(false);
  });

  it("Show Title", () => {
    const testProps = {
        title: "Test Title"
      },
      wrapper = mount(
        DipasModal,
        {
          props: testProps,
          global
        }
      );

    expect(wrapper.find("h2").exists()).toBe(true);
    expect(wrapper.find("h2").text()).toBe("Test Title");
  });

});
