import {describe, it, expect} from "vitest";

import {mount} from "@vue/test-utils";
import DipasMasterportal from "@/components/DipasMasterportal.vue";
import {global} from "../global.inc";

describe("DipasMasterportal", () => {
  const defaultProps = {};

  it("renders the component correctly", () => {
    const wrapper = mount(DipasMasterportal, {
      props: {
        ...defaultProps,
        src: "//lindenallee.localhost:8080/masterportal",
        iframeTitle: "masterportalTest"
      },
      global,
    });

    const mpIframe = wrapper.find(".masterportalIframe");

    expect(wrapper.classes()).toContain("DipasMasterportal");
    expect(wrapper.find("iframe").exists()).toBe(true);
    expect(mpIframe.attributes("src")).toBe("//lindenallee.localhost:8080/masterportal?postMessageUrl=http://localhost:3000");
  });
});
