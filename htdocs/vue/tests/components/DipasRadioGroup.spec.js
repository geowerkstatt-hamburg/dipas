import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasRadioGroup from "@/components/DipasRadioGroup.vue";

describe("DipasRadioGroup", () => {
  const defaultProps = {
    groupName: "testgroup",
    fieldLabel: "DipasRadioGroup test",
    modelValue: undefined,
    options: [
      {label: "First", value: 1},
      {label: "Second", value: 2},
      {label: "Third", value: 3},
    ],
  };

  it("Default rendering", async () => {
    const wrapper = mount(
      DipasRadioGroup,
      {
        props: Object.assign({}, defaultProps, {"onUpdate:modelValue": (newVal) => wrapper.setProps({modelValue: newVal})}),
        global
      }
    );

    expect(wrapper.find("legend").text()).toEqual("DipasRadioGroup test:");
    expect(wrapper.findAllComponents({name: "DipasRadio"})).toHaveLength(3);

    defaultProps.options.forEach((option, index) => {
      expect(wrapper.findAllComponents({name: "DipasRadio"})[index].props("label")).toEqual(option.label);
      expect(wrapper.findAllComponents({name: "DipasRadio"})[index].props("value")).toEqual(option.value);
    });

    // The wrapper method "setValue" is used to trigger an input event on the component
    // Since we are testing radio inputs the given value to setValue is ignored
    await wrapper.findAll("input")[1].setValue();
    expect(wrapper.props("modelValue")).toEqual(2);
  });

  it("Disabled rendering", async () => {
    const wrapper = mount(
      DipasRadioGroup,
      {
        props: Object.assign({}, defaultProps, {disabled: true, "onUpdate:modelValue": (newVal) => wrapper.setProps({modelValue: newVal})}),
        global
      }
    );

    await wrapper.findAll("input")[1].setValue();
    expect(wrapper.props("modelValue")).toEqual(undefined);
  });

  it("Inline rendering", () => {
    const wrapper = mount(
      DipasRadioGroup,
      {
        props: Object.assign({}, defaultProps, {optionsInline: true}),
        global
      }
    );

    expect(wrapper.classes()).toContain("inline");
  });

  it("Rendering with altered option/value keys", () => {
    const alteredOptions = [{text: "Text1", amount: "Amount1"}, {text: "Text2", amount: "Amount2"}, {text: "Text3", amount: "Amount3"}],
      wrapper = mount(
        DipasRadioGroup,
        {
          props: Object.assign({}, defaultProps, {optionValue: "amount", optionLabel: "text", options: alteredOptions}),
          global
        }
      );

    alteredOptions.forEach((option, index) => {
      expect(wrapper.findAllComponents({name: "DipasRadio"})[index].props("label")).toEqual(option.text);
      expect(wrapper.findAllComponents({name: "DipasRadio"})[index].props("value")).toEqual(option.amount);
    });
  });

  it("Emitting complete options on input", async () => {
    const wrapper = mount(
      DipasRadioGroup,
      {
        props: Object.assign({}, defaultProps, {emitCompleteOptionOnInput: true, "onUpdate:modelValue": (newVal) => wrapper.setProps({modelValue: newVal})}),
        global
      }
    );

    await wrapper.findAll("input")[1].setValue();
    expect(wrapper.props("modelValue")).toEqual({label: "Second", value: 2});
  });
});
