import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";

import DipasNavigation from "@/components/DipasNavigation.vue";
import {global} from "../global.inc";

describe("DipasNavigation", () => {
  const defaultProps = {
    links: [
      {
        linktext: "Link 1",
        endpoint: "endpoint1"
      },
      {
        linktext: "Link 2",
        endpoint: "endpoint2"
      },
      {
        linktext: "Link 3",
        endpoint: "endpoint3"
      }
    ]
  };

  it("Default rendering", () => {
    const wrapper = mount(
      DipasNavigation,
      {
        props: defaultProps,
        global: Object.assign({}, global, {
          mocks: {
            $route: {
              fullPath: "/endpoint2"
            }
          }
        })
      }
    );

    expect(wrapper.classes()).toContain("DipasNavigation");
    expect(wrapper.find("nav.DipasNavigation ul").exists()).toBe(true);
    expect(wrapper.findAll("nav.DipasNavigation ul li").length).toBe(3);

    // Active link must contain the path set in mocks.$route.$fullPath
    expect(wrapper.find("nav.DipasNavigation ul li a.linkActive").attributes("href")).toBe("/endpoint2");
    expect(wrapper.findAll("nav.DipasNavigation ul li a.linkActive").length).toBe(1);

    wrapper.findAll("nav.DipasNavigation ul li").forEach((listitem, linkIndex) => {
      expect(listitem.find("a").exists()).toBe(true);
      expect(listitem.find("a").attributes("href")).toBe("/" + defaultProps.links[linkIndex].endpoint);
      expect(listitem.find("a").text()).toBe(defaultProps.links[linkIndex].linktext);

      listitem.findAll("span").forEach((span, spanIndex) => {
        expect(span.classes()).toContain(["linkText", "activeIndicator"][spanIndex]);

        if (span.classes("linkText")) {
          expect(span.text()).toEqual(defaultProps.links[linkIndex].linktext);
        }
      });
    });
  });
});
