import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc";
import DipasCookieBanner from "@/components/DipasCookieBanner.vue";

describe("DipasCookieBanner.vue", () => {

  it("renders the component", () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    expect(wrapper.exists()).toBe(true);
  });

  it("displays the banner when cookies are not accepted", () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    expect(wrapper.find("#DipasCookieBanner").isVisible()).toBe(true);
  });

  it("sets 'bannerShown' to true the close button is clicked", () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    const closeButton = wrapper.find(".icon-close_black_24dp");

    closeButton.trigger("click");
    expect(wrapper.vm.bannerShown).toBe(true);
  });

  it("sets 'bannerShown' to true the 'Accept' button is clicked", () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    const acceptButton = wrapper.find(".acceptButton");

    acceptButton.trigger("click");
    expect(wrapper.vm.bannerShown).toBe(true);
  });

  it("sets 'bannerShown' to true after declining cookies", async () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    await wrapper.vm.declineCookies();
    expect(wrapper.vm.bannerShown).toBe(true);
  });

  it("sets 'bannerShown' to true after accepting cookies", () => {
    const wrapper = mount(DipasCookieBanner, {
      global,
    });

    wrapper.vm.acceptCookies();
    expect(wrapper.vm.bannerShown).toBe(true);
  });
});
