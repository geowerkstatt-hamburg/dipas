import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc";
import DipasImageGallery from "@/components/DipasImageGallery.vue";

describe("DipasImageGallery.vue", () => {
  it("Single image rendering", () => {
    const wrapper = mount(
      DipasImageGallery,
      {
        props: {
          images: [
            {
              src: "http://localhost/img1.jpg",
              alt: "Alt-Text Image 1",
              caption: "Caption image 1",
              derivates: {
                small: "http://localhost/small/img1.jpg",
                large: "http://localhost/large/img1.jpg"
              }
            }
          ]
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasImageGallery");
    expect(wrapper.classes()).toContain("singleImage");
    expect(wrapper.find("a.imageContainer").exists()).toBe(true);
    expect(wrapper.findAll("a.imageContainer").length).toBe(1);
    wrapper.findAll("a.imageContainer").forEach(image => {
      expect(image.attributes()).toHaveProperty("style");
      expect(image.attributes().style).toEqual("--imageurl: url(" + wrapper.props().images[0].derivates.large + ");");
      expect(image.attributes()).toHaveProperty("tabindex");
      expect(image.attributes().tabindex).toEqual("0");
      expect(image.attributes()).toHaveProperty("title");
      expect(image.attributes().title).toEqual("imageGallery.imagePreviewLinkLabel");
      expect(image.attributes()).toHaveProperty("aria-label");
      expect(image.attributes()["aria-label"]).toEqual("imageGallery.imagePreviewLinkLabel");
    });
  });

  it("Multiimage rendering", () => {
    const wrapper = mount(
      DipasImageGallery,
      {
        props: {
          images: [
            {
              src: "http://localhost/img1.jpg",
              alt: "Alt-Text Image 1",
              caption: "Caption image 1",
              derivates: {
                small: "http://localhost/small/img1.jpg",
                large: "http://localhost/large/img1.jpg"
              }
            },
            {
              src: "http://localhost/img2.jpg",
              alt: "Alt-Text Image 2",
              caption: "Caption image 2",
              derivates: {
                small: "http://localhost/small/img2.jpg",
                large: "http://localhost/large/img2.jpg"
              }
            },
            {
              src: "http://localhost/img3.jpg",
              alt: "Alt-Text Image 3",
              caption: "Caption image 3",
              derivates: {
                small: "http://localhost/small/img3.jpg",
                large: "http://localhost/large/img3.jpg"
              }
            }
          ]
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("multiImage");
    expect(wrapper.findAll("a.imageContainer").length).toBe(wrapper.props().images.length);

    wrapper.findAll("a.imageContainer").forEach((image, index) => {
      expect(image.attributes().style).toEqual("--imageurl: url(" + wrapper.props().images[index].derivates[index === 0 ? "large" : "small"] + ");");
      expect(image.attributes().tabindex).toEqual("0");
      expect(image.attributes().title).toEqual("imageGallery.imagePreviewLinkLabel");
      expect(image.attributes()["aria-label"]).toEqual("imageGallery.imagePreviewLinkLabel");
    });
  });

  it("Multiimage rendering (> 4 images)", () => {
    const wrapper = mount(
      DipasImageGallery,
      {
        props: {
          images: [
            {
              src: "http://localhost/img1.jpg",
              alt: "Alt-Text Image 1",
              caption: "Caption image 1",
              derivates: {
                small: "http://localhost/small/img1.jpg",
                large: "http://localhost/large/img1.jpg"
              }
            },
            {
              src: "http://localhost/img2.jpg",
              alt: "Alt-Text Image 2",
              caption: "Caption image 2",
              derivates: {
                small: "http://localhost/small/img2.jpg",
                large: "http://localhost/large/img2.jpg"
              }
            },
            {
              src: "http://localhost/img3.jpg",
              alt: "Alt-Text Image 3",
              caption: "Caption image 3",
              derivates: {
                small: "http://localhost/small/img3.jpg",
                large: "http://localhost/large/img3.jpg"
              }
            },
            {
              src: "http://localhost/img4.jpg",
              alt: "Alt-Text Image 4",
              caption: "Caption image 4",
              derivates: {
                small: "http://localhost/small/img4.jpg",
                large: "http://localhost/large/img4.jpg"
              }
            },
            {
              src: "http://localhost/img5.jpg",
              alt: "Alt-Text Image 5",
              caption: "Caption image 5",
              derivates: {
                small: "http://localhost/small/img5.jpg",
                large: "http://localhost/large/img5.jpg"
              }
            }
          ]
        },
        global
      }
    );

    expect(wrapper.findAll("a.imageContainer").length).toBe(4);
  });
});
