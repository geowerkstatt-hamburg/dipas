import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasSidebar from "@/components/DipasSidebar.vue";

describe("DipasSidebar", () => {
  it("Default rendering", () => {
    const wrapper = mount(DipasSidebar, {global});

    // Nothing testworthy in DipasSidebar.vue, so just check if component mounts
    // properly and contains root CSS class.
    expect(wrapper.find("div").classes()).toContain("DipasSidebar");
  });
});
