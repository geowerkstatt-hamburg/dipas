import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasRadio from "@/components/DipasRadio.vue";

describe("DipasRadio", () => {
  const defaultProps = {
    label: "DipasRadio",
    value: "RadioValue",
  };

  it("Default rendering", async () => {
    const wrapper = mount(
        DipasRadio,
        {
          props: defaultProps,
          global
        }
      ),
      input = wrapper.find("input"),
      inputAttributes = input.attributes(),
      label = wrapper.find("label"),
      labelAttributes = label.attributes();

    expect(wrapper.classes()).toContain("DipasRadio");

    expect(inputAttributes).toHaveProperty("id");
    expect(inputAttributes.id).toMatch(/^UUIDString-/);
    expect(inputAttributes).toHaveProperty("value");
    expect(inputAttributes.value).toEqual("RadioValue");
    expect(inputAttributes).toHaveProperty("name");
    expect(inputAttributes.name).toEqual("radio-group");
    expect(inputAttributes).not.toHaveProperty("disabled");

    expect(label.text()).toEqual("DipasRadio");
    expect(labelAttributes).toHaveProperty("for");
    expect(labelAttributes.for).toMatch(/^UUIDString-/);

    await label.trigger("click");
    expect(wrapper.emitted()).toHaveProperty("click");
  });

  it("Disabled rendering", async () => {
    const wrapper = mount(
      DipasRadio,
      {
        props: Object.assign({}, defaultProps, {disabled: true}),
        global
      }
    );

    expect(wrapper.find("input").attributes()).toHaveProperty("disabled");
  });
});
