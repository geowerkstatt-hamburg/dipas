import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasTextChip from "@/components/DipasTextChip.vue";

describe("DipasTextChip", () => {
  const defaultProps = {
    chipText: "DipasTextChip",
    backgroundColor: "#aabbcc"
  };

  it("Default rendering", () => {
    const wrapper = mount(
      DipasTextChip,
      {
        props: defaultProps
      }
    );

    expect(wrapper.classes()).toContain("DipasTextChip");
    expect(wrapper.find("span.chipText").text()).toEqual("DipasTextChip");
    expect(wrapper.attributes()).toHaveProperty("style");
    expect(wrapper.attributes().style).toEqual("--backgroundColor: #aabbcc; --borderColor: #aabbcc;");
    expect(wrapper.find("div.DipasInfoButton").exists()).toBe(false);
  });

  it("Rendering with tooltip icon", () => {
    const wrapper = mount(
      DipasTextChip,
      {
        props: Object.assign({}, defaultProps, {tooltipTitle: "DipasTextChip title", tooltipText: "DipasTextChip tooltip"}),
        global
      }
    );

    expect(wrapper.find("div.DipasInfoButton").exists()).toBe(true);
  });

  it("handles component with background-color correctly", () => {
    const wrapper = mount(
      DipasTextChip,
      {
        props: {
          ...defaultProps,
          backgroundColor: "#CC0000",
        },
        global
      }
    );

    expect(wrapper.attributes().style).toBe("--backgroundColor: #CC0000; --borderColor: #CC0000;");
    expect(wrapper.find("span.colorEdge").exists()).toBe(true);
    expect(wrapper.find("span.chipText.hasColorEdge").exists()).toBe(true);
  });

  it("handles component without background-color correctly", () => {
    const wrapper = mount(
      DipasTextChip,
      {
        props: {
          ...defaultProps,
          backgroundColor: null,
        },
        global
      }
    );

    expect(wrapper.attributes()).not.toHaveProperty("style");
    expect(wrapper.find("span.colorEdge").exists()).toBe(false);
    expect(wrapper.find("span.chipText.hasColorEdge").exists()).toBe(false);
  });
});
