import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasPaginator from "@/components/DipasPaginator.vue";
import DipasPaginatorItem from "@/components/DipasPaginatorItem.vue";

describe("DipasPaginator", () => {
  const defaultProps = {
    maxPageNumber: 1
  };

  it("Paginator default rendering", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: defaultProps,
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasPaginator");
    expect(wrapper.findComponent({name: "DipasPaginatorItem"}).exists()).toBe(true);
    expect(wrapper.findAllComponents(DipasPaginatorItem).length).toBe(5);

    const leftThreeDotsRef = wrapper.findComponent({ref: "showLeftThreeDots"});

    expect(leftThreeDotsRef.exists()).toBe(false);

    const rightThreeDotsRef = wrapper.findComponent({ref: "showRightThreeDots"});

    expect(rightThreeDotsRef.exists()).toBe(false);
  });

  it("Paginator 2 pages", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 2
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasPaginator");
    expect(wrapper.findComponent({name: "DipasPaginatorItem"}).exists()).toBe(true);
    expect(wrapper.findAllComponents(DipasPaginatorItem).length).toBe(6);

    const leftThreeDotsRef = wrapper.findComponent({ref: "showLeftThreeDots"});

    expect(leftThreeDotsRef.exists()).toBe(false);

    const rightThreeDotsRef = wrapper.findComponent({ref: "showRightThreeDots"});

    expect(rightThreeDotsRef.exists()).toBe(false);
  });

  it("Paginator 4 pages", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 4
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasPaginator");
    expect(wrapper.findComponent({name: "DipasPaginatorItem"}).exists()).toBe(true);
    expect(wrapper.findAllComponents(DipasPaginatorItem).length).toBe(8);

    const leftThreeDotsRef = wrapper.findComponent({ref: "showLeftThreeDots"});

    expect(leftThreeDotsRef.exists()).toBe(false);

    const rightThreeDotsRef = wrapper.findComponent({ref: "showRightThreeDots"});

    expect(rightThreeDotsRef.exists()).toBe(false);
  });

  it("Paginator 7 pages", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 7
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasPaginator");
    expect(wrapper.findComponent({name: "DipasPaginatorItem"}).exists()).toBe(true);
    expect(wrapper.findAllComponents(DipasPaginatorItem).length).toBe(11);

    const leftThreeDotsRef = wrapper.findComponent({ref: "showLeftThreeDots"});

    expect(leftThreeDotsRef.exists()).toBe(false);

    const rightThreeDotsRef = wrapper.findComponent({ref: "showRightThreeDots"});

    expect(rightThreeDotsRef.exists()).toBe(false);
  });

  it("Paginator 15 pages", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 15
        },
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasPaginator");
    expect(wrapper.findComponent({name: "DipasPaginatorItem"}).exists()).toBe(true);

    const leftThreeDotsRef = wrapper.findComponent({ref: "showLeftThreeDots"});

    expect(leftThreeDotsRef.exists()).toBe(false);

    const rightThreeDotsRef = wrapper.findComponent({ref: "showRightThreeDots"});

    expect(rightThreeDotsRef.exists()).toBe(true);
  });

  it("Paginator 15 pages click on forward and backward buttons", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 15
        },
        global
      }
    );

    // test forward button
    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(true);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(false);

    await wrapper.findComponent({ref: "forward"}).trigger("click");

    expect(wrapper.find(".active").html()).toContain(2);

    await wrapper.findComponent({ref: "forward"}).trigger("click");
    expect(wrapper.find(".active").html()).toContain(3);

    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(true);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(false);

    await wrapper.findComponent({ref: "forward"}).trigger("click");
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(true);

    // test backward button
    await wrapper.findComponent({ref: "backward"}).trigger("click");
    await wrapper.findComponent({ref: "backward"}).trigger("click");
    expect(wrapper.find(".active").html()).toContain(2);

    await wrapper.findComponent({ref: "backward"}).trigger("click");
    expect(wrapper.find(".active").html()).toContain(1);
    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(true);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(false);
  });

  it("Paginator 15 pages click on last and first button", async () => {
    const wrapper = mount(
      DipasPaginator,
      {
        props: {
          maxPageNumber: 15
        },
        global
      }
    );

    // test last button
    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(true);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(false);

    await wrapper.findAll(".firstLastElement")[1].trigger("click");
    expect(wrapper.find(".active").html()).toContain(15);
    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(false);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(true);

    // test first button
    await wrapper.findAll(".firstLastElement")[0].trigger("click");
    expect(wrapper.find(".active").html()).toContain(1);
    expect(wrapper.findComponent({ref: "showRightThreeDots"}).exists()).toBe(true);
    expect(wrapper.findComponent({ref: "showLeftThreeDots"}).exists()).toBe(false);
  });
});
