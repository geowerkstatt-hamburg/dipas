import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc";
import DipasCard from "@/components/DipasCard.vue";

describe("DipasCard", () => {
  const defaultProps = {};

  it("renders the base component", () => {
    const wrapper = mount(DipasCard, {
      props: {
        ...defaultProps,
      },
      global,
    });

    expect(wrapper.classes()).toContain("DipasCard");
  });

  it("handles noPadding attribute for content .inner", () => {
    const wrapper = mount(DipasCard, {
      props: {
        ...defaultProps,
        noPadding: true,
      },
      global,
    });

    expect(wrapper.find("div.inner").classes()).toContain("noPadding");
  });

  it("handles headerRightContainsActions attribute", () => {
    const wrapper = mount(DipasCard, {
      props: {
        ...defaultProps,
        headerRightContainsActions: true,
      },
      global,
    });

    expect(wrapper.classes()).toContain("headerRightContainsActions");
  });

  it("handles slots correctly", () => {
    const wrapper = mount(DipasCard, {
      props: {
        ...defaultProps,
      },
      global,
      slots: {
        headerLeft: "Left header slot",
        headerRight: "Right header slot",
        default: "Default slot",
      },
    });

    expect(wrapper.find("div.headerLeft").text()).toContain("Left header slot");
    expect(wrapper.find("div.headerRight").text()).toContain("Right header slot");
    expect(wrapper.find("div.inner").text()).toContain("Default slot");
  });

  it("handles isCollapsible attribute correctly", async () => {
    const wrapper = mount(DipasCard, {
      props: {
        ...defaultProps,
        isCollapsible: true,
      },
      global,
    });

    const button = wrapper.find("button.header");

    expect(button.exists()).toBe(true);

    // Before click, the content is visible.
    expect(wrapper.find("div.mainCard .inner").exists()).toBe(true);

    // Click
    await button.trigger("click");

    // After click, the content is invisible
    expect(wrapper.find("div.mainCard .inner").exists()).toBe(false);
  });
});

