import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "../global.inc.js";

import DipasCheckbox from "@/components/DipasCheckbox.vue";

describe("DipasCheckbox", () => {
  const defaultProps = {
    label: "Label 1",
    inputValue: "option1"
  };

  it("Default rendering", async () => {
    const wrapper = mount(
      DipasCheckbox,
      {
        props: defaultProps,
        global
      }
    );

    expect(wrapper.classes()).toContain("DipasCheckbox");
    expect(wrapper.find("label").text()).toEqual(defaultProps.label);
    expect(wrapper.find("input").attributes()).toHaveProperty("id");
    expect(wrapper.find("input").attributes()).toHaveProperty("type");
    expect(wrapper.find("input").attributes().type).toEqual("checkbox");
    expect(wrapper.find("input").attributes()).toHaveProperty("value");
    expect(wrapper.find("input").attributes().value).toEqual(defaultProps.inputValue);
    expect(wrapper.find("input").attributes()).not.toHaveProperty("disabled");
  });

  it("Disabled rendering", () => {
    const wrapper = mount(
      DipasCheckbox,
      {
        props: {...defaultProps, disabled: true},
        global
      }
    );

    expect(wrapper.find("input").attributes()).toHaveProperty("disabled");
  });

  it("Click behavior", async () => {
    const wrapper = mount(
      DipasCheckbox,
      {
        props: defaultProps,
        global
      }
    );

    wrapper.setProps({"onUpdate:modelValue": (newVal) => wrapper.setProps({modelValue: newVal})});

    await wrapper.find("label").trigger("click");

    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted()).toHaveProperty("update:modelValue");
  });
});
