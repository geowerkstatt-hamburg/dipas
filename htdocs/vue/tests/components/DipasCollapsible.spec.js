import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import {createApp} from "vue";
import DipasCollapsible from "@/components/DipasCollapsible.vue";
import DipasCollapsibleItem from "@/components/DipasCollapsibleItem.vue";

describe("DipasCollapsible", () => {
  const defaultProps = {};

  it("renders the component correctly", () => {
    const wrapper = mount(DipasCollapsible, {
      props: {
        ...defaultProps,
      },
      global,
    });

    expect(wrapper.classes()).toContain("DipasCollapsible");
  });

  it("handles renderAs accordion with button clicks correctly", async () => {
    const wrapper = mount(createApp({}), {
      template: `
        <DipasCollapsible renderAs="accordion">
          <DipasCollapsibleItem title="Title 1">
            Content 1
          </DipasCollapsibleItem>
          <DipasCollapsibleItem title="Title 2" :active="true">
            Content 2
          </DipasCollapsibleItem>
        </DipasCollapsible>
      `,
      components: {
        DipasCollapsible,
        DipasCollapsibleItem
      },
      global
    });

    const collapsibleItems = wrapper.findAll("div.DipasCollapsibleItemAccordion");
    const collapsibleItemButtons = wrapper.findAll("button.accordionHeader");

    // Correct item length.
    expect(collapsibleItems.length).toBe(2);

    // Correct titles
    expect(collapsibleItemButtons[0].text()).toBe("Title 1");
    expect(collapsibleItemButtons[1].text()).toBe("Title 2");

    // Content of Item 1 (invisible), Item 2 (visible)
    expect(collapsibleItems[0].find("div.bodyContainer div.body").exists()).toBe(false);
    expect(collapsibleItems[1].find("div.bodyContainer div.body").exists()).toBe(true);

    // First button click, now both items are visible
    await collapsibleItemButtons[0].trigger("click");

    // Content of Item 1 (visible), Item 2 (visible) - Also test, if the correct content is set for both
    expect(collapsibleItems[0].find("div.bodyContainer div.body").exists()).toBe(true);
    expect(collapsibleItems[1].find("div.bodyContainer div.body").exists()).toBe(true);
    expect(collapsibleItems[0].find("div.bodyContainer div.body").text()).toBe("Content 1");
    expect(collapsibleItems[1].find("div.bodyContainer div.body").text()).toBe("Content 2");

    await collapsibleItemButtons[1].trigger("click");

    // Content of Item 1 (visible), Item 2 (invisible)
    expect(collapsibleItems[0].find("div.bodyContainer div.body").exists()).toBe(true);
    expect(collapsibleItems[1].find("div.bodyContainer div.body").exists()).toBe(false);
  });

  it("handles renderAs tabs with button clicks correctly", async () => {
    const wrapper = mount(createApp({}), {
      template: `
        <DipasCollapsible renderAs="tabs">
          <DipasCollapsibleItem title="Title 1">
            Content 1
          </DipasCollapsibleItem>
          <DipasCollapsibleItem title="Title 2" :active="true">
            Content 2
          </DipasCollapsibleItem>
        </DipasCollapsible>
      `,
      components: {
        DipasCollapsible,
        DipasCollapsibleItem
      },
      global
    });

    // Trigger rendering of tab buttons on next tick!
    await wrapper.vm.$nextTick;

    const tabItemButtons = wrapper.findAll(".DipasCollapsibleTabs .tabs button");

    // Correct item length.
    expect(tabItemButtons.length).toBe(2);

    // Correct titles
    expect(tabItemButtons[0].text()).toBe("Title 1");
    expect(tabItemButtons[1].text()).toBe("Title 2");

    // Content of Item 1 (inactive), Item 2 (active)
    expect(tabItemButtons[0].classes()).not.toContain("isActive");
    expect(tabItemButtons[1].classes()).toContain("isActive");
    expect(wrapper.find("div.DipasCollapsibleItemTabs").text()).toBe("Content 2");

    // First button click, now both items are visible
    await tabItemButtons[0].trigger("click");

    // Content of Item 1 (active), Item 2 (inactive)
    expect(tabItemButtons[0].classes()).toContain("isActive");
    expect(tabItemButtons[1].classes()).not.toContain("isActive");
    expect(wrapper.find("div.DipasCollapsibleItemTabs").text()).toBe("Content 1");
  });

  it("handles renderAs transparent correctly", async () => {
    const wrapper = mount(createApp({}), {
      global,
      template: `
        <DipasCollapsible renderAs="transparent">
          <DipasCollapsibleItem title="Title 1">
            Content 1
          </DipasCollapsibleItem>
          <DipasCollapsibleItem title="Title 2" :active="true">
            Content 2
          </DipasCollapsibleItem>
        </DipasCollapsible>
      `,
      components: {
        DipasCollapsible,
        DipasCollapsibleItem
      },
    });

    // Trigger rendering of tab buttons on next tick!
    await wrapper.vm.$nextTick;

    const transparentItems = wrapper.findAll("div.DipasCollapsibleItemTransparent");

    // Transparent has no buttons, only content
    expect(transparentItems[0].text()).toBe("Content 1");
    expect(transparentItems[1].text()).toBe("Content 2");
  });

  it("handles attribute accordionAutoCollapse correctly", () => {
    const wrapper = mount(DipasCollapsible, {
      props: {
        ...defaultProps,
        renderAs: "accordion",
        accordionAutoCollapse: true,
      },
      global,
    });

    expect(wrapper.find("div.accordionAutoCollapse.accordionAutoCollapse").exists()).toBe(true);
  });

  it("handles attribute tabsHorizontal correctly", () => {
    const wrapper = mount(DipasCollapsible, {
      props: {
        ...defaultProps,
        renderAs: "tabs",
        tabsHorizontal: true,
      },
      global,
    });

    expect(wrapper.find("div.DipasCollapsibleTabs.render-as-tabs.tabsHorizontal").exists()).toBe(true);
  });

  it("handles attribute isSmall correctly", () => {
    const wrapper = mount(DipasCollapsible, {
      props: {
        ...defaultProps,
        isSmall: true,
      },
      global,
    });

    expect(wrapper.classes()).toContain("isSmall");
  });
});
