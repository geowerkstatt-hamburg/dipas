import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";

import DipasSidebarToggle from "@/components/DipasSidebarToggle.vue";

describe("DipasSidebarToggle", () => {
  const global = {
      // eslint-disable-next-line no-empty-function
      buttonCallback: () => {}
    },
    defaultProps = {
      buttons: [
        {
          label: "Button 1",
          title: "Title Button 1",
          icon: "icon1",
          id: "button1",
          callback: () => {
            global.buttonCallback(1);
          },
        },
        {
          label: "Button 2",
          title: "Title Button 2",
          icon: "icon2",
          id: "button2",
          callback: () => {
            global.buttonCallback(2);
          },
        },
        {
          label: "Button 3",
          title: "Title Button 3",
          icon: "icon3",
          id: "button3",
          callback: () => {
            global.buttonCallback(3);
          },
        },
      ]
    };

  it("Default rendering", async () => {
    const wrapper = mount(
        DipasSidebarToggle,
        {
          global,
          props: defaultProps
        }
      ),
      callbackSpy = vi.spyOn(global, "buttonCallback");

    expect(wrapper.findAllComponents({name: "DipasButton"})).toHaveLength(3);

    for (const buttonDefinition of defaultProps.buttons) {
      const buttonNumber = Number(buttonDefinition.label.substr(buttonDefinition.label.length - 1)),
        index = buttonNumber - 1,
        button = wrapper.findAllComponents({name: "DipasButton"})[index],
        buttonProps = button.props();

      // Testing for required properties
      for (const property of ["label", "title", "icon"]) {
        expect(buttonProps).toHaveProperty(property);
        expect(buttonProps[property]).toEqual(buttonDefinition[property]);
      }

      // Testing button callback execution
      await button.find("button").trigger("click");
      expect(callbackSpy).toHaveBeenCalledWith(buttonNumber);
    }
  });

  it("Redering with preselected button", () => {
    const wrapper = mount(
      DipasSidebarToggle,
      {
        global,
        props: Object.assign({}, defaultProps, {selected: "button2"})
      }
    );

    const button = wrapper.findAllComponents({name: "DipasButton"}).filter((DOMwrapper) => DOMwrapper.classes().includes("secondary"));

    expect(button[0].componentVM.label).toEqual("Button 2");
  });
});
