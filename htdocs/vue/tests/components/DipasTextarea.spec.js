import {describe, it, expect} from "vitest";

import {mount} from "@vue/test-utils";
import {global} from "../global.inc";
import DipasTextarea from "../../src/components/DipasTextarea.vue";

/**
 * ATTENTION: DipasTextarea extends the DipasTextfield component.
 * Because of this, nearly all tests are the same for both components.
 */

describe("DipasTextarea", () => {
  const defaultProps = {
    fieldLabel: "Label of DipasTextarea",
    placeholder: "Placeholder for DipasTextarea",
    title: "Title for DipasTextarea",
  };

  it("handles the basic attribute (fieldLabel, placeholder, title)", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
      },
      global,
    });

    expect(wrapper.classes()).toContain("DipasTextarea");
    expect(wrapper.find("div.wrapperLabel > label").text()).toBe(wrapper.props().fieldLabel);
    expect(wrapper.find("div.wrapperInput > textarea").attributes("placeholder")).toBe(wrapper.props().placeholder);
    expect(wrapper.find("div.wrapperInput > textarea").attributes("title")).toBe(wrapper.props().title);
  });

  it("handles the props attribute", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        rows: 10,
      },
      global,
    });

    expect(wrapper.find("div.wrapperInput > textarea").attributes("rows")).toBe("10");
  });

  it("handles the fieldLabelHidden attribute", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        fieldLabelHidden: true,
      },
      global,
    });

    expect(wrapper.find("label.visually-hidden").text()).toBe(wrapper.props().fieldLabel);
  });

  it("handles the disabledTitle attribute", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        disabled: true,
        disabledTitle: "DisabledTitle for DipasTextarea",
      },
      global,
    });

    expect(wrapper.find("div.wrapperInput > textarea[disabled]").exists()).toBe(true);
    expect(wrapper.find("div.wrapperInput > textarea").attributes("title")).toBe(wrapper.props().disabledTitle);
  });

  it("handles the required attribute", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        required: true,
      },
      global,
    });

    expect(wrapper.find("div.required").exists()).toBe(true);
    expect(wrapper.find("div.required > span[aria-hidden]").text()).toBe("*");
    expect(wrapper.find("div.required > span.visually-hidden").text()).toBe("form.general.required");
  });

  it("handles the fieldDescription attribute", () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        fieldDescription: "FieldDescription for DipasTextarea",
      },
      global,
    });

    expect(wrapper.find("div.description").text()).toBe(wrapper.props().fieldDescription);
  });

  it("handles the validationMinCharacterCount attribute", async () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        validationMinCharacterCount: {
          value: 3,
          error: "Enter min 3 characters"
        },
        modelValue: "",
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global
    });

    const input = wrapper.find("textarea");

    // Handle "invalid data" case

    await input.setValue("12");

    expect(wrapper.find("div.DipasTextarea.error").exists()).toBe(true);
    expect(wrapper.find("div.sublineText.error").text()).toBe(wrapper.props().validationMinCharacterCount.error);

    // Handle "valid data" case

    await input.setValue("123");

    expect(wrapper.find("div.DipasTextarea.error").exists()).not.toBe(true);
    expect(wrapper.find("div.sublineText.error").exists()).not.toBe(true);
  });

  it("handles the validationMaxCharacterCount attribute", async () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        validationMaxCharacterCount: {
          value: 3,
          showCounter: true,
          limitMaxLengthInput: false,
          error: "Enter max 3 characters",
        },
        modelValue: "",
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global
    });

    const input = wrapper.find("textarea");

    // Handle "invalid data" case

    await input.setValue("1234");

    expect(wrapper.find("span.characterCount").text()).toBe("4/3");
    expect(wrapper.find("div.DipasTextarea.error").exists()).toBe(true);
    expect(wrapper.find("div.sublineText.error").text()).toBe(wrapper.props().validationMaxCharacterCount.error);

    // Handle "valid data" case

    await input.setValue("123");

    expect(wrapper.find("span.characterCount").text()).toBe("3/3");
    expect(wrapper.find("div.DipasTextarea.error").exists()).not.toBe(true);
    expect(wrapper.find("div.sublineText.error").exists()).not.toBe(true);
  });

  it("handles the validationRegex attribute", async () => {
    const wrapper = mount(DipasTextarea, {
      props: {
        ...defaultProps,
        validationRegex: {
          value: new RegExp(/HELLO/),
          error: "The text must contain the word HELLO.",
        },
        modelValue: "",
        "onUpdate:modelValue": (e) => wrapper.setProps({modelValue: e}),
      },
      global
    });

    const input = wrapper.find("textarea");

    // Handle "invalid data" case

    await input.setValue("abc");

    expect(wrapper.find("div.DipasTextarea.error").exists()).toBe(true);
    expect(wrapper.find("div.sublineText.error").text()).toBe(wrapper.props().validationRegex.error);

    // Handle "valid data" case

    await input.setValue("abc HELLO xyz");

    expect(wrapper.find("div.DipasTextarea.error").exists()).not.toBe(true);
    expect(wrapper.find("div.sublineText.error").exists()).not.toBe(true);
  });
});
