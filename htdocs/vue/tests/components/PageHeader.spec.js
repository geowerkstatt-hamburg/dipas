import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import {global} from "@/../tests/global.inc.js";
import DipasNavigation from "@/components/DipasNavigation.vue";
import PageHeader from "@/components/PageHeader.vue";

// dipasSettings is imported globally into the global javascript
// namespace by /htdocs/vue/index.html (settings.js)
Object.defineProperty(window, "dipasSettings", {
  value: {
    "hasMenuLineLogo": true,
    "operatorLogoAlignment": "left",
    "operatorLogoAlttext": "powered by DIPAS - Digital participation system",
    "colors": {
      "primary2": "#e10019",
    }
  },
});

describe("PageHeader.vue", () => {
  const dipasNavigationDefaultProps = {
    links: [
      {endpoint: "/test", linktext: "test"},
    ],
  };

  it("renders the component", ()=> {
    const wrapper = mount(PageHeader, {
      props: dipasNavigationDefaultProps,
      global,
    });

    expect(wrapper.exists()).toBe(true);
  });

  it("renders navigation links", () => {
    const wrapper = mount(PageHeader, {
      props: dipasNavigationDefaultProps,
      global,
    });

    const dipasNavigationComponent = wrapper.findComponent(DipasNavigation);

    expect(dipasNavigationComponent.exists()).toBe(true);
  });

  it("toggles menuOpen when the menu button is clicked", async () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(PageHeader, {
      props: dipasNavigationDefaultProps,
      global
    });

    const menuButton = wrapper.find(".navigation .secondary");

    await menuButton.trigger("click");
    expect(wrapper.vm.menuOpen).toBe(1);
    await menuButton.trigger("click");
    expect(wrapper.vm.menuOpen).toBe(0);
  });

  it("displays the logo image", () => {
    const wrapper = mount(PageHeader, {
      props: dipasNavigationDefaultProps,
      global,
    });

    const logoImage = wrapper.find(".logo img");

    expect(logoImage.exists()).toBe(true);
    expect(logoImage.attributes("src")).toMatch(/\/drupal\/dipas\/design\/operatorlogo$/);
    expect(logoImage.attributes("alt")).toBe(wrapper.vm.settings.operatorLogoAlttext);
  });

  it("displays the menu line logo when settings.hasMenuLineLogo is true", () => {
    const wrapper = mount(PageHeader, {
      props: dipasNavigationDefaultProps,
      global,
    });

    const menuLineLogo = wrapper.find(".menuLineLogo");

    if (wrapper.vm.settings.hasMenuLineLogo) {
      expect(menuLineLogo.exists()).toBe(true);
    }
  });

});
