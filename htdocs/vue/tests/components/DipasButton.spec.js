import {describe, it, expect} from "vitest";
import {mount} from "@vue/test-utils";

import DipasButton from "@/components/DipasButton.vue";

describe("DipasButton", () => {
  const defaultProps = {
    buttonId: "DipasButtonID",
    label: "DipasButton Test",
    title: "DipasButton title string",
  };

  it("Button default rendering", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: defaultProps
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.text()).toEqual("DipasButton Test");
    expect(wrapper.classes()).toContain("DipasButton");
    expect(wrapper.attributes().title).toEqual("DipasButton title string");
    expect(wrapper.isDisabled()).toBe(false);
    expect(wrapper.html()).not.toMatch(/span class=".*?icon.*?"/);
    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted().click[0][0]).toEqual("DipasButtonID");
  });

  it("Button default rendering (disabled)", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {disabled: true, disabledTitle: "DipasButton title string disabled"})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.isDisabled()).toBe(true);
    expect(wrapper.attributes().title).toEqual("DipasButton title string disabled");
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });

  it("Button with icon", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon"})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.html()).toMatch(/span class="icon icon-TestIcon"/);
    expect(wrapper.isDisabled()).toBe(false);
    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted().click[0][0]).toEqual("DipasButtonID");
  });

  it("Button with icon (disabled)", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon", disabled: true})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.isDisabled()).toBe(true);
    expect(wrapper.html()).toMatch(/span class="icon icon-TestIcon"/);
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });

  it("Button with icon after text", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon", iconAfterText: true})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.html()).toMatch(/span[^>]+?class="dps-button__text".*?span[^>]+?class="[^"]*?icon/m);
    expect(wrapper.isDisabled()).toBe(false);
    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted().click[0][0]).toEqual("DipasButtonID");
  });

  it("Button with icon after text (disabled)", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon", iconAfterText: true, disabled: true})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.isDisabled()).toBe(true);
    expect(wrapper.html()).toMatch(/span[^>]+?class="dps-button__text".*?span[^>]+?class="[^"]*?icon/m);
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });

  it("Button with icon only", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon", iconOnly: true})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.isDisabled()).toBe(false);
    expect(wrapper.html()).toMatch(/span[^>]+?class="dps-button__text visually-hidden"/);
    expect(wrapper.emitted()).toHaveProperty("click");
    expect(wrapper.emitted().click[0][0]).toEqual("DipasButtonID");
  });

  it("Button with icon only (disabled)", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: Object.assign({}, defaultProps, {icon: "TestIcon", iconOnly: true, disabled: true})
      }
    );

    await wrapper.find("button").trigger("click");

    expect(wrapper.isDisabled()).toBe(true);
    expect(wrapper.html()).toMatch(/span[^>]+?class="dps-button__text visually-hidden"/);
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });

  it("handles inline attribute correctly", async () => {
    const wrapper = mount(
      DipasButton,
      {
        props: {
          ...defaultProps,
          inline: true,
        },
      }
    );

    expect(wrapper.classes()).contains("secondary");
    expect(wrapper.classes()).contains("inline");
  });
});
