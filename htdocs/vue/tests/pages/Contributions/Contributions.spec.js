import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import Contributions from "@/pages/Contributions/Contributions.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

// Fix for $refs.contentContainer.scrollIntoView() undefined error
Element.prototype.scrollIntoView = vi.fn();

describe("Contributions", () => {
  const defaultProps = {};

  it("renders the base component correctly", () => {
    const wrapper = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "map",
      },
      global,
    });

    expect(wrapper.classes()).toContain("Contributions");
  });

  it("handles click on info button correctly", async () => {
    const wrapper = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "map",
      },
      global,
    });

    // Modal not present
    expect(wrapper.find("h2.modalTitle").exists()).toBe(false);

    // Click button to open modal
    await wrapper.find("div.headerInfoContainer div.titleRow div.DipasInfoButton button").trigger("click");

    // Modal is present
    expect(wrapper.find("div.modalContent").text()).contains("pages.Contributions.infoButton.content");
  });

  it("does not show new contribution button, if new contributions aren't allowed (default)", async () => {
    const wrapper = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "map",
      },
      global,
    });

    const newContributionButton = wrapper.find("div.infoContainer button[title='pages.Contributions.buttonCreateContribution']");

    expect(newContributionButton.exists()).toBe(false);
  });

  it("shows new contribution button, if new contributions are allowed, and opens modal on click", async () => {
    const wrapper = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "map",
      },
      computed: {
        allowNewContributions () {
          return true;
        },
        proceedingTitle () {
          return "Title";
        },
        proceedingActive () {
          return true;
        },
        contributionsPageSubTitle () {
          return "SubTitle";
        },
      },
      global,
    });

    const newContributionButton = wrapper.find("div.infoContainer button[title='pages.Contributions.buttonCreateContribution']");

    expect(newContributionButton.exists()).toBe(true);

    // Modal not present
    expect(wrapper.find("div.modalContent").exists()).toBe(false);

    // Click button to open modal
    await newContributionButton.trigger("click");

    // Modal present
    expect(wrapper.find("div.modalContent").exists()).toBe(true);
  });

  it("shows the selected map and list tab correctly", async () => {
    const wrapperMap = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "map"
      },
      global,
    });

    await wrapperMap.vm.$nextTick();

    expect(wrapperMap.find("section.ContributionsMap").exists()).toBe(true);
    expect(wrapperMap.find("section.ContributionsList").exists()).toBe(false);

    const wrapperList = mount(Contributions, {
      props: {
        ...defaultProps,
        selectedTab: "list"
      },
      global,
    });

    await wrapperList.vm.$nextTick();

    expect(wrapperList.find("section.ContributionsMap").exists()).toBe(false);
    expect(wrapperList.find("section.ContributionsList").exists()).toBe(true);
  });
});
