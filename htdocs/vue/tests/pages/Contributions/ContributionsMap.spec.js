import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import ContributionsMap from "@/pages/Contributions/components/ContributionsMap.vue";
import DipasMasterportal from "@/components/DipasMasterportal.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("ContributionsMap", () => {
  const defaultProps = {};

  it("renders the base component correctly", () => {
    const wrapper = mount(ContributionsMap, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.classes()).toContain("ContributionsMap");
    expect(wrapper.findComponent(DipasMasterportal).exists()).toBe(true);
  });

  it("contains a DipasMasterportal component", () => {
    const wrapper = mount(ContributionsMap, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(DipasMasterportal).exists()).toBe(true);
  });

  it("creates the correct masterportal url", () => {
    const wrapper = mount(ContributionsMap, {
      props: {
        ...defaultProps,
        mapVisitorSettings: null,
      },
      global,
    });

    const masterportalSrc = wrapper.find("iframe.masterportalIframe").attributes("src");

    expect(masterportalSrc).contains("//masterportal.url?");
    expect(masterportalSrc).contains("postMessageUrl=http://localhost:3000");

    expect(wrapper.find("iframe.masterportalIframe").attributes("src")).toBe(masterportalSrc);
  });

  it("creates the correct masterportal url, if a visitor used the GFI more link and returns to the map.", () => {
    const wrapper = mount(ContributionsMap, {
      props: {
        ...defaultProps,
        mapVisitorSettings: {
          "Map/layerIds": "19969,contributions,projectarea",
          "visibility": "true,true,true",
          "transparency": "0,0,0",
          "Map/center": "[570064.8560506537,5934952.061677279]",
          "Map/zoomLevel": "9"
        }
      },
      global,
    });

    const masterportalSrc = wrapper.find("iframe.masterportalIframe").attributes("src");

    expect(masterportalSrc).contains("//masterportal.url?");
    expect(masterportalSrc).contains("astToPoint=true");
    expect(masterportalSrc).contains("Map/layerIds=19969,contributions,projectarea");
    expect(masterportalSrc).contains("visibility=true,true,true");
    expect(masterportalSrc).contains("transparency=0,0,0");
    expect(masterportalSrc).contains("Map/center=570064.8560506537,5934952.061677279");
    expect(masterportalSrc).contains("Map/zoomLevel=9");
    expect(masterportalSrc).contains("postMessageUrl=http://localhost:3000");

    expect(wrapper.find("iframe.masterportalIframe").attributes("src")).toBe(masterportalSrc);
  });
});
