import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import ContributionDetail from "@/pages/ContributionDetail/ContributionDetail.vue";
import CommentsList from "@/pages/components/Comments/CommentsList.vue";
import ContentTitleSection from "@/pages/components/ContentTitleSection.vue";
import DipasButton from "@/components/DipasButton.vue";
import Rating from "@/pages/ContributionDetail/components/Rating.vue";
import DipasCollapsible from "@/components/DipasCollapsible.vue";
import DipasCollapsibleItem from "@/components/DipasCollapsibleItem.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("ContributionDetail.vue", () => {
  const defaultProps = {
    allowNewComments: true,
  };

  it("renders contribution details correctly", () => {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234",
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },

      global,
    });

    expect(wrapper.classes()).toContain("ContributionDetail");
    expect(wrapper.findComponent(DipasButton).exists()).toBe(true);
  });

  it("renders CommentsList component correctly", () => {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234"
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(CommentsList).exists()).toBe(true);
  });

  it("renders ContentTitleSection correctly", () => {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234"
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(ContentTitleSection).exists()).toBe(true);
  });

  it("renders Rating component correctly", () => {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234"
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(Rating).exists()).toBe(true);
  });

  it("renders DipasCollapsible correctly", ()=> {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234"
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(DipasCollapsible).exists()).toBe(true);
  });

  it("renders DipasCollapsibleItem correctly", ()=> {
    global.mocks = Object.assign({}, global.mocks, {
      $route: {
        params: {
          id: "1234"
        }
      }
    });

    const wrapper = mount(ContributionDetail, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.findComponent(DipasCollapsibleItem).exists()).toBe(true);
  });

  it("creates the correct link back to map or list, based on given parameters.", async () => {
    let wrapper;

    // If the query contains the Map/center parameter, the component contains a "back to map" link.
    global.mocks = {
      ...global.mocks,
      $route: {
        name: "route_Contribution",
        query: {
          "Map/center": "[570064.8560506537,5934952.061677279]",
        },
        params: {
          id: "1234"
        }
      },
    };

    wrapper = mount(ContributionDetail, {global});

    expect(wrapper.find("button span.dps-button__text").text()).toBe("pages.ContributionDetails.linkBackMap");

    // Otherwise, the component contains a "back to list" link.
    global.mocks = {
      ...global.mocks,
      $route: {
        name: "route_Contribution",
        params: {
          id: "1234"
        }
      },
    };

    wrapper = mount(ContributionDetail, {global});

    expect(wrapper.find("button span.dps-button__text").text()).toBe("pages.ContributionDetails.linkBackList");
  });
});
