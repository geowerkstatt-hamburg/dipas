import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import ScheduleTable from "@/pages/Schedule/components/ScheduleTable.vue";
import {global} from "@/../tests/global.inc";

describe("ScheduleTable.vue", () => {
  const appointments = [
    {
      "title": "Testtermin 1",
      "topic": "Lorem ipsum dolor sit amet",
      "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
      "start": {date: "01.01.2090"},
      "organizer": "Lorem ipsum Hamburg e.V.",
      "street1": "Lorem ipsum-Straße 1",
      "street2": "",
      "zip": "12345",
      "city": "Hamburg",
      "lon": 10.003983200854165,
      "lat": 53.56253525285798
    },
    {
      "title": "Testtermin 2",
      "topic": "Lorem ipsum dolor sit amet",
      "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
      "start": {date: "01.01.2091"},
      "organizer": "Lorem ipsum Hamburg e.V.",
      "street1": "Lorem ipsum-Straße 1",
      "street2": "",
      "zip": "12345",
      "city": "Hamburg",
      "lon": 10.003983200854165,
      "lat": 53.56253525285798
    },
    {
      "title": "Testtermin 3",
      "topic": "Lorem ipsum dolor sit amet",
      "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
      "start": {date: "01.01.2091"},
      "organizer": "Lorem ipsum Hamburg e.V.",
      "street1": "Lorem ipsum-Straße 1",
      "street2": "",
      "zip": "12345",
      "city": "Hamburg",
      "lon": 10.003983200854165,
      "lat": 53.56253525285798
    }
  ];

  it("Filled appointment table rendering", () => {
    const wrapper = mount(
      ScheduleTable,
      {
        global,
        props: {
          appointments: appointments
        }
      }
    );

    expect(wrapper.classes()).toContain("ScheduleTable");
    expect(wrapper.find("div.ScheduleTimeline").exists()).toBe(true);
    expect(wrapper.findAll("div.ScheduleAppointment").length).toBe(3);
    wrapper.findAll("div.ScheduleAppointment").forEach((appointment, index) => {
      expect(appointment.find("p.date").text()).toEqual(appointments[index].start.date);
    });
  });

  it("Empty appointment table (running and upcoming appointments)", () => {
    const wrapper = mount(
      ScheduleTable,
      {
        global,
        props: {
          appointments: []
        }
      }
    );

    expect(wrapper.findAll("div.ScheduleAppointment").length).toBe(0);
    expect(wrapper.find("div.noAppointments").exists()).toBe(true);
    expect(wrapper.find("div.noAppointments").text()).toEqual("pages.Schedule.noAppointmentsAvailable.currentAndUpcoming");
  });

  it("Empty appointment table (past appointments)", () => {
    const wrapper = mount(
      ScheduleTable,
      {
        global,
        props: {
          appointments: [],
          pastAppointments: true
        }
      }
    );

    expect(wrapper.find("div.noAppointments").text()).toEqual("pages.Schedule.noAppointmentsAvailable.past");
  });
});
