import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import ScheduleAppointment from "@/pages/Schedule/components/ScheduleAppointment.vue";
import {global} from "@/../tests/global.inc";

describe("ScheduleAppointment.vue", () => {
  const appointmentOnlyStart = {
      "title": "Testtermin 1",
      "topic": "Lorem ipsum dolor sit amet",
      "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
      "start": {date: "01.01.2023"},
      "organizer": "Lorem ipsum Hamburg e.V.",
      "street1": "Lorem ipsum-Straße 1",
      "street2": "",
      "zip": "12345",
      "city": "Hamburg",
      "lon": 10.003983200854165,
      "lat": 53.56253525285798
    },
    appointmentStartAndEnd = {
      "title": "Testtermin 1",
      "topic": "Lorem ipsum dolor sit amet",
      "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
      "start": {date: "01.01.2023"},
      "end": {date: "31.12.2023"},
      "organizer": "Lorem ipsum Hamburg e.V.",
      "street1": "Lorem ipsum-Straße 1",
      "street2": "",
      "zip": "12345",
      "city": "Hamburg",
      "lon": 10.003983200854165,
      "lat": 53.56253525285798
    };

  it("Single day appointment rendering (active)", () => {
    const wrapper = mount(
      ScheduleAppointment,
      {
        global,
        props: {
          appointment: appointmentOnlyStart,
          active: true
        }
      }
    );

    expect(wrapper.classes()).toContain("ScheduleAppointment");

    expect(wrapper.find("div.appointmentdetails").exists()).toBe(true);
    wrapper.findAll("div.appointmentdetails").forEach(appointmentdetails => {
      expect(appointmentdetails.find("p.date").exists()).toBe(true);
      expect(appointmentdetails.find("p.date").text()).toEqual(appointmentOnlyStart.start.date);
      expect(appointmentdetails.find("p.time").exists()).toBe(true);
      expect(appointmentdetails.find("p.time").text()).toEqual("datetime.time");
      expect(appointmentdetails.find("p.address").exists()).toBe(true);
      expect(appointmentdetails.find("p.address").text()).toEqual(
        appointmentOnlyStart.organizer
        + ",  "
        + appointmentOnlyStart.street1
        + ",  "
        + appointmentOnlyStart.zip
        + " "
        + appointmentOnlyStart.city
      );
      expect(appointmentdetails.find("p.address").attributes().tabindex).toEqual(undefined);
    });

    expect(wrapper.find("div.DipasCollapsibleItem").exists()).toBe(true);
    wrapper.findAll("div.DipasCollapsibleItem").forEach(collapsible => {
      expect(collapsible.find("div.bodyContainer").exists()).toBe(true);

      collapsible.findAll("div.bodyContainer").forEach(collapsibleItem => {
        expect(collapsibleItem.find("div.body").text()).toEqual(appointmentOnlyStart.description);
      });
    });
  });

  it("Single day appointment rendering (active) on mobile", () => {
    global.mocks = Object.assign({}, global.mocks, {
      isMobile: true,
      isDesktop: false
    });

    const wrapper = mount(
      ScheduleAppointment,
      {
        global,
        props: {
          appointment: appointmentOnlyStart,
          active: true
        }
      }
    );

    wrapper.findAll("div.appointmentdetails").forEach(appointmentdetails => {
      expect(appointmentdetails.find("p.date").exists()).toBe(true);
      expect(appointmentdetails.find("p.date").text()).toEqual(appointmentOnlyStart.start.date);
      expect(appointmentdetails.find("p.time").exists()).toBe(true);
      expect(appointmentdetails.find("p.time").text()).toEqual("datetime.time");
      expect(appointmentdetails.find("p.address").exists()).toBe(true);
      expect(appointmentdetails.find("p.address").text()).toEqual(
        appointmentOnlyStart.organizer
        + ",  "
        + appointmentOnlyStart.street1
        + ",  "
        + appointmentOnlyStart.zip
        + " "
        + appointmentOnlyStart.city
      );
      expect(appointmentdetails.find("p.address").attributes().tabindex).toEqual("0");
    });

  });

  it("Single day appointment rendering (not active)", () => {
    const wrapper = mount(
      ScheduleAppointment,
      {
        global,
        props: {
          appointment: appointmentOnlyStart,
          active: false
        }
      }
    );

    expect(wrapper.find("div.DipasCollapsibleItem").exists()).toBe(true);
    wrapper.findAll("div.DipasCollapsibleItem").forEach(collapsible => {
      expect(collapsible.find("div.bodyContainer").exists()).toBe(true);

      collapsible.findAll("div.bodyContainer").forEach(collapsibleItem => {
        expect(collapsibleItem.find("div.body").exists()).toBe(false);
      });
    });
  });

  it("Multiday appointment rendering", () => {
    const wrapper = mount(
      ScheduleAppointment,
      {
        global,
        props: {
          appointment: appointmentStartAndEnd,
          active: true
        }
      }
    );

    wrapper.findAll("div.appointmentdetails").forEach(appointmentdetails => {
      expect(appointmentdetails.find("p.date").text()).toEqual(appointmentStartAndEnd.start.date + "  - " + appointmentStartAndEnd.end.date);
      expect(appointmentdetails.find("p.time").text()).toEqual("datetime.timespan");
    });
  });
});
