import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import ScheduleTimeline from "@/pages/Schedule/components/ScheduleTimeline.vue";
import {global} from "@/../tests/global.inc";

describe("ScheduleTimeline.vue", () => {
  const defaultProps = {
    dates: [
      {
        date: {date: "1.1.2023"},
        yPos: 0,
        active: true
      },
      {
        date: {date: "2.1.2023"},
        yPos: 100,
        active: false
      },
      {
        date: {date: "3.1.2023"},
        yPos: 200,
        active: false
      }
    ]
  };

  it("Default rendering", () => {
    const wrapper = mount(
      ScheduleTimeline,
      {
        global,
        props: defaultProps
      }
    );

    expect(wrapper.classes()).toContain("ScheduleTimeline");

    expect(wrapper.findAll("div.date").length).toBe(3);
    expect(wrapper.find("div.timeline").exists()).toBe(true);

    wrapper.findAll("div.date").forEach((datePoint, dateIndex) => {
      if (defaultProps.dates[dateIndex].active) {
        expect(datePoint.classes()).toContain("active");
      }
      else {
        expect(datePoint.classes()).not.toContain("active");
      }

      datePoint.findAll("a.dot").forEach((link) => {
        if (defaultProps.dates[dateIndex].active) {
          expect(link.classes()).toContain("active");
          expect(link.attributes().tabindex).toEqual(undefined);
        }
        else {
          expect(link.classes()).not.toContain("active");
          expect(link.attributes().tabindex).toEqual("0");
        }

        expect(link.attributes()["aria-label"]).toEqual(defaultProps.dates[dateIndex].date.date);
      });

      datePoint.findAll("p.date").forEach((date) => {
        expect(date.find("span.day").exists()).toBe(true);
        expect(date.find("span.day").text()).toEqual(defaultProps.dates[dateIndex].date.date);

        expect(date.find("span.month").exists()).toBe(true);
        expect(date.find("span.month").text()).toEqual("datetime.monthnames.3digit." + defaultProps.dates[dateIndex].date.date);

        expect(date.find("span.year").exists()).toBe(true);
        expect(date.find("span.year").text()).toEqual(defaultProps.dates[dateIndex].date.date);
      });
    });
  });

  it("Empty schedule rendering", () => {
    const wrapper = mount(
      ScheduleTimeline,
      {
        global,
        props: {dates: []}
      }
    );

    expect(wrapper.findAll("div.date").length).toBe(0);
    expect(wrapper.find("div.timeline").exists()).toBe(false);
  });
});
