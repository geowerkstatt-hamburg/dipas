import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import CommentForm from "@/pages/components/Comments/CommentForm.vue";
import DipasTextarea from "@/components/DipasTextarea.vue";
import DipasButton from "@/components/DipasButton.vue";
import DipasCard from "@/components/DipasCard.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("CommentForm.vue", () => {
  const defaultProps = {
    title: "my comment",
    commentEntityType: "node",
    rootEntityId: "1234",
    commentParentId: "123",
    allowNewComments: true,
  };

  it("renders CommentForm correctly", () => {
    const wrapper = mount(CommentForm, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.classes()).toContain("CommentForm");
    expect(wrapper.findComponent(DipasTextarea).exists()).toBe(true);
    expect(wrapper.findComponent(DipasButton).exists()).toBe(true);
    expect(wrapper.findComponent(DipasCard).exists()).toBe(true);
  });
});
