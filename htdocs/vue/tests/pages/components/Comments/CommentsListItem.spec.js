import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import CommentsListItem from "@/pages/components/Comments/CommentsListItem.vue";
import DipasButton from "@/components/DipasButton.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("CommentsListItem.vue", () => {
  const defaultProps = {
    item: {
      "parentCid": null,
      "cid": "23378",
      "subject": "",
      "comment": "testdsaf",
      "created": "2023-12-13T10:43:15Z",
      "visible": true
    },
    rootEntityId: "",
    allowNewComments: true
  };

  it("renders CommentsListItem correctly", () => {
    const wrapper = mount(CommentsListItem, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.classes()).toContain("CommentsListItem");
    expect(wrapper.findComponent(DipasButton).exists()).toBe(true);
  });
});
