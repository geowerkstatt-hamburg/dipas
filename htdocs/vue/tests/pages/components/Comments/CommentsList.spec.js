import {describe, it, expect, vi} from "vitest";
import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import CommentsList from "@/pages/components/Comments/CommentsList.vue";
import CommentsListItem from "@/pages/components/Comments/CommentsListItem.vue";
import CommentForm from "@/pages/components/Comments/CommentForm.vue";
import DipasCollapsible from "@/components/DipasCollapsible.vue";
import DipasCollapsibleItem from "@/components/DipasCollapsibleItem.vue";
import DipasButton from "@/components/DipasButton.vue";
import DipasCard from "@/components/DipasCard.vue";

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("CommentsList.vue", () => {
  const defaultProps = {
    rootEntityId: "1234",
    allowNewComments: true,
  };

  it("renders CommentsList correctly", () => {
    const wrapper = mount(CommentsList, {
      props: {
        ...defaultProps
      },
      global,
    });

    expect(wrapper.classes()).toContain("CommentsList");
    expect(wrapper.findComponent(CommentsListItem).exists()).toBe(true);
    expect(wrapper.findComponent(CommentForm).exists()).toBe(true);
    expect(wrapper.findComponent(DipasCollapsible).exists()).toBe(true);
    expect(wrapper.findComponent(DipasCollapsibleItem).exists()).toBe(true);
    expect(wrapper.findComponent(DipasButton).exists()).toBe(true);
    expect(wrapper.findComponent(DipasCard).exists()).toBe(true);
  });
});
