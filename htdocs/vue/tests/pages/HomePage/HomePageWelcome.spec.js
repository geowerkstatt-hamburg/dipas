import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import HomePageWelcome from "@/pages/HomePage/components/HomePageWelcome.vue";
import {global} from "@/../tests/global.inc";

describe("HomePageWelcome.vue", () => {

  it("renders infocontainer", () => {
    const wrapper = mount(HomePageWelcome, {
      global,
    });

    const infoContainer = wrapper.find(".infoContainer");
    const welcomeButtonContainer = infoContainer.find(".welcomeButtonContainer");

    expect(infoContainer.exists()).toBe(true);
    expect(infoContainer.find("h1").exists()).toBe(true);
    expect(welcomeButtonContainer.exists()).toBe(true);
  });
});
