import {describe, it, expect, vi} from "vitest";

import {mount} from "@vue/test-utils";
import {global} from "@/../tests/global.inc";
import ContentPage from "@/pages/ContentPage/ContentPage.vue";


Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

describe("ContentPage", () => {
  it("Default rendering and contain ContentPageParagraphText", () => {
    const defaultProps = {
        endpoint: "custompage",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "text",
              "field_headline": "Liebe Schulungsteilnehmer*innen,",
              "field_text": "<p>damit Sie sich im Vorfeld mit den Funktionen der Onlinebeteiligungsseite vertraut machen können, haben wir eine kleine Übung für Sie erstellt: wir möchten Sie bitten, Ihre Erwartungen an die DIPAS-Schulung auf der&nbsp;<strong><a href=\"https://schulung.beteiligung.hamburg/dipas/#/contributionmap\">Beitragskarte</a></strong>&nbsp;zu verorten. Beiträge der anderen Teilnehmer können Sie sich in der&nbsp;<strong><a href=\"https://schulung.beteiligung.hamburg/dipas/#/contributionlist\">Beitragsliste</a></strong>&nbsp;anschauen. Diese können Sie auch&nbsp;<strong>bewerten</strong>&nbsp;und&nbsp;<strong>kommentieren</strong>.</p>\r\n\r\n<p>Herzlichen Dank!</p>\r\n\r\n<p>Ihr Stadtwerkstatt-Team</p>"
            }
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.classes()).toContain("ContentPage");
    expect(wrapper.findComponent({name: "ContentPageParagraphText"}).exists()).toBe(true);
    expect(wrapper.find("h1").exists()).toBe(true);
    expect(wrapper.find("h2").exists()).toBe(true);
  });

  it("Render ContentPageParagraphImage", () => {
    const defaultProps = {
        endpoint: "custompage",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "image",
              "field_image": {
                "type": "media",
                "bundle": "image",
                "field_caption": "Paragraph Image",
                "field_media_image": {
                  "url": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=VNgVIM7U",
                  "origin": "//schulung.localhost:8080/drupal/sites/default/files/2023-08/alsterarkaden-hamburg-imago.jpg",
                  "srcset": {
                    "1600": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=VNgVIM7U",
                    "1200": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_lg/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=JS5sbu45",
                    "1000": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_md/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=1VmB1A2j",
                    "800": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_sm/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=6Nsh_RCf",
                    "500": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xs/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=lYfqn3Zh"
                  },
                  "alt": "Arkaden"
                }
              }
            }
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.findComponent({name: "ContentPageParagraphImage"}).exists()).toBe(true);
    expect(wrapper.find("h1").exists()).toBe(true);
    expect(wrapper.find("h2").exists()).toBe(false);
    expect(wrapper.find("img").exists()).toBe(true);
    expect(wrapper.find("p").exists()).toBe(false);
  });

  it("Render ContentPageParagraphImageWithText", () => {
    const defaultProps = {
        endpoint: "custompage",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "image_with_text",
              "field_image": {
                "type": "media",
                "bundle": "image",
                "field_caption": "Paragraph Image with Text",
                "field_media_image": {
                  "url": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=VNgVIM7U",
                  "origin": "//schulung.localhost:8080/drupal/sites/default/files/2023-08/alsterarkaden-hamburg-imago.jpg",
                  "srcset": {
                    "1600": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=VNgVIM7U",
                    "1200": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_lg/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=JS5sbu45",
                    "1000": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_md/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=1VmB1A2j",
                    "800": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_sm/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=6Nsh_RCf",
                    "500": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xs/public/2023-08/alsterarkaden-hamburg-imago.jpg?itok=lYfqn3Zh"
                  },
                  "alt": "Arkaden"
                }
              },
              "field_image_position": {
                "value": "image_right"
              },
              "field_text": "<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum alias odio quaerat quos accusamus non laborum distinctio ad quisquam quo similique, adipisci sint enim, est, repellendus dolor praesentium. Laboriosam id delectus doloremque eos quaerat atque vitae vel, nesciunt, iusto a ut! Sit dicta ipsa mollitia nulla quam nobis provident nemo.  </p>"
            },
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.findComponent({name: "ContentPageParagraphImageWithText"}).exists()).toBe(true);
    expect(wrapper.find("h1").exists()).toBe(true);
    expect(wrapper.find("h2").exists()).toBe(false);
    expect(wrapper.find("img").exists()).toBe(true);
    expect(wrapper.find("p").exists()).toBe(true);
  });

  it("Render ContentPageParagraphVideo", () => {
    const defaultProps = {
        endpoint: "custompage",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "video",
              "field_video": "<iframe width=\"854\" height=\"480\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\" src=\"//www.youtube.com/embed/eav7Yw0IoMw?autoplay=0&amp;start=0&amp;rel=0\"/>"
            }
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.findComponent({name: "ContentPageParagraphVideo"}).exists()).toBe(true);
    expect(wrapper.find("h1").exists()).toBe(true);
    expect(wrapper.find("h2").exists()).toBe(false);
    expect(wrapper.find("iframe").exists()).toBe(true);
    expect(wrapper.find("p").exists()).toBe(false);
  });

  it("Render ContentPageParagraphAccordeon", () => {
    const defaultProps = {
        endpoint: "custompage",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "accordeon",
              "field_content": [
                {
                  "type": "paragraph",
                  "bundle": "accordeon_item",
                  "field_headline": "Worum geht es?",
                  "field_text": "<p>Hier finden Sie alle Informationen über das Planungsverfahren und die Möglichkeiten, sich zu beteiligen! Dazu gehören die Zuständigkeiten, die Ziele der Planung und der Spielraum für die Mitwirkung der Öffentlichkeit, ebenso wie die Laufzeit.</p>"
                },
                {
                  "type": "paragraph",
                  "bundle": "accordeon_item",
                  "field_headline": "Was gilt es zu beachten?",
                  "field_text": "<p>Hier finden Sie alle Informationen über das Planungsverfahren und die Möglichkeiten, sich zu beteiligen! Dazu gehören die Zuständigkeiten, die Ziele der Planung und der Spielraum für die Mitwirkung der Öffentlichkeit, ebenso wie die Laufzeit.</p>"
                },
                {
                  "type": "paragraph",
                  "bundle": "accordeon_item",
                  "field_headline": "Wer ist Zuständig?",
                  "field_text": "<p>Hier finden Sie alle Informationen über das Planungsverfahren und die Möglichkeiten, sich zu beteiligen! Dazu gehören die Zuständigkeiten, die Ziele der Planung und der Spielraum für die Mitwirkung der Öffentlichkeit, ebenso wie die Laufzeit.</p>"
                },
                {
                  "type": "paragraph",
                  "bundle": "accordeon_item",
                  "field_headline": "Vandkunsten",
                  "field_text": "<h1>Horner Geest 2030 I grün und lebendig</h1>\r\n\r\n<p>Die übergeordnete Idee für die Horner Geest ist das Schaffen einer klaren Landschafts- und Stadtraumhierarchie, die die Horner Geest zu einem einzigartigem, grünen und weiterhin bezahlbarem Stadtteil werden lässt.</p>\r\n\r\n<ul>\r\n\t<li>Hamburgs blau-grüner Stadtteil / der grüne Ring<br />\r\n\tDie Horner Geest wird in der Zukunft von einem blau-grünen Ring geprägt, der bereits vorhandene und neue Freiflächen zusammenbindet, mit Leben füllen wird, aber auch eine große Bedeutung für nachhaltigkeitsbezogene Funktionen, wie z.B. den naturnahen Umgang mit Regenwasser haben wird.</li>\r\n\t<li>Zentrum Horner Geest<br />\r\n\tDie Manshardtstraße wird als neue wichtige Stadtteilstraße ausgebildet. Der Stadtteilplatz mit Geschäften, Restaurants und Wohnungen wir komplett neu aufgebaut.</li>\r\n\t<li>Erhalten und Anknüpfen an den Bestand<br />\r\n\tNeue Wohnungen werden als An- oder Neubauten mit Respekt für den Gebäude- und Baumbestand errichtet, der weitestgehend erhalten wird.<br />\r\n\tDie Nachverdichtung wird neuen Wohnraum für großen Vielfalt an neuen Bewohner erzeugen: Familien, Alleinerziehende, Senioren, Wohn- und Baugemeinschaften.</li>\r\n</ul>\r\n\r\n<p><img alt=\"Entwurf Vandkunsten\" data-entity-type=\"\" data-entity-uuid=\"\" src=\"https://horner-geest-2030.beteiligung.hamburg/drupal/sites/default/files/2021-03/vandkunsten-abschluss-01.png\" /></p>"
                }
              ]
            }
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.findComponent({name: "ContentPageParagraphAccordeon"}).exists()).toBe(true);
  });

  it("Render ContentPageParagraphDivisionInPlanningSubareas", () => {
    const defaultProps = {
        endpoint: "conceptionlist",
        content: {
          "title": "Paragraph Text: Herzlich Willkommen zur  Schulung \"Onlinebeteiligung mit DIPAS\"",
          "content": [
            {
              "type": "paragraph",
              "bundle": "division_in_planning_subareas",
              "field_content": [
                {
                  "type": "paragraph",
                  "bundle": "planning_subarea",
                  "field_content": [
                    {
                      "type": "paragraph",
                      "bundle": "conception",
                      "field_conception": {
                        "type": "node",
                        "bundle": "conception",
                        "nid": "10703",
                        "title": "Entwurf B",
                        "field_conception_media_image": {
                          "type": "media",
                          "bundle": "image",
                          "field_caption": "Harmony Hills",
                          "field_copyright": "Christian Lamine",
                          "field_media_image": {
                            "url": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2024-01/harmony_hills_0.jpg?itok=_eLPq0LX",
                            "origin": "//schulung.localhost:8080/drupal/sites/default/files/2024-01/harmony_hills_0.jpg",
                            "srcset": {
                              "1600": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2024-01/harmony_hills_0.jpg?itok=_eLPq0LX",
                              "1200": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_lg/public/2024-01/harmony_hills_0.jpg?itok=aPXmW0aU",
                              "1000": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_md/public/2024-01/harmony_hills_0.jpg?itok=UDf1e-LY",
                              "800": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_sm/public/2024-01/harmony_hills_0.jpg?itok=7xHNvqRo",
                              "500": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xs/public/2024-01/harmony_hills_0.jpg?itok=hPcSSluG"
                            },
                            "alt": "Harmony Hills"
                          }
                        }
                      }
                    },
                    {
                      "type": "paragraph",
                      "bundle": "conception",
                      "field_conception": {
                        "type": "node",
                        "bundle": "conception",
                        "nid": "14113",
                        "title": "Entwurf 2b",
                        "field_conception_media_image": {
                          "type": "media",
                          "bundle": "image",
                          "field_caption": "Köhlbrandbrücke",
                          "field_copyright": "Christian Lamine",
                          "field_media_image": {
                            "url": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2024-01/koehlbrandbruecke.jpg?itok=-z_Wlo-x",
                            "origin": "//schulung.localhost:8080/drupal/sites/default/files/2024-01/koehlbrandbruecke.jpg",
                            "srcset": {
                              "1600": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xl/public/2024-01/koehlbrandbruecke.jpg?itok=-z_Wlo-x",
                              "1200": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_lg/public/2024-01/koehlbrandbruecke.jpg?itok=2YQpj26s",
                              "1000": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_md/public/2024-01/koehlbrandbruecke.jpg?itok=hlTu6pCx",
                              "800": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_sm/public/2024-01/koehlbrandbruecke.jpg?itok=y_Lj4H10",
                              "500": "//schulung.localhost:8080/drupal/sites/default/files/styles/content_image_xs/public/2024-01/koehlbrandbruecke.jpg?itok=tk10uwbY"
                            },
                            "alt": "Köhlbrandbrücke"
                          }
                        }
                      }
                    }
                  ],
                  "field_name": "Entwurf 2"
                }
              ]
            }
          ],
          "image_gallery": []
        }
      },
      wrapper = mount(
        ContentPage,
        {
          props: defaultProps,
          global
        }
      );

    expect(wrapper.findComponent({name: "ContentPageParagraphDivisionInPlanningSubareas"}).exists()).toBe(true);
    expect(wrapper.findComponent({name: "ContentPageParagraphPlanningSubarea"}).exists()).toBe(true);
    expect(wrapper.findComponent({name: "ContentPageParagraphConception"}).exists()).toBe(true);

    const componentsContentPageParagraphPlanningSubarea = wrapper.findAllComponents({name: "ContentPageParagraphPlanningSubarea"});

    expect(componentsContentPageParagraphPlanningSubarea.length).toBe(1);

    const componentsContentPageParagraphConception = wrapper.findAllComponents({name: "ContentPageParagraphConception"});

    expect(componentsContentPageParagraphConception.length).toBe(2);
  });
});
