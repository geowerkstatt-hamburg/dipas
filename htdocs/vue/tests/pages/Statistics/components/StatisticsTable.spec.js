import {mount} from "@vue/test-utils";
import {describe, it, expect} from "vitest";
import StatisticsTable from "@/pages/Statistics/components/StatisticsTable.vue";
import {global} from "@/../tests/global.inc";

describe("StatisticsTable", () => {
  const tableData = [
      {
        "data": 12,
        "color": "#703980",
        "label": "Frage"
      },
      {
        "data": 10,
        "color": "#003063",
        "label": "Idee"
      },
      {
        "data": 2,
        "color": "#FB624F",
        "label": "Kritik"
      },
      {
        "data": 2,
        "color": "#C63C76",
        "label": "Sonstiges"
      }
    ],
    defaultProps = {
      tableData: tableData
    };

  it("renders the table correctly without table header", () => {
    const wrapper = mount(StatisticsTable, {
      props: {
        ...defaultProps,
      },
      global,
    });

    expect(wrapper.classes()).toContain("StatisticsTable");

    expect(wrapper.find("thead").exists()).toBe(false);
    expect(wrapper.findAll("th").length).toBe(0);
    expect(wrapper.findAll("tr").length).toBe(4);

    wrapper.findAll("tr").forEach((content, index) => {
      expect(content.find("td.label").text()).toEqual(tableData[index].label.toString());
    });

    wrapper.findAll("tr").forEach((content, index) => {
      expect(content.find("td.number").text()).toEqual(tableData[index].data.toString());
    });

    expect(wrapper.findAll(".DipasTextChip").length).toBe(0);
  });


  it("renders the table correctly with table header", () => {
    const tableHeader = [
        "Typ",
        "Anzahl"
      ],
      wrapper = mount(StatisticsTable, {
        props: {
          ...defaultProps,
          tableHeader: tableHeader
        },
        global,
      });

    expect(wrapper.find("thead").exists()).toBe(true);
    expect(wrapper.findAll("th").length).toBe(2);

    wrapper.findAll("th").forEach((header, index) => {
      expect(header.text()).toEqual(tableHeader[index]);
    });

    expect(wrapper.findAll("tr").length).toBe(5);
  });


  it("renders the table correctly with text chips", () => {
    const wrapper = mount(StatisticsTable, {
      props: {
        ...defaultProps,
        useChips: true
      },
      global,
    });

    expect(wrapper.findAll(".DipasTextChip").length).toBe(4);

    wrapper.findAll(".DipasTextChip").forEach((content, index) => {
      expect(content.attributes().style).toEqual("--backgroundColor: " + tableData[index].color + "; --borderColor: " + tableData[index].color + ";");
    });
  });

  it("renders the table correctly with icons", () => {
    const wrapper = mount(StatisticsTable, {
      props: {
        ...defaultProps,
        useIcon: true,
        iconToUse: "test_icon"
      },
      global,
    });

    expect(wrapper.findAll("span.icon").length).toBe(4);
    expect(wrapper.find("span.test_icon").exists()).toBe(true);
  });
});
