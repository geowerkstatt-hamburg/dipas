/* eslint-disable no-empty-function */

import DipasModal from "@/components/DipasModal.vue";
import DipasNavigation from "@/components/DipasNavigation.vue";
import {router} from "./router.js";

export const global = {
  mocks: {
    menuOpen: () => false,
    isDesktop: true,
    isTablet: false,
    isMobile: false,
    bannerShown: () => false,
    $t: (translationID) => translationID,
    $dayjs: (date) => {
      return {
        format: () => date.date
      };
    },
    $generateUUID: () => "UUIDString-" + Math.random(),
    $ucFirst: (string, lowerCaseTail = false) => {
      return string.substring(0, 1).toUpperCase() + (!lowerCaseTail ? string.substring(1) : string.substring(1).toLowerCase());
    },
    $snakeCaseToPascalCase: (string, delimiter = "_") => {
      let stringElements = string.split(delimiter);

      for (const index in stringElements) {
        stringElements[index] = stringElements[index].substring(0, 1).toUpperCase() + stringElements[index].substring(1).toLowerCase();
      }

      return stringElements.join("");
    },
    $shuffleArray: (array) => array,
    activeBootstrapBreakpoint: () => "xl",
    $store: {
      dispatch: () => {
      },
      getters: {
        "proceeding/configurationValue": (key) => {
          switch (key) {
            case "contributionWizard":
              return [
                {
                  "id": "description",
                  "mandatory": true
                },
                {
                  "id": "category",
                  "mandatory": true
                },
                {
                  "id": "location",
                  "mandatory": false
                },
                {
                  "id": "type",
                  "mandatory": true
                }
              ];

            default:
              return null;
          }
        },
        "proceeding/masterportalUrl": () => "//masterportal.url",
        "proceeding/navigationLinks": () => [],
        "proceeding/proceedingTitle": "",
        "proceeding/proceedingActive": true,
        "proceeding/contributionsPageSubheadline": "",
        "proceeding/landingPageText": "",
        "proceeding/proceedingPhase": "",
        "proceeding/landingPageImage": {path: "", alttext: ""},
        "proceeding/landingPageButtons": {button_default_behavior: 1},
        "proceeding/taxonomyTerms": () => {
          return [
            {
              "id": "24",
              "name": "Wert1"
            },
            {
              "id": "33",
              "name": "Wert2"
            }
          ];
        },
        "proceeding/taxonomyTermProperty": () => "",
        "proceeding/contributionsCommentsShow": () => true,
        "proceeding/contributionsRatingsShowExisting": true,
        "proceeding/contributionsCommentsMaxLength": "",
        "proceeding/contributionsCommentsOpen": true,
        "contributions/contributionsPage": () => [],
        "contributions/contributionDetail": () => "",
        "contributions/relatedContributions": () => [],
        "contributions/loadContributionDetails": "",
        "interactions/commentsCount": () => "",
        "interactions/comments": () => [
          {
            "parentCid": null,
            "cid": "23378",
            "subject": "",
            "comment": "testdsaf",
            "created": "2023-12-13T10:43:15Z",
            "visible": true
          },
        ],
        "proceeding/contributionGeometryType": [
          "point",
          "linestring",
          "polygon"
        ],
        "interactions/insertedCommentID": "",
      },
      state: {
        navigations: [],
        cookiesAccepted: false,
      },
    }
  },
  components: {
    DipasModal,
    DipasNavigation
  },
  plugins: [router],
};
