import {createRouter, createWebHistory} from "vue-router";

export const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "route_HomePage",
      component: () => import("@/pages/HomePage/HomePage.vue")
    },
    {
      path: "/contributionmap",
      name: "route_Contributions",
      component: () => import("@/pages/Contributions/Contributions.vue")
    },
    {
      path: "/contribution/:id",
      name: "route_ContributionDetail",
      component: () => import("@/pages/ContributionDetail/ContributionDetail.vue")
    },

    {
      path: "/endpoint1",
      name: "route_Endpoint1"
    },
    {
      path: "/endpoint2",
      name: "route_Endpoint2"
    },
    {
      path: "/endpoint3",
      name: "route_Endpoint3"
    },
  ]
});
