import {describe, it, expect} from "vitest";
import {$createSimpleChecksum, $generateObjectChecksum, $serializeObject} from "@/plugins/jsObjectChecksum";

describe("jsObjectChecksum", () => {
  const data = {
    obj1: {
      object: {item1: {subItemB: 2, subItemA: undefined}, item2: [{keyC: 1}, {keyE: 3, keyD: 2}]},
      objectSerialized: "item1:subItemA:undefined|subItemB:2|item2:[\"keyC:1\",\"keyD:2|keyE:3\"]",
      checksum: "P451897847",
    },
    // This object is ordered differently than obj1, but contains the same data! It is equal to obj1
    obj2EqualsObj1: {
      object: {item2: [{keyC: 1}, {keyE: 3, keyD: 2}], item1: {subItemB: 2, subItemA: undefined}},
      objectSerialized: "item1:subItemA:undefined|subItemB:2|item2:[\"keyC:1\",\"keyD:2|keyE:3\"]",
      checksum: "P451897847",
    },
    // This object looks nearly like obj1, but the array is reordered. It is not equal to obj1 anymore!
    obj3IsDifferent: {
      object: {item1: {subItemB: 2, subItemA: undefined}, item2: [{keyE: 3, keyD: 2}, {keyC: 1}]},
      objectSerialized: "item1:subItemA:undefined|subItemB:2|item2:[\"keyD:2|keyE:3\",\"keyC:1\"]",
      checksum: "N1654762325",
    },
  };

  it("serializes object correctly using $serializeObject", () => {
    // Test, if serialized data looks like expected
    expect($serializeObject(data.obj1.object)).toEqual(data.obj2EqualsObj1.objectSerialized);
    expect($serializeObject(data.obj2EqualsObj1.object)).toEqual(data.obj2EqualsObj1.objectSerialized);
    expect($serializeObject(data.obj3IsDifferent.object)).toEqual(data.obj3IsDifferent.objectSerialized);

    // obj1 and obj2EqualsObj1 must have the same objectSerialized
    expect($serializeObject(data.obj1.object)).toEqual($serializeObject(data.obj2EqualsObj1.object));

    // obj1 and obj3IsDifferent are different, and not must have the same objectSerialized
    expect($serializeObject(data.obj1.object)).not.toEqual($serializeObject(data.obj3IsDifferent.object));
    expect($serializeObject(data.obj1.object)).toEqual(data.obj1.objectSerialized);
  });

  it("creates a simple checksum for a given string correctly using $createSimpleChecksum", () => {
    expect($createSimpleChecksum(data.obj1.objectSerialized)).toEqual(data.obj1.checksum);
    expect($createSimpleChecksum(data.obj2EqualsObj1.objectSerialized)).toEqual(data.obj2EqualsObj1.checksum);
    expect($createSimpleChecksum(data.obj3IsDifferent.objectSerialized)).toEqual(data.obj3IsDifferent.checksum);
  });

  it("generates the checksum for a given object correctly using $generateObjectChecksum", () => {
    expect($generateObjectChecksum(data.obj1.object)).toEqual(data.obj1.checksum);
    expect($generateObjectChecksum(data.obj2EqualsObj1.object)).toEqual(data.obj2EqualsObj1.checksum);
    expect($generateObjectChecksum(data.obj3IsDifferent.object)).toEqual(data.obj3IsDifferent.checksum);
  });
});
