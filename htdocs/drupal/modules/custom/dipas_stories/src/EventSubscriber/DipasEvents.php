<?php

namespace Drupal\dipas_stories\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\dipas\Event\DipasRestApiResponseEventInterface;
use Drupal\dipas_stories\DipasStoriesAccessCheckTrait;
use Drupal\dipas_stories\LoadEntityTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DipasEvents implements EventSubscriberInterface {

  use LoadEntityTrait,
    DipasStoriesAccessCheckTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param AccountInterface $current_user
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      \Drupal\dipas\Event\DipasEvents::RestApiResponseAlter => 'onRestApiAlter',
    ];
  }

  public function onRestApiAlter(DipasRestApiResponseEventInterface $event) {
    // Check all endpoints that can potentially include DIAPS stories for actual story access
    if (in_array(
      $event->getResponseKey(),
      [
        'accessibility',
        'conceptiondetails',
        'contributiondetails',
        'custompage',
        'dataprivacy',
        'faq',
        'frontpage',
        'imprint',
        'projectinfo',
      ])
    ) {
      $responseContent = $event->getResponseData();
      $storiesWithoutAccess = [];

      if (
        isset($responseContent['content']) &&
        is_array($responseContent['content'])
      ) {
        foreach ($responseContent['content'] as $index => $paragraph) {
          if ($paragraph['bundle'] === 'dipas_story') {
            if (!$this->checkStoryAccess($paragraph['field_story']['nid'])) {
              $storiesWithoutAccess[] = $index;
            }
          }
        }
      }

      if (count($storiesWithoutAccess)) {
        $storiesWithoutAccess = array_reverse($storiesWithoutAccess);

        foreach ($storiesWithoutAccess as $index) {
          array_splice($responseContent['content'], $index, 1);
        }

        $event->setResponseData($responseContent);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCurrentUser() {
    return $this->currentUser;
  }

}
