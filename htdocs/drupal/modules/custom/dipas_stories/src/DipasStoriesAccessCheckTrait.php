<?php

namespace Drupal\dipas_stories;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;

trait DipasStoriesAccessCheckTrait {

  /**
   * Helper function to check if the current user has access to a story identified by its ID
   *
   * @param int $storyID
   *   The ID of the story requested
   *
   * @return bool
   *
   * @throws InvalidPluginDefinitionException
   * @throws MissingDataException
   * @throws PluginNotFoundException
   */
  protected function checkStoryAccess($storyID) {
    /* @var \Drupal\node\NodeInterface $node */
    $node = $this->getEntity('node', $storyID);

    /* @var int $author */
    $author = (int) $node->get('uid')->first()->getString();

    if (!$node->isPublished()) {
      $userMayViewUnpublishedContent = $this->getCurrentUser()->hasPermission('view any unpublished content') ||
        $author !== $this->getCurrentUser()->id() &&
        $this->getCurrentUser()->hasPermission('view own unpublished content');

      if (!$userMayViewUnpublishedContent) {
        return FALSE;
      }
    }

    return TRUE;
  }

  abstract protected function getEntity($entityTypeID, $entityID);

  /**
   * @return AccountInterface
   */
  abstract protected function getCurrentUser();

}
