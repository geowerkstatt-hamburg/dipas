/**
 * @license GPL-2.0-or-later
 */

(function ($, Drupal, drupalSettings, window) {

  'use strict';

  Drupal.MasterportalConfigSection = {
    markedIDs: [],
    markedFields: [],
    resetMarkings: function () {
      let elem;

      (['markedIDs', 'markedFields']).forEach(property => {
        while (elem = Drupal.MasterportalConfigSection[property].shift()) {
          elem.removeClass('error');
        }
      });
    }
  };

  Drupal.behaviors.MasterportalConfigSection = {
    attach: function (context) {
      $('input[type="submit"][name="op"]').on('click', Drupal.MasterportalConfigSection.resetMarkings);

      $('input, select, textarea').each((index, elem) => {
        elem.addEventListener('invalid', (evt) => {
          let details = $(evt.target).parents().filter('details.masterportalSettingsSection'),
            detailsID = details.attr('id'),
            $tab = $('a[href="#' + detailsID + '"]').parent(),
            $field = $(evt.target);

          Drupal.MasterportalConfigSection.markedFields.push($field);
          Drupal.MasterportalConfigSection.markedIDs.push($tab);

          $field.addClass('error');
          $tab.addClass('error');
          evt.preventDefault();
        });
      });
    }
  };

}(jQuery, Drupal));
