<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\Form;

use Drupal\Core\Form\FormStateInterface;

trait ElementValidateLayerStyleTrait {

  /**
   * Element validate function to verify a given file contains (a) valid layer style definition(s)
   *
   * Method/check can only be run consecutive to element validate method "validateJsonFile"!
   *
   * @param array $element
   * @param FormStateInterface $form_state
   *
   * @return void
   */
  public function validateLayerStyle(array &$element, FormStateInterface $form_state) {
    $persistentContent = drupal_static(sprintf('file-contents-%s', $element['#content_check_hash']));

    if ($element['#valid_json']) {
      $styles = json_decode($persistentContent);

      // Make sure that the content format is an array
      if (!is_array($styles)) {
        $styles = [$styles];
      }

      // Assume all definitions are fine
      $definitionIsValid = TRUE;

      // Check each array element for the correct format
      foreach ($styles as $style) {
        // Check 1: each style definition has to carry a "styleId" property
        if (!isset($style->styleId) || !is_string($style->styleId) || empty($style->styleId)) {
          $definitionIsValid = FALSE;
          break;
        }

        // Check 2: each style definition has to carry a "rules" property
        if (!isset($style->rules) || !is_array($style->rules) || empty($style->rules)) {
          $definitionIsValid = FALSE;
          break;
        }
        else {
          foreach ($style->rules as $rule) {
            // Each rule has to have at least a "style" property
            if (!isset($rule->style)) {
              $definitionIsValid = FALSE;
              break;
            }
          }
        }
      }

      if (!$definitionIsValid) {
        $form_state->setError(
          $element,
          'The given location does not contain (a) valid layer style(s).'
        );
      }
      else {
        $element['#valid_layer_styles'] = TRUE;
      }
    }
  }

}
