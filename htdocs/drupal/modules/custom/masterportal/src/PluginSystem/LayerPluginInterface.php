<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\PluginSystem;

/**
 * Interface LayerPluginInterface.
 *
 * Describes the API for layer plugins.
 *
 * @package Drupal\masterportal\PluginSystem
 */
interface LayerPluginInterface {

  /**
   * Returns the layer definition of the GeoJSON layer provided.
   *
   * @return \stdClass
   *   The layer definition object.
   */
  public function getLayerDefinition();

  /**
   * Returns the actual GeoJSON data of the layer.
   *
   * @param bool $requireAbsoluteURLs
   *   Indicates if URLs within the layer content must be absolute URLs
   *
   * @return \Drupal\masterportal\GeoJSONFeatureInterface[]
   */
  public function getGeoJSONFeatures($requireAbsoluteURLs);

  /**
   * Returns cache tags associated with the layer data.
   *
   * @return array
   */
  public function getCacheTags();

}
