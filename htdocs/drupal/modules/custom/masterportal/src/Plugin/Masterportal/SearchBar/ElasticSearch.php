<?php

namespace Drupal\masterportal\Plugin\Masterportal\SearchBar;

use Drupal\Core\DependencyInjection\Container;
use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\Annotation\SearchBarPlugin;
use Drupal\masterportal\PluginSystem\SearchBarPluginInterface;
use Drupal\masterportal\Plugin\Masterportal\PluginBase;

/**
 * Defines a SearchBar plugin implementation for Elasticsearch address search in GeoPortal.
 * Compatible with Masterportal >= v2.9.0.
 *
 * @SearchBarPlugin(
 *   id = "elasticSearch",
 *   title = @Translation("Elasticsearch (GeoPortal)", context="Masterportal"),
 *   description = @Translation("A search bar plugin to utilize the Elasticsearch service in GeoPortal.", context="Masterportal"),
 *   configProperty = "elasticSearch"
 * )
 */
class ElasticSearch extends PluginBase implements SearchBarPluginInterface {

  private const PAYLOAD = '{"id": "query", "portalId": %%portalId%%, "searchString": "%%searchString%%", '
    . '"maxResultAmount": %%maxResultAmount%%, "searchOffset": %%searchOffset%%}';

  private const HIT_MAP = '{"id": "id", "coordinate": "coordinate", "name": "displayValue"}';

  /**
   * Service ID
   *
   * @var string
   */
  protected $serviceId;

  /**
   * Portal ID
   *
   * @var string
   */
  protected $portalId;

  /**
   * Minimum amount of characters required to start a search.
   *
   * @var int
   */
  protected $minChars;

  /**
   * Payload.
   *
   * @var string
   */
  protected $payload;

  /**
   * Maximum amount of search results to retrieve
   *
   * @var int
   */
  protected $maxResultAmount;

  /**
   * Search offset.
   *
   * @var int
   */
  protected $searchOffset;

  /**
   * Response JSON attribute path to found features.
   *
   * @var string
   */
  protected $responseEntryPath;

  /**
   * Object mapping result object attributes to keys.
   *
   * @var string
   */
  protected $hitMap;

  /**
   * @var \Drupal\masterportal\Service\ServiceManagerInterface
   */
  protected $servicesManager;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(Container $container) {
    $this->servicesManager = $container->get('masterportal.servicesmanager');
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    return [
      'serviceId' => NULL,
      'minChars' => 3,
      'portalId' => NULL,
      'maxResultAmount' => 25,
      'searchOffset' => 3,
      'responseEntryPath' => 'hits.hits',
      'hitMap' => self::HIT_MAP,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $dependantSelector = FALSE, $dependantSelectorProperty = NULL, $dependantSelectorValue = NULL) {
    $states = [
      'required' => [$dependantSelector => [$dependantSelectorProperty => $dependantSelectorValue]],
    ];

    return [
      'serviceId' => [
        '#type' => 'select',
        '#title' => $this->t('Service ID', [], ['context' => 'Masterportal']),
        '#options' => $this->servicesManager->getServiceOptions(),
        '#default_value' => $this->serviceId,
        '#states' => $states,
      ],
      'minChars' => [
        '#type' => 'number',
        '#title' => $this->t('Minimum character count', [], ['context' => 'Masterportal']),
        '#min' => 1,
        '#max' => 10,
        '#step' => 1,
        '#default_value' => $this->minChars,
      ],
      'portalId' => [
        '#type' => 'textfield',
        '#title' => $this->t('Portal ID', [], ['context' => 'Masterportal']),
        '#default_value' => $this->portalId,
        '#states' => $states,
      ],
      'maxResultAmount' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum amount of search results to retrieve', [], ['context' => 'Masterportal']),
        '#min' => 1,
        '#max' => 1000,
        '#step' => 1,
        '#default_value' => $this->maxResultAmount,
      ],
      'searchOffset' => [
        '#type' => 'number',
        '#title' => $this->t('Search offset', [], ['context' => 'Masterportal']),
        '#min' => 1,
        '#max' => 10,
        '#step' => 1,
        '#default_value' => $this->searchOffset,
      ],
      'responseEntryPath' => [
        '#type' => 'textfield',
        '#title' => $this->t('Response JSON attribute path to found features', [], ['context' => 'Masterportal']),
        '#default_value' => $this->responseEntryPath,
        '#states' => $states,
      ],
      'hitMap' => [
        '#type' => 'textfield',
        '#title' => $this->t('Object mapping: result object attributes to keys', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Example JSON value: @json', ['@json' => self::HIT_MAP], ['context' => 'Masterportal']),
        '#default_value' => $this->hitMap,
        '#states' => $states,
        '#element_validate' => [
          [$this, 'validateJsonInput'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationArray(FormStateInterface $form_state) {
    return [
      'serviceId' => (string) $this->serviceId,
      'minChars' => (int) $this->minChars,
      'portalId' => (string) $this->portalId,
      'maxResultAmount' => (int) $this->maxResultAmount,
      'searchOffset' => (int) $this->searchOffset,
      'responseEntryPath' => (string) $this->responseEntryPath,
      'hitMap' => (string) $this->hitMap,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function injectConfiguration(\stdClass &$pluginSection) {
    $pluginSection->serviceId = $this->serviceId;
    $pluginSection->minChars = $this->minChars;
    $pluginSection->payload = json_decode($this->parsePayload());
    $pluginSection->maxResultAmount = $this->maxResultAmount;
    $pluginSection->responseEntryPath = $this->responseEntryPath;
    $pluginSection->hitMap = json_decode($this->hitMap);
    $pluginSection->hitType = $this->t('Address', [], ['context' => 'Masterportal']);
  }

  protected function parsePayload() {
    $payload = self::PAYLOAD;
    $payload = str_replace('%%portalId%%', $this->portalId, $payload);
    $payload = str_replace('%%maxResultAmount%%', $this->maxResultAmount, $payload);
    return str_replace('%%searchOffset%%', $this->searchOffset, $payload);
  }

}
