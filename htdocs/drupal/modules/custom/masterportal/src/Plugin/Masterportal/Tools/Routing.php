<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\Plugin\Masterportal\Tools;

use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\Plugin\Masterportal\PluginBase;
use Drupal\masterportal\PluginSystem\ToolPluginInterface;

/**
 * Defines a tool plugin implementation for Routing.
 *
 * @ToolPlugin(
 *   id = "Routing",
 *   title = @Translation("Routing"),
 *   description = @Translation("A plugin that let's users create route on the map."),
 *   configProperty = "routing",
 *   isAddon = false
 * )
 */
class Routing extends PluginBase implements ToolPluginInterface
{

  /**
   * The label of this plugin in the Masterportal.
   *
   * @var string
   */
  protected $name;

  /**
   * Flag determining if this tool is listed in the Masterportal menu.
   *
   * @var boolean
   */
  protected $visibleInMenu;

  /**
   * Flag determining if this tool is automatically open.
   *
   * @var boolean
   */
  protected $active;

  /**
   * Flag determining if this tool will be opened in window
   *
   * @var boolean
   */
  protected $renderToWindow;

  /**
   * The label of this plugin in the Masterportal.
   *
   * @var string
   */
  protected $geosearch;

  /**
   * The label of this plugin in the Masterportal.
   *
   * @var string
   */
  protected $geosearchReverse;

  /**
   * The label of this plugin in the Masterportal.
   *
   * @var string
   */
  protected $directionsSettings;

  /**
   * Erreichbarkeitsanalyse
   *
   * @var boolean
   */
  protected $isochronesSettings;

  /**
   * flag variable to decide to show isochrones settings
   *
   * @var boolean
   */
  protected $showIsochronesSettings;

  /**
   * Options for routing tool in masterportal
   *
   * @var Array
   */
  protected $routingToolOptions;

  public static function getDefaults()
  {
    return [
      'name' => 'Routing and Reachability Analysis',
      'visibleInMenu' => TRUE,
      'renderToWindow' => FALSE,
      'active' => FALSE,
      'geosearch' => '{"type": "BKG", "serviceId": "5"}',
      'geosearchReverse' => '{"type": "BKG", "serviceId": "5"}',
      'directionsSettings' => '{"type": "ORS", "serviceId": "bkg_ors", "speedProfile": "CAR", "batchProcessing": {"enabled": false, "active": false}}',
      'isochronesSettings' => '{"type": "ORS", "serviceId": "bkg_ors", "speedProfile": "CAR", "batchProcessing": {"enabled": false, "active": false}}',
      'showIsochronesSettings' => TRUE,
      'routingToolOptions' => ['DIRECTIONS', 'ISOCHRONES'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $dependantSelector = FALSE, $dependantSelectorProperty = NULL, $dependantSelectorValue = NULL)
  {

    return [
      'name' => [
        '#type' => 'textfield',
        '#title' => $this->t('Name', [], ['context' => 'Masterportal']),
        '#default_value' => $this->name,
        '#states' => [
          'required' => [
            ['input[name=settings[ToolSettings]\[details_Routing][pluginsettings]]'],
            [$dependantSelector => [$dependantSelectorProperty => $dependantSelectorValue]],
          ]
        ],
      ],
      'visibleInMenu' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Routing-Tool is visible in Masterportal menu', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Uncheck to hide this tool from being listed in the Masterportal menu.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->visibleInMenu,
      ],
      'renderToWindow' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Routing-Tool opens in a seperate window', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Check, to open this tool in a seperate window.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->renderToWindow ?? FALSE,
      ],
      'active' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Routing-Tool opens automatically', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Check, to open Routing-Tool automatically.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->active,
      ],
      'geosearch' => [
        '#type' => 'textfield',
        '#title' => $this->t('Geosearch', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Routing-Tool geosearch', [], ['context' => 'Masterportal']),
        '#default_value' => $this->geosearch,
      ],
      'geosearchReverse' => [
        '#type' => 'textfield',
        '#title' => $this->t('Geosearch Reverse', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Routing-Tool geosearch reverse', [], ['context' => 'Masterportal']),
        '#default_value' => $this->geosearchReverse,
      ],
      'directionsSettings' => [
        '#type' => 'textfield',
        '#title' => $this->t('Directions Settings', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Routing-Tool directions settings', [], ['context' => 'Masterportal']),
        '#default_value' => $this->directionsSettings,
      ],
      'showIsochronesSettings' =>  [
        '#type' => 'checkbox',
        '#title' => $this->t('Isoschronessettings is visible in Masterportal menu', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Uncheck to hide this tool from being listed in the Masterportal menu.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->showIsochronesSettings,

      ],
      'isochronesSettings' => [
        '#type' => 'textfield',
        '#title' => $this->t('Isochrones Settings', [], ['context' => 'Masterportal']),
        '#description' => $this->t('Routing-Tool Isochrones Settings', [], ['context' => 'Masterportal']),
        '#default_value' => $this->isochronesSettings,
        '#states' => [
          'visible' => [
            ':input[type="checkbox"][name="settings[ToolSettings][details_Routing][pluginsettings][showIsochronesSettings]"]' => ['checked' => TRUE],
          ]
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationArray(FormStateInterface $form_state)
  {
    return [
      'name' => $this->name,
      'visibleInMenu' => $this->visibleInMenu,
      'renderToWindow' => $this->renderToWindow,
      'active' => $this->active,
      'geosearch' => $this->geosearch,
      'geosearchReverse' => $this->geosearchReverse,
      'directionsSettings' => $this->directionsSettings,
      'isochronesSettings' => $this->isochronesSettings,
      'showIsochronesSettings' => $this->showIsochronesSettings,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function injectConfiguration(\stdClass &$pluginSection)
  {
    $pluginSection->name = $this->name;
    $pluginSection->visibleInMenu = (bool) $this->visibleInMenu;
    $pluginSection->renderToWindow = (bool) $this->renderToWindow ?? FALSE;
    $pluginSection->active = (bool) $this->active;
    $pluginSection->geosearch = json_decode($this->geosearch);
    $pluginSection->geosearchReverse = json_decode($this->geosearchReverse);
    $pluginSection->directionsSettings = json_decode($this->directionsSettings);
    $pluginSection->isochronesSettings = json_decode($this->isochronesSettings);
    if ($this->showIsochronesSettings == TRUE) {
      $pluginSection->routingToolOptions = $this->routingToolOptions;
    } else {
      $pluginSection->routingToolOptions = ['DIRECTIONS'];
    }
  }
}
