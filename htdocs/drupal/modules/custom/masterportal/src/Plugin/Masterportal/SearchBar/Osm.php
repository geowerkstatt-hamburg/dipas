<?php
namespace Drupal\masterportal\Plugin\Masterportal\SearchBar;

use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\Annotation\SearchBarPlugin;
use Drupal\masterportal\Plugin\Masterportal\PluginBase;
use Drupal\masterportal\PluginSystem\SearchBarPluginInterface;

/**
 * Defines a SearchBar plugin implementation for Gdi.
 *
 * @SearchBarPlugin(
 *   id = "Osm",
 *   title = @Translation("Osm"),
 *   description = @Translation("A search bar plugin to utilize the OSM search."),
 *   configProperty = "osm"
 * )
 */
class Osm extends PluginBase implements SearchBarPluginInterface {
  /**
   * Minimum number of characters at which the search is initiated
   *
   * @var number
   */
  protected $minChars;

  /**
   * The OSM service id to use for the search.
   *
   * @var string
   */
  protected $serviceId;

    /**
   * Maximum amount of requested unfiltered results.
   *
   * @var number
   */
  protected $limit;

 /**
   * May contain federal state names with arbitrary separators.
   *
   * @var string
   */
  protected $states;

  /**
   * May contain the OSM classes to search for.
   *
   * @var string
   */
  protected $classes;

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    return [
      'minChars' => 3,
      'serviceId' => '10',
      'limit' => 50,
      'states' => '',
      'classes' => 'place,highway,building,shop,historic,leisure,city,county',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(FormStateInterface $form_state, $dependantSelector = FALSE, $dependantSelectorProperty = NULL, $dependantSelectorValue = NULL) {
    $states = [
      'required' => [$dependantSelector => [$dependantSelectorProperty => $dependantSelectorValue]],
    ];

    return [
      'minChars' => [
        '#type' => 'number',
        '#title' => $this->t('Number of characters after which the search is initiated.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->minChars,
      ],
      'serviceId' => [
        '#type' => 'textfield',
        '#title' => $this->t('Layer to be used for the OSM search.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->serviceId,
        '#states' => $states,
      ],
      'limit' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum amount of requested unfiltered results.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->limit,
      ],
      'states' => [
        '#type' => 'textfield',
        '#title' => $this->t('May contain federal state names with arbitrary separators.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->states,
      ],
      'classes' => [
        '#type' => 'textfield',
        '#title' => $this->t('May contain the OSM classes to search for.', [], ['context' => 'Masterportal']),
        '#default_value' => $this->classes,
        '#states' => $states,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationArray(FormStateInterface $form_state) {
    return [
      'minChars' => $this->minChars,
      'serviceId' => $this->serviceId,
      'limit' => $this->limit,
      'states' => $this->states,
      'classes' => $this->classes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function injectConfiguration(\stdClass &$pluginSection) {
    $pluginSection->minChars = $this->minChars;
    $pluginSection->serviceId = $this->serviceId;
    $pluginSection->limit = $this->limit;
    $pluginSection->states = $this->states;
    $pluginSection->classes = $this->classes;
  }

}
