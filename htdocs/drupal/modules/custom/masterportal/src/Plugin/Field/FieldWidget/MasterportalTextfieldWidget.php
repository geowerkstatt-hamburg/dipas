<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\DomainAwareTrait;
use Drupal\masterportal\EnsureObjectStructureTrait;
use Drupal\masterportal\Service\InstanceServiceInterface;
use Drupal\masterportal\Service\MasterportalInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Field widget implementation to use the Masterportal as an input device.
 *
 * @FieldWidget(
 *   id = "masterportal_textfield_widget",
 *   label = @Translation("Masterportal"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class MasterportalTextfieldWidget extends StringTextareaWidget {

  use DomainAwareTrait;
  use EnsureObjectStructureTrait;

  /**
   * Custom instance service.
   *
   * @var InstanceServiceInterface
   */
  protected $instanceService;

  /**
   * The currently processed request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The Masterportal renderer service.
   *
   * @var MasterportalInterface
   */
  protected $masterportalRenderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('request_stack'),
      $container->get('masterportal.instanceservice'),
      $container->get('masterportal.renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    RequestStack $request_stack,
    InstanceServiceInterface $instance_service,
    MasterportalInterface $masterportal_renderer
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->instanceService = $instance_service;
    $this->masterportalRenderer = $masterportal_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'masterportal_instance' => NULL,
      'width' => '100',
      'unit' => '%',
      'aspect_ratio' => 'aspect_ratio_16_9',
      'editingZoomLevel' => 6,
      'allowedGeometryTypes' => ['Point', 'LineString', 'Polygon'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [
      '#attached' => [
        'library' => ['masterportal/fieldWidgetSettingsForm'],
      ],
    ];

    $chosenMasterportalInstance = $this->getSetting('masterportal_instance');
    if ($this->isDomainModuleInstalled() && !preg_match('~^[^.]+?\.[^.]+~', $chosenMasterportalInstance)) {
      $chosenMasterportalInstance = sprintf('%s.%s', $this->getActiveDomain(), $chosenMasterportalInstance);
    }

    $elements['masterportal_instance'] = [
      '#type' => 'select',
      '#title' => $this->t('Masterportal instance'),
      '#description' => $this->t('Select the Masterportal instance that will be used for this field.'),
      '#required' => TRUE,
      '#default_value' => $chosenMasterportalInstance,
      '#options' => $this->instanceService->getInstanceOptions(['config']),
    ];

    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Map width', [], ['context' => 'Masterportal']),
      '#description' => $this->t('The width the map gets integrated in.', [], ['context' => 'Masterportal']),
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
      '#min' => 10,
      '#max' => 1000,
      '#step' => 1,
      '#attributes' => [
        'class' => ['mapWidthValue'],
        'style' => 'width: 70px;',
      ],
    ];

    $elements['unit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Unit', [], ['context' => 'Masterportal']),
      '#description' => $this->t('Is the width value stated in pixel or percent?', [], ['context' => 'Masterportal']),
      '#default_value' => $this->getSetting('unit'),
      '#required' => TRUE,
      '#options' => [
        '%' => $this->t('Percent', [], ['context' => 'Masterportal']),
        'px' => $this->t('Pixel', [], ['context' => 'Masterportal']),
      ],
      '#attributes' => [
        'class' => ['mapWidthUnit'],
      ],
    ];

    $elements['aspect_ratio'] = [
      '#type' => 'select',
      '#title' => $this->t('Aspect ratio', [], ['context' => 'Masterportal']),
      '#description' => $this->t('The aspect ratio the map gets integrated in.', [], ['context' => 'Masterportal']),
      '#options' => static::getAspectRatios(),
      '#default_value' => $this->getSetting('aspect_ratio'),
    ];

    $elements['editingZoomLevel'] = [
      '#type' => 'select',
      '#title' => $this->t('Editing zoom level', [], ['context' => 'Masterportal']),
      '#description' => $this->t('The zoom level to use when editing existing content.', [], ['context' => 'Masterportal']),
      '#options' => array_combine(range(0, 9, 1), range(0, 9, 1)),
      '#default_value' => $this->getSetting('editingZoomLevel'),
    ];

    $elements['allowedGeometryTypes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed geometry types', [], ['context' => 'Masterportal']),
      '#description' => $this->t('Which types of geometry should be able to draw in this widget?', [], ['context' => 'Masterportal']),
      '#options' => [
        'Point' => $this->t('Point', [], ['context' => 'Masterportal']),
        'LineString' => $this->t('Line', [], ['context' => 'Masterportal']),
        'Polygon' => $this->t('Area', [], ['context' => 'Masterportal'])
      ],
      '#default_value' => $this->getSetting('allowedGeometryTypes'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $aspectRatios = static::getAspectRatios();
    $instances = $this->instanceService->getInstanceOptions(['config']);
    $chosenInstance = $this->getSetting('masterportal_instance');
    if ($this->isDomainModuleInstalled() && !preg_match('~^[^.]+?\.[^.]+~', $chosenInstance)) {
      $chosenInstance = sprintf('%s.%s', $this->getActiveDomain(), $chosenInstance);
    }
    $chosenInstance = $instances[$chosenInstance];

    return [
      $this->t('Map instance: @instance', ['@instance' => $chosenInstance], ['context' => 'Masterportal']),
      $this->t('Map width: @width@unit', ['@width' => $this->getSetting('width'), '@unit' => $this->getSetting('unit')], ['context' => 'Masterportal']),
      $this->t('Aspect ratio: @aspect_ratio', ['@aspect_ratio' => $aspectRatios[$this->getSetting('aspect_ratio')]], ['context' => 'Masterportal']),
      $this->t('Editing zoom level is @editingZoomLevel', ['@editingZoomLevel' => $this->getSetting('editingZoomLevel')], ['context' => 'Masterportal']),
      $this->t('Allowed geometry types: @types', ['@types' => join(', ', array_filter($this->getSetting('allowedGeometryTypes')))], ['context' => 'Masterportal'])
    ];
  }

  /**
   * Returns a set of available aspect ratios, keyed by CSS class.
   *
   * @return array
   *   The aspect ratio options.
   */
  public static function getAspectRatios() {
    return [
      'aspect_ratio_1_1' => '1:1',
      'aspect_ratio_4_3' => '4:3',
      'aspect_ratio_16_9' => '16:9',
      'aspect_ratio_16_10' => '16:10',
      'aspect_ratio_21_10' => '21:10',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    // Get the form element from Textfield.
    $formElement = parent::formElement($items, $delta, $element, $form, $form_state);

    // Add a CSS class to the default inputs.
    static::ensureConfigPath($formElement, '*value->*#attributes->*class');
    $formElement["value"]['#attributes']['class'][] = 'masterportalWidget';
    $formElement["value"]['#attributes']['class'][] = sprintf('masterportalWidget-%s', $items->getName());

    if (empty($formElement['value']['#default_value']) || ($geometry = json_decode($formElement['value']['#default_value'])) === null) {
      $geometry = FALSE;
    }
    else {
      $iframeUrlParams = [
        'projection' => 'EPSG:4326',
      ];

      switch ($geometry->geometry->type) {
        case 'Point':
          $iframeUrlParams['center'] = sprintf('%s,%s', $geometry->geometry->coordinates[0], $geometry->geometry->coordinates[1]);
          $iframeUrlParams['zoomLevel'] = $this->getSetting('editingZoomLevel');
          break;

        case 'LineString':
        case 'Polygon':
          $extent = $this->calculateExtent($geometry->geometry->type, $geometry->geometry->coordinates);
          $iframeUrlParams['zoomToExtent'] = sprintf(
            '%s,%s,%s,%s',
            $extent['lonMin'],
            $extent['latMin'],
            $extent['lonMax'],
            $extent['latMax']
          );
          break;
      }
    }

    $availableGeometryTypes = [
      'Point' => $this->t('Point', [], ['context' => 'Masterportal']),
      'LineString' => $this->t('Line', [], ['context' => 'Masterportal']),
      'Polygon' => $this->t('Area', [], ['context' => 'Masterportal'])
    ];

    // Add the Masterportal as part of the widget.
    $formElement['masterportalWidget'] = [
      '#title' => $formElement['value']['#title'],
      '#description_display' => 'before',
      '#theme_wrappers' => ['fieldset'],
      '#attributes' => [
        'class' => ['masterportalWidgetMap'],
        'data-fieldname' => $items->getName(),
        'data-fieldtype' => 'Textfield',
      ],
      '#attached' => [
        'library' => [
          'masterportal/masterportalWidget',
        ],
        'drupalSettings' => [
          'masterportal' => [
            $items->getName() => $geometry,
          ],
        ],
      ],
      'drawOptions' => [
        '#weight' => -1,
        '#title' => $this->t('Select the type of drawing you want to make', [], ['context' => 'Masterportal']),
        '#type' => 'radios',
        '#options' => array_filter(
          $availableGeometryTypes,
          function ($key) {
            return in_array($key, $this->getSetting('allowedGeometryTypes'));
          },
          ARRAY_FILTER_USE_KEY
        ),
        '#attributes' => [
          'class' => ['geometryType'],
        ],
      ],
      'map' => $this->masterportalRenderer->iframe(
        $this->instanceService->loadInstance($this->getSetting('masterportal_instance')),
        sprintf('%s%s', $this->getSetting('width'), $this->getSetting('unit')),
        $this->getSetting('aspect_ratio'),
        NULL,
        NULL,
        NULL,
        $geometry ? $iframeUrlParams : []
      )
    ];

    return $formElement;
  }

  /**
   * Calculates the extent coordinate values out of the geometry data.
   *
   * @param string $type
   *   The type of geometry data
   * @param array $geometries
   *   The geometries as stated in the geometry data
   *
   * @return float[]|false
   *   The extent coordinates or FALSE if empty.
   */
  protected function calculateExtent($type, array $geometries) {
    $extent = FALSE;

    if (!empty($geometries)) {
      $extent = [
        'lonMin' => 9999999999,
        'latMin' => 9999999999,
        'lonMax' => 0,
        'latMax' => 0,
      ];
      if ($type !== 'Polygon') {
        $geometries = [$geometries];
      }
      foreach ($geometries as $geometry) {
        foreach ($geometry as $coords) {
          [$lon, $lat] = $coords;
          if ($lon < $extent['lonMin']) {
            $extent['lonMin'] = $lon;
          }
          if ($lat < $extent['latMin']) {
            $extent['latMin'] = $lat;
          }
          if ($lon > $extent['lonMax']) {
            $extent['lonMax'] = $lon;
          }
          if ($lat > $extent['latMax']) {
            $extent['latMax'] = $lat;
          }
        }
      }

      $padding = 0.00002;
      $extent['lonMin'] -= $padding;
      $extent['latMin'] -= $padding;
      $extent['lonMax'] += $padding;
      $extent['latMax'] += $padding;
    }

    return $extent;
  }

}
