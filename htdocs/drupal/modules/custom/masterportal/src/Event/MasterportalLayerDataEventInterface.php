<?php

namespace Drupal\masterportal\Event;

interface MasterportalLayerDataEventInterface extends MasterportalLayerEventInterface {

  public function setLayerData(array $layerData);

  public function setCacheTags(array $cacheTags);

}
