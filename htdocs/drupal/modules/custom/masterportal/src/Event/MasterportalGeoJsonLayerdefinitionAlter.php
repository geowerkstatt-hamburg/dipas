<?php

namespace Drupal\masterportal\Event;

/**
 * Class MasterportalGeoJsonLayerdefinitionAlter.
 *
 * @package Drupal\masterportal\Event
 */
class MasterportalGeoJsonLayerdefinitionAlter extends MasterportalLayerEventBase implements MasterportalLayerDefinitionEventInterface {

  const EVENT_NAME = MasterportalLayerEvents::LayerGeoJsonPluginLayerdefinitionAlter;

  public function __construct(
    string $layerID,
    \stdClass $layerDefinition
  ) {
    parent::__construct($layerID);
    $this->layerDefinition = $layerDefinition;
  }

  public function setLayerDefinition(\stdClass $definition) {
    $this->layerDefinition = $definition;
  }

}
