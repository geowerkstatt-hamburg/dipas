<?php

namespace Drupal\masterportal\Event;

interface MasterportalLayerEventInterface {

  public function getLayerID();

  public function getLayerDefinition();

  public function getLayerData();

  public function getCacheTags();

}
