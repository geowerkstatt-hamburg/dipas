<?php

namespace Drupal\masterportal\Event;

interface MasterportalLayerDefinitionEventInterface extends MasterportalLayerEventInterface {

  public function setLayerDefinition(\stdClass $definition);

}
