<?php

namespace Drupal\masterportal\Event;

final class MasterportalLayerEvents {

  public const LayerGeoJsonPluginLayerdefinitionAlter = 'masterportal.config.alter.layerdefinition.geojson';

  public const LayerGeoJsonPluginDataAlter = 'masterportal.config.alter.data.geojson';

}
