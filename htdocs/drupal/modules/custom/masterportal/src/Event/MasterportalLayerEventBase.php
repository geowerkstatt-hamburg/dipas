<?php

namespace Drupal\masterportal\Event;

use \stdClass;
use Symfony\Contracts\EventDispatcher\Event;

abstract class MasterportalLayerEventBase extends Event implements MasterportalLayerEventInterface {

  protected $layerID;

  protected $layerDefinition;

  protected $layerData;

  protected $cacheTags;

  public function __construct(
    string $layerID
  ) {
    $this->layerID = $layerID;
  }

  public function getLayerID() {
    return $this->layerID;
  }

  public function getLayerDefinition() {
    return $this->layerDefinition ?? FALSE;
  }

  public function getLayerData() {
    return $this->layerData ?? FALSE;
  }

  public function getCacheTags() {
    return $this->cacheTags ?? [];
  }

}
