<?php

namespace Drupal\masterportal\Event;

/**
 * Class MasterportalGeoJsonDataAlter.
 *
 * @package Drupal\masterportal\Event
 */
class MasterportalGeoJsonDataAlter extends MasterportalLayerEventBase implements MasterportalLayerDataEventInterface {

  const EVENT_NAME = MasterportalLayerEvents::LayerGeoJsonPluginDataAlter;

  public function __construct(
    string $layerID,
    array $layerData
  ) {
    parent::__construct($layerID);
    $this->layerData = $layerData;
  }

  public function setLayerData(array $layerData) {
    $this->layerData = $layerData;
  }

  public function setCacheTags(array $cacheTags) {
    $this->cacheTags = $cacheTags;
  }

}
