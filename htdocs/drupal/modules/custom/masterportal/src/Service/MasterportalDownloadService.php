<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\Service;

use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\Exception\UnknownExtensionTypeException;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\masterportal\Entity\MasterportalInstanceInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ZipArchive;

/**
 * Interface MasterportalDownloadServiceInterface.
 *
 * @package Drupal\masterportal\Service
 */
class MasterportalDownloadService implements MasterportalDownloadServiceInterface {

  /**
   * Drupal's file system service.
   *
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The currently processed request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The Masterportal rendering service.
   *
   * @var MasterportalInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * @var string[]
   */
  protected $localLayers;

  /**
   * MasterportalDownloadService constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Drupal's file system service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\masterportal\Service\MasterportalInterface $masterportal_renderer
   *   The Masterportal rendering service.
   */
  public function __construct(
    FileSystemInterface $file_system,
    RequestStack $request_stack,
    MasterportalInterface $masterportal_renderer,
    ExtensionPathResolver $extension_path_resolver
  ) {
    $this->fileSystem = $file_system;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->renderer = $masterportal_renderer;
    $this->extensionPathResolver = $extension_path_resolver;

    $this->localLayers = drupal_static('masterportal.downloadservice.locallayers', []);
  }

  /**
   * {@inheritdoc}
   */
  public function createZip(MasterportalInstanceInterface $masterportal_instance) {
    // Copy the library.
    $tmpPath = $this->copyLibrary();

    // Unlink unnecessary files (only used by the config ui).
    $this->fileSystem->unlink(sprintf('%s/css/masterportalAspectRatios.css', $tmpPath));

    // Copy MasterportalAddons
    $files = $this->fileSystem->scanDirectory(sprintf('%s/libraries/MasterportalAddons', DRUPAL_ROOT,), '~.*~');
    foreach ($files as $file => $fileObj) {
      [,$targetDir] = explode('.', $file);
      $this->fileSystem->copy(
        $file,
        sprintf('%s/masterportal/%s', $this->fileSystem->getTempDirectory(), $targetDir)
      );
    }

    // Create dynamic files.
    $dynamicFiles = [
      ['index.html', 'BasicSettings.html_structure', 'text/html; charset=utf-8', NUll],
      ['config.js', 'BasicSettings.js', 'application/javascript; charset=utf-8', 'generateJavascriptSettingsObject'],
      ['config.json', 'JSON', 'application/json; charset=utf-8', 'generateJsonSettingsObject'],
      ['layerdefinitions.json', 'Layerconfiguration', 'application/json; charset=utf-8', 'enrichLayerDefinitions'],
      ['services.json', 'BasicSettings.service_definitions', 'application/json; charset=utf-8', 'generateServicesJson'],
      ['layerstyles.json', 'LayerStyles', 'application/json; charset=utf-8', 'generateLayerStyles'],
    ];

    foreach ($dynamicFiles as $file) {
      $this->createDynamicFile(
        $tmpPath,
        $file[0],
        $masterportal_instance,
        $file[1],
        $file[2],
        $file[3]
      );
    }

    if (count($this->localLayers)) {
      $layerpath = sprintf('%s/masterportal/layer', $this->fileSystem->getTempDirectory());
      $this->fileSystem->mkdir($layerpath);

      foreach ($this->localLayers as $layerID) {
        $layerContent = $this->renderer->renderLayer($layerID, TRUE);

        $this->fileSystem->saveData(
          $layerContent,
          sprintf(
            '%s/masterportal/layer/%s.json',
            $this->fileSystem->getTempDirectory(),
            $layerID
          ),
          FileSystemInterface::EXISTS_REPLACE
        );
      }
    }

    // Create the ZIP.
    $zip = $this->createZipFile($tmpPath);

    // Create the response and attach the file.
    $response = new BinaryFileResponse($zip);
    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'masterportal.zip');
    $response->deleteFileAfterSend(TRUE);

    // Remove the temporary Masterportal directory.
    $this->fileSystem->deleteRecursive($tmpPath);

    return $response;
  }

  /**
   * Copies all necessary files of the Masterportal to a temporary location.
   *
   * @param null $src
   *   Only used internally.
   * @param null $dst
   *   Only used internally.
   *
   * @return string
   *   The temporary path of the copy.
   *
   * @throws UnknownExtensionException
   * @throws UnknownExtensionTypeException
   */
  protected function copyLibrary($src = NULL, $dst = NULL) {
    if (empty($src)) {
      $src = sprintf(
        '%s/libraries/masterportal',
        $this->extensionPathResolver->getPath('module', 'masterportal')
      );
    }
    if (empty($dst)) {
      $tmpPath = sprintf('%s/masterportal', $this->fileSystem->getTempDirectory());
      $this->fileSystem->mkdir($tmpPath);
      $dst = $this->fileSystem->realpath($tmpPath);
    }
    if (!is_dir($dst)) {
      $this->fileSystem->mkdir($dst);
    }
    $dir = opendir($src);
    while ((FALSE !== ($file = readdir($dir)))) {
      if (in_array($file, ['.', '..'])) {
        continue;
      }
      elseif (is_dir($current = sprintf('%s/%s', $src, $file))) {
        $this->copyLibrary($current, sprintf('%s/%s', $dst, $file));
      }
      else {
        copy($current, sprintf('%s/%s', $dst, $file));
      }
    }
    closedir($dir);
    return $dst;
  }

  /**
   * Create a file from the instance configuration.
   *
   * @param string $tmpPath
   *   The file path to store the file in.
   * @param string $filename
   *   The name of the file.
   * @param MasterportalInstanceInterface $instance
   *   The Masterportal instance.
   * @param string $key
   *   The configuration key.
   * @param string $contentType
   *   The content-type header.
   * @param string $preprocess
   *   The name of the preprocess function to call.
   *
   * @throws ConfigValueException
   * @throws NotFoundHttpException
   */
  protected function createDynamicFile($tmpPath, $filename, MasterportalInstanceInterface $instance, $key, $contentType, $preprocess) {
    $this->currentRequest->attributes->set('cacheID', hash('sha256', microtime(TRUE)));
    $this->currentRequest->attributes->set('cacheExclude', TRUE);

    $content = $this->renderer->createResponse($key, $contentType, $preprocess, $instance)->getContent();

    $base_path = base_path();

    // Replace urls pointing to Masterportal configuration files with a local url
    $content = str_replace($base_path . 'modules/custom/masterportal/libraries/masterportal/', '', $content);
    $content = str_replace(sprintf($base_path . 'masterportal/%s/', $instance->id()), '', $content);
    $content = str_replace($base_path . 'masterportal/(?!layer)', '', $content);

    switch ($filename) {
      case 'index.html':
        // Copy all linked libraries (Cesium etc)
        $masterportalModulePath = $this->extensionPathResolver->getPath('module', 'masterportal');

        preg_match_all(
          sprintf('~(["\'])(%s%s/libraries/[^\1]*?)\1~ism',
            $base_path,
            $masterportalModulePath
          ),
          $content,
          $matches
        );

        if (count($matches)) {
          $libraryDestinationPath = sprintf('%s/masterportal/libraries', $this->fileSystem->getTempDirectory());
          $this->fileSystem->mkdir($libraryDestinationPath);

          foreach ($matches[2] as $library) {
            [$libraryname,] = explode('/', preg_replace(sprintf('~%s%s/libraries/~', $base_path, $masterportalModulePath), '', $library));
            $this->copyLibraryRecursive($libraryname, $libraryDestinationPath);

            $content = preg_replace(
              sprintf('~%s%s/libraries/%s~', $base_path, $masterportalModulePath, $libraryname),
              'libraries/' . $libraryname,
              $content
            );
          }
        }
        break;

      case 'config.js':
        // Remove the remoteInterface configuration.
        $content = preg_replace('~,\s*?remoteInterface:\s*?\{[^\}]*?\}~ism', '', $content);
        break;

      case 'layerdefinitions.json':
        // Change the URL of locally generated layers in relative URLs
        // and remember the layer IDs to be also generated into physical files
        $content = json_decode($content);

        foreach ($content as $layer) {
          if (preg_match('~/masterportal/(layer/.*)~', $layer->url, $matches)) {
            $layer->url = './' . $matches[1] . '.json';
            $this->localLayers[] = $layer->id;
          }
        }

        $content = \json_encode($content, Masterportal::JSON_OUTPUT_OPTIONS);
        break;

      case 'layerstyles.json':
        // Copy contribution icons.
        preg_match_all('~"imageName": "([^"]+?)"~', $content, $matches);

        foreach ($matches[1] as $file) {
          $iconfilename = (function ($path) {
            $parts = explode('/', $path);
            return array_pop($parts);
          })($file);
          $source = sprintf('%s%s', DRUPAL_ROOT, $file);
          $destination = sprintf('%s/img/%s', $tmpPath, $iconfilename);
          copy($source, $destination);
          $content = preg_replace(
            sprintf('~%s~', preg_quote($file, '~')),
            sprintf('img/%s', $iconfilename),
            $content
          );
        }
        break;
    }

    $this->fileSystem->saveData($content, sprintf('%s/%s', $tmpPath, $filename), FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * Packs all files of a Masterportal as a ZIP archive.
   *
   * @param $path
   *   The path in which the ZIP file should get created in.
   *
   * @return string
   *   The path to the zip file.
   */
  protected function createZipFile($path) {
    $target = sprintf('%s/masterportal.zip', $this->fileSystem->getTempDirectory());
    $zip = new ZipArchive();
    $zip->open($target, ZipArchive::CREATE | ZipArchive::OVERWRITE);
    $files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($path),
      RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file) {
      if (!$file->isDir()) {
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($path) + 1);
        $zip->addFile($filePath, $relativePath);
      }
    }
    $zip->close();
    return $target;
  }

  protected function copyLibraryRecursive($library, $targetDirectory) {
    $localLibraryPathPrefix = sprintf(
      '%s/%s/libraries/',
      DRUPAL_ROOT,
      $this->extensionPathResolver->getPath('module', 'masterportal')
    );

    $files = $this->fileSystem->scanDirectory(
      sprintf(
        '%s/%s',
        $localLibraryPathPrefix,
        $library
      ), '~.?~');

    $subdirs = [];
    $filesToCopy = [];

    foreach ($files as $file => $fileObj) {
      $relativePath = preg_replace("~^$localLibraryPathPrefix/~", '', $file);
      $filesToCopy[] = $relativePath;
      $subDirsToPrepare = [];

      $pathToPrepare = explode('/', preg_replace('~(.*?)/[^/]*?$~', '\1', $relativePath));

      while (($nearestSubDir = array_shift($pathToPrepare)) !== NULL) {
        $subDirToPrepare = ($subDirToPrepare ?? $targetDirectory) . "/$nearestSubDir";
        $subDirsToPrepare[] = $subDirToPrepare;
      }

      unset($subDirToPrepare);
      $subDirsToPrepare = array_unique($subDirsToPrepare);
      $subdirs = array_merge($subdirs, $subDirsToPrepare);
    }

    $subdirs = array_unique($subdirs);


    foreach ($subdirs as $subdir) {
      $this->fileSystem->mkdir($subdir);
      $this->fileSystem->prepareDirectory($subdir);
    }

    foreach ($filesToCopy as $file) {
      $this->fileSystem->copy(
        $localLibraryPathPrefix . $file,
        $targetDirectory . '/' . $file
      );
    }
  }

}
