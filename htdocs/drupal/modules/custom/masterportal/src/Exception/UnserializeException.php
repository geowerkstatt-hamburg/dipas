<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\masterportal\Exception;

/**
 * Class UnserializeException.
 *
 * Exception that is thrown when deserialization of a string fails.
 *
 * @package Drupal\masterportal
 */
class UnserializeException extends \Exception {}
