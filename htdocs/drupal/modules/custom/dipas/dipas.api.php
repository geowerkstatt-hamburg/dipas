<?php

/**
 * @file
 * Hooks specific to the DIPAS module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide slugs that should get ignored by the DIPAS Rest API
 *
 * @returns string[]
 *   Collection of URL slugs that should not get handled by the DIPAS Rest API
 */
function hook_dipas_api_links() {
  return [
    'customslug' // DIPAS module ignores requests to /dipas/customslug(/*)
  ];
}

/**
 * Provide Masterportal instance configurations that should NOT get cloned for new domains.
 *
 * @return string[]
 */
function hook_dipas_noclone_masterportal_instances() {
  return [
    'instance_name'
  ];
}

/**
 * Called upon cloning a Masterportal instance, allowing to act on the new clone.
 *
 * @param string domain
 *   The domain the clone is created for.
 * @param \Drupal\masterportal\Entity\MasterportalInstanceInterface $masterportal
 *   The newly created, cloned instance as a reference.
 *
 * @return void
 */
function hook_dipas_on_masterportal_instance_clone($domain, &$masterportal) {
  // Actions to take on newly created clone before saving.
}

/**
 * @} End of "addtogroup hooks".
 */
