/**
 * @license GPL-2.0-or-later
 */

(function ($, Drupal, drupalSettings, window) {

  'use strict';

  Drupal.DipasConfigSection = {
    markedIDs: [],
    markedFields: [],
    resetMarkings: function () {
      let elem;

      (['markedIDs', 'markedFields']).forEach(property => {
        while (elem = Drupal.DipasConfigSection[property].shift()) {
          elem.removeClass('error');
        }
      });
    }
  };

  Drupal.behaviors.DipasConfigSection = {
    attach: function (context) {
      $('input[type="submit"][name="op"]').on('click', Drupal.DipasConfigSection.resetMarkings);

      $('input, select, textarea').each((index, elem) => {
        elem.addEventListener('invalid', (evt) => {
          let details = $(evt.target).parents().filter('details.dipasSettingsSection'),
            detailsID = details.attr('id'),
            $tab = $('a[href="#' + detailsID + '"]').parent(),
            $field = $(evt.target);

          Drupal.DipasConfigSection.markedFields.push($field);
          Drupal.DipasConfigSection.markedIDs.push($tab);

          $field.addClass('error');
          $tab.addClass('error');
          evt.preventDefault();
        });
      });
    }
  };

}(jQuery, Drupal));
