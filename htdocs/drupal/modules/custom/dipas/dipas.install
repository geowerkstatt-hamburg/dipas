<?php

/**
 * @file
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

use Drupal\Core\Database\Database;

/**
 * Migrate geodata from GeoField to text field.
 */
function dipas_update_8102() {
  /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = \Drupal::getContainer()->get('entity_type.manager');
  /* @var \Drupal\Core\Entity\EntityStorageInterface $nodeStorage */
  $nodeStorage = $entityTypeManager->getStorage('node');

  /** @var \Drupal\node\NodeInterface[] $nodes */
  $nodes = $nodeStorage->loadByProperties([
    'type' => 'contribution',
  ]);

  foreach ($nodes as $node) {
    $localization = $node->get('field_localization');
    if ($fieldValue = $localization->first()) {
      $geodata = [
        'type' => 'Point',
        'coordinates' => [
          $fieldValue->get('lon')->getValue(),
          $fieldValue->get('lat')->getValue(),
        ],
      ];
      $node->set('field_geodata', json_encode($geodata));
      $node->save();
    }
  }
}

/**
 * Create center points for existing geometries in field geodata.
 */
function dipas_update_8103() {
  /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = \Drupal::getContainer()->get('entity_type.manager');
  /* @var \Drupal\Core\Entity\EntityStorageInterface $nodeStorage */
  $nodeStorage = $entityTypeManager->getStorage('node');

  /** @var \Drupal\node\NodeInterface[] $nodes */
  $nodes = $nodeStorage->loadByProperties([
    'type' => 'contribution',
  ]);

  foreach ($nodes as $node) {
    $geofield = $node->get('field_geodata');
    if (($fieldValue = $geofield->first()) && $fieldValue = json_decode($fieldValue->getString())) {

      if (empty($fieldValue->type)) {
        $geodata = $fieldValue;
      }

      switch ($fieldValue->type) {
        case 'Point':
          $geodata = [
            'geometry' => $fieldValue,
            'centerPoint' => $fieldValue,
          ];
          break;

        case 'LineString':
          $middleIndex = ceil(count($fieldValue->coordinates) / 2);
          $middleCoordinates = $fieldValue->coordinates[$middleIndex];
          $centerPoint = [
            'type' => 'Point',
            'coordinates' => $middleCoordinates,
          ];
          $geodata = [
            'geometry' => $fieldValue,
            'centerPoint' => $centerPoint,
          ];
          break;

        case 'Polygon':
          $coordsToCheck = $fieldValue->coordinates[0];

          $extent = [
            'lon_min' => 9999999999,
            'lat_min' => 9999999999,
            'lon_max' => 0,
            'lat_max' => 0,
          ];

          $latall = [];
          $lonall = [];

          /*
           * Copy all longitude coordinates in one and all latitude
           * coordinates in another array.
           */
          foreach ($coordsToCheck as $coordinate) {
            $lonall[] = $coordinate[0];
            $latall[] = $coordinate[1];
          }

          // Get the min and the max of both arrays.
          $extent = [
            'lon_min' => min($lonall),
            'lat_min' => min($latall),
            'lon_max' => max($lonall),
            'lat_max' => max($latall),
          ];

          $extent['lon_diff'] = $extent['lon_max'] - $extent['lon_min'];
          $extent['lon_avg'] = $extent['lon_min'] + $extent['lon_diff'] / 2;
          $extent['lat_diff'] = $extent['lat_max'] - $extent['lat_min'];
          $extent['lat_avg'] = $extent['lat_min'] + $extent['lat_diff'] / 2;

          $centerPoint = [
            'type' => 'Point',
            'coordinates' => [$extent['lon_avg'], $extent['lat_avg']],
          ];

          $geodata = [
            'geometry' => $fieldValue,
            'centerPoint' => $centerPoint,
          ];
      }

      $node->set('field_geodata', json_encode($geodata));
      $node->save();
    }
  }
}

/**
 * Exceptionally remove of masterportal setting within dipas_update hook: Removing extent.
 */
function dipas_update_8105() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('masterportal.instance.') as $masterportal_instance) {
    $config = $config_factory->getEditable($masterportal_instance);
    if ($config->get('settings') && $config->get('settings')['MapSettings'] && $config->get('settings')['MapSettings']['extent']) {
      $settings = $config->get('settings');
      unset($settings['MapSettings']['extent']);

      # remove entire object if empty
      if (count($settings['MapSettings']) === 0) {
        unset($settings['MapSettings']);
      }

      $config->set('settings', $settings);
      $config->save(TRUE);
    }
  }
}

/**
 * Remove overwriteFrontpage Tickbox from projectSchedule settings and check the box on MenuSettings if it was set
 */
function dipas_update_8106() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('dipas.') as $dipas_config) {
    $config = $config_factory->getEditable($dipas_config);

    $settings = $config->get('ProjectSchedule');

    if ($settings && in_array('overwriteFrontpage', $settings)) {

      $settingsValue = $settings['overwriteFrontpage'];

      if ($config->get('MenuSettings') && $settingsValue) {
        $menuSettings = $config->get('MenuSettings');

        $menuSettings['mainmenu']['conceptionlist']['enabled'] = TRUE;
        $menuSettings['mainmenu']['conceptionlist']['name'] = 'Entwürfe Ansehen';
        $menuSettings['mainmenu']['conceptionlist']['icon'] = 'compare_arrows';
        $menuSettings['mainmenu']['conceptionlist']['overwriteFrontpage'] = $settingsValue;

        $config->set('MenuSettings', $menuSettings);

      }

      unset($settings['overwriteFrontpage']);
      $config->set('ProjectSchedule', $settings);
      $config->save(TRUE);
    }
  }
}

/**
 * Introducing a new table for DIPAS Cockpit
 */
function dipas_update_8107() {
  $spec = [
    'description' => 'Table to store the contributions and comments per month over all proceedings.',
    'fields' => [
        'dipas_cockpit_data_id' => [
          'description' => 'the unique id for the data',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'year' => [
          'description' => 'The year were the contribution or comment was entered',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'month' => [
          'description' => 'The month were the contribution or comment was entered',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'contributions' => [
          'description' => 'The count of the contribution were entered',
          'type' => 'int',
          'not null' => FALSE,
        ],
        'comments' => [
          'description' => 'The count were the comments were entered',
          'type' => 'int',
          'not null' => FALSE,
       ],
    ],
    'primary key' => [
      'dipas_cockpit_data_id',
    ],
  ];
  // Fetch the actual schema from the default database
  $schema = Database::getConnection()->schema();
  // CREATE TABLE dipas_cockpit_data
  $schema->createTable('dipas_cockpit_data', $spec);
}

/**
 * Calculating and adding centerpoints for project areas
 */
function dipas_update_8108() {
  $configNames = \Drupal::service('config.storage')->listAll('dipas.');
  $configNames = array_filter($configNames, function($item){return preg_match('~^dipas\..+?\.configuration$~i', $item) && $item !== 'dipas.default.configuration';});

  foreach ($configNames as $configFile) {
    $config = \Drupal::configFactory()->getEditable($configFile);

    $geodata = json_decode($config->get('ProjectArea')['project_area']);

    $boundingbox = [
      'xmin' => 9999999,
      'xmax' => 0,
      'ymin' => 9999999,
      'ymax' => 0,
    ];

    foreach($geodata->coordinates as $polygon) {
      foreach($polygon as $point) {
        if ($point[0] < $boundingbox['xmin']) {
          $boundingbox['xmin'] = $point[0];
        };

        if ($point[1] < $boundingbox['ymin']) {
          $boundingbox['ymin'] = $point[1];
        };

        if ($point[0] > $boundingbox['xmax']) {
          $boundingbox['xmax'] = $point[0];
        };

        if ($point[1] > $boundingbox['ymax']) {
          $boundingbox['ymax'] = $point[1];
        };
      }
    }

    $centerpoint = [
      $boundingbox['xmin'] + ($boundingbox['xmax'] - $boundingbox['xmin']) / 2,
      $boundingbox['ymin'] + ($boundingbox['ymax'] - $boundingbox['ymin']) / 2,
    ];

    $config->set('ProjectArea.project_area_centerpoint', implode(', ', $centerpoint));
    $config->save();
  }
}

/**
 * Migrate geodata from GeoField to text field for appointments.
 */
function dipas_update_9101() {
  /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = \Drupal::getContainer()->get('entity_type.manager');
  /* @var \Drupal\Core\Entity\EntityStorageInterface $nodeStorage */
  $nodeStorage = $entityTypeManager->getStorage('node');

  /** @var \Drupal\node\NodeInterface[] $nodes */
  $nodes = $nodeStorage->loadByProperties([
    'type' => 'appointment',
  ]);

  foreach ($nodes as $node) {
    $localization = $node->get('field_localization');
    if ($fieldValue = $localization->first()) {
      $geodata = [
        'geometry' => [
          'type' => 'Point',
          'coordinates' => [
            $fieldValue->get('lon')->getValue(),
            $fieldValue->get('lat')->getValue(),
          ]
        ],
        'centerPoint' => [
          'type' => 'Point',
          'coordinates' => [
            $fieldValue->get('lon')->getValue(),
            $fieldValue->get('lat')->getValue(),
          ]
        ],
      ];
      $node->set('field_geodata', json_encode($geodata));
      $node->save();
    }
  }
}

/**
 * Remove now unused GeoField from the field configuration of appointment nodes.
 *
 * Re-iterated updatehook since it seems not to be run on previous updates
 */
function dipas_update_9103() {
  if ($fieldConfig = \Drupal\field\Entity\FieldConfig::loadByName('node', 'appointment', 'field_localization')) {
    $fieldConfig->delete();
  }
  if ($fieldStorage = \Drupal\field\Entity\FieldStorageConfig::loadByName('node', 'field_localization')) {
    $fieldStorage->delete();
  }
}

/**
 * Remove potentially invalid Masterportal instances and re-clone them from the default instance
 */
function dipas_update_9104() {
  // Fetch a database connection
  /*
   * @var \Drupal\Core\Database\Connection $database
   */
  $database = \Drupal::database();

  // UUID Generator
  /*
   * @var \Drupal\Component\Uuid\UuidInterface $uuidGenerator
   */
  $uuidGenerator = \Drupal::service('uuid');

  // Determine the default instances defined
  $defaultInstances = $database->select('config')
    ->fields('config')
    ->condition('name', 'masterportal.instance.default.%', 'LIKE')
    ->execute()
    ->fetchAll();

  // Now determine the cloned instances
  $clonedInstances = $database->select('config')
    ->fields('config')
    ->condition('name', 'masterportal.instance.%', 'LIKE')
    ->condition('name', 'masterportal.instance.default.%', 'NOT LIKE')
    ->execute()
    ->fetchAll();

  // Determine which instances are not properly defined
  $invalidInstances = array_filter(
    $clonedInstances,
    function ($instance) {
      $data = unserialize($instance->data);

      return $data === FALSE || !isset($data['settings']);
    }
  );

  $databaseChanged = (bool) count($invalidInstances);

  // Process each invalid instance
  foreach ($invalidInstances as $instance) {
    // Prepare the id parts of the name
    $id = explode('.', $instance->name);

    // Filter the default instance definitions for the matching one
    $defaultBlueprint = (function ($id, $collection) use ($defaultInstances) {
      $matchingInstance = array_filter(
        $defaultInstances,
        function ($instance) use ($id, $collection) {
          return $instance->collection === $collection &&
            $instance->name === sprintf('masterportal.instance.default.%s', $id);
        }
      );

      return array_shift($matchingInstance);
    })($id[3], $instance->collection);

    // unserialize the blueprint's data
    $properData = unserialize($defaultBlueprint->data);

    // Set a new UUID to make sure the newly cloned data is valid
    $properData['uuid'] = $uuidGenerator->generate();

    // Fix the id
    $properData['id'] = sprintf('%s.%s', $id[2], $id[3]);

    // Serialize the fixed data
    $instance->data = serialize($properData);

    // write the fixed data back to the database
    $database->update('config')
      ->fields([
        'data' => $instance->data,
      ])
      ->condition('name', $instance->name, '=')
      ->execute();
  }

  // Invalidate Drupal caches if the database was changed.
  if ($databaseChanged) {
    drupal_flush_all_caches();
  }
}

/**
 * Ensure all previously cloned Masterportal instances have a unique UUID and proper filter settings
 */
function dipas_update_9105() {
  // Get the entity type manager.
  /*
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  $entityTypeManager = \Drupal::entityTypeManager();

  // UUID Generator
  /*
   * @var \Drupal\Component\Uuid\UuidInterface $uuidGenerator
   */
  $uuidGenerator = \Drupal::service('uuid');

  // Load all Masterportal instances defined in the database.
  /*
   * @var \Drupal\masterportal\Entity\MasterportalInstanceInterface[] $instances
   */
  $instances = $entityTypeManager->getStorage('masterportal_instance')->loadMultiple();

  // Process each instance
  foreach ($instances as $instance) {
    // Ensure filter settings are not outdated
    $settings = $instance->get('settings');
    if (
      !isset($settings['MenuSettings']['filter']) &&
      isset($settings['MenuSettings']['filterGeneral'])
    ) {
      $settings['MenuSettings']['filter'] = $settings['MenuSettings']['filterGeneral'];
      unset($settings['MenuSettings']['filterGeneral']);

      $instance->set('settings', $settings);
    }

    // First we have to delete the old instance
    $instance->delete();

    // Make sure the UUID differs in each instance
    $instance->set('uuid', $uuidGenerator->generate());

    // Mark the instance as new to enable proper saving
    $instance->enforceIsNew();

    // Save the instance
    $instance->save();
  }

  // Invalidate Drupal caches
  drupal_flush_all_caches();
}

/**
 * Update the filter settings of all defined Masterportal instances which utilize the contributions layer.
 */
function dipas_update_9106() {
  // Get the entity type manager.
  $entityTypeManager = \Drupal::entityTypeManager();

  // Load all Masterportal instances defined in the database.
  /*
   * @var \Drupal\masterportal\Entity\MasterportalInstanceInterface[] $instances
   */
  $instances = $entityTypeManager->getStorage('masterportal_instance')->loadMultiple();

  // Filter out all instances that do not use the "contributions" layer
  $instances = array_filter(
    $instances,
    function ($instance) {
      $settings = $instance->get('settings');

      $instancelayer = [];
      foreach (['BackgroundLayerSettings', 'ForegroundLayerSection'] as $section) {
        $sectionlayer = array_map(
          function ($definition) {
            return $definition['id'];
          },
          $settings[$section]['layer']
        );

        $instancelayer = array_merge($instancelayer, $sectionlayer);
      }

      return in_array('contributions', $instancelayer);
    }
  );

  $databaseChanged = (bool) count($instances);

  // Process each remaining instance.
  foreach ($instances as $instance) {
    // Retrieve the instance settings
    $settings = $instance->get('settings');

    // Make sure an entry exists for filter layers
    if (!isset($settings['MenuSettings']['filter']['layers'])) {
      $settings['MenuSettings']['filter']['layers'] = '[]';
    }

    // Try to decode it's filter layer value
    $definedLayers = json_decode($settings['MenuSettings']['filter']['layers']);

    // If the value derived is not an array, turn it into one
    if (!is_array($definedLayers)) {
      // In case the encoding failed (it was a bad definition then anyways)
      if (!$definedLayers) {
        $definedLayers = [];
      }
      else {
        $definedLayers = [$definedLayers];
      }
    }

    // Make sure there's no previous definition for the contributions layer
    $definedLayers = array_filter(
      $definedLayers,
      function ($layer) {
        // Make sure it's an array, not an object.
        $layer = (array) $layer;

        // Determine which keys(properties) this layer has
        $keys = array_map(
          function ($key) {
            return strtolower($key);
          },
          array_keys($layer)
        );

        // Is a key named "layerid" present?
        if (in_array('layerid', $keys)) {
          // Find the index of that key
          $index = array_search('layerid', $keys);

          // Now determine which value that key has
          $layerid = array_values($layer)[$index];

          // If the layerid matches we need to filter out that layer definition
          if ($layerid === 'contributions') {
            return FALSE;
          }
        }

        // In all other cases this definition may pass
        return TRUE;
      }
    );

    // Now we need to add the new contribution filter layer definition
    $definedLayers[] = json_decode('{
        "layerId": "contributions",
        "strategy": "active",
        "showHits": false,
        "snippetTags": false,
        "snippets": [
          {
              "type": "dropdown",
              "attrName": "Thema",
              "operator": "IN",
              "display": "list",
              "multiselect": true,
              "addSelectAll": false,
              "renderIcons": "fromLegend",
              "info": false,
              "prechecked": "all",
              "showAllValues": true,
              "resetLayer": true
          }
        ]
      }');

    // The now updated layer definition field value is now to be encoded and written back
    $settings['MenuSettings']['filter']['layers'] = json_encode($definedLayers, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT);

    // Make sure the flags and values set on the filter are valid values
    foreach (
      [
        'deactivateGFI' => FALSE,
        'active' => FALSE,
        'liveZoomToFeatures' => FALSE,
        'layerSelectorVisible' => FALSE,
        'isVisibleInMenu' => FALSE
      ] as $property => $default
    ) {
      if (
        !isset($settings['MenuSettings']['filter'][$property]) ||
        !is_bool($settings['MenuSettings']['filter'][$property])
      ) {
        $settings['MenuSettings']['filter'][$property] = $default;
      }

      // Special handling for some instances
      if (preg_match('~\.(?:dipas|default)$~i', $instance->id())) {
        if (preg_match('~\.dipas$~i', $instance->id())) {
          $settings['MenuSettings']['filter']['active'] = TRUE;
        }
        else {
          $settings['MenuSettings']['filter']['isVisibleInMenu'] = TRUE;
        }
      }
    }

    // Write back the updated settings to the configuration and save it
    $instance->set('settings', $settings);
    $instance->save();
  }

  // Invalidate Drupal caches if any instances were changed.
  if ($databaseChanged) {
    drupal_flush_all_caches();
  }
}

/**
 * Initialize the new config flag 'ContributionSettings.display_existing_ratings' on old proceedings.
 */
function dipas_update_9107() {
  $database = \Drupal::database();
  $query = $database->select('comment', 'c');
  $query->addJoin(
    'LEFT',
    'comment__field_domain_access',
    'fda',
    'c.comment_type = fda.bundle AND c.cid = fda.entity_id AND c.langcode = fda.langcode'
  );
  $query->groupBy('fda.field_domain_access_target_id');
  $query->fields('fda', ['field_domain_access_target_id']);
  $query->addExpression('COUNT(c.cid)', 'comments');
  $query->orderBy('comments', 'DESC');

  $raw = $query->execute()->fetchAll();
  $comments = array_combine(
    array_map(function ($row) { return $row->field_domain_access_target_id; }, $raw),
    array_map(function ($row) { return $row->comments; }, $raw)
  );

  /* @var \Drupal\dipas\Service\DipasConfigInterface $dipasConfigService */
  $dipasConfigService = \Drupal::service('dipas.config');
  foreach ($dipasConfigService->getIds() as $configID) {
    [,$proceedingID,] = explode('.', $configID);
    $config = $dipasConfigService->getEditable($configID);
    $config->set('ContributionSettings.display_existing_ratings', (int) in_array($proceedingID, array_keys($comments)));
    $config->save();
  }
}

/**
 * Drop a now deprecated, unused database table
 */
function dipas_update_9108() {
  $database = \Drupal::database();
  $database->schema()->dropTable('dipas_cockpit_data');
}

/**
 * Remove Masterportal searchbar renderToDom settings
 */
function dipas_update_9109() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('masterportal.instance.') as $masterportal_instance) {
    $config = $config_factory->getEditable($masterportal_instance);
    if (($settings = $config->get('settings')) && isset($settings['SearchBarSettings'])) {

      $settings['SearchBarSettings']['renderToDOM'] = '';

      $config->set('settings', $settings);
      $config->save(TRUE);
    }
  }
}

/**
 * Transform existing DIPAS configuration settings to the new setting value structure. Drop the now obsolete dipas_keywords database table.
 */
function dipas_update_10100() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('dipas.') as $configID) {
    $config = $config_factory->getEditable($configID);

    $config->set('LandingPage', [
      'background_image' => $config->get('ProjectInformation.project_image'),
      'text' => $config->get('ProjectInformation.text'),
      'node' => $config->get('MenuSettings.mainmenu.projectinfo.node'),
    ]);

    $sidebarConfig = $config->get('SidebarSettings.blocks');

    // Move the FAQ setting to the sidebar component configuration if existing
    if ($config->get('MenuSettings.footermenu.faq.enabled')) {
      $sidebarConfig['faq'] = [
        'node' => $config->get('MenuSettings.footermenu.faq.node'),
      ];
    }

    // Since a configurable headline for the Newsletter panel is new, we set a generic "Newsletter" headline as default
    $sidebarConfig['newsletter']['headline'] = 'Newsletter';

    // Remove non-existent sidebar blocks
    unset($sidebarConfig['contact']);
    unset($sidebarConfig['projectlogo']);
    unset($sidebarConfig['downloads']);
    unset($sidebarConfig['contact']);

    $config->set('SidebarSettings.blocks', $sidebarConfig);

    $config->save();
  }

  $database = \Drupal::database();
  $database->schema()->dropTable('dipas_keywords');
}

/**
 * Initiate the new configuration settings for the contribution wizard.
 */
function dipas_update_10101() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('dipas.') as $configID) {
    // Get the proceeding's configuration object.
    $config = $config_factory->getEditable($configID);

    // Determine if the current proceeding utilizes contribution types
    $useRubrics = $config->get('ContributionSettings.rubrics_use');

    // Determine if contributions have to be localized in this proceeding
    $mustBeLocalized = $config->get('ContributionSettings.contributions_must_be_localized');

    // Prepare the step array
    $stepOrder = [
      [
        'id' => 'description',
        'enabled' => TRUE,
        'fillInMandatory' => TRUE,
      ],
      [
        'id' => 'category',
        'enabled' => TRUE,
        'fillInMandatory' => TRUE,
      ],
      [
        'id' => 'type',
        'enabled' => (bool) $useRubrics,
        'fillInMandatory' => (bool) $useRubrics,
      ],
      [
        'id' => 'location',
        'enabled' => TRUE,
        'fillInMandatory' => (bool) $mustBeLocalized,
      ],
    ];

    // Sort the array
    $enabled = [];
    foreach ($stepOrder as $index => $step) {
      $enabled[$index] = $step['enabled'] ? $index : 99999;
    }
    array_multisort($enabled, SORT_NUMERIC, $stepOrder);

    // Set the step order on the configuration object.
    $config->set('ContributionWizard', ['stepOrder' => $stepOrder]);

    // Unset the now useless old contribution type flag
    $config->clear('ContributionSettings.rubrics_use');

    // Unset the now obsolete localization flag
    $config->clear('ContributionSettings.contributions_must_be_localized');

    // Save the updates.
    $config->save();
  }
}

/**
 * Adapt menu settings of existing proceedings
 */
function dipas_update_10102() {
  $config_factory = \Drupal::configFactory();

  foreach ($config_factory->listAll('dipas.') as $configID) {
    // Get the proceeding's configuration object.
    $config = $config_factory->getEditable($configID);

    // Retrieve the current mainmenu configuration
    $mainmenu = $config->get('MenuSettings.mainmenu');

    // Retrieve the current footermenu configuration
    $footermenu = $config->get('MenuSettings.footermenu');

    // Make sure all menu items have a weight attached to them
    foreach (['main', 'footer'] as $menu) {
      $index = 0;
      foreach (${"{$menu}menu"} as &$item) {
        if (!isset($item['weight'])) {
          $item['weight'] = $index++;
        }
      }
    }

    // Make sure only one "contributions" link is active (if both legacy links are configured)
    // We'll leave the link name unchanged and up to the project administrator to edit
    if (isset($mainmenu['contributionmap']) && isset($mainmenu['contributionlist'])) {
      $mainmenu['contributions'] = $mainmenu['contributionmap'];
      unset($mainmenu['contributionmap']);
      unset($mainmenu['contributionlist']);
    }

    // Remove the former "FAQ" footer menu entry (moved to sidebar)
    unset($footermenu['faq']);

    // Add the new "accessibility" endpoint (disabled by default)
    $footermenu['accessibility'] = [
      'enabled' => FALSE,
      'name' => 'Barrierefreiheit',
      'weight' => 9999,
      'node' => ''
    ];

    // Write back all changes to the mainmenu
    $config->set('MenuSettings.mainmenu', $mainmenu);

    // Write back all changes to the footermenu
    $config->set('MenuSettings.footermenu', $footermenu);

    // Save the changes to the configuration
    $config->save();
  }
}

/**
 * Migrate legacy contribution teaser images to media library items.
 */
function dipas_update_10103(&$sandbox) {
  /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
  $entity_type_manager = \Drupal::service('entity_type.manager');

  /* @var \Drupal\Core\Entity\ContentEntityStorageInterface[] $storageInterfaces */
  $storageInterfaces = [
    'node' => $entity_type_manager->getStorage('node'),
    'media' => $entity_type_manager->getStorage('media'),
    'file' => $entity_type_manager->getStorage('file'),
  ];

  if (!isset($sandbox['total'])) {
    $conceptions = $storageInterfaces['node']->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'conception', '=')
      ->execute();
    $sandbox['total'] = count($conceptions);
    $sandbox['current'] = 0;

    if (empty($sandbox['total'])) {
      $sandbox['#finished'] = 1;
      return;
    }
  }

  $conceptions_per_batch = 25;
  $conception_ids = $storageInterfaces['node']->getQuery()
    ->accessCheck(FALSE)
    ->condition('type', 'conception', '=')
    ->range($sandbox['current'], $conceptions_per_batch)
    ->execute();

  if (empty($conception_ids)) {
    $sandbox['#finished'] = 1;
    return;
  }

  foreach ($conception_ids as $conception_id) {
    /* @var \Drupal\node\NodeInterface $conception */
    $conception = $storageInterfaces['node']->load($conception_id);

    if ($conception_image_field_value = $conception->get('field_conception_image')->first()) {
      $domain = $conception->get('field_domain_access')->getValue();
      $conception_image_id = $conception_image_field_value->get('target_id')->getString();
      $conception_image_file_entity = $storageInterfaces['file']->load($conception_image_id);
      $conception_image_filename = $conception_image_file_entity->get('filename')->getString();
      $conception_image_alt = $conception_image_field_value->get('alt')->getString();

      $new_media_item = $storageInterfaces['media']->create([
        'bundle' => 'image',
        'status' => 1,
        'name' => sprintf(
          '%1$s (Generated media library item: teaser image of Conception "%2$s" (Node-ID %3$d))',
          $conception_image_filename,
          $conception->label(),
          $conception->id()
        ),
        'field_media_image' => [
          'target_id' => $conception_image_id,
          'alt' => $conception_image_alt,
        ],
        'field_domain_access' => $domain,
      ]);

      $new_media_item->save();

      $conception->set('field_conception_media_image', $new_media_item);
      $conception->save();
    }

    $sandbox['current']++;
  }

  \Drupal::messenger()->addMessage($sandbox['current'] . ' conceptions processed.');

  if ($sandbox['current'] >= $sandbox['total']) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * Migrate the previously hardcoded color values for rubric terms to a new color field.
 */
function dipas_update_10104() {
  $config_factory = \Drupal::configFactory();
  $entity_type_manager = \Drupal::entityTypeManager();
  $termStorage = $entity_type_manager->getStorage('taxonomy_term');
  $termColors = ['#003063', '#703980', '#C63C76', '#FB624F', '#FFA600', '#8F9800', '#107C10', '#BAD0AF', '#033005', '#FFFFFF', '#488F31', '#000000'];

  foreach ($config_factory->listAll('dipas.') as $configID) {
    [, $proceedingID,] = explode('.', $configID);
    $rubricTermIDs = $termStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('vid', 'rubrics', '=')
      ->condition('field_domain_access', $proceedingID, '=')
      ->execute();

    if (count($rubricTermIDs)) {
      $rubricTerms = array_values($termStorage->loadMultiple($rubricTermIDs));

      foreach ($rubricTerms as $index => $term) {
        $color = $index < count($termColors)
          ? $termColors[$index]
          : '#' . strtoupper(dechex(rand(16, 255))) . strtoupper(dechex(rand(16, 255))) . strtoupper(dechex(rand(16, 255)));

        $term->set('field_color', [['color' => $color, 'opacity' => NULL]]);
        $term->save();
      }
    }
  }
}
