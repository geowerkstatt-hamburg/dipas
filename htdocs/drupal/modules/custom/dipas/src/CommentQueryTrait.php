<?php

namespace Drupal\dipas;

trait CommentQueryTrait {

  private function __getCommentsQuery(
    string $recursiveQueryName,
    string $query,
    array $nodeIDs = [],
    string $nodeType = NULL,
    array $limitToDomains = [],
    int $start = NULL,
    int $end = NULL
  ) {
    $recursiveQueryName = 'comments_cte';

    $recursiveBasequery = "SELECT cfd_base.entity_id, cfd_base.cid FROM comment_field_data \"cfd_base\" WHERE entity_type='node' AND cfd_base.status = '1'";

    $recursiveSubquery = sprintf(
      "SELECT %1\$s.entity_id, cfd.cid FROM %1\$s JOIN comment_field_data \"cfd\" on cfd.entity_type='comment' and cfd.entity_id = %1\$s.cid AND cfd.status = '1'",
      $recursiveQueryName
    );

    $recursiveQueryExtension = sprintf(
      'WITH RECURSIVE %1$s AS (%2$s UNION ALL %3$s)',
      $recursiveQueryName,
      $recursiveBasequery,
      $recursiveSubquery
    );

    $query .= ' LEFT JOIN node_field_data "n" ON n.nid = cte.entity_id';

    if (!empty($limitToDomains)) {
      $query .= ' LEFT JOIN node__field_domain_access "assigned_domains" ON n.type = assigned_domains.bundle AND n.nid = assigned_domains.entity_id AND n.vid = assigned_domains.revision_id  AND assigned_domains.deleted = 0';
      $query .= ' LEFT JOIN node__field_domain_all_affiliates "assigned_to_all" ON n.type = assigned_to_all.bundle AND n.nid = assigned_to_all.entity_id AND n.vid = assigned_to_all.revision_id  AND assigned_to_all.deleted = 0';
    }

    $query .= " WHERE n.status = '1' ";

    if (!empty($nodeType)) {
      $query .= sprintf("AND n.type = '%s' ", $nodeType);
    }

    if (!empty($limitToDomains)) {
      $query .= sprintf(
        "AND (assigned_domains.field_domain_access_target_id IN ('%s') OR assigned_to_all.field_domain_all_affiliates_value = 1) ",
        implode("', '", $limitToDomains)
      );
    }

    if (!empty($nodeIDs)) {
      $query .= sprintf('AND entity_id IN (%s) ', implode(', ', $nodeIDs));
    }

    if ($start !== NULL) {
      $query .= sprintf("AND n.created >= '%s' ", $start);
    }

    if ($end !== NULL) {
      $query .= sprintf("AND n.created <= '%s' ", $end);
    }

    return "$recursiveQueryExtension $query";
  }

  final protected function getCommentEntityIDsForNodes(
    array $nodeIDs = [],
    string $nodeType = NULL,
    array $limitToDomains = [],
    int $start = NULL,
    int $end = NULL
  ) {
    $recursiveQueryName = 'comments_cte';

    $query = $this->__getCommentsQuery(
      $recursiveQueryName,
      sprintf('SELECT cte.entity_id, cte.cid FROM %1$s "cte"', $recursiveQueryName),
      $nodeIDs,
      $nodeType,
      $limitToDomains,
      $start,
      $end
    );

    $query .= ' ORDER BY cte.entity_id, cte.cid ASC';

    $result = $this->getDatabase()->query($query)->fetchAll();

    $return = [];

    foreach ($result as $row) {
      if (!isset($return[$row->entity_id])) {
        $return[$row->entity_id] = [];
      }

      $return[$row->entity_id][] = $row->cid;
    }

    return $return;
  }

  final protected function getCommentEntityIDsByYears(
    array $nodeIDs = [],
    string $nodeType = NULL,
    array $limitToDomains = [],
    int $start = NULL,
    int $end = NULL
  ) {
    $recursiveQueryName = 'comments_cte';

    $query = $this->__getCommentsQuery(
      $recursiveQueryName,
      sprintf('SELECT DATE_PART(\'YEAR\', TO_TIMESTAMP(n.created)) AS "year", cte.cid FROM %1$s "cte"', $recursiveQueryName),
      $nodeIDs,
      $nodeType,
      $limitToDomains,
      $start,
      $end
    );

    $query .= ' ORDER BY year, cte.cid ASC';

    $result = $this->getDatabase()->query($query)->fetchAll();

    $return = [];

    foreach ($result as $row) {
      if (!isset($return[$row->year])) {
        $return[$row->year] = [];
      }

      $return[$row->year][] = $row->cid;
    }

    return $return;
  }

  /**
   * Executes a recursive database query to determine the number of comments on nodes
   *
   * @param array $nodeIDs
   *   The node ids of the nodes the comment count should be determined for
   * @param string $nodeType
   *   The node type the results should get constrained to
   * @param array $limitToDomains
   *   Limit the result set to nodes belonging to the given domain ids (empty array for all domains).
   * @param int $start
   *   Date of first comment to select (UNIX timestamp)
   * @param int $end
   *   Date of last comment to select (UNIX timestamp)
   * @param int $limit
   *   The number of rows the result should get limited to (0 for no limit)
   *
   * @return array
   *   An array containing the comment count for each node id, keyed by the node id.
   *
   * @throws \Drupal\Core\Database\IntegrityConstraintViolationException
   */
  final protected function getCommentCountForNodes(
    array $nodeIDs = [],
    string $nodeType = NULL,
    array $limitToDomains = [],
    int $start = NULL,
    int $end = NULL,
    int $limit = 0
  ) {
    $recursiveQueryName = 'comments_cte';

    $query = $this->__getCommentsQuery(
      $recursiveQueryName,
      sprintf('SELECT cte.entity_id, COUNT(*) AS "comments" FROM %1$s "cte"', $recursiveQueryName),
      $nodeIDs,
      $nodeType,
      $limitToDomains,
      $start,
      $end
    );

    $query .= 'GROUP BY cte.entity_id';

    if (!empty($nodeType)) {
      $query .= ', n.type';
    }

    if (!empty($limitToDomains)) {
      $query .= ', assigned_domains.field_domain_access_target_id, assigned_to_all.field_domain_all_affiliates_value';
    }

    $query .= ' ORDER BY comments DESC ';

    if ($limit > 0) {
      $query .= "LIMIT 3 OFFSET 0";
    }

    $result = $this->getDatabase()->query($query)->fetchAll();

    return array_combine(
      array_map(function ($row) { return $row->entity_id; }, $result),
      array_map(function ($row) { return (int) $row->comments; }, $result)
    );
  }

  /**
   * Executes a recursive database query to determine the number of comments per year
   *
   * @param array $nodeIDs
   *   The node ids of the nodes the comment count should be determined for
   * @param string $nodeType
   *   The node type the results should get constrained to
   * @param array $limitToDomains
   *   Limit the result set to nodes belonging to the given domain ids (empty array for all domains).
   * @param int $start
   *   Date of first comment to select (UNIX timestamp)
   * @param int $end
   *   Date of last comment to select (UNIX timestamp)
   * @param int $limit
   *   The number of rows the result should get limited to (0 for no limit)
   *
   * @return array
   *   An array containing the comment count for each node id, keyed by the year.
   *
   * @throws \Drupal\Core\Database\IntegrityConstraintViolationException
   */
  final protected function getCommentCountByYears(
    array $nodeIDs = [],
    string $nodeType = NULL,
    array $limitToDomains = [],
    int $start = NULL,
    int $end = NULL
  ) {
    $recursiveQueryName = 'comments_cte';

    $query = $this->__getCommentsQuery(
      $recursiveQueryName,
      sprintf('SELECT cte.entity_id, DATE_PART(\'YEAR\', TO_TIMESTAMP(n.created)) AS "year", COUNT(*) AS "comments" FROM %1$s "cte"', $recursiveQueryName),
      $nodeIDs,
      $nodeType,
      $limitToDomains,
      $start,
      $end
    );

    $query .= 'GROUP BY cte.entity_id, year';

    if (!empty($nodeType)) {
      $query .= ', n.type';
    }

    if (!empty($limitToDomains)) {
      $query .= ', assigned_domains.field_domain_access_target_id, assigned_to_all.field_domain_all_affiliates_value';
    }

    $query .= ' ORDER BY year ASC ';

    $result = $this->getDatabase()->query($query)->fetchAll();

    $return = [];

    foreach ($result as $row) {
      if (!isset($return[$row->year])) {
        $return[$row->year] = 0;
      }

      $return[$row->year] += $row->comments;
    }

    return $return;
  }

  /**
   * @return \Drupal\Core\Database\Connection
   */
  abstract protected function getDatabase();

}
