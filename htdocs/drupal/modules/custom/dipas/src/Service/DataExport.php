<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Service;

use Drupal\comment\CommentManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\dipas\CommentQueryTrait;
use Drupal\dipas\Plugin\ResponseKey\DateTimeTrait;
use Drupal\dipas\Plugin\ResponseKey\NodeListingTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class DataExport implements DataExportInterface {

  use NodeListingTrait;
  use DateTimeTrait;
  use StringTranslationTrait;
  use CommentQueryTrait;

  /**
   * @var string
   */
  protected $exportNodeType;

  /**
   * The Drupal database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\csv_serialization\Encoder\CsvEncoder
   */
  protected $csvEncoder;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commentStorage;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * @var \DateTimeZone
   */
  protected $utcTimeZone;

  /**
   * @var \DateTimeZone
   */
  protected $drupalTimeZone;

  /**
   * Export service constructor.
   *
   * @param \Drupal\Core\Database\Connection $db_connection
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   * @param \Symfony\Component\Serializer\Encoder\EncoderInterface $csv_encoder
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    Connection $db_connection,
    DateFormatterInterface $date_formatter,
    EncoderInterface $csv_encoder,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack
  ) {
    $this->database = $db_connection;
    $this->dateFormatter = $date_formatter;
    $this->csvEncoder = $csv_encoder;
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $request_stack->getCurrentRequest();

    $this->csvEncoder->setSettings([
      'delimiter' => ';',
      'enclosure' => '"',
      'escape_char' => '\\',
      'encoding' => 'utf8',
      'strip_tags' => FALSE,
      'trim' => TRUE,
    ]);
    $this->utcTimeZone = new \DateTimeZone('UTC');
    $this->drupalTimeZone = new \DateTimeZone(date_default_timezone_get());
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->commentStorage = $this->entityTypeManager->getStorage('comment');
  }

  /**
   * {@inheritdoc}
   */
  public function export($type) {
    switch ($type) {
      case 'contributions':
        $this->exportNodeType = 'contribution';
        $categories = $this->getTaxonomyTermArray('categories');
        $rubrics = $this->getTaxonomyTermArray('rubrics');
        $nodes = $this->getQuery()->execute()->fetchAll();
        $nodeIDs = $this->getNodeIDs($nodes);

        array_walk(
          $nodes,
          function (&$item) {
            $item = (array) $item;
          }
        );

        $nodes = array_map(
          function ($item) use ($categories, $rubrics) {
            return [
              'Node ID' => $item['nid'],
              'Category' => $categories[$item['category']],
              'Type' => $rubrics[$item['rubric']],
              'Created (UTC)' => $this->convertTimestampToUTCDateTimeString($item['created'], TRUE),
              'Title' => $item['title'],
              'Contributiontext' => str_replace(["\r\n", "\r", "\n"], ' ', $item['text']),
              'Location' => empty($item['geodata']) ? 'No' : 'Yes',
              'Contribution Type' => empty($item['geodata']) ? 'None' : $this->getContributionType($item['geodata']),
            ];
          },
          $nodes
        );

        $this->enrichContributionData($nodes, $nodeIDs);

        $data = $this->csvEncoder->encode($nodes, 'csv');
        $filename = 'contributions';
        break;

      case 'contribution_comments':
        $this->exportNodeType = 'contribution';
        $nodes = $this->getQuery()->execute()->fetchAll();
        $nodeIDs = $this->getNodeIDs($nodes);
        $comments = $this->getEntityComments($nodeIDs);
        $data = $this->csvEncoder->encode($comments, 'csv');
        $filename = 'contributioncomments';
        break;

      case 'conception_comments':
        $this->exportNodeType = 'conception';
        $nodes = $this->getQuery()->execute()->fetchAll();
        $nodeIDs = $this->getNodeIDs($nodes);
        $comments = $this->getEntityComments($nodeIDs);
        $data = $this->csvEncoder->encode($comments, 'csv');
        $filename = 'conceptioncomments';
        break;

      default:
        throw new NotFoundHttpException('The desired export type copuld not be found!');
    }

    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s.csv"', $filename));
    // Sets the BOM which helps to display special characters in Excel.
    $bom = chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'));
    $response->setContent($bom . $data);

    return $response;
  }

  /**
   * Helper function to extract the node IDs from the database result
   *
   * @param array $nodes
   *
   * @return array
   */
  protected function getNodeIDs(array $nodes) {
    return array_map(
      function ($item) {
        return $item->nid;
      },
      $nodes
    );
  }

  /**
   * Enriches contribution node data by number of comments and rating information.
   *
   * @param array $nodes
   *   The besic contribution data fetched so far
   */
  protected function enrichContributionData(array &$nodes, array $nodeIDs = []) {
    if (count($nodeIDs)) {
      $commentCounts = $this->getCommentCountForNodes($nodeIDs);

      $ratingQuery = $this->getDatabase()->select('votingapi_vote', 'upvotes');
      $ratingQuery->condition('upvotes.type', 'vote', '=');
      $ratingQuery->condition('upvotes.entity_type', 'node', '=');
      $ratingQuery->condition('upvotes.value', '1', '=');
      $ratingQuery->condition('upvotes.entity_id', $nodeIDs, 'IN');
      $ratingQuery->addField('upvotes', 'entity_id', 'nid');
      $ratingQuery->addExpression('COUNT(DISTINCT upvotes.id)', 'upvotes');
      $ratingQuery->groupBy('upvotes.entity_id');
      $result = $ratingQuery->execute()->fetchAll();
      $upvotes = array_combine(
        array_map(
          function ($row) {
            return $row->nid;
          },
          $result
        ),
        array_map(
          function ($row) {
            return $row->upvotes;
          },
          $result
        )
      );

      $ratingQuery = $this->getDatabase()->select('votingapi_vote', 'downvotes');
      $ratingQuery->condition('downvotes.type', 'vote', '=');
      $ratingQuery->condition('downvotes.entity_type', 'node', '=');
      $ratingQuery->condition('downvotes.value', '-1', '=');
      $ratingQuery->condition('downvotes.entity_id', $nodeIDs, 'IN');
      $ratingQuery->addField('downvotes', 'entity_id', 'nid');
      $ratingQuery->addExpression('COUNT(DISTINCT downvotes.id)', 'downvotes');
      $ratingQuery->groupBy('downvotes.entity_id');
      $result = $ratingQuery->execute()->fetchAll();
      $downvotes = array_combine(
        array_map(
          function ($row) {
            return $row->nid;
          },
          $result
        ),
        array_map(
          function ($row) {
            return $row->downvotes;
          },
          $result
        )
      );

      foreach ($nodes as &$node) {
        $nid = $node['Node ID'];

        $node['Comments'] = isset($commentCounts[$nid]) ? (int) $commentCounts[$nid] : 0;
        $node['Upvotes'] = isset($upvotes[$nid]) ? (int) $upvotes[$nid] : 0;
        $node['Downvotes'] = isset($downvotes[$nid]) ? (int) $downvotes[$nid] : 0;
        $node['Total votes'] = $node['Upvotes'] + $node['Downvotes'];
        $node['Rating'] = $node['Upvotes'] - $node['Downvotes'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDatabase() {
    return $this->database;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  protected function getJoins() {
    if ($this->exportNodeType === 'contribution') {
      $joins = [
        [
          'type' => 'LEFT',
          'table' => 'node__field_text',
          'alias' => 'text',
          'condition' => 'base.type = text.bundle AND base.nid = text.entity_id AND base.vid = text.revision_id AND attr.langcode = text.langcode AND text.deleted = 0',
          'fields' => [
            'field_text_value' => 'text',
          ],
        ],
        [
          'type' => 'LEFT',
          'table' => 'node__field_category',
          'alias' => 'category',
          'condition' => 'base.type = category.bundle AND base.nid = category.entity_id AND base.vid = category.revision_id AND attr.langcode = category.langcode AND category.deleted = 0',
          'fields' => [
            'field_category_target_id' => 'category',
          ],
        ],
        [
          'type' => 'LEFT',
          'table' => 'node__field_rubric',
          'alias' => 'rubric',
          'condition' => 'base.type = rubric.bundle AND base.nid = rubric.entity_id AND base.vid = rubric.revision_id AND attr.langcode = rubric.langcode AND rubric.deleted = 0',
          'fields' => [
            'field_rubric_target_id' => 'rubric',
          ],
        ],
        [
          'type' => 'LEFT',
          'table' => 'node__field_geodata',
          'alias' => 'geodata',
          'condition' => 'base.type = geodata.bundle AND base.nid = geodata.entity_id AND base.vid = geodata.revision_id AND attr.langcode = geodata.langcode AND geodata.deleted = 0',
          'fields' => [
            'field_geodata_value' => 'geodata',
          ],
        ],
      ];

      return $joins;
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getExpressions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getGroupBy() {
    if ($this->exportNodeType === 'contribution') {
      return [
        'text.field_text_value',
        'category.field_category_target_id',
        'rubric.field_rubric_target_id',
        'geodata.field_geodata_value',
      ];
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getConditions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSortingField() {
    return 'nid';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSortingDirection() {
    return 'ASC';
  }

  /**
   * Returns an array of taxonomy terms keyed by tid.
   *
   * @param $vocab
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTaxonomyTermArray($vocab) {
    $terms = $this->termStorage->loadTree($vocab, 0, NULL, TRUE);
    $return = [];
    foreach ($terms as $term) {
      /* @var \Drupal\taxonomy\TermInterface $term */
      $return[$term->id()] = $term->label();
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeType() {
    return $this->exportNodeType;
  }

  /**
   * Fetches and flattens all comments to entities.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getEntityComments(array $nodeIDs = []) {
    $return = [];

    if (count($nodeIDs)) {
      $entities = $this->nodeStorage->loadMultiple($nodeIDs);

      foreach ($entities as $entity) {
        /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $comments = $this->loadCommentsForEntity($entity);

        foreach ($comments as $commentStack) {
          $return[] = [
            'Contribution ID' => $entity->id(),
            'Comment ID' => $commentStack['cid'],
            'Comment Subject' => $commentStack['subject'],
            'Comment Text' => $commentStack['comment'],
            'created (UTC)' => $commentStack['created'],
          ];

          foreach ($commentStack['replies'] as $reply) {
            $return[] = [
              'Contribution ID' => $entity->id(),
              'Comment ID' => $reply['cid'],
              'Comment Subject' => $reply['subject'],
              'Comment Text' => $reply['comment'],
              'created (UTC)' => $reply['created'],
            ];
          }
        }
      }
    }

    return $return;
  }

  /**
   * Returns stored comments recursively.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to fetch comments.
   *
   * @return array
   *   The comments for the entity.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function loadCommentsForEntity(ContentEntityInterface $entity) {
    $commentList = [];
    $commentsField = $entity->getEntityTypeId() === 'node' ? 'field_comments' : 'field_replies';
    $comments = $this->commentStorage->loadThread($entity, $commentsField, CommentManagerInterface::COMMENT_MODE_FLAT);

    foreach ($comments as $comment) {
      /* @var \Drupal\comment\CommentInterface $comment */
      $subject = ($subject = $comment->get('subject')->first()) ? $subject->getString() : '';
      $commentListEntry = [
        'cid' => $comment->id(),
        'subject' => $subject !== '(No subject)' ? $subject : '',
        'comment' => $comment->get('field_comment')->first()->getString(),
        'created' => $this->convertTimestampToUTCDateTimeString($comment->getCreatedTime(), FALSE),
        'replies' => $this->loadCommentsForEntity($comment),
      ];
      $commentList[] = $commentListEntry;
    }

    return $commentList;
  }

  /**
   * Returns a string containing the contribution type.
   *
   * @param $geodata
   *
   * @return string
   */
  protected function getContributionType($geodata) {
    $geodata = json_decode($geodata);

    if (($contributionType = $geodata->geometry->type) === 'LineString') {
      return 'Line';
    }
    else {
      return $contributionType;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCurrentRequest() {
    return $this->currentRequest;
  }

}
