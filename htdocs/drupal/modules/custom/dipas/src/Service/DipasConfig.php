<?php

namespace Drupal\dipas\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\masterportal\DomainAwareTrait;

/**
 * The DipasConfig class proxies the ConfigEntity for the current active domain.
 *
 * @package Drupal\dipas\Service
 */
class DipasConfig implements DipasConfigInterface {

  use DomainAwareTrait {
    getActiveDomain AS traitGetActiveDomain;
  }

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   *   The Drupal config factory instance.
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig[]
   *   Array holding ImmutableConfiguration objects keyed by proceeding ID.
   */
  protected $configurations;

  /**
   * @var string
   */
  protected string $configurationDomain;

  /**
   * DipasConfig constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal's configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;

    $this->configurationDomain = $this->traitGetActiveDomain();

    // Initialize this instance with the configuration of the current domain.
    $this->configurations[$this->configurationDomain] = $this->configFactory->get(
      sprintf(
        'dipas.%s.configuration',
        $this->getActiveDomain()
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    return $this->configurations[$this->getActiveDomain()]->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->configurations[$this->getActiveDomain()]->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getEditable($id = 'dipas.default.domain'): Config {
    return $this->configFactory->getEditable($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    if ($this->isDomainModuleInstalled()) {

      $ids = [];
      /** @var \Drupal\domain\DomainStorage $domainStorage */
      $domainStorage = \Drupal::entityTypeManager()->getStorage('domain');

      $domains = $domainStorage->loadMultiple();

      foreach ($domains as $domain) {
        /** @var \Drupal\domain\Entity\Domain $domain */
        $ids[] = sprintf("dipas.%s.configuration", $domain->id());
      }

      return $ids;
    }

    return [
      'dipas.default.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDomain() {
    return $this->getActiveDomain();
  }

  /**
   * Overrides DomainAwareTraits functionality
   *
   * @return string
   */
  public function getActiveDomain() {
    return $this->configurationDomain;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigDomain(string $proceedingID) {
    $this->configurationDomain = $proceedingID;

    if (!isset($this->configurations[$this->configurationDomain])) {
      $this->configurations[$this->configurationDomain] = $this->configFactory->get(
        sprintf(
          'dipas.%s.configuration',
          $this->getActiveDomain()
        )
      );
    }
  }

}
