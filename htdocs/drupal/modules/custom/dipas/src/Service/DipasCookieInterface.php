<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Service;

use stdClass;

interface DipasCookieInterface {

  /**
   * Returns a cookie object.
   *
   * @param String $cookieName
   *   Name of the cookie to get (defaults to 'dipas')
   *
   * @return \Symfony\Component\HttpFoundation\Cookie
   */
  public function getCookie($cookieName = 'dipas');

  /**
   * Returns TRUE if cookies has been enabled.
   *
   * @return bool
   */
  public function hasCookiesEnabled();

  /**
   * Sets the name of the cookie to deal with
   *
   * @param string $name
   *
   * @return void
   */
  public function setCookieName($name);

  /**
   * Returns the data of the Diüpas cookie or FALSE, if no cookie is present.
   *
   * @param String $cookieName
   *   Name of the cookie to get (defaults to 'dipas')
   *
   * @return array|FALSE
   */
  public function getCookieData($cookieName = 'dipas');

  /**
   * Set the cookie data.
   *
   * @param array $data
   *   Data to set as the cookie contents
   *
   * @param String $cookieName
   *   Name of the cookie to set the data for (defaults to 'dipas')
   */
  public function setCookieData(stdClass $data, $cookieName = 'dipas');

}
