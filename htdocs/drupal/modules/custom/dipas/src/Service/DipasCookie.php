<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Service;

use stdClass;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

class DipasCookie implements DipasCookieInterface {

  protected $cookieName = 'dipas';

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * @var array
   */
  protected $cookieData;

  /**
   * DipasCookie constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requests
   */
  public function __construct(RequestStack $requests) {
    $this->currentRequest = $requests->getCurrentRequest();
    $this->cookieData['dipas'] = $this->getCookieData() ?: (object) [];
  }

  /**
   * {@inheritdoc}
   */
  public function setCookieName($name) {
    $this->cookieName = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getCookie($cookieName = 'dipas') {
    return new Cookie(
      $cookieName,
      json_encode($this->cookieData[$cookieName]),
      time() + 60 * 60 * 24 * 365,
      '/',
      null,
      FALSE,

      FALSE,
      FALSE,
      'lax'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasCookiesEnabled() {
    return $this->currentRequest->cookies->has($this->cookieName);
  }

  /**
   * {@inheritdoc}
   */
  public function getCookieData($cookieName = 'dipas') {
    if (
      $this->hasCookiesEnabled() &&
      $this->currentRequest->cookies->has($cookieName) &&
      $cookiedata = $this->currentRequest->cookies->get($cookieName)
    ) {
      return json_decode($cookiedata);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCookieData(stdClass $data, $cookieName = 'dipas') {
    $this->cookieData[$cookieName] = $data;
  }

}
