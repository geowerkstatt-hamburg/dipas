<?php

namespace Drupal\dipas;

trait ReliablyHashValueTrait {

  private function __sortObject($object) {
    $array = (array) $object;
    ksort($array);

    foreach ($array as $key => $value) {
      if (is_array($value) || is_object($value)) {
        $array[$key] = $this->__sortObject($value);
      }
    }

    return $array;
  }

  final protected function reliablyHashValue($value) {
    if (!is_array($value) && !is_object($value)) {
      return md5($value);
    }
    else {
      return md5(serialize($this->__sortObject($value)));
    }
  }

}
