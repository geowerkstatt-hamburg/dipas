<?php

namespace Drupal\dipas;

trait ProceedingListingMethodsTrait {

  /**
   * Returns a list of available proceeding IDs.
   *
   * @param string $include
   *   Determines the type of proceedings that should get included (all|visible|active).
   *
   * @return string[]
   *   List of proceeding IDs.
   */
  protected function getProceedingIDs($include = 'all') {
    $proceedingids = drupal_static(
      'dipas_proceedingids',
      [
        'all' => null,
        'active' => null,
        'visible' => null,
      ]
    );

    $include = strtolower($include);
    if (!in_array($include, ["all", "visible", "active"])) {
      $include = "all";
    }

    if ($proceedingids['all'] === null) {
      $now = time();

      if (!$this->isDomainModuleInstalled()) {
        $proceedingids['all'] = ['default'];
        $proceedingids['visible'] = [];
        $proceedingids['active'] = [];

        $defaultConfig = $this->getConfig('dipas.default.configuration');
        $start = strtotime($defaultConfig->get('ProjectSchedule.project_start'));
        $end = strtotime($defaultConfig->get('ProjectSchedule.project_end'));

        if (!$defaultConfig->get('Export.proceeding_is_internal')) {
          $proceedingids['visible'][] = 'default';
        }

        if ($start <= $now && $now < $end) {
          $proceedingids['active'][] = 'default';
        }
      }
      else {
        $proceedingids['all'] = [];
        $proceedingids['active'] = [];
        $proceedingids['visible'] = [];

        $result = $this->getDatabase()->select('config', 'c')
          ->fields('c', ['name'])
          ->condition('c.name', 'dipas.%.configuration', 'LIKE')
          ->execute()
          ->fetchAll();

        foreach ($result as $configid) {
          [, $proceedingid,] = explode('.', $configid->name);

          if ($proceedingid === 'default') {
            continue;
          }

          $proceedingids['all'][] = $proceedingid;

          $proceedingConfig = $this->getConfig($proceedingid);

          if (!$proceedingConfig->get('Export.proceeding_is_internal')) {
            $proceedingids['visible'][] = $proceedingid;
          }

          $start = strtotime($proceedingConfig->get('ProjectSchedule.project_start'));
          $end = strtotime($proceedingConfig->get('ProjectSchedule.project_end'));

          if (
            $start && $end && $start <= $now && $now < $end &&
            !$proceedingConfig->get('Export.proceeding_is_internal')
          ) {
            $proceedingids['active'][] = $proceedingid;
          }
        }
      }
    }

    return $proceedingids[$include];
  }

  /**
   * Return a filtered proceedingId list.
   *
   * @param string $configProperty
   *   Name of the config property within the proceeding configuration
   *
   * @param string[]|int[] $filterTermIDs
   *   Term IDs to filter by
   *
   * @param string[]|FALSE $prefilteredProceedingIDs
   *   List of prefiltered proceeding ids. (optional)
   *
   * @param string $filterMethod
   *   Method to use for filtering (all, any, none)
   *
   * @return string[]
   *   List of filtered proceeding ids.
   */
  protected function filterProceedingsByAssignedTerms(string $configProperty, array $filterTermIDs, array|bool $prefilteredProceedingIDs = FALSE, string $filterMethod = 'any'): array {
    if ($prefilteredProceedingIDs === FALSE) {
      $prefilteredProceedingIDs = $this->getProceedingIDs('visible');
    }

    $proceedingConfigs = array_combine(
      $prefilteredProceedingIDs,
      array_map(function ($domainid) {
        return $this->getConfigFactory()->get(sprintf('dipas.%s.configuration', $domainid));
      }, $prefilteredProceedingIDs)
    );

    $filteredProceedingIds = array_filter($proceedingConfigs, function ($config) use ($filterMethod, $filterTermIDs, $configProperty) {
      $assignedTermIDs = array_map(function ($termID) {
        return (int) $termID;
      }, $config->get(sprintf('ProjectInformation.%s', $configProperty)) ?? []);

      $filterTermIDs = array_map(function ($termID) {
        return (int) $termID;
      }, $filterTermIDs);

      switch ($filterMethod) {
        case 'all':
          return empty(array_diff($filterTermIDs, $assignedTermIDs));

        case 'any':
          return count(array_intersect($filterTermIDs, $assignedTermIDs)) >= 1;

        case 'none':
          return count(array_intersect($filterTermIDs, $assignedTermIDs)) === 0;
      }
    });

    return array_keys($filteredProceedingIds);
  }

  /**
   * @return \Drupal\Core\Database\Connection
   */
  abstract protected function getDatabase();

  /**
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   */
  abstract protected function getConfigFactory();

  /**
   * @return \Drupal\dipas\Service\DipasConfigInterface
   */
  abstract protected function getDipasConfig();

  /**
   * Helper function to retrieve the proceeding configuration object.
   *
   * @param string $domainid
   *   The proceeding id.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The desired configuration object.
   */
  abstract protected function getConfig($domainid);

  /**
   * @return bool
   */
  abstract protected function isDomainModuleInstalled();

  /**
   * Helper function to retrieve a list of assigned terms to a proceeding.
   *
   * @param String $vocab
   *   The vocabulary ID
   * @param array $include_fields
   *   An array of fields to include from the terms fetched in the form ['fieldname' => function () { return $value; }]
   * @param array|NULL $assigned_ids
   *   Array holding the assigned term ids
   * @param String $flatten_to_property
   *   Should a term get flattened to a specific property?
   *
   * @return array
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  abstract protected function getAssignedTerms($vocab, array $include_fields, $assigned_ids, $flatten_to_property = FALSE);
}
