<?php

namespace Drupal\dipas\Event;

use Symfony\Contracts\EventDispatcher\Event;

class DipasConfigurationPluginEventAlterDefaults extends Event implements DipasConfigurationPluginEventInterface {

  const EVENT_NAME = DipasEvents::ConfigurationPluginAlterDefaults;

  /**
   * @var array
   */
  protected $pluginDefinition;

  /**
   * @var array
   */
  protected $defaultValues;

  public function __construct(
    array $pluginDefinition,
    array $defaultValues
  ) {
    $this->pluginDefinition = $pluginDefinition;
    $this->defaultValues = $defaultValues;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginValues() {
    return $this->defaultValues;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginValues(array $values) {
    $this->defaultValues = $values;
  }

}
