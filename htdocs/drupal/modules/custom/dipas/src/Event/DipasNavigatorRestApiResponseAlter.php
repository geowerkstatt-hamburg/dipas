<?php

namespace Drupal\dipas\Event;

class DipasNavigatorRestApiResponseAlter extends DipasNavigatorRestApiResponseAlterBase {

  const EVENT_NAME = DipasEvents::NavigatorRestApiResponseAlter;

}
