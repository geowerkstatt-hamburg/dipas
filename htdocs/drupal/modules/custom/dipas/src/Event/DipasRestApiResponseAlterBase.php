<?php

namespace Drupal\dipas\Event;

use Drupal\dipas\ResponseContent;
use Symfony\Contracts\EventDispatcher\Event;

abstract class DipasRestApiResponseAlterBase extends Event implements DipasRestApiResponseEventInterface {

  protected string $responseKey;

  protected ResponseContent $responseData;

  protected array $responseCacheTags;

  public function __construct(
    string $responseKey,
    ResponseContent $responseData,
    array $responseCacheTags
  ) {
    $this->responseKey = $responseKey;
    $this->responseData = $responseData;
    $this->responseCacheTags = $responseCacheTags;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseKey() {
    return $this->responseKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseData() {
    return $this->responseData->getResponseContent();
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseCacheTags() {
    return $this->responseCacheTags;
  }

  /**
   * {@inheritdoc}
   */
  public function setResponseData(array $responseData) {
    $this->responseData->updateContent($responseData);
  }

  /**
   * {@inheritdoc}
   */
  public function setResponseCacheTags(array $responseCacheTags) {
    $this->responseCacheTags = $responseCacheTags;
  }

}
