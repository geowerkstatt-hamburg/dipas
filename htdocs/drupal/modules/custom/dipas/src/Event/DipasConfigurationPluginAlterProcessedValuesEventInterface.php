<?php

namespace Drupal\dipas\Event;

interface DipasConfigurationPluginAlterProcessedValuesEventInterface extends DipasConfigurationPluginEventInterface {

  /**
   * @return array
   */
  public function getRawPluginValues();

}
