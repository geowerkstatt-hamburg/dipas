<?php

namespace Drupal\dipas\Event;

use Symfony\Contracts\EventDispatcher\Event;

class DipasConfigurationPluginEventAlterForm extends Event implements DipasConfigurationPluginAlterFormEventInterface {

  const EVENT_NAME = DipasEvents::ConfigurationPluginAlterForm;

  /**
   * @var array
   */
  protected $pluginDefinition;

  /**
   * @var array
   */
  protected $liveConfiguration;

  /**
   * @var array
   */
  protected $pluginForm;

  public function __construct(
    array $pluginDefinition,
    array $liveConfiguration,
    array $pluginForm
  ) {
    $this->pluginDefinition = $pluginDefinition;
    $this->liveConfiguration = $liveConfiguration;
    $this->pluginForm = $pluginForm;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLiveConfiguration() {
    return $this->liveConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginValues() {
    return $this->pluginForm;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginValues(array $form) {
    $this->pluginForm = $form;
  }

}
