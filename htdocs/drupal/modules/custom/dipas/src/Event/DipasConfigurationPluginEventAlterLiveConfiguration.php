<?php

namespace Drupal\dipas\Event;

use Symfony\Contracts\EventDispatcher\Event;

class DipasConfigurationPluginEventAlterLiveConfiguration extends Event implements DipasConfigurationPluginEventInterface {

  const EVENT_NAME = DipasEvents::ConfigurationPluginAlterLiveConfiguration;

  /**
   * @var array
   */
  protected $pluginDefinition;

  /**
   * @var array
   */
  protected $configurationValues;

  public function __construct(
    array $pluginDefinition,
    array $configurationValues
  ) {
    $this->pluginDefinition = $pluginDefinition;
    $this->configurationValues = $configurationValues;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginValues() {
    return $this->configurationValues;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginValues(array $values) {
    $this->configurationValues = $values;
  }

}
