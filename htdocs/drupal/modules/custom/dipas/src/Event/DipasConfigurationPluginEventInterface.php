<?php

namespace Drupal\dipas\Event;

interface DipasConfigurationPluginEventInterface {

  /**
   * @return string
   */
  public function getPluginId();

  /**
   * @return array
   */
  public function getPluginValues();

  /**
   * @param array $values
   *
   * @return void
   */
  public function setPluginValues(array $values);

}
