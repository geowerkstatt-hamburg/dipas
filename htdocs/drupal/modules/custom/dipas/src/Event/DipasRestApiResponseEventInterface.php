<?php

namespace Drupal\dipas\Event;

interface  DipasRestApiResponseEventInterface {

  /**
   * @return string
   */
  public function getResponseKey();

  /**
   * @return array
   */
  public function getResponseData();

  /**
   * @return array
   */
  public function getResponseCacheTags();

  /**
   * @param array $responseData
   *
   * @return void
   */
  public function setResponseData(array $responseData);

  /**
   * @param array $responseCacheTags
   *
   * @return void
   */
  public function setResponseCacheTags(array $responseCacheTags);

}
