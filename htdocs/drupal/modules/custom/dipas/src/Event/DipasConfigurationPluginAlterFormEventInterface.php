<?php

namespace Drupal\dipas\Event;

interface DipasConfigurationPluginAlterFormEventInterface extends DipasConfigurationPluginEventInterface {

  /**
   * @return array
   */
  public function getPluginLiveConfiguration();

}
