<?php

namespace Drupal\dipas\Event;

use Symfony\Contracts\EventDispatcher\Event;

class DipasConfigurationPluginEventAlterProcessedValues extends Event implements DipasConfigurationPluginAlterProcessedValuesEventInterface {

  const EVENT_NAME = DipasEvents::ConfigurationPluginAlterProcessedValues;

  /**
   * @var array
   */
  protected $pluginDefinition;

  /**
   * @var array
   */
  protected $pluginRawValues;

  /**
   * @var array
   */
  protected $pluginProcessedValues;

  public function __construct(
    array $pluginDefinition,
    array $pluginRawValues,
    array $pluginProcessedValues
  ) {
    $this->pluginDefinition = $pluginDefinition;
    $this->pluginRawValues = $pluginRawValues;
    $this->pluginProcessedValues = $pluginProcessedValues;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRawPluginValues() {
    return $this->pluginRawValues;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginValues() {
    return $this->pluginProcessedValues;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginValues(array $values) {
    $this->pluginProcessedValues = $values;
  }

}
