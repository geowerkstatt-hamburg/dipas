<?php

namespace Drupal\dipas\Event;

final class DipasEvents {

  public const RestApiResponseAlter = 'dipas_rest_api_response_alter';

  public const NavigatorRestApiResponseAlter = 'dipas_navigator_rest_api_response_alter';

  public const ConfigurationPluginAlterDefaults = 'dipas_configuration_plugin_alter_defaults';

  public const ConfigurationPluginAlterLiveConfiguration = 'dipas_configuration_plugin_alter_live_configuration';

  public const ConfigurationPluginAlterForm = 'dipas_configuration_plugin_alter_form';

  public const ConfigurationPluginAlterProcessedValues = 'dipas_configuration_plugin_alter_processed_values';

}
