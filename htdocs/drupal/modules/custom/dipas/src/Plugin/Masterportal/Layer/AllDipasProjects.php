<?php

namespace Drupal\dipas\Plugin\Masterportal\Layer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dipas\Plugin\CockpitDataResponse\ProceedingEntityTrait;
use Drupal\dipas\Plugin\ResponseKey\DateTimeTrait;
use Drupal\masterportal\Annotation\Layer;
use Drupal\masterportal\UrlFromUriFixTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a GeoJson layer of all dipas project areas for the Masterportal.
 *
 * @Layer(
 *   id = "alldipasprojects",
 *   title = @Translation("All DIPAS projects")
 * )
 *
 * @package Drupal\dipas\Plugin\Masterportal\Layer
 */
class AllDipasProjects extends AllProjectAreas implements ContainerFactoryPluginInterface {

  use DateTimeTrait,
    ProceedingEntityTrait,
    UrlFromUriFixTrait;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlgenerator;

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('extension.path.resolver')
    );
  }

  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    FileUrlGeneratorInterface $file_url_generator,
    ExtensionPathResolver $extension_path_resolver
  ) {
    parent::__construct($config_factory);

    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlgenerator = $file_url_generator;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayerDefinition() {
    $definition = parent::getLayerDefinition();

    $definition->clusterDistance = 20;
    $definition->styleId = 'alldipasprojectsstyles';
    $definition->gfiTheme = 'dipasCockpit';

    $definition->gfiAttributes = (object) [
      'status' => 'status',
      'status_icon' => 'status_icon',
      'proceeding' => 'proceeding',
      'themes' => 'themes',
      'description' => 'description',
      'responsible' => 'responsible',
      'link' => 'link',
      'documentation' => 'documentation',
      'document.name' => 'document.name',
      'document.url' => 'document.url',
      'document.icon' => 'document.icon',
      'dateStart' => 'dateStart',
      'dateEnd' => 'dateEnd',
      'numberContributions' => 'numberContributions',
      'numberComments' => 'numberComments',
    ];

    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getGeoJSONFeatures($requireAbsoluteURLs) {
    $features = parent::getGeoJSONFeatures();

    foreach ($features as $feature) {
      $dipasConfigDomain = $this->configFactory->get(
        sprintf(
          'dipas.%s.configuration',
          $feature->getProperty('id')
        )
      );

      $centerpoint = array_map(
        'floatval',
        explode(', ', $dipasConfigDomain->get('ProjectArea.project_area_centerpoint'))
      );

      $feature->setGeometry((object) [
        'type' => 'Point',
        'coordinates' => $centerpoint,
      ]);

      // Add missing properties for the DIPAS GFI Theme
      $feature->addProperty('description', $dipasConfigDomain->get('ProjectInformation.text'));
      $feature->addProperty('districts', join('; ', $this->getAssignedTerms('districts', [], $dipasConfigDomain->get('ProjectInformation.data_districtselection'), 'name')));
      $feature->addProperty('dateStart', $this->convertTimestampToUTCDateTimeString(
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_start')),
        TRUE
      ));
      $feature->addProperty('dateEnd', $this->convertTimestampToUTCDateTimeString(
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_end')),
        TRUE
      ));
      $feature->addProperty('numberContributions', (string) $this->getNodeCount($feature->getProperty('id'), 'contribution'));
      $feature->addProperty('numberComments', (string) $this->getCommentCount($feature->getProperty('id')));
      $feature->addProperty('documentation', $this->getDokumentationDetails($feature->getProperty('id')));

      if ($feature->getProperty('status') === 'aktiv') {
        $statusIcon = self::fromUriFixed(
            'base:/' . $this->getExtensionPathResolver()->getPath('module', 'dipas') .'/assets/',
            ['absolute' => TRUE]
          )->toString() . 'icon_active_gfi.svg';
      }
      elseif ($feature->getProperty('status') === 'inaktiv') {
        $statusIcon = self::fromUriFixed(
            'base:/' . $this->getExtensionPathResolver()->getPath('module', 'dipas') .'/assets/',
            ['absolute' => TRUE]
          )->toString() . 'icon_inactive_gfi.svg';
      }
      else {
        $statusIcon = self::fromUriFixed(
            'base:/' . $this->getExtensionPathResolver()->getPath('module', 'dipas') .'/assets/',
            ['absolute' => TRUE]
          )->toString() . 'icon_latest_gfi.svg';
      }

      $feature->addProperty('status_icon', $statusIcon);
    }

    return $features;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileUrlGenerator() {
    return $this->fileUrlgenerator;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExtensionPathResolver() {
    return $this->extensionPathResolver;
  }

}
