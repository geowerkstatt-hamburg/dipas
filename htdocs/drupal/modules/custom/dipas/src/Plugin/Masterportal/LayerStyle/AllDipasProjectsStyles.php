<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\Masterportal\LayerStyle;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\masterportal\Annotation\LayerStyle;
use Drupal\masterportal\PluginSystem\LayerStylePluginInterface;
use Drupal\masterportal\UrlFromUriFixTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProjectAreaStyle
 *
 * @LayerStyle(
 *   id = "alldipasprojectsstyles",
 *   title = @Translation("GeoJSON layer styles for the All DIPAS projects layer.")
 * )
 *
 * @package Drupal\dipas\Plugin\Masterportal\LayerStyle
 */
class AllDipasProjectsStyles implements LayerStylePluginInterface, ContainerFactoryPluginInterface {

  use UrlFromUriFixTrait;

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * @var String
   */
  protected $dipasModulePath;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('extension.path.resolver')
    );
  }

  public function __construct(
    ExtensionPathResolver $extensionPathResolver
  ) {
    $this->extensionPathResolver = $extensionPathResolver;

    $this->dipasModulePath = $this->extensionPathResolver->getPath('module', 'dipas');
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleObject() {
    $colors = [
      'aktivFill' => '#61bd2b',
      'aktivLine' => '#61bd2b',
      'inaktivFill' => '#3D3D3D',
      'inaktivLine' => '#3D3D3D',
    ];
    array_walk(
      $colors,
      function (&$hexColor) {
        $hexColor = substr($hexColor, 1);
        $hexColor = hexdec($hexColor);
        $hexColor = [
          ($hexColor & 0xFF0000) >> 16,
          ($hexColor & 0x00FF00) >> 8,
          $hexColor & 0x0000FF,
        ];
      }
    );

    return (object) [
      'styleId' => 'alldipasprojectsstyles',
      'rules' => [
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'aktiv',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString(),
            'imageName' => 'icon_active_map.svg',
            'imageScale'=> 1.8,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
          ],
        ],
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'inaktiv',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString(),
            'imageName' => 'icon_inactive_map.svg',
            'imageScale'=> 1.7,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
          ],
        ],
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'neu',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString(),
            'imageName' => 'icon_latest_map.svg',
            'imageScale'=> 1.8,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
          ],
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['dipas:alldipasprojectsstyles'];
  }

}
