<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\Masterportal\Layer;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\masterportal\Annotation\Layer;
use Drupal\masterportal\GeoJSONFeature;
use Drupal\masterportal\PluginSystem\LayerPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dipas\TaxonomyTermFunctionsTrait;
use Drupal\masterportal\DomainAwareTrait;

/**
 * Implements a GeoJson layer of all dipas project areas for the Masterportal.
 *
 * @Layer(
 *   id = "allprojectareas",
 *   title = @Translation("Project areas of all DIPAS projects")
 * )
 *
 * @package Drupal\dipas\Plugin\Masterportal\Layer
 */
class AllProjectAreas implements LayerPluginInterface, ContainerFactoryPluginInterface {

  use TaxonomyTermFunctionsTrait;
  use DomainAwareTrait;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Array of cache tags for the layer data.
   *
   * @var array
   */
  protected $cacheTags;

  /**
   * URL to the DIPAS base installation not containing any proceeding prefix.
   *
   * @var string
   */
  protected $dipasBaseRoute;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    $this->configFactory = $config_factory;

    $this->dipasBaseRoute = Url::fromRoute(
      '<front>',
      [],
      ['absolute' => TRUE]
    )->toString();

    if (preg_match(sprintf('~//%s\.~', $this->getActiveDomain()), $this->dipasBaseRoute)) {
      $this->dipasBaseRoute = preg_replace(
        sprintf('~//%s\.~', $this->getActiveDomain()),
        '//',
        $this->dipasBaseRoute
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLayerDefinition() {
    return (object) [
      'version' => 1,
      'styleId' => 'allprojectareasstyle',
      'gfiAttributes' => (object) [
        'proceeding' => 'Verfahren',
        'themes' => 'Themen',
        'status' => 'Status',
        'timeperiod' => 'Zeitraum',
        'responsible' => 'Zuständig',
        'link' => 'Link',
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getGeoJSONFeatures($requireAbsoluteURLs) {
    foreach ($this->configFactory->listAll('dipas.') as $dipas_config_identifier) {
      // skip default-Domain
      if (
        $this->isDomainModuleInstalled() &&
        $dipas_config_identifier === 'dipas.default.configuration'
      ) {
        continue;
      }

      $domain_id = preg_replace('/dipas\.(.+?)\.configuration/', '$1', $dipas_config_identifier);

      if (
        ($domain_config = $this->configFactory->get(sprintf('domain.record.%s', $domain_id))) &&
        !(
          !$domain_config->isNew() &&
          $domain_config->get('status')
        )
      ) {
        continue;
      }

      $dipasConfigDomain = $this->configFactory->get($dipas_config_identifier);

      if (!$project_area = json_decode($dipasConfigDomain->get('ProjectArea.project_area'))) {
        continue;
      };

      // Create a feature container for the current project.
      $featureObject = new GeoJSONFeature();

      // Add the geolocation information to it.
      if (!empty($project_area->geometry)) {
        $featureObject->setGeometry($project_area->geometry);
      }
      elseif ($project_area) {
        $featureObject->setGeometry($project_area);
      }

      // Add gfi information to it.
      $featureObject->addProperty('id', $domain_id);
      $featureObject->addProperty('proceeding', $dipasConfigDomain->get('ProjectInformation.site_name'));
      $featureObject->addProperty('themes', implode(", ", $this->getAssignedTerms('topics', [], $dipasConfigDomain->get('ProjectInformation.data_topicselection'), 'name')));
      $featureObject->addProperty('status', $this->getStatus($dipasConfigDomain));
      $featureObject->addProperty('timeperiod', $this->getProceedingTimeSpan($dipasConfigDomain));
      $featureObject->addProperty('responsible', $dipasConfigDomain->get('ProjectInformation.department'));
      $featureObject->addProperty(
        'link',
        preg_replace(
          '~://~',
          "://$domain_id.",
          preg_replace(
            '~/drupal/.*$~',
            '/#',
            $this->dipasBaseRoute
          )
        )
      );

      $features[] = $featureObject;
    }

    return $features;
  }

  /**
   * Returns a string indicating if the proceeding is active or inactive.
   *
   * @param object $dipasConfigDomain
   *   The config of the coresponding domain.
   *
   * @return String
   */
  protected function getStatus($dipasConfigDomain) {
    $startDate = strtotime($dipasConfigDomain->get('ProjectSchedule.project_start'));
    $endDate = strtotime($dipasConfigDomain->get('ProjectSchedule.project_end'));
    $now = time();

    if ($now >= $startDate && $now < $endDate) {
      return 'aktiv';
    }
    else {
      return 'inaktiv';
    }
  }

  /**
   * Returns a string indcluding the set time period in the dipas domain config.
   *
   * @param object $dipasConfigDomain
   *   The config of the coresponding domain.
   *
   * @return String
   */
  protected function getProceedingTimeSpan($dipasConfigDomain) {
    $startDate = strtotime($dipasConfigDomain->get('ProjectSchedule.project_start'));
    $endDate = strtotime($dipasConfigDomain->get('ProjectSchedule.project_end'));
    $startDateObject = new DateTime($dipasConfigDomain->get('ProjectSchedule.project_start'));
    $endDateObject = new DateTime($dipasConfigDomain->get('ProjectSchedule.project_end'));
    $durationDays = $startDateObject->diff($endDateObject)->format("%a");

    return date("d.m.Y", $startDate) . ' bis ' . date("d.m.Y", $endDate) . ' (' . $durationDays . ' Tage)';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['Layer:Projectarea'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermStorage() {
    return \Drupal::getContainer()->get('entity_type.manager')->getStorage('taxonomy_term');
  }

}
