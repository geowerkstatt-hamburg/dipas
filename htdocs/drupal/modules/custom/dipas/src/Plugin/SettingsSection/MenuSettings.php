<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SettingsSection;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dipas\Annotation\SettingsSection;

/**
 * Class MenuSettings.
 *
 * @SettingsSection(
 *   id = "MenuSettings",
 *   title = @Translation("Menu settings"),
 *   description = @Translation("Settings related to the site menus the user is provided with."),
 *   weight = 50,
 *   affectedConfig = {}
 * )
 *
 * @package Drupal\dipas\Plugin\SettingsSection
 */
class MenuSettings extends SettingsSectionBase {

  use NodeSelectionTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\masterportal\Service\InstanceServiceInterface
   */
  protected $instanceService;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * @var \stdClass
   */
  protected $mainmenu;

  /**
   * @var \stdClass
   */
  protected $footermenu;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(Container $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->instanceService = $container->get('masterportal.instanceservice');
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    return [
      'mainmenu' => [
        'projectinfo' => [
          'enabled' => 1,
          'name' => 'Über das Verfahren',
          'weight' => 0,
        ],
        'contributions' => [
          'enabled' => 1,
          'name' => 'Beitragskarte',
          'weight' => 0,
        ],
        'conceptionlist' => [
          'enabled' => 0,
          'name' => 'Entwürfe vergleichen',
          'weight' => 0,
        ],
        'schedule' => [
          'enabled' => 1,
          'name' => 'Termine',
          'mapinstance' => 'default',
          'weight' => 0,
        ],
        'survey' => [
          'enabled' => 1,
          'name' => 'Umfrage',
          'url' => '',
          'weight' => 0,
        ],
        'custompage' => [
          'enabled' => 0,
          'name' => 'eigene Seite',
          'node' => '',
          'weight' => 0,
        ],
        'statistics' => [
          'enabled' => 1,
          'name' => 'Auswertungen',
          'weight' => 0,
        ],
      ],
      'footermenu' => [
        'dataprivacy' => [
          'enabled' => 1,
          'name' => 'Datenschutz',
          'node' => '',
        ],
        'imprint' => [
          'enabled' => 1,
          'name' => 'Impressum',
          'node' => ''
        ],
        'contact' => [
          'enabled' => 1,
          'name' => 'Kontakt',
          'node' => '',
        ],
        'accessibility' => [
          'enabled' => 1,
          'name' => 'Barrierefreiheit',
          'node' => '',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $form = [
      '#tree' => TRUE,
      'mmtitle' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Main menu', [], ['context' => 'DIPAS']),
      ],
      'projectinfohint' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Hint: the menu item "projectinfo" cannot be activated unless there is a configured node on the "Landing page" settings tab.', [], ['context' => 'DIPAS']),
      ],
      'mainmenu' => [
        '#type' => 'table',
        '#attributes' => [
          'id' => 'mainmenu-table'
        ],
        '#tabledrag' => [[
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'draggable-weight',
        ]],
      ],
      'fmtitle' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Footer menu', [], ['context' => 'DIPAS']),
        '#attributes' => [
          'style' => 'margin: 50px 0 20px 0;',
        ],
      ],
      'footermenu' => [
        '#type' => 'table',
        '#attributes' => [
          'id' => 'footermenu-table'
        ],
        '#tabledrag' => [[
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'draggable-weight',
        ]],
      ],
    ];

    $defaults = static::getDefaults();
    $availableEndpoints = array_merge(
      array_keys(self::getDefaults()['mainmenu']),
      array_keys(self::getDefaults()['footermenu'])
    );

    foreach (['mainmenu', 'footermenu'] as $menu) {
      $menus = array_merge(
        $defaults[$menu],
        array_filter(
          $this->{$menu},
          function ($endpoint) use ($availableEndpoints) {
            return in_array($endpoint, $availableEndpoints);
          },
          ARRAY_FILTER_USE_KEY
        )
      );

      // Sort the menu items based on their weight
      $weights = [];
      $i = 0;
      foreach ($menus as $endpoint => $settings) {
        $weights[$endpoint] = isset($settings['weight']) ? (int) $settings['weight'] : ++$i;
      }
      array_multisort($weights, SORT_NUMERIC, $menus);

      foreach ($menus as $endpoint => $settings) {
        if ($endpoint === 'conceptionlist') {
          $enabledSettings = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enabled', [], ['context' => 'DIPAS']),
            '#default_value' => '',
            '#weight' => 10,
            '#attributes' => [
              'disabled' => 'disabled',
              'title' => $this->t(
                'This menu item will be enabled automatically if the project features a second phase and the project phase passed phase 1.',
                [],
                ['context' => 'DIPAS']
              ),
            ],
            '#states' => [
              'checked' => [':input[type="checkbox"][name="settings[ProjectSchedule][phase_2_enabled]"]' => ['checked' => TRUE]],
            ],
          ];
        }
        else {
          $enabledSettings = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enabled', [], ['context' => 'DIPAS']),
            '#default_value' => $settings['enabled'],
            '#weight' => 10,
          ];

          if ($endpoint === 'projectinfo') {
            $enabledSettings['#states'] = [
              'disabled' => ['select[name="settings[LandingPage][node]"]' => ['value' => '']],
            ];
          }
        }

        $form[$menu][$endpoint] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'container-inline',
              'draggable',
            ],
          ],
          'endpoint' => [
            '#type' => 'value',
            '#value' => $endpoint,
          ],
          'weight' => [
            '#type' => 'weight',
            '#title' => $this->t('Weight'),
            '#title_display' => 'invisible',
            '#default_value' => $settings['weight'] ?? 0,
            '#attributes' => [
              'class' => [
                'draggable-weight'
              ]
            ],
          ],
          'menuitem' => [
            '#type' => 'markup',
            '#markup' => $this->t('Endpoint: %endpoint', ['%endpoint' => $endpoint], ['context' => 'DIPAS']),
            '#prefix' => '<div class="endpointname">',
            '#suffix' => '</div>',
            '#weight' => 1,
          ],
          'enabled' => $enabledSettings,
          'name' => [
            '#type' => 'textfield',
            '#title' => $this->t('Name', [], ['context' => 'DIPAS']),
            '#default_value' => $settings['name'],
            '#required' => TRUE,
            '#size' => 20,
            '#weight' => 20,
          ],
        ];

        if (isset($defaults[$menu][$endpoint]['node'])) {
          $defaultValue = isset($settings['node'])
            ? $settings['node']
            : $defaults[$menu][$endpoint]['node'];

          $form[$menu][$endpoint]['node'] = [
            '#type' => 'select',
            '#title' => $this->t('Page', [], ['context' => 'DIPAS']),
            '#options' => $this->getPageOptions(),
            '#default_value' => $defaultValue,
            '#states' => [
              'required' => [sprintf(':input[type="checkbox"][name="settings[MenuSettings][%s][%s][enabled]"]', $menu, $endpoint) => ['checked' => TRUE]],
              'visible' => [sprintf(':input[type="checkbox"][name="settings[MenuSettings][%s][%s][enabled]"]', $menu, $endpoint) => ['checked' => TRUE]],
            ],
            '#weight' => 40,
          ];
        }

        if (isset($settings['mapinstance']) || isset($defaults[$menu][$endpoint]['mapinstance'])) {
          $form[$menu][$endpoint]['mapinstance'] = [
            '#type' => 'select',
            '#title' => $this->t('Map instance', [], ['context' => 'DIPAS']),
            '#required' => TRUE,
            '#empty_option' => $this->t('Please choose', [], ['context' => 'DIPAS']),
            '#options' => $this->instanceService->getInstanceOptions(['config', 'contribution']),
            '#states' => [
              'required' => [sprintf(':input[type="checkbox"][name="settings[MenuSettings][%s][%s][enabled]"]', $menu, $endpoint) => ['checked' => TRUE]],
            ],
            '#default_value' => $settings['mapinstance'],
            '#weight' => 50,
          ];
        }

        if (isset($settings['url']) || isset($defaults[$menu][$endpoint]['url'])) {
          $form[$menu][$endpoint]['url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('URL', [], ['context' => 'DIPAS']),
            '#default_value' => $settings['url'],
            '#states' => [
              'required' => [sprintf(':input[type="checkbox"][name="settings[MenuSettings][%s][%s][enabled]"]', $menu, $endpoint) => ['checked' => TRUE]],
            ],
            '#size' => 50,
            '#weight' => 60,
          ];
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProcessedValues(array $plugin_values, array $form_values) {
    $sectionsettings = [];

    foreach ($plugin_values as $menuname => $section) {
      if (!isset($sectionsettings[$menuname])) {
        $sectionsettings[$menuname] = [];
      }

      foreach ($section as $settings) {
        $sectionsettings[$menuname][$settings['endpoint']] = [
          'enabled' => (bool) $settings['enabled'],
          'name' => $settings['name'],
          'weight' => $settings['weight'],
        ];

        foreach (['node', 'mapinstance', 'url', 'overwriteFrontpage'] as $property) {
          if (isset($settings[$property])) {
            $sectionsettings[$menuname][$settings['endpoint']][$property] = $settings[$property];
          }
        }
      }
    }

    return $sectionsettings;
  }

  /**
   * {@inheritdoc}
   */
  public function onSubmit() {
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeStorage() {
    return $this->nodeStorage;
  }

}
