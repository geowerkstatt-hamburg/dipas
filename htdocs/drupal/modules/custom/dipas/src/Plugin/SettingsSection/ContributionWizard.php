<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SettingsSection;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dipas\Annotation\SettingsSection;
use Drupal\masterportal\Form\MultivalueRowTrait;

/**
 * Class ContributionWizard.
 *
 * @SettingsSection(
 *   id = "ContributionWizard",
 *   title = @Translation("Contribution Wizard"),
 *   description = @Translation("Settings related to the contributions wizard users utilize to create contributions."),
 *   weight = 25,
 *   affectedConfig = {}
 * )
 *
 * @package Drupal\dipas\Plugin\SettingsSection
 */
class ContributionWizard extends SettingsSectionBase {

  use MultivalueRowTrait;

  /**
   * @var array
   */
  protected $stepOrder;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(Container $container) {}

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    // fillInMandatory indicates, that a step can be displayed, but the user must not enter data to complete the wizard
    // If unstated (or TRUE), the user has to fill in data in the respective step in order to advance the wizard
    return [
      'stepOrder' => [
        ['id' => 'description', 'displayName' => 'Description', 'enabled' => TRUE, 'displayMandatory' => TRUE],
        ['id' => 'category', 'displayName' => 'Category', 'enabled' => TRUE, 'displayMandatory' => TRUE],
        ['id' => 'type', 'displayName' => 'Type', 'enabled' => TRUE, 'displayMandatory' => FALSE],
        ['id' => 'location', 'displayName' => 'Location', 'enabled' => TRUE, 'displayMandatory' => FALSE, 'fillInMandatory' => FALSE],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $availableWizardSteps = $this->stepOrder ?? self::getDefaults()['stepOrder'];

    $form = [
      'steps' => [
        '#type' => 'fieldgroup',
        '#title' => $this->t('Contribution wizard steps', [], ['context' => 'DIPAS']),
        '#description' => $this->t('In this interface you can set the wizard step order by drag and drop. Optional steps can be deactivated by unchecking the respective checkbox.', [], ['context' => 'DIPAS']),
        '#plugin' => 'ContributionWizard',
      ],
    ];

    $this->createMultivalueFormPortion(
      $form['steps'],
      'steps',
      $form_state,
      $availableWizardSteps,
      'No available wizard steps.'
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getInputRow($property, $delta, array $row_defaults, FormStateInterface $form_state) {
    $row = [
      'container' => [
        '#type' => 'container',

        'id' => [
          '#type' => 'value',
          '#value' => $row_defaults['id'],
        ],
        'name' => [
          '#type' => 'html_tag',
          '#tag' => 'h5',
          '#value' => '&#8597; ' . $this->t(self::getStepDefaults($row_defaults['id'], 'displayName'), [], ['context' => 'DIPAS']),
          '#attributes' => [
            'class' => [
              'sortHandle',
            ],
          ],
        ],
        'enabled' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enabled'),
          '#description' => $this->t(
            self::getStepDefaults($row_defaults['id'], 'displayMandatory')
              ? 'This is a mandatory step and can\'t be deactivated.'
              : 'To omit this step within the contribution wizard, uncheck this checkbox.',
            [],
            ['context' => 'DIPAS']
          ),
          '#default_value' => $row_defaults['enabled'],
          '#attributes' => [
            'data-wizardstep' => $row_defaults['id'],
            'disabled' => self::getStepDefaults($row_defaults['id'], 'displayMandatory'),
          ],
        ],
      ],
    ];

    if (self::getStepDefaults($row_defaults['id'], 'fillInMandatory') === FALSE) {
      $row['container']['fillInMandatory'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Fill-in mandatory', [], ['context' => 'DIPAS']),
        '#description' => $this->t(
          'If unchecked, the user may skip this wizard step without entering any data.',
          [],
          ['context' => 'DIPAS']
        ),
        '#default_value' => $row_defaults['fillInMandatory'] ?? TRUE,
        '#states' => [
          'visible' => [
            ':input[type="checkbox"][data-wizardstep="' . $row_defaults['id'] . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    if ($row_defaults['id'] === 'location') {
      // Need to encapsulate omithint inside a container since html_tag or markup can't use #states
      $row['container']['sectionDeactivated'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[type="checkbox"][name*="use_localization"]' => ['checked' => FALSE],
          ],
        ],

        'omithint' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Localization step omitted due to the localization feature disabled (see settings on the "Contributions" settings tab to the left)', [], ['context' => 'DIPAS']),
        ],
      ];

      $row['container']['enabled']['#states'] = [
        'visible' => [
          ':input[type="checkbox"][name*="use_localization"]' => ['checked' => TRUE],
        ],
      ];

      if (self::getStepDefaults($row_defaults['id'], 'fillInMandatory') === FALSE) {
        $row['container']['fillInMandatory']['#states'] = [
          'visible' => [
            ':input[type="checkbox"][name*="use_localization"]' => ['checked' => TRUE],
          ],
        ];
      }
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDataToAdd($property, array $current_state, array $user_input, $addSelectorValue, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected static function isSortable($property) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected static function hasOwnSortableHandleSelector($property) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function canRowsBeRemoved($property) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function canRowsBeAdded($property) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProcessedValues(array $plugin_values, array $form_values) {
    $configuredOrder = self::getData('steps', $plugin_values);

    // Remove the surplus "container" subarray
    array_walk($configuredOrder, function (&$elem) { $elem = $elem['container']; });

    // Make sure 'enabled' is carried properly, since disabled checkboxes get transferred as unchecked, as
    // well as the fillInMandatory property.
    array_walk($configuredOrder, function (&$elem) {
      $elem['enabled'] = $elem['enabled'] || self::getStepDefaults($elem['id'], 'displayMandatory');
      $elem['fillInMandatory'] = @$elem['fillInMandatory'] || self::getStepDefaults($elem['id'], 'fillInMandatory') !== FALSE;
    });

    // Sort the disabled steps at last position
    $enabled = [];
    foreach ($configuredOrder as $index => $row) {
      $enabled[$index] = $row['enabled'] ? $index : 99999;
    }
    array_multisort($enabled, SORT_NUMERIC, $configuredOrder);

    return [
      'stepOrder' => $configuredOrder,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onSubmit() {}

  /**
   * Helper function to retrieve the default settings of a given step id.
   *
   * @param string $id
   *   The ID of a specific requested step (optional). If not provided, all defaults keyed by theior ID will be returned.
   * @param string $property
   *   The single property requested (optional). All defaults of a given row will be provided if not stated.
   *
   * @return array
   */
  private static function getStepDefaults($id = FALSE, $property = FALSE) {
    $defaults = drupal_static('DIPAS_CONTRIBUITION_WIZARD_STEP_DEFAULTS', NULL);

    if (is_null($defaults)) {
      $defaults = array_combine(
        array_map(function ($elem) { return $elem['id']; }, self::getDefaults()['stepOrder']),
        self::getDefaults()['stepOrder']
      );
    }

    return $id
      ? $property ? ($defaults[$id][$property] ?? NULL) : $defaults[$id]
      : $defaults;
  }

}
