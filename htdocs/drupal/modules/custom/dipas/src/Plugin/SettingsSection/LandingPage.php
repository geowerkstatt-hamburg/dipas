<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SettingsSection;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\dipas\Annotation\SettingsSection;
use Drupal\masterportal\DomainAwareTrait;

/**
 * Class SidebarSettings.
 *
 * @SettingsSection(
 *   id = "LandingPage",
 *   title = @Translation("Landing page"),
 *   description = @Translation("Configure background image, greeting text etc for the landing page."),
 *   weight = 15,
 *   affectedConfig = {}
 * )
 *
 * @package Drupal\dipas\Plugin\SettingsSection
 */
class LandingPage extends SettingsSectionBase {

  use DomainAwareTrait,
    MediaSelectionTrait,
    NodeSelectionTrait;

  const DIPAS_LANDINGPAGE_TEXT_MAXLENGTH = 330;

  /**
   * Drupal's entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node storage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Media storage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * @var string
   */
  protected $background_image;

  /**
   * @var string
   */
  protected $text;

  /**
   * @var string
   */
  protected $node;

  /**
   * @var \stdClass
   */
  protected $buttons;

  /**
   * @var bool
   */
  protected $button_default_behavior;

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    return [
      'background_image' => NULL,
      'text' => '',
      'node' => '',
      'button_default_behavior' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(Container $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $button_options = [
      'default' => $this->t('Default behavior', [], ['context' => 'DIPAS']),
      'hidden' => $this->t('Hidden / invisible', [], ['context' => 'DIPAS']),
      'contributions' => $this->t('Page: Contributions', [], ['context' => 'DIPAS']),
      'conceptionlist' => $this->t('Page: Conceptions', [], ['context' => 'DIPAS']),
      'schedule' => $this->t('Page: Schedule', [], ['context' => 'DIPAS']),
      'survey' => $this->t('Page: Survey', [], ['context' => 'DIPAS']),
      'statistics' => $this->t('Page: Statistics', [], ['context' => 'DIPAS']),
      'custompage' => $this->t('Page: Custom page', [], ['context' => 'DIPAS']),
    ];

    $custom_button_label_max_length = 20;

    $section = [
      'text' => [
        '#type' => 'textarea',
        '#title' => $this->t('Welcome Text', [], ['context' => 'DIPAS']),
        '#required' => TRUE,
        '#description' => $this->t('Define the text displayed on the landing page in the upper section (max. @max characters).', ['@max' => self::DIPAS_LANDINGPAGE_TEXT_MAXLENGTH], ['context' => 'DIPAS']),
        '#default_value' => $this->text,
        '#maxlength' => self::DIPAS_LANDINGPAGE_TEXT_MAXLENGTH,
      ],

      'background_image' => [
        '#type' => 'select',
        '#options' => $this->getMediaOptions('image'),
        '#title' => $this->t('Landing page background image', [], ['context' => 'DIPAS']),
        '#description' => $this->t(
          'Select the media item that contains the background image for the landing page. If you need to create one first, click @here.',
          [
            '@here' => Link::fromTextAndUrl(
              $this->t('here', [], ['context' => 'DIPAS']),
              Url::fromRoute('entity.media.add_form', [
                'media_type' => 'image',
                'destination' => Url::fromRoute('dipas.configform')->toString(),
              ])
            )->toString(),
          ],
          ['context' => 'DIPAS']
        ),
        '#default_value' => $this->background_image,
      ],

      'node' => [
        '#type' => 'select',
        '#title' => $this->t('Project information page', [], ['context' => 'DIPAS']),
        '#options' => $this->getPageOptions(),
        '#default_value' => $this->node,
        '#required' => FALSE,
      ],

      'buttons' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Landing page buttons', [], ['context' => 'DIPAS']),
        '#description' => $this->t('Define how the buttons on the landing page work.', [], ['context' => 'DIPAS']),

        'button_default_behavior' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Default behavior', [], ['context' => 'DIPAS']),
          '#description' => $this->t('Uncheck, if you want to explicitly specify the target destinations for the landing page buttons', [], ['context' => 'DIPAS']),
          '#default_value' => $this->buttons['button_default_behavior'] ?? TRUE,
        ],

        'overridden_behavior' => [
          '#type' => 'fieldset',
          '#title' => $this->t('Overridden behavior', [], ['context' => 'DIPAS']),
          '#description' => $this->t('Define where the buttons on the landing page link to.', [], ['context' => 'DIPAS']),
          '#states' => [
            'visible' => [':input[type="checkbox"][name="settings[LandingPage][buttons][button_default_behavior]"]' => ['checked' => FALSE]],
          ],

          'left' => [
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => $this->t('Left button', [], ['context' => 'DIPAS']),

            'behavior' => [
              '#type' => 'select',
              '#title' => $this->t('Button behavior / target', [], ['context' => 'DIPAS']),
              '#options' => $button_options,
              '#default_value' => $this->buttons['overridden_behavior']['left']['behavior'] ?? NULL,
              '#states' => [
                'required' => [':input[type="checkbox"][name="settings[LandingPage][buttons][button_default_behavior]"]' => ['checked' => FALSE]],
              ],
            ],

            'text' => [
              '#type' => 'textfield',
              '#title' => $this->t('Button text', [], ['context' => 'DIPAS']),
              '#description' => $this->t('Maximum length: @max_length characters', ['@max_length' => $custom_button_label_max_length], ['context' => 'DIPAS']),
              '#default_value' => $this->buttons['overridden_behavior']['left']['text'] ?? NULL,
              '#maxlength' => $custom_button_label_max_length,
              '#states' => [
                'required' => [':input[type="checkbox"][name="settings[LandingPage][buttons][button_default_behavior]"]' => ['checked' => FALSE]],
              ],
            ],
          ],
          'right' => [
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => $this->t('Right button', [], ['context' => 'DIPAS']),

            'behavior' => [
              '#type' => 'select',
              '#title' => $this->t('Button behavior / target', [], ['context' => 'DIPAS']),
              '#options' => $button_options,
              '#default_value' => $this->buttons['overridden_behavior']['right']['behavior'] ?? NULL,
              '#states' => [
                'required' => [':input[type="checkbox"][name="settings[LandingPage][buttons][button_default_behavior]"]' => ['checked' => FALSE]],
              ],
            ],

            'text' => [
              '#type' => 'textfield',
              '#title' => $this->t('Button text', [], ['context' => 'DIPAS']),
              '#description' => $this->t('Maximum length: @max_length characters', ['@max_length' => $custom_button_label_max_length], ['context' => 'DIPAS']),
              '#default_value' => $this->buttons['overridden_behavior']['right']['text'] ?? NULL,
              '#maxlength' => $custom_button_label_max_length,
              '#states' => [
                'required' => [':input[type="checkbox"][name="settings[LandingPage][buttons][button_default_behavior]"]' => ['checked' => FALSE]],
              ],
            ],
          ],
        ],
      ],
    ];

    return $section;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProcessedValues(array $plugin_values, array $form_values) {
    return [
      'background_image' => $plugin_values['background_image'],
      'text' => $plugin_values['text'],
      'node' => $plugin_values['node'],
      'buttons' => $plugin_values['buttons'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onSubmit() {}

  /**
   * {@inheritdoc}
   */
  protected function getMediaStorage() {
    return $this->mediaStorage;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeStorage() {
    return $this->nodeStorage;
  }

}
