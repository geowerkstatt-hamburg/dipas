<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SettingsSection;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dipas\Annotation\SettingsSection;
use Drupal\dipas\TaxonomyTermFunctionsTrait;
use Drupal\masterportal\DomainAwareTrait;

/**
 * Class ProjectInformation.
 *
 * @SettingsSection(
 *   id = "ProjectInformation",
 *   title = @Translation("Project information"),
 *   description = @Translation("Basic project settings."),
 *   weight = 0,
 *   affectedConfig = {}
 * )
 *
 * @package Drupal\dipas\Plugin\SettingsSection
 */
class ProjectInformation extends SettingsSectionBase {

  use MediaSelectionTrait;
  use TaxonomyTermFunctionsTrait;
  use DomainAwareTrait;

  /**
   * Drupal's entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node storage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Drupals taxonomy term storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * Media storage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * @var string
   */
  protected $site_name;

  /**
   * @var string
   */
  protected $site_email_name;

  /**
   * @var string
   */
  protected $site_email_address;

  /**
   * @var string
   */
  protected $department;

  /**
   * @var string
   */
  protected $street1;

  /**
   * @var string
   */
  protected $street2;

  /**
   * @var string
   */
  protected $zip;

  /**
   * @var string
   */
  protected $city;

  /**
   * @var string
   */
  protected $contact_email;

  /**
   * @var string
   */
  protected $contact_telephone;

  /**
   * @var string
   */
  protected $contact_website;

  /**
   * @var string
   */
  protected $data_responsible;

  /**
   * @var array
   */
  protected $data_topicselection;

  /**
   * @var array
   */
  protected $data_districtselection;

  /**
   * @var array
   */
  protected $project_owners;

  /**
   * @var array
   */
  protected $topics;

  /**
   * @var array
   */
  protected $districts;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(Container $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaults() {
    return [
      'site_name' => '',
      'site_email_name' => '',
      'site_email_address' => '',
      'department' => '',
      'street1' => '',
      'street2' => '',
      'zip' => '',
      'city' => '',
      'topics' => [],
      'districts' => [],
      'contact_email' => '',
      'contact_telephone' => '',
      'contact_website' => '',
      'data_responsible' => '',
      'data_topicselection' => [],
      'data_districtselection' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $topics = $this->getTermList('topics');
    $districts = $this->getTermList('districts');
    $project_owners = $this->getTermList('project_owner');

    $section = [
      'site_settings' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Site-wide settings', [], ['context' => 'DIPAS']),
        '#description' => $this->t('Settings that are directly related to the website configuration.', [], ['context' => 'DIPAS']),

        'site_name' => [
          '#type' => 'textfield',
          '#title' => $this->t('Project title', [], ['context' => 'DIPAS']),
          '#description' => $this->t('The name of this DIPAS process.', [], ['context' => 'DIPAS']),
          '#required' => TRUE,
          '#default_value' => $this->site_name,
        ],
        'site_email_name' => [
          '#type' => 'textfield',
          '#title' => $this->t('Email: Sender name', [], ['context' => 'DIPAS']),
          '#description' => $this->t('The name that is used when emails are sent. Leave blank to use the project title above.', [], ['context' => 'DIPAS']),
          '#default_value' => $this->site_email_name,
        ],
        'site_email_address' => [
          '#type' => 'textfield',
          '#title' => $this->t('Email: sender address ("from")', [], ['context' => 'DIPAS']),
          '#description' => $this->t('The address that is used as the sender email address in outgoing emails.', [], ['context' => 'DIPAS']),
          '#required' => TRUE,
          '#default_value' => $this->site_email_address,
        ],
      ],

      'project_contact' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Project contact', [], ['context' => 'DIPAS']),
        '#description' => $this->t('Enter the contact details of the department responsible for this DIPAS project. This information will only be used visually/non-functional.', [], ['context' => 'DIPAS']),

        'project_owners' => [
          '#type' => 'select2',
          '#multiple' => true,
          '#title' => $this->t('Project Owner(s)'),
          '#options' => array_map(function ($project_owners) { return $project_owners['name']; }, $project_owners),
          '#description' => $this->t('Please select here all responible project owners.'),
          '#required' => TRUE,
          '#default_value' => $this->project_owners,
        ],

        'contact_details' => [
          '#type' => 'address',
          '#title' => $this->t('Department contact information', [], ['context' => 'DIPAS']),
          '#required' => TRUE,
          '#default_value' => [
            'organization' => $this->department,
            'address_line1' => $this->street1,
            'address_line2' => $this->street2,
            'postal_code' => $this->zip,
            'locality' => $this->city,
            'country_code' => 'DE',
            'given_name' => NULL,
            'additional_name' => NULL,
            'family_name' => NULL,
            'sorting_code' => NULL,
            'dependent_locality' => NULL,
            'administrative_area' => NULL,
            'langcode' => NULL,
          ],
          '#available_countries' => ['DE'],
          '#used_fields' => [
            AddressField::ORGANIZATION,
            AddressField::ADDRESS_LINE1,
            AddressField::ADDRESS_LINE2,
            AddressField::POSTAL_CODE,
            AddressField::LOCALITY,
          ],
        ],

        'contact_telephone' => [
          '#type' => 'tel',
          '#title' => $this->t('Telephone number', [], ['context' => 'DIPAS']),
          '#required' => TRUE,
          '#default_value' => $this->contact_telephone,
        ],

        'contact_email' => [
          '#type' => 'email',
          '#title' => $this->t('Email address', [], ['context' => 'DIPAS']),
          '#required' => TRUE,
          '#default_value' => $this->contact_email,
        ],

        'contact_website' => [
          '#type' => 'url',
          '#title' => $this->t('Website URL', [], ['context' => 'DIPAS']),
          '#required' => FALSE,
          '#default_value' => $this->contact_website,
        ],

        'additional_info' => [
          '#type' => 'fieldset',
          '#title' => $this->t('Additonal information (will be undisclosed)', [], ['context' => 'DIPAS']),
          '#description' => $this->t('Some additional information on the project which will not be displayed on the frontend.', [], ['context' => 'DIPAS']),

          'data_responsible' => [
            '#type' => 'textfield',
            '#title' => $this->t('Data responsible organization', [], ['context' => 'DIPAS']),
            '#required' => FALSE,
            '#default_value' => $this->data_responsible,
          ],

          'data_topicselection' => [
            '#type' => 'checkboxes',
            '#title' => $this->t('Topic selection', [], ['context' => 'DIPAS']),
            '#description' => $this->t('Choose at least one topic for your participation project. For further information hover the checkbox.', [], ['context' => 'DIPAS']),
            '#options' => array_map(function ($topic) { return $topic['name']; }, $topics),
            '#default_value' => is_array($this->data_topicselection) ? $this->data_topicselection : [],
            '#required' => TRUE,
          ],

          'data_districtselection' => [
            '#type' => 'checkboxes',
            '#title' => $this->t('District selection', [], ['context' => 'DIPAS']),
            '#description' => $this->t('Choose at least one main district or subarea for your participation project.', [], ['context' => 'DIPAS']),
            '#options' => array_map(function ($district) { return $district['name']; }, $districts),
            '#default_value' => is_array($this->data_districtselection) ? $this->data_districtselection : [],
            '#required' => TRUE,
          ],
        ],
      ],
    ];

    foreach ($topics as $tid => $value) {
      $section['project_contact']['additional_info']['data_topicselection'][$tid]['#attributes'] = array('title' => strip_tags($value['description'] ?? ''));
    }
    foreach ($districts as $tid => $value) {
      $section['project_contact']['additional_info']['data_districtselection'][$tid]['#attributes'] = array('title' => strip_tags($value['description'] ?? ''));
    }

    return $section;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProcessedValues(array $plugin_values, array $form_values) {
    return [
      'site_name' => $plugin_values['site_settings']['site_name'],
      'site_email_name' => $plugin_values['site_settings']['site_email_name'],
      'site_email_address' => $plugin_values['site_settings']['site_email_address'],
      'department' => $plugin_values["project_contact"]["contact_details"]["organization"],
      'street1' => $plugin_values["project_contact"]["contact_details"]["address_line1"],
      'street2' => $plugin_values["project_contact"]["contact_details"]["address_line2"],
      'zip' => $plugin_values["project_contact"]["contact_details"]["postal_code"],
      'city' => $plugin_values["project_contact"]["contact_details"]["locality"],
      'contact_email' => $plugin_values["project_contact"]['contact_email'],
      'contact_telephone' => $plugin_values["project_contact"]['contact_telephone'],
      'contact_website' => $plugin_values["project_contact"]['contact_website'],
      'data_responsible' => $plugin_values["project_contact"]['additional_info']['data_responsible'],
      'data_topicselection' => !is_null($plugin_values["project_contact"]['additional_info']['data_topicselection']) ? array_keys(array_filter($plugin_values["project_contact"]['additional_info']['data_topicselection'])) : '',
      'data_districtselection' => !is_null($plugin_values["project_contact"]['additional_info']['data_districtselection']) ? array_keys(array_filter($plugin_values["project_contact"]['additional_info']['data_districtselection'])) : '',
      'project_owners' => !is_null($plugin_values["project_contact"]['project_owners']) ? array_keys(array_filter($plugin_values["project_contact"]['project_owners'])) : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onSubmit() {}

  /**
   * {@inheritdoc}
   */
  protected function getTermStorage() {
    return $this->termStorage;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaStorage() {
    return $this->mediaStorage;
  }

}
