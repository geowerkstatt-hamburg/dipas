<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\dipas\Annotation\ResponseKey;
use Drupal\dipas\CommentQueryTrait;

/**
 * Class ContributionList.
 *
 * @ResponseKey(
 *   id = "contributionlist",
 *   description = @Translation("Returns a list of contributions currently contained in the database within given limits and filters."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true
 * )
 *
 * @package Drupal\dipas\Plugin\ResponseKey
 */
class ContributionList extends PagedNodeListingBase {

  use ContributionDetailsTrait;
  use CommentQueryTrait;

  /**
   * {@inheritdoc}
   */
  protected function postProcessNodeData(array &$nodes) {
    $nids = array_map(
      function ($row) {
        return (int) $row->nid;
      },
      $this->getNodes()
    );

    $commentCounts = $this->getCommentCountForNodes($nids);

    array_walk(
      $nodes,
      function (&$node) use ($commentCounts) {
        $node->comments = $commentCounts[$node->nid] ?? 0;
      }
    );

    if (
      $this->currentRequest->query->has('sort') &&
      ($field = $this->currentRequest->query->get('sort')) &&
      strtolower($field) === 'comments'
    ) {
      $commentCounts = [];

      foreach ($nodes as $index => $nodeData) {
        $commentCounts[$index] = $nodeData->comments;
      }

      array_multisort($commentCounts, SORT_NUMERIC, $nodes);

      if (
        $this->currentRequest->query->has('direction') &&
        ($field = $this->currentRequest->query->get('direction')) &&
        strtolower($field) === 'desc'
      ) {
        $nodes = array_reverse($nodes);
      }

      $start = ($this->getCurrentPage() - 1) * parent::itemsPerPage();
      $nodes = array_slice($nodes, $start, parent::itemsPerPage());
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getConditions() {
    $filters = [];

    if ($this->currentRequest->query->has('category')) {
      $filters[] = [
        'field' => 'category.field_category_target_id',
        'value' => explode(',', $this->currentRequest->query->get('category')),
        'operator' => 'IN'
      ];
    }

    if ($this->currentRequest->query->has('rubric')) {
      $filters[] = [
        'field' => 'rubric.field_rubric_target_id',
        'value' => explode(',', $this->currentRequest->query->get('rubric')),
        'operator' => 'IN'
      ];
    }

    if ($this->currentRequest->query->has('search')) {
      $textSearch = $this->getOrConditionGroup();

      $textSearch->condition('attr.title', '%' . $this->currentRequest->query->get('search') . '%', 'LIKE');
      $textSearch->condition('text.field_text_value', '%' . $this->currentRequest->query->get('search') . '%', 'LIKE');

      $filters[] = $textSearch;
    }

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSortingField() {
    if (
      $this->currentRequest->query->has('sort') &&
      ($field = $this->currentRequest->query->get('sort')) &&
      in_array(strtolower($field), ['created', 'rating'])
    ) {
      return strtolower($field);
    }

    return 'created';
  }

  /**
   * {@inheritdoc}
   */
  protected function itemsPerPage() {
    static $itemsPerPage = NULL;

    if (
      $this->currentRequest->query->has('sort') &&
      ($field = $this->currentRequest->query->get('sort')) &&
      strtolower($field) === 'comments'
    ) {
      $itemsPerPage = PagedNodeListingInterface::INFINITE_ITEMS_VALUE;

      return $itemsPerPage;
    }
    else {
      return parent::itemsPerPage();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getTotalPages() {
    if (
      $this->currentRequest->query->has('sort') &&
      ($field = $this->currentRequest->query->get('sort')) &&
      strtolower($field) === 'comments'
    ) {
      $originalItemsPerPage = parent::itemsPerPage();

      return (int) ceil($this->getCount() / $originalItemsPerPage);
    }
    else {
      return parent::getTotalPages();
    }
  }

}
