<?php

namespace Drupal\dipas\Plugin\ResponseKey;

use Symfony\Component\HttpFoundation\Request;

trait UrlProceedingsFilterTrait {

  /**
   * Helper method which detects, if URL filter params are present in the current request (singleton)
   *
   * @return bool
   *   TRUE if a configured filter parameter is found in the current request
   */
  protected function isURLFilterActive(): bool {
    $filterActive = drupal_static('dipas_proceeding_filter_state', NULL);

    if ($filterActive === NULL) {
      $filterActive = FALSE;

      foreach ($this->getURLProceedingFilterParams() as $URLParam => $config) {
          if (
            $this->getCurrentRequest()->query->has($URLParam) &&
            $this->getValidatedFilterValues($this->getCurrentRequest()->query->all()[$URLParam], $config['URLParamValidation'])
          ) {
            $filterActive = TRUE;
            break;
          }
      }
    }

    return $filterActive;
  }

  /**
   * Filters given proceeding IDs through a set of given URL filter params
   *
   * @param string[] $prefilteredProceedingIDs
   *   List of proceeding IDs.
   *
   * @return string[]
   *   List of filtered proceeding ids.
   */
  protected function retrieveFilteredProceedingIDsFromURL(array $prefilteredProceedingIDs = []): array {
    if (empty($prefilteredProceedingIDs)) {
      $prefilteredProceedingIDs = $this->getProceedingIDs('visible');
    }

    foreach ($this->getURLProceedingFilterParams() as $URLParam => $config) {

      if (is_null($config['configParamName'])) {
        continue;
      }

      if (
        $this->getCurrentRequest()->query->has($URLParam) &&
        $filterValue = $this->getValidatedFilterValues($this->getCurrentRequest()->query->all()[$URLParam], $config['URLParamValidation'])
      ) {
        $prefilteredProceedingIDs = $this->filterProceedingsByAssignedTerms($config['configParamName'], $filterValue, $prefilteredProceedingIDs);
      }
    }

    return $prefilteredProceedingIDs;
  }

  /**
   * Return a filtered proceedingId list.
   *
   * @param string $configProperty
   *   Name of the config property within the proceeding configuration
   *
   * @param string[]|int[] $filterTermIDs
   *   Term IDs to filter by
   *
   * @param string[]|FALSE $prefilteredProceedingIDs
   *   List of prefiltered proceeding ids. (optional)
   *
   * @param string $filterMethod
   *   Method to use for filtering (all, any, none)
   *
   * @return string[]
   *   List of filtered proceeding ids.
   */
  abstract protected function filterProceedingsByAssignedTerms(string $configProperty, array $filterTermIDs, array|bool $prefilteredProceedingIDs = FALSE, string $filterMethod = 'all'): array;

  /**
   * @return \Symfony\Component\HttpFoundation\Request
   */
  abstract protected function getCurrentRequest();

  /**
   * Returns a list of available proceeding IDs.
   *
   * @param string $include
   *   Determines the type of proceedings that should get included (all|visible|active).
   *
   * @return string[]
   *   List of proceeding IDs.
   */
  abstract protected function getProceedingIDs($include = 'all');

  /**
   * Provides a list of parameters to filter proceedings.
   *
   * Also used as the source to generated the cache key and validate values.
   *
   * Array-Structure:
   * [
   *   URL parameter name => [
   *     'configParamName' => string|NULL, Configuration parameter name (NULL, if there is no associated config),
   *     'URLParamValidation' => [
   *       'valueIsArray' => boolean, Request parameters to be expected as an array?
   *       'regex' => string, Regex to validate the request parameter
   *     ],
   *   ]
   *   ...
   * ]
   *
   * @return array
   *   List of parameters
   */
  abstract protected function getURLProceedingFilterParams(): array;

  /**
   * Validates value against a validation configuration.
   *
   * @param mixed $filterValue
   *   Value of the filter to validate
   *
   * @param array $validationConfig
   *   Validation configuration, see $this->getURLProceedingFilterParams()
   *
   *   Array-Structure:
   *   [
   *     'valueIsArray' => boolean, Request parameters to be expected as an array?
   *     'regex' => string, Regex to validate the request parameter
   *   ]
   *
   * @return mixed
   *   Returns the validated value
   */
  abstract protected function getValidatedFilterValues(mixed $filterValue, array $validationConfig): mixed;

}
