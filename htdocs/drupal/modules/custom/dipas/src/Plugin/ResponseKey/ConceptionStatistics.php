<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\dipas\Annotation\ResponseKey;

/**
 * Class ConceptionStatistics.
 *
 * @ResponseKey(
 *   id = "conceptionstatistics",
 *   description = @Translation("Returns the data for the statistics page."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true
 * )
 *
 * @package Drupal\dipas\Plugin\ResponseKey
 */
class ConceptionStatistics extends Statistics {

  /**
   * {@inheritdoc}
   */
  protected function getNodeType() {
    return 'conception';
  }

}
