<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\dipas\Service\EntityServicesInterface;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait NodeContentTrait {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function getPluginResponse() {
    if ($node = $this->loadNode()) {
      $content = [
        'nid' => $node->id(),
        'bundle' => $node->bundle(),
        'langcode' => $node->language()->getId(),
        'title' => $node->label(),
        'created' => $this->convertTimestampToUTCDateTimeString($node->getCreatedTime(), FALSE),
        'author' => $node->getRevisionUser()->getDisplayName(),
        'content' => [],
      ];

      foreach ($node->get('field_content')->getValue() as $fieldDelta) {
        /* @var \Drupal\Core\Entity\ContentEntityInterface $paragraph */
        if ($paragraph = $this->getEntityTypeManager()->getStorage('paragraph')->load($fieldDelta['target_id'])) {
          $content['content'][] = $this->parseEntityContent($paragraph, $node);
        }
      }

      if (
        $node->hasField('field_image_gallery') &&
        ($imageGallery = $node->get('field_image_gallery')) &&
        ($galleryContents = $imageGallery->getValue())
      ) {
        /* @var EntityServicesInterface $entityServices */
        $entityServices = \Drupal::service('dipas.entity_services');

        /* @var \Drupal\Core\Image\ImageFactory $imagefactory */
        $imagefactory = \Drupal::service('image.factory');

        /* @var \Drupal\image\ImageStyleInterface[] $imageStyles */
        $imageStyles = [
          'small' => ImageStyle::load('medium'),
          'large' => ImageStyle::load('large'),
        ];

        $images = $entityServices->loadEntities(
          'media',
          array_map(function ($item) { return $item['target_id']; }, $galleryContents)
        );

        $files = $entityServices->loadEntities(
          'file',
          array_map(
            function (ContentEntityInterface $image) use ($imagefactory) {
              return $image->get('field_media_image')->first()->get('target_id')->getString();
            },
            $images
          )
        );

        $image_gallery = [];

        foreach ($images as $imageEntity) {
          $file = $files[$imageEntity->get('field_media_image')->first()->get('target_id')->getString()];
          $image = $imagefactory->get($file->getFileUri());

          if ($image->isValid()) {
            $galleryImage = [
              'alt' => $imageEntity->get('field_media_image')->first()->get('alt')->getString(),
              'caption' => ($field = $imageEntity->get('field_caption')->first()) ? $field->getString() : '',
              'copyright' => ($field = $imageEntity->get('field_copyright')->first()) ? $field->getString() : '',
              'src' => $this->getFileUrlGenerator()->generateAbsoluteString($file->getFileUri()),
              'derivates' => [
                'small' => $imageStyles['small']->buildUrl($file->getFileUri()),
                'large' => $imageStyles['large']->buildUrl($file->getFileUri()),
              ],
            ];

            $image_gallery[] = $galleryImage;
          }
        }

        if (count($image_gallery)) {
          $content['image_gallery'] = $image_gallery;
        }
      }

      return $content;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Parse the contents of a given entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity to parse.
   * @param ContentEntityInterface $root_entity
   *   The root entity (just for logging purposes in case of faulty references)
   * @param string $viewmode
   *   The view mode the entity should get rendered in.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function parseEntityContent(ContentEntityInterface $entity, ContentEntityInterface $root_entity, $viewmode = 'default') {
    $result = [
      'type' => $entity->getEntityTypeId(),
      'bundle' => $entity->bundle(),
    ];

    if ($entity->getEntityTypeId() === 'node') {
      $result['nid'] = $entity->id();
      $result['title'] = $entity->label();
    }

    $bundleFields = \Drupal::service('dipas.entity_services')->getEntityTypeBundleFieldsInViewMode(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $viewmode,
      TRUE,
      ['field_comments', 'field_rating']
    );

    foreach ($bundleFields as $field => $definition) {
      foreach ($entity->get($field)->getValue() as $delta => $value) {
        switch ($definition->getType()) {
          case 'entity_reference_revisions':
          case 'entity_reference':
            $entityTypeId = $definition->getSetting('target_type');
            $entityId = $value['target_id'];

            if ($subentity = $this->getEntityTypeManager()->getStorage($entityTypeId)->load($entityId)) {
              $result[$field][$delta] = $this->parseEntityContent(
                $subentity,
                $root_entity,
                $definition->display['settings']['view_mode'] ?? 'default'
              );
            }
            else {
              $this->getLogger()->warning(
                'Content Entity %content_entity %root_entity refers in field %field on delta %delta to a missing subentity %subentity_type.',
                [
                  '%content_entity' => sprintf('%s/%s/%s', $entity->getEntityTypeId(), $entity->bundle(), $entity->id()),
                  '%root_entity' => sprintf('(root entity: %s/%s/%s)', $root_entity->getEntityTypeId(), $root_entity->bundle(), $root_entity->id()),
                  '%field' => $field,
                  '%delta' => $delta,
                  '%subentity_type' => sprintf('%s/%s', $entityTypeId, $entityId),
                ]
              );
            }
            break;

          case 'string':
          case 'text_long':
            $result[$field][$delta] = trim($entity->get($field)->getValue()[$delta]['value']);
            break;

          case 'video_embed_field':
            $formatter = \Drupal::service('plugin.manager.field.formatter')->createInstance(
              'video_embed_field_video',
              [
                'field_definition' => $definition,
                'settings' => $definition->display['settings'],
                'label' => $definition->display['label'],
                'view_mode' => 'default',
                'third_party_settings' => $definition->display['third_party_settings'],
              ]
            );
            $renderarray = $formatter->view($entity->get($field));
            $html = \Drupal::service('renderer')->render($renderarray);
            $iframe = simplexml_load_string($html)->xpath('//iframe')[0]->asXML();
            $iframe = preg_replace('~src=("|\')https?://~', 'src=\1//', $iframe);
            $result[$field][$delta] = $iframe;
            break;

          case 'image':
            if ($file = $this->getEntityTypeManager()->getStorage('file')->load($value['target_id'])) {
              switch ($definition->display['type']) {
                case 'responsive_image':
                  $image = \Drupal::service('image.factory')->get($file->getFileUri());
                  $renderarray = [
                    '#theme' => 'responsive_image',
                    '#width' => $image->isValid() ? $image->getWidth() : NULL,
                    '#height' => $image->isValid() ? $image->getHeight() : NULL,
                    '#responsive_image_style_id' => $definition->display['settings']['responsive_image_style'],
                    '#uri' => $file->getFileUri(),
                  ];
                  $html = trim(\Drupal::service('renderer')->render($renderarray));
                  $result[$field][$delta]['html'] = $html;
                  $result[$field][$delta]['origin'] = $this->getFileUrlGenerator()->generateAbsoluteString($file->getFileUri());
                  break;

                case 'image':
                default:
                  if (!empty($definition->display['settings']['image_style'])) {
                    $result[$field][$delta]['url'] = $this->stripProtocolIndicatorFromUrl(
                      ImageStyle::load($definition->display['settings']['image_style'])->buildUrl($file->getFileUri())
                    );
                  }
                  else {
                    $result[$field][$delta]['url'] = $this->stripProtocolIndicatorFromUrl(
                      $this->getFileUrlGenerator()->generateAbsoluteString($file->getFileUri())
                    );
                  }
                  $result[$field][$delta]['origin'] = $this->stripProtocolIndicatorFromUrl(
                    $this->getFileUrlGenerator()->generateAbsoluteString($file->getFileUri())
                  );
                  $imageStyles = $this->getContentImageStyleList();
                  $result[$field][$delta]['srcset'] = [];
                  foreach ($imageStyles as $style) {
                    $result[$field][$delta]['srcset'][$style['width']] = $this->stripProtocolIndicatorFromUrl(
                      ImageStyle::load($style['style'])->buildUrl($file->getFileUri())
                    );
                  }
                  break;
              }

              if (!empty($value['alt'])) {
                $result[$field][$delta]['alt'] = $value['alt'];
              }
            }
            break;

          default:
            $result[$field][$delta] = $value;
            break;
        }
      }

      // Flatten the field result array if only one entry exists.
      // Exception:
      // accordeons
      // field "field_content" of paragraph types "division_in_planning_subareas"
      // and "planning_subarea" should never get flattened.
      if (
        isset($result[$field]) &&
        is_array($result[$field]) &&
        count($result[$field]) === 1 &&
        (
          !in_array(
            sprintf('%s.%s', $result['type'], $result['bundle']),
            [
              'paragraph.accordeon',
              'paragraph.division_in_planning_subareas',
              'paragraph.planning_subarea',
            ]
          )
          ||
          $field !== 'field_content'
        )
      ) {
        $result[$field] = array_shift($result[$field]);
      }

    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return [sprintf('node:%d', $this->loadNode()->id())];
  }

  /**
   * Loads a node object (singleton).
   *
   * @return \Drupal\node\NodeInterface
   */
  protected function loadNode() {
    static $node = NULL;
    if (is_null($node) && !empty($nid = $this->getNodeId()) && is_numeric($nid)) {
      /* @var \Drupal\node\NodeInterface $node */
      $node = $this->getEntityTypeManager()->getStorage('node')->load($nid);
    }
    return $node;
  }

  /**
   * Returns the ID of the node that should get loaded.
   *
   * @return int
   *   The node id.
   */
  protected function getNodeId() {
    return $this->dipasConfig->get($this->getContentConfigPath());
  }

  /**
   * Returns the path to the configured node id to load.
   *
   * @return string
   */
  protected function getContentConfigPath() {
    return '';
  }

  /**
   * Strips out the leading "http(s)" from an url.
   *
   * @param string $url
   *  The input URL.
   *
   * @return string
   *   The URL without the protocol indicator.
   */
  protected function stripProtocolIndicatorFromUrl($url) {
    return preg_replace('~^https?://~', '//', $url);
  }

  /**
   * Returns a list of content image styles with their configured width.
   *
   * @return array
   *   An array of content images styles, keyed by their image style name
   */
  abstract protected function getContentImageStyleList();

  /**
   * Formats a given DateTime object into an UTC datetime string.
   *
   * @param int $timestamp
   * @param boolean $isUTC
   *
   * @return string
   * @throws \Exception
   */
  abstract protected function convertTimestampToUTCDateTimeString($timestamp, $isUTC);

  /**
   * Returns the node storage interface.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  abstract protected function getEntityTypeManager();

  /**
   * @return \Drupal\Core\File\FileUrlGeneratorInterface
   */
  abstract function getFileUrlGenerator();

  /**
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   */
  abstract protected function getLogger();

}
