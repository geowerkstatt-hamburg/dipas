<?php

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\dipas\Annotation\ResponseKey;

/**
 * Class Statistics.
 *
 * @ResponseKey(
 *   id = "versioninfo",
 *   description = @Translation("Returns version information on the DIPAS system."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true
 * )
 *
 * @package Drupal\dipas\Plugin\ResponseKey
 */
class VersionInfo extends ResponseKeyBase {

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return ['DipasVersionInfo'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginResponse() {
    return array_map(
      "nl2br",
      _dipas_retrieve_version_info()
    );
  }

}
