<?php

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\Core\Url;
use Drupal\dipas\Annotation\ResponseKey;
use Drupal\dipas\Plugin\SettingsSection\ContributionWizard;
use Drupal\image\Entity\ImageStyle;
use Drupal\masterportal\DomainAwareTrait;
use Drupal\dipas\FileHelperFunctionsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dipas\TaxonomyTermFunctionsTrait;
use Drupal\dipas\ProceedingListingMethodsTrait;

/**
 * Class Init.
 *
 * @ResponseKey(
 *   id = "init",
 *   description = @Translation("Combines all basic project information into a
 *   single response."), requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true
 * )
 *
 * @package Drupal\dipas\Plugin\ResponseKey
 */
class Init extends ResponseKeyBase {

  use DateTimeTrait;
  use ProjectDataTrait;
  use DomainAwareTrait;
  use TaxonomyTermFunctionsTrait;
  use FileHelperFunctionsTrait;
  use ProceedingListingMethodsTrait;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupals taxonomy term storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function setAdditionalDependencies(ContainerInterface $container) {
    $this->dateFormatter = $container->get('date.formatter');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->fileUrlGenerator = $container->get('file_url_generator');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
  * {@inheritdoc}
  */
  protected function getConfigFactory() {
    return $this->configFactory;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDipasConfig() {
    return $this->dipasConfig;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfig($domainid)
  {
    return $this->dipasConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginResponse() {
    if (!$this->isDomainDefined()) {
      $domain_settings = \Drupal::config('domain.settings');
      $redirect_url = "https://www.hamburg.de/stadtwerkstatt";

      if ($domain_settings->get('redirect_url') && $domain_settings->get('redirect_url') != '') {
        $redirect_url = $domain_settings->get('redirect_url');
      }

      return ['redirect_url' => $redirect_url];
    }

    $proceedingUtilizedLocalizationFeature = $this->dipasConfig->get('ContributionSettings.use_localization') ?? TRUE;

    $path_landingpage_background_image = $this->getImagePathFromMediaItemId(
      $this->dipasConfig->get('LandingPage.background_image')
    );

    $downloadData = $this->getDownloadPathFromEntities();

    // Prepare contribution wizard settings
    $contributionWizardSteps = $this->dipasConfig->get('ContributionWizard.stepOrder');

    if (!$contributionWizardSteps) {
      $stepOrderDefaults = ContributionWizard::getDefaults()['stepOrder'];

      $contributionWizardSteps = array_map(function ($elem) {
        return [
          'id' => $elem['id'],
          'fillInMandatory' => TRUE,
          'enabled' => TRUE,
        ];
      }, $stepOrderDefaults);
    }

    // Filter out deactivated steps
    $contributionWizardSteps = array_filter($contributionWizardSteps, function ($elem) {
      return $elem['enabled'];
    });

    // If this proceeding does not utilize the location feature, make sure
    // the localization step is omitted in the contribution wizard
    if (!$proceedingUtilizedLocalizationFeature) {
      $indexOfLocalizationStep = (function ($array, $callback) {
        foreach ($array as $key => $value)
          if ($callback($value))
            return $key;
      })(
        $contributionWizardSteps,
        function ($step) {
          return $step['id'] === 'location';
        }
      );

      if ($indexOfLocalizationStep !== NULL) {
        array_splice($contributionWizardSteps, $indexOfLocalizationStep, 1);
      }
    }

    // Remap the property "fillInMandatory" to "mandatory" and
    // get rid of the surplus "enabled" property
    array_walk(
      $contributionWizardSteps,
      function (&$elem) {
        $elem['mandatory'] = $elem['fillInMandatory'];
        unset($elem['fillInMandatory']);
        unset($elem['enabled']);
      }
    );

    $settings = [
      'maintenanceMode' => FALSE,
      'projectphase' => $this->getProjectPhase(),
      'enabledPhase2' => $this->dipasConfig->get('ProjectSchedule.phase_2_enabled'),
      'projectperiod' => [
        'start' => $this->convertTimestampToUTCDateTimeString(
          strtotime($this->dipasConfig->get('ProjectSchedule.project_start')),
          TRUE
        ),
        'end' => $this->convertTimestampToUTCDateTimeString(
          strtotime($this->dipasConfig->get('ProjectSchedule.project_end')),
          TRUE
        ),
      ],
      'useLocalizationFeature' => $proceedingUtilizedLocalizationFeature,
      'contributionWizard' => $contributionWizardSteps,
      'randomize_conception_order' => $this->dipasConfig->get('ProjectSchedule.randomize_conception_order') && (bool) $this->dipasConfig->get('ProjectSchedule.randomize_conception_order'),
      'conception_comments_state' => (bool) $this->dipasConfig->get('ProjectSchedule.allow_conception_comments') ? 'open' : 'closed',
      'display_existing_conception_comments' => (bool) $this->dipasConfig->get('ProjectSchedule.display_existing_conception_comments'),
      'projecttitle' => $this->dipasConfig->get('ProjectInformation.site_name'),
      'projectowner' => [
        'name' => $this->dipasConfig->get('ProjectInformation.department'),
        'street1' => $this->dipasConfig->get('ProjectInformation.street1'),
        'street2' => $this->dipasConfig->get('ProjectInformation.street2'),
        'zip' => $this->dipasConfig->get('ProjectInformation.zip'),
        'city' => $this->dipasConfig->get('ProjectInformation.city'),
        'telephone' => $this->dipasConfig->get('ProjectInformation.contact_telephone'),
        'email' => $this->dipasConfig->get('ProjectInformation.contact_email'),
        'website' => $this->dipasConfig->get('ProjectInformation.contact_website'),
      ],
      'landingpage' => [
        'text' => $this->dipasConfig->get('LandingPage.text'),
        'image' => [
          'path' => $path_landingpage_background_image,
          'alttext' => $this->getAlternativeTextForMediaItem($this->dipasConfig->get('LandingPage.background_image')),
        ],
        'buttons' => $this->dipasConfig->get('LandingPage.buttons') ?? [
          'button_default_behavior' => 1,
        ],
        'hasProjectInfopage' => $this->dipasConfig->get('LandingPage.node') ? TRUE : FALSE,
      ],
      'menus' => [
        'main' => $this->getMenuLinks('main'),
        'footer' => $this->getMenuLinks('footer'),
      ],
      'taxonomy' => [
        'categories' => array_values($this->getTermList(
          'categories',
          [
            'field_category_icon' => function ($fieldvalue) {
              return preg_replace('~^https?:~i', '', $this->createImageUrl($this->getFileUriFromFileId($fieldvalue->get('target_id')
                ->getString())));
            },
            'field_color' => function ($fieldvalue) {
              return $fieldvalue->getString();
            },
          ]
        )),
        'rubrics' => array_values($this->getTermList(
          'rubrics',
          [
            'field_color' => function ($fieldvalue) {
              return $fieldvalue->getString();
            },
          ]
        )),
        'tags' => array_values($this->getTermList('tags')),
      ],
      'image_styles' => $this->getContentImageStyleList(),
      'contributions' => [
        'contributions_page_subheadline' => $this->dipasConfig->get('ContributionSettings.contributions_page_subheadline'),
        'status' => $this->dipasConfig->get('ContributionSettings.contribution_status'),
        'maxlength' => $this->dipasConfig->get('ContributionSettings.maximum_character_count_per_contribution'),
        'geometry' => $this->dipasConfig->get('ContributionSettings.geometry'),
        'comments' => [
          'form' => $this->dipasConfig->get('ContributionSettings.comments_allowed') ? 'open' : 'closed',
          'maxlength' => $this->dipasConfig->get('ContributionSettings.comments_maxlength'),
          'display' => $this->dipasConfig->get('ContributionSettings.display_existing_comments'),
        ],
        'ratings' => $this->dipasConfig->get('ContributionSettings.rating_allowed'),
        'display_existing_ratings' => $this->dipasConfig->get('ContributionSettings.display_existing_ratings') ?? TRUE,
      ],
      'masterportal_instances' => [
        'contributionmap' => preg_replace('~^https?:~i', '', Url::fromRoute('masterportal.fullscreen', ['masterportal_instance' => $this->dipasConfig->get('ContributionSettings.masterportal_instances.contributionmap')], ['absolute' => TRUE])
          ->toString()),
        'singlecontribution' => [
          'url' => preg_replace('~^https?:~i', '', Url::fromRoute('masterportal.fullscreen', ['masterportal_instance' => $this->dipasConfig->get('ContributionSettings.masterportal_instances.singlecontribution.instance')], ['absolute' => TRUE])
            ->toString()),
          'other_contributions' => $this->dipasConfig->get('ContributionSettings.masterportal_instances.singlecontribution.other_contributions'),
        ],
        'createcontribution' => [
          'url' => preg_replace('~^https?:~i', '', Url::fromRoute('masterportal.fullscreen', ['masterportal_instance' => $this->dipasConfig->get('ContributionSettings.masterportal_instances.createcontribution')], ['absolute' => TRUE])
            ->toString()),
          'must_be_localized' => (bool) array_filter(
            $contributionWizardSteps,
            function ($elem) { return $elem['id'] === 'location' && isset($elem['fillInMandatory']) && $elem['fillInMandatory']; }
          ),
        ],
        'schedule' => preg_replace('~^https?:~i', '', Url::fromRoute('masterportal.fullscreen', ['masterportal_instance' => $this->dipasConfig->get('MenuSettings.mainmenu.schedule.mapinstance')], ['absolute' => TRUE])
          ->toString()),
      ],
      'downloads' => $downloadData,
      'sidebar' => $this->dipasConfig->get('SidebarSettings.blocks'),
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function postProcessResponse(array $responsedata) {
    [$cypher, $security_token, $passphrase, $initvector] = require_once(realpath(__DIR__ . '/RestApiToken.php'));

    $responsedata['checksum'] = openssl_encrypt($security_token, $cypher, $passphrase, 0, $initvector);
    $responsedata['signature'] = $initvector;
    $responsedata['timestamp'] = time();

    return $responsedata;
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return [];
  }

  /**
   * Helper function to retrieve the ALT text from a media entity.
   *
   * @param int $id
   *   The ID of the media entity.
   *
   * @return string
   *   The alternative text
   */
  protected function getAlternativeTextForMediaItem($id) {
    if (
      !is_null($id) &&
      is_numeric($id) &&
      $media_entity = $this->entityTypeManager
        ->getStorage('media')
        ->load($id)
    ) {
      return $media_entity->get('field_media_image')
        ->first()
        ->get('alt')
        ->getString();
    }
    return '';
  }

  /**
   * Returns the image path from a media item entity.
   *
   * @param int $id
   *   The ID of the media entity.
   * @param string $image_style
   *   The image style to use for the path (optional).
   *
   * @return string
   *   The path to the image stored in the media item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Exceptions thrown by the entity type manager.
   */
  protected function getImagePathFromMediaItemId($id, $image_style = FALSE) {
    if (
      !is_null($id) &&
      is_numeric($id) &&
      $media_entity = $this->entityTypeManager
        ->getStorage('media')
        ->load($id)
    ) {
      $image_fid = $media_entity->get('field_media_image')
        ->first()
        ->get('target_id')
        ->getString();
      return preg_replace('~^https?:~i', '', $this->createImageUrl($this->getFileUriFromFileId($image_fid), $image_style));
    }
    return '';
  }

  /**
   * Creates a fully qualified image url from a wrapper uri.
   *
   * @param string $wrapperUri
   *   The image wrapper uri.
   * @param bool $image_style
   *   Optional image style to be used.
   *
   * @return string
   *   The fully qualified image url.
   */
  protected function createImageUrl($wrapperUri, $image_style = FALSE) {
    return $image_style !== FALSE
      ? ImageStyle::load($image_style)->buildUrl($wrapperUri)
      : $this->fileUrlGenerator->generateAbsoluteString($wrapperUri);
  }

  /**
   * Generates a sorted array of links for a given menu.
   *
   * @param String $menu
   *   Either "main" or "footer"
   *
   * @return array
   */
  protected function getMenuLinks(String $menu) : array {
    // Fetch the configured menu items.
    $menuitems = $this->dipasConfig->get("MenuSettings.{$menu}menu");

    // Make sure the "Compare conceptions" menu item is active, if the project
    // has phase 2 enabled and the project phase has passed phase 1
    if (
      $menu === "main" &&
      $this->dipasConfig->get('ProjectSchedule.phase_2_enabled') &&
      in_array($this->getProjectPhase(), ['phase2', 'phasemix', 'frozen'])
    ) {
      $menuitems['conceptionlist']['enabled'] = TRUE;
    }

    // Filter out disabled menu items
    $menuitems = array_filter($menuitems, function ($item) {
      return $item['enabled'];
    });

    // Sort the menu items based on their weight
    $weights = [];
    foreach ($menuitems as $endpoint => $settings) {
      $weights[$endpoint] = isset($settings['weight'])
        ? (int) $settings['weight']
        : 0;
    }
    array_multisort($weights, SORT_NUMERIC, $menuitems);

    // Transform the value of each array item into the needed form for the frontend
    $menuitems = array_map(
      function ($item) {
        $menuitem = [
          'name' => $item['name'],
        ];

        if (isset($item['url'])) {
          $menuitem['url'] = $item['url'];
        }

        return $menuitem;
      },
      $menuitems
    );

    return $menuitems;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermStorage() {
    return $this->termStorage;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileUrlGenerator() {
    return $this->fileUrlGenerator;
  }
}
