<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\ResponseKey;

use Drupal\dipas\Annotation\ResponseKey;
use Drupal\dipas\Exception\MalformedRequestException;
use Drupal\dipas\Exception\StatusException;
use Drupal\votingapi\Entity\Vote;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Rate.
 *
 * @ResponseKey(
 *   id = "rate",
 *   description = @Translation("Rate a given entity."),
 *   requestMethods = {
 *     "POST",
 *   },
 *   isCacheable = false,
 *   shieldRequest = true
 * )
 *
 * @package Drupal\dipas\Plugin\ResponseKey
 */
class Rate extends ResponseKeyBase {

  use RetrieveRatingTrait;
  use DateTimeTrait;

  protected const VOTINGCOOKIENAME = 'dipasv';

  /**
   * Drupal's cache tags invalidation service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagInvalidator;

  /**
   * @var \Drupal\dipas\Service\DipasCookieInterface
   */
  protected $dipasCookie;

  /**
   * @var array
   */
  protected $postedFields;

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entityToRate;

  /**
   * @var int
   */
  protected $rating;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->cacheTagInvalidator = $container->get('cache_tags.invalidator');
    $this->dipasCookie = $container->get('dipas.cookie');
    $this->dateFormatter = $container->get('date.formatter');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId() {
    return $this->entityToRate->getEntityTypeId();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityId() {
    return $this->entityToRate->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginResponse() {
    $this->checkRequest();
    $this->castVote();
    $this->updateVotingCookie();
    $this->cacheTagInvalidator->invalidateTags([sprintf('%s:%s', $this->entityToRate->getEntityTypeId(), $this->entityToRate->id())]);

    return [
      'results' => $this->getRating(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCookies() {
    $cookieData = $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME) !== FALSE
      ? $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME)
      : (object) [];

    if (!isset($cookieData->votes)) {
      $cookieData->votes = (object) [];
    }

    if (!isset($cookieData->votes->{$this->entityToRate->getEntityTypeId()})) {
      $cookieData->votes->{$this->entityToRate->getEntityTypeId()} = [];
    }
    else {
      $cookieData->votes->{$this->entityToRate->getEntityTypeId()} = (array) $cookieData->votes->{$this->entityToRate->getEntityTypeId()};
    }

    $cookieData->votes->{$this->entityToRate->getEntityTypeId()}[$this->entityToRate->id()] = $this->rating;
    $this->dipasCookie->setCookieData($cookieData, self::VOTINGCOOKIENAME);

    return [$this->dipasCookie->getCookie(self::VOTINGCOOKIENAME)];
  }

  /**
   * Checks, if the request is legitimate. Throws exception when not.
   */
  protected function checkRequest() {
    // Check if the proceeding allows ratings in the first place.
    if (!$this->dipasConfig->get('ContributionSettings.rating_allowed')) {
      throw new StatusException('Ratings are closed.', 403);
    }

    // In order to rate, the user has to accept cookies
    if (!$this->dipasCookie->hasCookiesEnabled()) {
      throw new StatusException('Cookies must be accepted in order to vote!', 400);
    }

    // Check, if the request contains any data at all.
    $postedFields = json_decode($this->currentRequest->getContent());
    if (!$postedFields) {
      throw new MalformedRequestException('Request data could not be decoded!', 400);
    }
    $this->postedFields = (array) $postedFields;

    // Collect necessary rating-centered data
    $entityType = isset($this->postedFields['entity_type']) ? $this->postedFields['entity_type'] : 'node';
    $storage = $this->entityTypeManager->getStorage($entityType);
    $id = $this->currentRequest->attributes->get('id');

    // Check if the entity to rate actually exists
    if (!is_numeric($id) || !($entity = $storage->load($id))) {
      throw new MalformedRequestException('The id given is invalid!', 400);
    }

    // Cache the entity to rate in this instance for this request
    $this->entityToRate = $entity;

    // Prepare the data container for the voting cookie
    $cookieData = $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME) !== FALSE
      ? $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME)
      : (object) [
        'votes' => [],
      ];

    // Check if the entity wasn't already voted on
    if (
      isset($cookieData->votes->{$this->entityToRate->getEntityTypeId()}) &&
      in_array($this->entityToRate->id(), array_keys((array) $cookieData->votes->{$this->entityToRate->getEntityTypeId()} ?? []))
    ) {
      throw new StatusException('A vote for the given entity has already been casted!', 403);
    }

    // Make sure the request contains an actual vote
    if (!isset($this->postedFields['rating']) || !is_numeric($this->postedFields['rating'])) {
      throw new MalformedRequestException('Missing data!', 400);
    }

    // Aöö checks have been passed, cache the casted vote
    switch (true) {
      case (int) $this->postedFields['rating'] > 0:
        $this->rating = 1;
        break;

      case (int) $this->postedFields['rating'] < 0:
        $this->rating = -1;
        break;
    }
  }

  /**
   * Cast a vote on the entity provided.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function castVote() {
    $vote = Vote::create(['type' => 'vote']);
    $vote->setVotedEntityType($this->entityToRate->getEntityTypeId());
    $vote->setVotedEntityId($this->entityToRate->id());
    $vote->setValueType('points');
    $vote->setValue($this->rating);
    $vote->setOwnerId(0);
    $vote->save();
  }

  /**
   * Update the voting cookie (set if previously unset)
   *
   * @return void
   */
  protected function updateVotingCookie() {
    $cookieData = $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME) !== FALSE
      ? $this->dipasCookie->getCookieData(self::VOTINGCOOKIENAME)
      : (object) [
        'votes' => (object) [
          $this->entityToRate->getEntityTypeId() => [],
        ],
      ];

    $cookieData->votes->{$this->entityToRate->getEntityTypeId()} = (array) $cookieData->votes->{$this->entityToRate->getEntityTypeId()};
    $cookieData->votes->{$this->entityToRate->getEntityTypeId()}[$this->entityToRate->id()] = $this->rating;

    $this->dipasCookie->setCookieData($cookieData, self::VOTINGCOOKIENAME);
    $this->dipasCookie->getCookie(self::VOTINGCOOKIENAME);
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }
}
