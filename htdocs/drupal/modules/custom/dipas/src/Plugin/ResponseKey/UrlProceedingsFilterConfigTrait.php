<?php

namespace Drupal\dipas\Plugin\ResponseKey;

use Symfony\Component\HttpFoundation\Request;

trait UrlProceedingsFilterConfigTrait
{
  /**
   * Provides a list of parameters to filter proceedings.
   *
   * Also used as the source to generate the cache key and validate values.
   *
   * Array-Structure:
   * [
   *   URL parameter name => [
   *     'configParamName' => string|NULL, Configuration parameter name (NULL, if there is no associated config),
   *     'URLParamValidation' => [
   *       'valueIsArray' => boolean, Request parameters to be expected as an array?
   *       'regex' => string, Regex to validate the request parameter
   *     ],
   *   ]
   *   ...
   * ]
   *
   * @return array
   *   List of parameters
   */
  protected function getURLProceedingFilterParams(): array {
    return [
      'districts' => [
        'configParamName' => 'data_districtselection',
        'configParamNameExternalProceeding' => 'proceedingdistricts',
        'URLParamValidation' => [
          'valueIsArray' => TRUE,
          'regex' => '^\d+$',
        ],
      ],
      'topics' => [
        'configParamName' => 'data_topicselection',
        'configParamNameExternalProceeding' => 'proceedingtopics',
        'URLParamValidation' => [
          'valueIsArray' => TRUE,
          'regex' => '^\d+$',
        ],
      ],
      'owners' => [
        'configParamName' => 'project_owners',
        'configParamNameExternalProceeding' => 'proceedingowners',
        'URLParamValidation' => [
          'valueIsArray' => TRUE,
          'regex' => '^\d+$',
        ],
      ],
      'dateStart' => [
        'configParamName' => NULL,
        'configParamNameExternalProceeding' => NULL,
        'URLParamValidation' => [
          'valueIsArray' => FALSE,
          'regex' => '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        ],
      ],
      'dateEnd' => [
        'configParamName' => NULL,
        'configParamNameExternalProceeding' => NULL,
        'URLParamValidation' => [
          'valueIsArray' => FALSE,
          'regex' => '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        ],
      ],
    ];
  }

  /**
   * Generates a cache key for the filter, if filter params are found in current request.
   *
   * @param Request $request
   *   Current request object
   *
   * @return string|NULL
   *   Generated cache key, if params are found in request. NULL if no match.
   */
  protected function getURLProceedingFilterCacheKey(Request $request): NULL|string {
    $filters = [];

    foreach ($this->getURLProceedingFilterParams() as $URLParam => $config) {
      if ($request->query->has($URLParam)) {
        if (!$filterValue = $this->getValidatedFilterValues($request->query->all()[$URLParam], $config['URLParamValidation'])) {
          continue;
        }

        if (is_array($filterValue)) {
          sort($filterValue);
        }

        $filters[$URLParam] = $filterValue;
      }
    }

    return $filters
      ? 'proceedingFilter=' . json_encode($filters)
      : NULL;
  }

  /**
   * Validates value against a validation configuration.
   *
   * @param mixed $filterValue
   *   Value of the filter to validate
   *
   * @param array $validationConfig
   *   Validation configuration, see $this->getURLProceedingFilterParams()
   *
   *   Array-Structure:
   *   [
   *     'valueIsArray' => boolean, Request parameters to be expected as an array?
   *     'regex' => string, Regex to validate the request parameter
   *   ]
   *
   * @return mixed
   *   Returns the validated value
   */
  protected function getValidatedFilterValues(mixed $filterValue, array $validationConfig): mixed {
    $validatedValue = NULL;

    if ($validationConfig['valueIsArray']) {
      if (is_array($filterValue)) {
        $validatedValue = array_filter($filterValue, function ($value) use ($validationConfig) {
          return preg_match('#' . $validationConfig['regex'] . '#', $value) === 1;
        });
        $validatedValue = array_unique($validatedValue);
      }
    }
    elseif (
      !is_array($filterValue) &&
      preg_match('#' . $validationConfig['regex'] . '#', $filterValue) === 1
    ) {
      $validatedValue = $filterValue;
    }

    return $validatedValue;
  }
}
