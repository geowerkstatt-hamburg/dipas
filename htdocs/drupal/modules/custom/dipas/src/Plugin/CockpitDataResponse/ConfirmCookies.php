<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Annotation\CockpitDataResponse;
use Drupal\dipas\Exception\MalformedRequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmCookies.
 *
 * @CockpitDataResponse(
 *   id = "confirmcookies",
 *   description = @Translation("Confirms the use of cookies."),
 *   requestMethods = {
 *     "POST",
 *   },
 *   isCacheable = false
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class ConfirmCookies extends CockpitDataResponseBase {

  /**
   * @var \Drupal\dipas\Service\DipasCookieInterface
   */
  protected $dipasCookie;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->dipasCookie = $container->get('dipas.cookie');
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginResponse() {
    if (($content = json_decode($this->currentRequest->getContent())) && isset($content->confirmCookies)) {
      return [
        'message' => 'The use of cookies was confirmed.',
      ];
    }

    throw new MalformedRequestException('The use of cookies was not confirmed!', 400);
  }

  /**
   * {@inheritdoc}
   */
  public function getCookies() {
    $this->dipasCookie->setCookieName('dipasn');
    $cookieData = $this->dipasCookie->getCookieData('dipasn');

    if (!$cookieData) {
      $cookieData = new \stdClass();
    }
    $cookieData->cookiesConfirmed = gmdate('Y-m-d\TH:i:s\Z');
    $this->dipasCookie->setCookieData($cookieData, 'dipasn');

    return [$this->dipasCookie->getCookie('dipasn')];
  }

}
