<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\Core\Url;
use Drupal\dipas\Annotation\CockpitDataResponse;
use Drupal\dipas\ReliablyHashValueTrait;
use Drupal\masterportal\GeoJSONFeature;
use Drupal\masterportal\UrlFromUriFixTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dipas\Plugin\ResponseKey\DateTimeTrait;
use Drupal\dipas\TaxonomyTermFunctionsTrait;
use Drupal\masterportal\DomainAwareTrait;

/**
 * Class CockpitDataMap.
 *
 * @CockpitDataResponse(
 *   id = "cockpitdatamap",
 *   description = @Translation("Returns proceeding statistics data for map showing proceeding areas on participation cockpit."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true,
 *   maxAge = 5
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class CockpitDataMap extends CockpitDataResponseBase {

  use DateTimeTrait,
    TaxonomyTermFunctionsTrait,
    DomainAwareTrait,
    ExternalProceedingsTrait,
    ProceedingEntityTrait,
    UrlFromUriFixTrait,
    ReliablyHashValueTrait;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal's node storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Drupals taxonomy term storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * Drupal's config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlgenerator;

  /**
   * @var String
   */
  protected $dipasModulePath;

  /**
   * {@inheritdoc}
   */
  public function setAdditionalDependencies(ContainerInterface $container) {
    $this->configFactory = $container->get('config.factory');
    $this->dateFormatter = $container->get('date.formatter');
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->extensionPathResolver = $container->get('extension.path.resolver');
    $this->fileUrlgenerator = $container->get('file_url_generator');

    $this->dipasModulePath = $this->extensionPathResolver->getPath('module', 'dipas');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginResponse() {
    $features = [];

    foreach ($this->configFactory->listAll('dipas.') as $dipas_config_identifier) {
      [,$domain_id,] = explode('.', $dipas_config_identifier);

      // skip default-Domain
      if ($this->isDomainModuleInstalled() && $domain_id === 'default') {
        continue;
      }

      // Skip non-existent proceedings (former proceedings, to which the domain
      // configuration was deleted as well as inactive domain entries)
      if (
        ($domain_config = $this->configFactory->get(sprintf('domain.record.%s', $domain_id))) &&
        !(
          !$domain_config->isNew() &&
          $domain_config->get('status')
        )
      ) {
        continue;
      }

      $dipasConfigDomain = $this->configFactory->get($dipas_config_identifier);

      // Skip unpublished proceedings
      if ($dipasConfigDomain->get('Export.proceeding_is_internal')) {
        continue;
      }

      // Skip proceedings without centerpoint
      if (!$project_area_centerpoint = $dipasConfigDomain->get('ProjectArea.project_area_centerpoint')) {
        continue;
      }

      // Skip unstarted proceedings
      if (time() < strtotime($dipasConfigDomain->get('ProjectSchedule.project_start'))) {
        continue;
      }
      $dateStart = $this->convertTimestampToUTCDateTimeString(
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_start')),
        TRUE
      );

      $status = $this->getStatus(
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_start')),
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_end'))
      );

      if ($status === 'aktiv') {
        $statusIcon = self::fromUriFixed(
          'base:/' . $this->dipasModulePath .'/assets/',
          ['absolute' => TRUE]
         )->toString() . 'icon_active_gfi.svg';
      }
      elseif ($status === 'inaktiv') {
        $statusIcon = self::fromUriFixed(
          'base:/' . $this->dipasModulePath .'/assets/',
          ['absolute' => TRUE]
         )->toString() . 'icon_inactive_gfi.svg';
      }
      else {
        $statusIcon = self::fromUriFixed(
          'base:/' . $this->dipasModulePath .'/assets/',
          ['absolute' => TRUE]
         )->toString() . 'icon_latest_gfi.svg';
      }

      // Create a feature container for the current project.
      $featureObject = new GeoJSONFeature();

      $coordinates = array_map('floatval', explode(', ', $project_area_centerpoint));
      $featureObject->addPoint($coordinates);
      $featureObject->setGeometryType('Point');

      // Add gfi information to it.
      $featureObject->addProperty('id', $domain_id);
      $featureObject->addProperty('proceeding', $dipasConfigDomain->get('ProjectInformation.site_name'));
      $featureObject->addProperty('description', $dipasConfigDomain->get('ProjectInformation.text'));
      $featureObject->addProperty('districts', join('; ', $this->getAssignedTerms('districts', [], $dipasConfigDomain->get('ProjectInformation.data_districtselection'), 'name')));
      $featureObject->addProperty('themes', join('; ', $this->getAssignedTerms('topics', [], $dipasConfigDomain->get('ProjectInformation.data_topicselection'), 'name')));
      $featureObject->addProperty('status', $status);
      $featureObject->addProperty('status_icon', $statusIcon);
      $featureObject->addProperty('dateStart', $dateStart);
      $featureObject->addProperty('dateEnd', $this->convertTimestampToUTCDateTimeString(
        strtotime($dipasConfigDomain->get('ProjectSchedule.project_end')),
        TRUE
      ));
      $featureObject->addProperty('responsible', join('; ', $this->getAssignedTerms('project_owner', [], $dipasConfigDomain->get('ProjectInformation.project_owners'), 'name')));
      $featureObject->addProperty('numberContributions', $this->getNodeCount($domain_id, 'contribution'));
      $featureObject->addProperty('numberComments', $this->getCommentCount($domain_id));
      $featureObject->addProperty('documentation', $this->getDokumentationDetails($domain_id));
      $featureObject->addProperty('link', preg_replace('/drupal\/.*$/', $domain_id . '/#', Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString()));

      $features[] = $featureObject->getFeature();
    }

    // Include potential external proceedings
    if ($externalProceedings = $this->getExternalProceedings()) {
      foreach ($externalProceedings as $proceeding) {
        $featureObject = new GeoJSONFeature();

        $proceedingGeometry = json_decode($proceeding->get('field_geodata')->first()->getString());
        $proceedingDescription = $proceeding->get('field_description')->first()->getValue();

        $featureObject->addPoint($proceedingGeometry->geometry->coordinates);
        $featureObject->setGeometryType('Point');

        $featureObject->addProperty('id', $proceeding->id());
        $featureObject->addProperty('proceeding', $proceeding->getTitle());
        $featureObject->addProperty('description', strip_tags($proceedingDescription['value']));

        $featureObject->addProperty(
          'districts',
          join('; ', $this->getExternalProceedingTerms(
            $proceeding->id(),
            'districts',
            'field_proceeding_districts',
            [],
            'name'
          ))
        );

        $featureObject->addProperty(
          'themes',
          join('; ', $this->getExternalProceedingTerms(
            $proceeding->id(),
            'topics',
            'field_proceeding_topics',
            [],
            'name'
          ))
        );

        $status = $this->getStatus(
          strtotime($proceeding->get('field_date')->first()->get('value')->getString()),
          strtotime($proceeding->get('field_date')->first()->get('end_value')->getString())
        );

        switch ($status) {
          case 'aktiv':
            $statusIcon = self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString() . 'icon_active_gfi.svg';
            break;

          case 'inaktiv':
            $statusIcon = self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString() . 'icon_inactive_gfi.svg';
            break;

          default:
            $statusIcon = self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
            )->toString() . 'icon_latest_gfi.svg';
        }

        $featureObject->addProperty('status', $status);
        $featureObject->addProperty('status_icon', $statusIcon);
        $featureObject->addProperty(
          'dateStart',
          $this->convertTimestampToUTCDateTimeString(
            strtotime($proceeding->get('field_date')->first()->get('value')->getString()),
            TRUE
          )
        );
        $featureObject->addProperty(
          'dateEnd',
          $this->convertTimestampToUTCDateTimeString(
            strtotime($proceeding->get('field_date')->first()->get('end_value')->getString()),
            TRUE
          )
        );

        $featureObject->addProperty(
          'responsible',
          join('; ', $this->getExternalProceedingTerms(
            $proceeding->id(),
            'project_owner',
            'field_proceeding_operators',
            [],
            'name'
          ))
        );

        $featureObject->addProperty(
          'numberContributions',
          (int) $proceeding->get('field_contribution_count')->first()->getString()
        );

        $featureObject->addProperty(
          'numberComments',
          (int) $proceeding->get('field_comment_count')->first()->getString()
        );

        $proceedingDocumentations = array_map(
          function ($documentation) {
            $documentationName = explode('/', $documentation->url);
            $documentationName = array_pop($documentationName);

            return [
              'name' => $documentationName,
              'url' => $documentation->url,
              'icon' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
                ['absolute' => TRUE]
              )->toString() . 'picture_as_pdf_white_24dp.svg'
            ];
          },
          $this->getExternalProceedingDocumentations($proceeding->id())
        );

        $featureObject->addProperty('documentation', array_values($proceedingDocumentations));

        if ($proceeding->get('field_live_proceeding')->first()->get('value')->getValue()) {
          $featureObject->addProperty(
            'link',
            $proceeding->get('field_proceeding_url')->first()->getString()
          );
        }

        $features[] = $featureObject->getFeature();
      }
    }

    //define styling
    $styles = (object) [
      'styleId' => 'projectAreaStyles',
      'rules' => [
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'aktiv',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
             )->toString(),
            'imageName' => 'icon_active_map.svg',
            'imageScale'=> 1.8,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
            'clusterType' => 'circle',
            'clusterCircleRadius' => 15,
            'clusterCircleFillColor' => [100, 200, 230, 0.7],
            'clusterCircleStrokeColor' => [255, 255, 255, 1],
            'clusterCircleStrokeWidth' => 2,
            'clusterTextType' => 'counter',
            'clusterTextAlign' => 'center',
            'clusterTextScale' => 2,
            'clusterTextFillColor' => [0, 0, 0],
            'clusterTextOffsetX' => 0,
            'clusterTextOffsetY' => 1,
          ],
        ],
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'inaktiv',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
             )->toString(),
             'imageName' => 'icon_inactive_map.svg',
            'imageScale'=> 1.7,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
            'clusterType' => 'circle',
            'clusterCircleRadius' => 15,
            'clusterCircleFillColor' => [100, 200, 230, 0.7],
            'clusterCircleStrokeColor' => [255, 255, 255, 1],
            'clusterCircleStrokeWidth' => 2,
            'clusterTextType' => 'counter',
            'clusterTextAlign' => 'center',
            'clusterTextScale' => 2,
            'clusterTextFillColor' => [0, 0, 0],
            'clusterTextOffsetX' => 0,
            'clusterTextOffsetY' => 1,
          ],
        ],
        (object) [
          'conditions' => (object) [
            'properties' => (object) [
              'status' => 'neu',
            ],
          ],
          'style' => (object) [
            'type' => 'icon',
            'imagePath' => self::fromUriFixed(
              'base:/' . $this->dipasModulePath .'/assets/',
              ['absolute' => TRUE]
             )->toString(),
            'imageName' => 'icon_latest_map.svg',
            'imageScale'=> 1.8,
            'imageWidth' => 24,
            'imageHeight' => 24,
            'imageOffsetX' => 0.5,
            'imageOffsetY'=> 0.5,
            'clusterType' => 'circle',
            'clusterCircleRadius' => 15,
            'clusterCircleFillColor' => [100, 200, 230, 0.7],
            'clusterCircleStrokeColor' => [255, 255, 255, 1],
            'clusterCircleStrokeWidth' => 2,
            'clusterTextType' => 'counter',
            'clusterTextAlign' => 'center',
            'clusterTextScale' => 2,
            'clusterTextFillColor' => [0, 0, 0],
            'clusterTextOffsetX' => 0,
            'clusterTextOffsetY' => 1,
          ],
        ]
      ],
    ];

    // construct final geojson object
    $geojson['type'] = 'FeatureCollection';
    $geojson['features'] = $features;
    $geojson['styles'][] = $styles;

    return $geojson;
  }

  /**
   * Returns a string indicating if the proceeding is active or inactive.
   *
   * @param object $dipasConfigDomain
   *   The config of the coresponding domain.
   *
   * @return String
   */
  protected function getStatus($startTimestamp, $endTimestamp) {
    $now = time();

    if ($startTimestamp > strtotime('-12 days')) {
      return 'neu';
    }
    if ($now >= $startTimestamp && $now < $endTimestamp) {
      return 'aktiv';
    }
    else {
      return 'inaktiv';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileUrlGenerator() {
    return $this->fileUrlgenerator;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExtensionPathResolver() {
    return $this->extensionPathResolver;
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return ['CockpitDataResponseMapData'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermStorage() {
    return $this->termStorage;
  }

}
