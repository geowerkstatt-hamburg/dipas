<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Annotation\CockpitDataResponse;

/**
 * Class ContributionStatistics.
 *
 * @CockpitDataResponse(
 *   id = "contributionstatistics",
 *   description = @Translation("Provides statistical data on participatory actions across all proceedings and matching potential filters."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true,
 *   maxAge = 5
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class ContributionStatistics extends StatisticsBase {

  /**
   * {@inheritdoc}
   */
  protected function getStatisticsEntity() {
    return 'contributions';
  }

  /**
   * {@inheritdoc}
   */
  protected function getStatisticsType() {
    return 'count';
  }

}
