<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Annotation\CockpitDataResponse;
use Drupal\dipas\CommentQueryTrait;
use Drupal\dipas\Plugin\ResponseKey\NodeListingTrait;
use Drupal\dipas\Plugin\ResponseKey\DateTimeTrait;
use Drupal\dipas\Plugin\ResponseKey\UrlProceedingsFilterConfigTrait;
use Drupal\dipas\Plugin\ResponseKey\UrlProceedingsFilterTrait;
use Drupal\dipas\ProceedingListingMethodsTrait;
use Drupal\dipas\ReliablyHashValueTrait;
use Drupal\masterportal\DomainAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dipas\TaxonomyTermFunctionsTrait;

/**
 * Class MostCommentedContributions.
 *
 * @CockpitDataResponse(
 *   id = "mostcommentedcontributions",
 *   description = @Translation("Lists the three most commented on contributions accross all active domains."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true,
 *   maxAge = 15
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class MostCommentedContributions extends CockpitDataResponseBase {

  use NodeListingTrait;
  use UrlProceedingsFilterConfigTrait;
  use UrlProceedingsFilterTrait {
    UrlProceedingsFilterTrait::retrieveFilteredProceedingIDsFromURL insteadof NodeListingTrait;
  }
  use DateTimeTrait;
  use TaxonomyTermFunctionsTrait;
  use ExternalProceedingsTrait, ExternalProceedingsFilterTrait {
    ExternalProceedingsTrait::getExternalProceedingMetadata as getUnfilteredExternalProceedingMetadata;
    ExternalProceedingsFilterTrait::getExternalProceedingMetadata insteadof ExternalProceedingsTrait;
  }
  use ProceedingListingMethodsTrait;
  use DomainAwareTrait;
  use CommentQueryTrait;
  use ReliablyHashValueTrait;

  const CONTRIBUTION_LIMIT = 3;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * @var array
   */
  protected $commentCountsForDipasNodes;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->dateFormatter = $container->get('date.formatter');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');

    $this->listingIsDomainSensitive(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  protected function setupTasks() {
    $listDomains = 'active';
    $visibleDomains = !$this->isURLFilterActive() ? $this->getProceedingIDs($listDomains) : [];

    if ($this->isURLFilterActive() || !count($visibleDomains)) {
      $listDomains = 'visible';
      $visibleDomains = $this->getProceedingIDs($listDomains);
    }

    $this->commentCountsForDipasNodes = drupal_static(
      'DIPAS_NAVIGATOR_MOST_COMMENTED_CONTRIBUTIONS',
      $this->getCommentCountForNodes(
        [],
        'contribution',
        $visibleDomains,
        NULL,
        NULL,
        self::CONTRIBUTION_LIMIT
      ),
    );

    if (
      !count($this->commentCountsForDipasNodes) &&
      $listDomains === 'active'
    ) {
      $listDomains = 'visible';
      $visibleDomains = $this->getProceedingIDs($listDomains);
      $this->commentCountsForDipasNodes = $this->getCommentCountForNodes(
        [],
        'contribution',
        $visibleDomains,
        NULL,
        NULL,
        self::CONTRIBUTION_LIMIT
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return ['CockpitDataResponse', 'CockpitDataResponseMostCommentedContributions'] +
      array_map(function ($node) {
        return sprintf('node-%d', $node->nid);
      }, $this->getNodes());
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginResponse() {
    $contributions = [];

    if (count($this->commentCountsForDipasNodes)) {
      $dipas_contributions = $this->getNodes();

      array_walk(
        $dipas_contributions,
        function (&$contribution) {
          $contribution->commentsCount = $this->commentCountsForDipasNodes[$contribution->nid];
        }
      );

      $contributions = array_merge(
        $dipas_contributions,
        $this->getExternalProceedingMostCommentedContributions(self::CONTRIBUTION_LIMIT)
      );

      $contributionComments = [];
      foreach ($contributions as $index => $contribution) {
        $contributionComments[$index] = (int) $contribution->commentsCount;
      }
      array_multisort($contributionComments, SORT_DESC, $contributions);
      $contributions = array_slice($contributions, 0, self::CONTRIBUTION_LIMIT);
    }

    return ['contributions' => $contributions];
  }

  /**
   * {@inheritdoc}
   */
  protected function postProcessNodes(array &$nodes) {
    $allProceedingIDs = array_unique(array_map(function ($node) {
      return $node->domainid;
    }, $nodes));

    $proceedingConfigs = array_combine(
      $allProceedingIDs,
      array_map(function ($domainid) {
        return $this->configFactory->get(sprintf('dipas.%s.configuration', $domainid));
      }, $allProceedingIDs)
    );

    foreach ($nodes as &$node) {
      $node->content = nl2br($node->content);
      $node->proceedingname = $proceedingConfigs[$node->domainid]->get('ProjectInformation.site_name') ?: '';
      $node->proceedingdistricts = $this->getAssignedTerms('districts', [], $proceedingConfigs[$node->domainid]->get('ProjectInformation.data_districtselection'), 'name');
      $node->proceedingtopics = $this->getAssignedTerms('topics', [], $proceedingConfigs[$node->domainid]->get('ProjectInformation.data_topicselection'), 'name');
      $node->proceedingOwner = $this->getAssignedTerms('project_owner', [], $proceedingConfigs[$node->domainid]->get('ProjectInformation.project_owners'), 'name');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeType() {
    return 'contribution';
  }

  /**
   * {@inheritdoc}
   */
  protected function getJoins() {
    return [
      [
        'type' => 'LEFT',
        'table' => 'node__field_domain_access',
        'alias' => 'assigned_domains',
        'condition' => 'base.type = assigned_domains.bundle AND base.nid = assigned_domains.entity_id AND base.vid = assigned_domains.revision_id AND attr.langcode = assigned_domains.langcode AND assigned_domains.deleted = 0',
        'fields' => [
          'field_domain_access_target_id' => 'domainid',
        ],
      ],
      [
        'type' => 'LEFT',
        'table' => 'node__field_domain_all_affiliates',
        'alias' => 'assigned_to_all',
        'condition' => 'base.type = assigned_to_all.bundle AND base.nid = assigned_to_all.entity_id AND base.vid = assigned_to_all.revision_id AND attr.langcode = assigned_to_all.langcode AND assigned_to_all.deleted = 0',
      ],
      [
        'type' => 'LEFT',
        'table' => 'node__field_text',
        'alias' => 'content',
        'condition' => 'base.type = content.bundle AND base.nid = content.entity_id AND base.vid = content.revision_id AND attr.langcode = content.langcode AND content.deleted = 0',
        'fields' => [
          'field_text_value' => 'content',
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getExpressions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getConditions() {
    $conditions = [];

    if (count($this->commentCountsForDipasNodes)) {
      $conditions[] = [
        'field' => 'base.nid',
        'value' => $this->commentCountsForDipasNodes,
        'operator' => 'IN',
      ];
    }

    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  protected function getGroupBy() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSortingField() {
    return 'nid';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSortingDirection() {
    return 'DESC';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermStorage() {
    return $this->termStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCurrentRequest() {
    return $this->currentRequest;
  }

}
