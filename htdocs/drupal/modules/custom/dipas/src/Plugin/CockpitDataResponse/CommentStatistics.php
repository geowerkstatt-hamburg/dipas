<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Annotation\CockpitDataResponse;

/**
 * Class CommentStatistics.
 *
 * @CockpitDataResponse(
 *   id = "commentstatistics",
 *   description = @Translation("Provides statistical data on participatory actions across all proceedings and matching potential filters."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true,
 *   maxAge = 5
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class CommentStatistics extends StatisticsBase {

  /**
   * {@inheritdoc}
   */
  protected function getStatisticsEntity() {
    return 'comments';
  }

  /**
   * {@inheritdoc}
   */
  protected function getStatisticsType() {
    return 'count';
  }

}
