<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Exception\MalformedRequestException;
use Drupal\dipas\CommentQueryTrait;
use Drupal\dipas\Plugin\ResponseKey\DateTimeTrait;
use Drupal\dipas\Plugin\ResponseKey\UrlProceedingsFilterConfigTrait;
use Drupal\dipas\Plugin\ResponseKey\UrlProceedingsFilterTrait;
use Drupal\dipas\ProceedingListingMethodsTrait;
use Drupal\dipas\ReliablyHashValueTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class StatisticsBase extends CockpitDataResponseBase {

  use UrlProceedingsFilterTrait,
    UrlProceedingsFilterConfigTrait,
    ProceedingListingMethodsTrait,
    CommentQueryTrait,
    DateTimeTrait,
    ExternalProceedingsTrait,
    ReliablyHashValueTrait;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->dateFormatter = $container->get('date.formatter');
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginResponse() {
    $filters = [];

    foreach ($this->getURLProceedingFilterParams() as $URLParam => $config) {
      if ($this->currentRequest->query->has($URLParam)) {
        if (!$filterValue = $this->getValidatedFilterValues($this->currentRequest->query->all()[$URLParam], $config['URLParamValidation'])) {
          continue;
        }

        if (is_array($filterValue)) {
          sort($filterValue);
        }

        $filters[$URLParam] = $filterValue;
      }
    }

    $proceedingIDs = $this->getProceedingIDs('visible');

    if (isset($filters['districts']) && count($filters['districts'])) {
      $proceedingIDs = $this->filterProceedingsByAssignedTerms('data_districtselection', $filters['districts'], $proceedingIDs);
    }

    if (isset($filters['topics']) && count($filters['topics'])) {
      $proceedingIDs = $this->filterProceedingsByAssignedTerms('data_topicselection', $filters['topics'], $proceedingIDs);
    }

    if (isset($filters['owners']) && count($filters['owners'])) {
      $proceedingIDs = $this->filterProceedingsByAssignedTerms('project_owners', $filters['owners'], $proceedingIDs);
    }

    // If no proceedings are left to display, return an empty result set
    if (empty($proceedingIDs)) {
      return [$this->getStatisticsEntity() => []];
    }

    if (isset($filters['dateStart']) || isset($filters['dateEnd'])) {
      $user_timezone = new \DateTimeZone(date_default_timezone_get());

      if (isset($filters['dateStart'])) {
        $start = \DateTimeImmutable::createFromFormat(
          "Y-m-d H:i:s",
          $filters['dateStart'] . ' 00:00:00',
          $user_timezone
        )->getTimestamp();
      }

      if (isset($filters['dateEnd'])) {
        $end = \DateTimeImmutable::createFromFormat(
          "Y-m-d H:i:s",
          $filters['dateEnd'] . ' 23:59:59',
          $user_timezone
        )->getTimestamp();
      }
    }

    if ($this->getStatisticsEntity() === 'contributions') {
      $query = $this->getDatabase()
        ->select('node_field_data', 'n')
        ->condition('n.status', '1', '=')
        ->condition('n.type', 'contribution', '=');

      $query->addExpression("DATE_PART('YEAR', TO_TIMESTAMP(n.created)::timestamp without time zone at time zone 'Etc/UTC')", 'year');

      // We only need the actual domain assignment since contributions or comments are always specific
      // to their respective proceeding and do never belong to more than one domain at the same time.
      $query->addJoin(
        'LEFT',
        'node__field_domain_access',
        'domain',
        'n.nid = domain.entity_id AND n.vid = domain.revision_id AND n.type = domain.bundle AND n.langcode = domain.langcode'
      );
      $query->condition('domain.field_domain_access_target_id', $proceedingIDs, 'IN');

      if (isset($start)) {
        $query->condition('n.created', $start, '>=');
      }

      if (isset($end)) {
        $query->condition('n.created', $end, '<=');
      }

      if ($this->getStatisticsType() === 'ids') {
        $results = $query
          ->fields('n', ['nid'])
          ->orderBy('n.nid', 'ASC')
          ->execute()
          ->fetchAll();

        $return = [];

        foreach ($results AS $row) {
          if (!isset($return[$row->year])) {
            $return[$row->year] = [];
          }

          $return[$row->year][] = $row->nid;
        }

        return ['contributions' => $return];
      }
      elseif ($this->getStatisticsType() === 'count') {
        $query->groupBy('year');
        $query->addExpression('COUNT(n.nid)', 'contributions');

        $result = $query->execute()->fetchAll();

        $contributions = array_combine(
          array_map(function ($row) { return $row->year; }, $result),
          array_map(function ($row) { return $row->contributions; }, $result)
        );

        $externalProceedings = $this->getExternalProceedings(
          $filters['districts'] ?? [],
          $filters['topics'] ?? [],
          $filters['owners'] ?? [],
          $start ?? NULL,
          $end ?? NULL
        );

        // Conception comments are statistically treated like contributions
        $conceptioncomments = $this->getCommentCountByYears(
          [],
          'conception',
          $proceedingIDs,
          $start ?? NULL,
          $end ?? NULL
        );

        $years = array_unique(array_merge(array_keys($contributions), array_keys($conceptioncomments)));

        $return = [];

        foreach ($years as $year) {
          $return[$year] = ($contributions[$year] ?? 0) + ($conceptioncomments[$year] ?? 0);
        }

        foreach ($externalProceedings as $proceeding) {
          $year = $this->createDrupalDateTimeObject(
            strtotime($proceeding->get('field_date')->first()->get('value')->getValue())
          )->format('Y');

          if (!isset($return[$year])) {
            $return[$year] = 0;
          }

          $return[$year] += (int) $proceeding->get('field_contribution_count')->first()->getString() ?? 0;
        }

        ksort($return);

        return [
          'contributioncounts' => $return,
        ];
      }
      else {
        throw new MalformedRequestException('Unknown statistics type.');
      }
    }
    else if ($this->getStatisticsEntity() === 'comments') {
      if ($this->getStatisticsType() === 'ids') {
        return [
          'comments' => $this->getCommentEntityIDsByYears(
            [],
            'contribution',
            $proceedingIDs,
            $start ?? NULL,
            $end ?? NULL
          ),
        ];
      }
      elseif ($this->getStatisticsType() === 'count') {
        $externalProceedings = $this->getExternalProceedings(
          $filters['districts'] ?? [],
          $filters['topics'] ?? [],
          $filters['owners'] ?? [],
          $start ?? NULL,
          $end ?? NULL
        );

        $commentCounts = $this->getCommentCountByYears(
          [],
          'contribution',
          $proceedingIDs,
          $start ?? NULL,
          $end ?? NULL
        );

        foreach ($externalProceedings as $proceeding) {
          $year = $this->createDrupalDateTimeObject(
            strtotime($proceeding->get('field_date')->first()->get('value')->getValue())
          )->format('Y');

          if (!isset($commentCounts[$year])) {
            $commentCounts[$year] = 0;
          }

          $commentCounts[$year] += (int) $proceeding->get('field_comment_count')->first()->getString() ?? 0;
        }

        ksort($commentCounts);

        return [
          'commentcounts' => $commentCounts,
        ];
      }
      else {
        throw new MalformedRequestException('Unknown statistics type.');
      }
    }
    else {
      throw new MalformedRequestException('Unknown request entity.');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * @return string
   *   Either 'contributions' or 'comments'
   */
  abstract protected function getStatisticsEntity();

  /**
   * @return string
   *   Either 'id' or 'count'
   */
  abstract protected function getStatisticsType();

}
