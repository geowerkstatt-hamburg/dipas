<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\masterportal\UrlFromUriFixTrait;

trait ProceedingEntityTrait {

  use UrlFromUriFixTrait;

  /**
   * Returns the number of nodes related to the project.
   *
   * @param string $domain_id
   *   The id of the selected domain.
   * @return Number
   */
  protected function getNodeCount($domain_id, $nodeType) {
    return $this->getEntityTypeManager()
      ->getStorage('node')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $nodeType, '=')
      ->condition('status', 1, '=')
      ->condition('field_domain_access', $domain_id, '=')
      ->count()
      ->execute();
  }

  /**
   * Returns the number of comments to the project.
   *
   * @param string $domain_id
   *   The id of the selected domain.
   * @return Number
   */
  protected function getCommentCount($domain_id) {
    return $this->getEntityTypeManager()
      ->getStorage('comment')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1, '=')
      ->condition('field_domain_access', $domain_id, '=')
      ->count()
      ->execute();
  }

  /**
   * Returns a list of all documentation of a project.
   *
   * List the name, the url and the icon to be used in the masterportal gfi.
   *
   * @param $domain_id
   *
   * @return array
   *   array of all media entities of type 'download' and flag for documentation
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDokumentationDetails($domain_id) {
    $mediaStorage = $this->getEntityTypeManager()->getStorage('media');

    $media_entity_id_list = $mediaStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('bundle', 'download', '=')
      ->condition('field_domain_access', $domain_id, '=')
      ->condition('field_serve_for_documentation', TRUE, '=')
      ->execute();

    $documents = [];

    foreach ($media_entity_id_list as $media_entity_id => $value) {
      $media_entity = $mediaStorage->load($media_entity_id);

      $document_fid = $media_entity->get('field_media_file')
        ->first()
        ->get('target_id')
        ->getString();

      $document_name = $media_entity->getName();

      if ($document = $this->getEntityTypeManager()->getStorage('file')->load($document_fid)) {
        $document_data = [
          'name' => $document_name,
          'url' => $this->getFileUrlGenerator()->generateAbsoluteString($document->getFileUri()),
          'icon' => self::fromUriFixed(
              'base:/' . $this->getExtensionPathResolver()->getPath('module', 'dipas') .'/assets/',
              ['absolute' => TRUE]
            )->toString() . 'picture_as_pdf_white_24dp.svg'
        ];

        $documents[] = $document_data;
      }
    }

    return $documents;
  }

  /**
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  abstract protected function getEntityTypeManager();

  /**
   * @return \Drupal\Core\File\FileUrlGeneratorInterface
   */
  abstract protected function getFileUrlGenerator();

  /**
   * @return \Drupal\Core\Extension\ExtensionPathResolver
   */
  abstract protected function getExtensionPathResolver();

}
