<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;
use \DateTime;
use \DateTimeZone;

trait ExternalProceedingsTrait {

  protected function getExternalProceedingDocumentations($proceedingid = NULL) {
    $documentations = $this->getExternalProceedingMetadata(
      'field_documentations',
      [
        'upload_date' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) {
          $user_timezone = new \DateTimeZone(date_default_timezone_get());
          $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
          $documentationTime = new DrupalDateTime($paragraph->get('field_date')->first()->getString(), $storage_timezone);

          return $this->convertTimestampToUTCDateTimeString($documentationTime->setTimezone($user_timezone)->getTimestamp(), FALSE);
        },
        'url' => function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
          return $paragraph->get('field_external_url')->first()->getString();
        },
        'fordoc' => function () { return TRUE; },
      ]
    );

    if ($proceedingid) {
      $documentations = array_filter(
        $documentations,
        function ($documentation) use ($proceedingid) {
          return $documentation->proceedingid === $proceedingid;
        }
      );
    }

    return $documentations;
  }

  protected function getExternalProceedingAppointments() {
    $user_timezone = new \DateTimeZone(date_default_timezone_get());
    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);

    $appointments = $this->getExternalProceedingMetadata(
      'field_appointments',
      [
        'title' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) {
          return $paragraph->get('field_headline')->first()->getString();
        },
        'description' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) {
          return $paragraph->get('field_description')->first()->getString();
        },
        'start' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) use ($user_timezone, $storage_timezone) {
          $appointmentStart = new DrupalDateTime($paragraph->get('field_date')->first()->get('value')->getString(), $storage_timezone);

          return $this->convertTimestampToUTCDateTimeString($appointmentStart->setTimezone($user_timezone)->getTimestamp(), FALSE);
        },
        'end' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) use ($user_timezone, $storage_timezone) {
          $appointmentEnd = new DrupalDateTime(
            $paragraph->get('field_date')->first()->get('end_value')->getString()
              ? $paragraph->get('field_date')->first()->get('end_value')->getString()
              : $paragraph->get('field_date')->first()->get('value')->getString(),
            $storage_timezone
          );

          return $this->convertTimestampToUTCDateTimeString($appointmentEnd->setTimezone($user_timezone)->getTimestamp(), FALSE);
        },
        'expires' => function(NodeInterface $proceeding, ParagraphInterface $paragraph) use ($user_timezone, $storage_timezone) {
          $appointmentEnd = new DrupalDateTime(
            $paragraph->get('field_date')->first()->get('end_value')->getString()
              ? $paragraph->get('field_date')->first()->get('end_value')->getString()
              : $paragraph->get('field_date')->first()->get('value')->getString(),
            $storage_timezone
          );

          return $this->convertTimestampToUTCDateTimeString($appointmentEnd->setTimezone($user_timezone)->getTimestamp(), FALSE);
        },
      ]
    );

    return array_filter(
      $appointments,
      function ($appointment) {
        $now = strtotime($this->convertTimestampToUTCDateTimeString(time(), FALSE));
        $appointmentExpires = strtotime($appointment->expires);

        return $now <= $appointmentExpires;
      }
    );
  }

  protected function getExternalProceedingMostRecentContributions($count = 3) {
    $contributions = $this->getExternalProceedingContributions(FALSE);

    $creationDates = [];
    foreach ($contributions as $index => $contribution) {
      $creationDates[$index] = strtotime($contribution->created);
    }
    array_multisort($creationDates, SORT_DESC, $contributions);

    return array_slice($contributions, 0, $count);
  }

  protected function getExternalProceedingMostCommentedContributions($count = 3) {
    $contributions = $this->getExternalProceedingContributions();

    $commentCounts = [];
    foreach ($contributions as $index => $contribution) {
      $commentCounts[$index] = (int) $contribution->commentsCount;
    }
    array_multisort($commentCounts, SORT_DESC, $contributions);

    return array_slice($contributions, 0, $count);
  }

  protected function getExternalProceedingContributions($includeCommentCount = TRUE) {
    $enrichWith = [
      'title' => function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
        return $paragraph->get('field_headline')->first()->getString();
      },
      'created' => function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
        $user_timezone = new \DateTimeZone(date_default_timezone_get());
        $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
        $createdTime = new DrupalDateTime($paragraph->get('field_date')->first()->get('value')->getString(), $storage_timezone);

        return $this->convertTimestampToUTCDateTimeString($createdTime->setTimezone($user_timezone)->getTimestamp(), FALSE);
      },
      'content' => function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
        return $paragraph->get('field_description')->first()->getString();
      },
      'url' => function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
        return ($url = $paragraph->get('field_external_url')->first()) ? $url->getString() : '';
      },
    ];

    if ($includeCommentCount) {
      $enrichWith['commentsCount'] = function (NodeInterface $proceeding, ParagraphInterface $paragraph) {
        return $paragraph->get('field_comment_count')->first()->getString();
      };
    }

    return $this->getExternalProceedingMetadata('field_contributions', $enrichWith);
  }

  protected function getExternalProceedingsContributionCount() {
    $proceedings = $this->getExternalProceedings();

    $user_timezone = new \DateTimeZone(date_default_timezone_get());
    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);

    $contributionsByYear = [];
    foreach ($proceedings as $proceeding) {
      $proceedingYear = new DrupalDateTime($proceeding->get('field_date')->first()->get('value')->getString(), $storage_timezone);
      $proceedingYear->setTimezone($user_timezone);

      if (!isset($contributionsByYear[$proceedingYear->format('Y')])) {
        $contributionsByYear[$proceedingYear->format('Y')] = 0;
      }

      $contributionsByYear[$proceedingYear->format('Y')] += (int) $proceeding->get('field_contribution_count')->first()->getString();
    }

    return $contributionsByYear;
  }

  protected function getExternalProceedingsCommentCount() {
    $proceedings = $this->getExternalProceedings();

    $user_timezone = new \DateTimeZone(date_default_timezone_get());
    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);

    $commentCountByYear = [];
    foreach ($proceedings as $proceeding) {
      $proceedingYear = new DrupalDateTime($proceeding->get('field_date')->first()->get('value')->getString(), $storage_timezone);
      $proceedingYear->setTimezone($user_timezone);

      if (!isset($commentCountByYear[$proceedingYear->format('Y')])) {
        $commentCountByYear[$proceedingYear->format('Y')] = 0;
      }

      $commentCountByYear[$proceedingYear->format('Y')] += (int) $proceeding->get('field_comment_count')->first()->getString();
    }

    return $commentCountByYear;
  }

  final protected function getExternalProceedingMetadata(string $fieldname, array $enrichWith = []) {
    $user_timezone = new \DateTimeZone(date_default_timezone_get());
    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);

    $metadataGrouped = array_map(
      function (NodeInterface $proceeding) use ($fieldname, $enrichWith, $user_timezone, $storage_timezone) {
        $paragraphs = array_map(
          function ($entry) {
            return $entry['target_id'];
          },
          $proceeding->get($fieldname)->getValue()
        );
        $paragraphs = $this->getEntityStorage('paragraph')->loadMultiple($paragraphs);

        $entries = [];

        array_walk(
          $paragraphs,
          function (ParagraphInterface $paragraph) use ($enrichWith, $proceeding, &$entries, $user_timezone, $storage_timezone) {
            $startTime = new DrupalDateTime(
              $proceeding->get('field_date')->first()->get('value')->getString(), $storage_timezone
            );
            $endTime = new DrupalDateTime(
              $proceeding->get('field_date')->first()->get('end_value')->getString(), $storage_timezone
            );

            $entry = [
              'proceedingid' => $proceeding->id(),
              'proceedingname' => $proceeding->getTitle(),
              'proceedingschedule_start' => $this->convertTimestampToUTCDateTimeString($startTime->setTimezone($user_timezone)->getTimestamp(), FALSE),
              'proceedingschedule_end' => $this->convertTimestampToUTCDateTimeString($endTime->setTimezone($user_timezone)->getTimestamp(), FALSE),
              'proceedingtopics' => $this->getExternalProceedingTerms(
                $proceeding->id(),
                'topics',
                'field_proceeding_topics',
                [],
                'name'
              ),
              'proceedingdistricts' => $this->getExternalProceedingTerms(
                $proceeding->id(),
                'districts',
                'field_proceeding_districts',
                [],
                'name'
              ),
              'proceedingowners' => $this->getExternalProceedingTerms(
                $proceeding->id(),
                'project_owner',
                'field_proceeding_operators',
                [],
                'name'
              ),
              'externalProceeding' => TRUE,
              'proceedingIsOnline' => (bool) $proceeding->get('field_live_proceeding')->first()->get('value')->getString(),
              'proceedingURL' => ($url = $proceeding->get('field_proceeding_url')->first()) ? $url->getString() : '',
            ];

            foreach ($enrichWith as $propertyName => $callback) {
              $entry[$propertyName] = $callback($proceeding, $paragraph);
            }

            $entries[] = $entry;
          }
        );

        return $entries;
      },
      array_filter(
        $this->getExternalProceedings(),
        function (NodeInterface $proceeding) use ($fieldname) {
          return !$proceeding->get($fieldname)->isEmpty();
        }
      )
    );

    $metadata = [];

    foreach ($metadataGrouped as $proceeding) {
      $metadata = array_merge($metadata, array_map(function ($item) { return (object) $item; }, $proceeding));
    }

    return $metadata;
  }

  /**
   * @return \Drupal\node\NodeInterface[]
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  final protected function getExternalProceedings(
    array $districts = [],
    array $topics = [],
    array $owners = [],
    int $start = NULL,
    int $end = NULL
  ) {
    $requestHash = $this->reliablyHashValue([
      'districts' => $districts,
      'topics' => $topics,
      'owners' => $owners,
      'start' => $start,
      'end' => $end,
    ]);
    $proceedings = drupal_static(sprintf('dipas_external_proceedings_%s', $requestHash), []);

    if (empty($proceedings)) {
      $query = $this->getEntityTypeManager()
        ->getStorage('node')
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('type', 'external_proceeding', '=')
        ->condition('status', '1', '=');

      if (count($districts)) {
        $query->condition('field_proceeding_districts', $districts, 'IN');
      }

      if (count($topics)) {
        $query->condition('field_proceeding_topics', $topics, 'IN');
      }

      if (count($owners)) {
        $query->condition('field_proceeding_operators', $owners, 'IN');
      }

      if ($start) {
        $query->condition(
          'field_date.value',
          $this->createDrupalDateTimeObject($start, FALSE)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          '>='
        );
      }

      if ($end) {
        $query->condition(
          'field_date.end_value',
          $this->createDrupalDateTimeObject($end, FALSE)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          '<='
        );
      }

      $ids = $query->execute();

      $proceedings = $this->getEntityStorage('node')->loadMultiple($ids);
    }

    return $proceedings;
  }

  /**
   * @param string $entity_type_id
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  final protected function getEntityStorage($entity_type_id) {
    $storages = drupal_static('dipas_entity_storage_interfaces', []);

    if (empty($storages[$entity_type_id])) {
      $storages[$entity_type_id] = $this->getEntityTypeManager()->getStorage($entity_type_id);
    }

    return $storages[$entity_type_id];
  }

  final protected function getExternalProceedingTerms(int $proceedingID, string $vocab, string $fieldname, array $include_fields = [], $flatten_to_property = FALSE) {
    $proceeding = $this->getExternalProceedings()[$proceedingID];

    return $this->getAssignedTerms(
      $vocab,
      $include_fields,
      array_map(
        function ($item) {
          return $item['target_id'];
        },
        $proceeding->get($fieldname)->getValue()
      ),
      $flatten_to_property
    );
  }

  /**
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  abstract public function getEntityTypeManager();

  /**
   * @param int $timestamp
   * @param \DateTimeZone $timeZone
   *
   * @return \DateTime
   */
  abstract protected function createDateTimeObject($timestamp, DateTimeZone $timeZone);

  /**
   * @param int $timestamp
   * @param boolean $isUTC
   *
   * @return string
   * @throws \Exception
   */
  abstract protected function convertTimestampToUTCDateTimeString($timestamp, $isUTC);

  /**
   * @param String $vocab
   *   The vocabulary ID
   * @param array $include_fields
   *   An array of fields to include from the terms fetched in the form ['fieldname' => function () { return $value; }]
   * @param array|NULL $assigned_ids
   *   Array holding the assigned term ids
   * @param String $flatten_to_property
   *   Should a term get flattened to a specific property?
   *
   * @return array
   */
  abstract protected function getAssignedTerms($vocab, array $include_fields, $assigned_ids, $flatten_to_property = FALSE);

  /**
   * @param mixed $value
   *   The value to hash
   * @return string
   *   The hash for the value provided.
   */
  abstract protected function reliablyHashValue($value);

}
