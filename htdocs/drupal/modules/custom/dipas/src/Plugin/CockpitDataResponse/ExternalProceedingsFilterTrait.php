<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

trait ExternalProceedingsFilterTrait {

  /**
   * Creates a list of external proceedings for the DIPAS navigator, filtered by URL parameters.
   *
   * This method is used to overwrite the original implementation of the getExternalProceedingMetadata
   * in the ExternalProceedingsTrait, see MostCommentedContributions and RecentContributions
   *
   * @param string $fieldname
   *  Name of the key to access the Drupal config data, e.g. field_contributions
   * @param array $enrichWith
   *   List of callbacks to enrich the external proceeding items
   * @return array
   *   Filtered external proceedings items
   */
  final protected function getExternalProceedingMetadata(string $fieldname, array $enrichWith = []) {
    // Get the unfiltered data
    $metadata = $this->getUnfilteredExternalProceedingMetadata($fieldname, $enrichWith);

    // Filter by URL parameters for DIPAS navigator
    if (!empty($this->getCurrentRequest()->query->all())) {
      if ($this->isURLFilterActive()) {
        // Filter params, e.g. topics, districts, owners
        foreach ($this->getURLProceedingFilterParams() as $URLParam => $config) {
          if (is_null($config['configParamNameExternalProceeding'])) {
            continue;
          }

          if (
            $this->getCurrentRequest()->query->has($URLParam) &&
            $filterValues = $this->getValidatedFilterValues($this->getCurrentRequest()->query->all()[$URLParam], $config['URLParamValidation'])
          ) {
            $metadata = array_filter($metadata, function($data) use ($config, $filterValues) {
              return count(array_intersect($filterValues, array_keys($data->{$config['configParamNameExternalProceeding']}))) >= 1;
            });
          }
        }

        $start = $this->getCurrentRequest()->query->get('dateStart');
        $end = $this->getCurrentRequest()->query->get('dateEnd');

        // Filter date
        if ($start && $end) {
          $metadata = array_filter($metadata, function($data) use ($start, $end) {
            $createdTimestamp = strtotime(date('Y-m-d', strtotime($data->created)));

            return $createdTimestamp >= strtotime($start) && $createdTimestamp <= strtotime($end);
          });
        }
      }
    }

    return $metadata;
  }

  /**
   * Creates the unfiltered list of external proceedings.
   *
   * This method replaces the implementation of getExternalProceedingMetadata in the ExternalProceedingsTrait.
   * see MostCommentedContributions and RecentContributions
   *
   * @param string $fieldname
   *  Name of the key to access the Drupal config data, e.g. field_contributions
   * @param array $enrichWith
   *   List of callbacks to enrich the external proceeding items
   * @return array
   *   Filtered external proceedings items
   */
  abstract protected function getUnfilteredExternalProceedingMetadata(string $fieldname, array $enrichWith = []);

  /**
   * @return \Symfony\Component\HttpFoundation\Request
   */
  abstract protected function getCurrentRequest();

  /**
   * Helper method which detects, if URL filter params are present in the current request (singleton)
   *
   * @return bool
   *   TRUE if a configured filter parameter is found in the current request
   */
  abstract protected function isURLFilterActive(): bool;

  /**
   * Provides a list of parameters to filter proceedings.
   *
   * Also used as the source to generate the cache key and validate values.
   *
   * Array-Structure:
   * [
   *   URL parameter name => [
   *     'configParamName' => string|NULL, Configuration parameter name (NULL, if there is no associated config),
   *     'URLParamValidation' => [
   *       'valueIsArray' => boolean, Request parameters to be expected as an array?
   *       'regex' => string, Regex to validate the request parameter
   *     ],
   *   ]
   *   ...
   * ]
   *
   * @return array
   *   List of parameters
   */
  abstract protected function getURLProceedingFilterParams(): array;

  /**
   * Validates value against a validation configuration.
   *
   * @param mixed $filterValue
   *   Value of the filter to validate
   *
   * @param array $validationConfig
   *   Validation configuration, see $this->getURLProceedingFilterParams()
   *
   *   Array-Structure:
   *   [
   *     'valueIsArray' => boolean, Request parameters to be expected as an array?
   *     'regex' => string, Regex to validate the request parameter
   *   ]
   *
   * @return mixed
   *   Returns the validated value
   */
  abstract protected function getValidatedFilterValues(mixed $filterValue, array $validationConfig): mixed;
}
