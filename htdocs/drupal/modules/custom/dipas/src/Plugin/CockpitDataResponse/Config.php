<?php

namespace Drupal\dipas\Plugin\CockpitDataResponse;

use Drupal\dipas\Annotation\CockpitDataResponse;

/**
 * Class Config.
 *
 * This plugin returns selected config and translation data.
 * The translated data is based on the language code set in the URL, e.g. /drupal/de, /drupal/en.
 * If the language code is not set, the default language is used as a fallback.
 *
 * /drupal/de/dipas/navigator/config -> German
 * /drupal/en/dipas/navigator/config -> English
 * /drupal/dipas/navigator/config -> English (default fallback)
 *
 * @CockpitDataResponse(
 *   id = "config",
 *   description = @Translation("Returns selected DIPAS config values / translations required by the DIPAS navigator."),
 *   requestMethods = {
 *     "GET",
 *   },
 *   isCacheable = true,
 *   maxAge = 9999
 * )
 *
 * @package Drupal\dipas\Plugin\CockpitDataResponse
 */
class Config extends CockpitDataResponseBase {

  /**
   * {@inheritdoc}
   */
  protected function getPluginResponse(): array {
    return [
      'config' => [
        'translations' => [
          'taxonomies' => [
            'districts' => $this->configFactory->get('taxonomy.vocabulary.districts')->get()['name'],
            'topics' => $this->configFactory->get('taxonomy.vocabulary.topics')->get()['name'],
            'owners' => $this->configFactory->get('taxonomy.vocabulary.project_owner')->get()['name'],
          ]
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getResponseKeyCacheTags(): array {
    return ['CockpitDataResponse', 'CockpitDataResponseConfig'];
  }

}
