<?php

namespace Drupal\dipas\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;

/**
 * Plugin implementation of the 'configurable_daterange' widget.
 *
 * @FieldWidget(
 *   id = "configurable_daterange",
 *   label = @Translation("Date and time range (configurable)"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class ConfigurableDateRange extends DateRangeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'showEndDate' => TRUE,
      'collectTime' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $endDateOptional = (bool) $this->getFieldSetting('optional_end_date');

    return [
      'showEndDate' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show end date'),
        '#default_value' => $endDateOptional ? $this->getSetting('showEndDate') : 1,
        '#disabled' => !$endDateOptional,
      ],
      'collectTime' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Collect time'),
        '#default_value' => $this->getSetting('collectTime'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t(
        'Show end date: @showEndDate',
        [
          '@showEndDate' => $this->getSetting('showEndDate')
            ? $this->t('Yes')
            : $this->t('No')
        ],
      ),
      $this->t(
        'Collect time: @collectTime',
        [
          '@collectTime' => $this->getSetting('collectTime')
            ? $this->t('Yes')
            : $this->t('No')
        ],
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (!$settings['collectTime']) {
      foreach (['value', 'end_value'] as $key) {
        $element[$key]['#date_time_element'] = 'none';
        $element[$key]['#date_time_format'] = '';
      }
    }

    if (!$settings['showEndDate']) {
      $element['value']['#title'] = $this->t('Date');
      unset($element['#element_validate']);
      unset($element['end_value']);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $datetime_type = $this->getFieldSetting('datetime_type');
    if ($datetime_type === DateRangeItem::DATETIME_TYPE_DATE) {
      $storage_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
    }
    else {
      $storage_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    }

    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
    $user_timezone = new \DateTimeZone(date_default_timezone_get());

    $collectTime = (bool) $this->getSetting('collectTime');

    foreach ($values as &$item) {
      if (!empty($item['value'])) {
        foreach (['value', 'end_value'] as $key) {
          if (isset($item[$key])) {
            $dateTimeObject = $item[$key] instanceof DrupalDateTime
              ? $item[$key]
              : new DrupalDateTime($item[$key], $user_timezone);

            $dateTimeObject->setTimeZone($user_timezone);

            if (!$collectTime) {
              call_user_func_array([$item[$key], 'setTime'], $key === 'value' ? [0, 0, 0] : [23, 59, 59]);
            }

            $item[$key] = $dateTimeObject->setTimezone($storage_timezone)->format($storage_format);
          }
        }
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($this->getSetting('showEndDate')) {
      parent::validateStartEnd($element, $form_state, $complete_form);
    }
  }

}
