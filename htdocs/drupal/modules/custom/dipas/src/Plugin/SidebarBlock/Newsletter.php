<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SidebarBlock;

use Drupal\dipas\Annotation\SidebarBlock;

/**
 * Class Newsletter
 *
 * @SidebarBlock(
 *   id = "newsletter",
 *   name = @Translation("Newsletter subscription"),
 *   description = @Translation("Toggles the availability of the newsletter subscription sidebar component.")
 * )
 *
 * @package Drupal\dipas\Plugin\SidebarBlock
 */
class Newsletter extends SidebarBlockBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm($requiredSelector) {
    return [
      'headline' => [
        '#type' => 'textfield',
        '#title' => $this->t('Headline', [], ['context' => 'DIPAS']),
        '#default_value' => isset($this->settings['headline']) ? $this->settings['headline'] : '',
        '#maxlength' => 100,
        '#states' => [
          'required' => [$requiredSelector => ['checked' => TRUE]],
        ],
      ],
      'snippet' => [
        '#type' => 'textarea',
        '#title' => $this->t('JavaScript snippet', [], ['context' => 'DIPAS']),
        '#default_value' => isset($this->settings['snippet']) ? $this->settings['snippet'] : '',
        '#states' => [
          'required' => [$requiredSelector => ['checked' => TRUE]],
        ],
      ],
    ];
  }

}
