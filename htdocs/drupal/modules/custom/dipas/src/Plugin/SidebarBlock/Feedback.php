<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SidebarBlock;

use Drupal\dipas\Annotation\SidebarBlock;

/**
 * Class Feedback
 *
 * @SidebarBlock(
 *   id = "feedback",
 *   name = @Translation("Feedback"),
 *   description = @Translation("Toggles the availability of the Feedback sidebar component.")
 * )
 *
 * @package Drupal\dipas\Plugin\SidebarBlock
 */
class Feedback extends SidebarBlockBase {

  /**
   * {@inheritdoc}
   *
   * TODO: #states/required does not work properly, although bug is marked as fixed
   * @see https://www.drupal.org/project/drupal/issues/997826
   */
  public function getSettingsForm($requiredSelector) {
    return [
      'feedbackText' => [
        '#type' => 'text_format',
        '#format' => 'filtered_html',
        '#allowed_formats' => ['filtered_html'],
        '#title' => $this->t('Feedback text', [], ['context' => 'DIPAS']),
        '#description' => $this->t('This text is going to be displayed in the sidebar panel, when the feedback panel toggle has been activated.', [], ['context' => 'DIPAS']),
        '#default_value' => $this->settings['feedbackText']['value'] ?? '',
      ],
    ];
  }

}
