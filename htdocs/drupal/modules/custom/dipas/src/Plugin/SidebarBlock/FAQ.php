<?php

/**
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html GPL-2.0-or-later
 */

namespace Drupal\dipas\Plugin\SidebarBlock;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\dipas\Annotation\SidebarBlock;
use Drupal\dipas\Plugin\SettingsSection\NodeSelectionTrait;
use Drupal\masterportal\DomainAwareTrait;

/**
 * Class FAQ
 *
 * @SidebarBlock(
 *   id = "faq",
 *   name = @Translation("FAQ"),
 *   description = @Translation("Toggles the availability of the FAQ sidebar component.")
 * )
 *
 * @package Drupal\dipas\Plugin\SidebarBlock
 */
class FAQ extends SidebarBlockBase {

  use NodeSelectionTrait,
    DomainAwareTrait;

  /**
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->nodeStorage = $container->get('entity_type.manager')->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm($requiredSelector) {
    return [
      'node' => [
        '#type' => 'select',
        '#title' => $this->t('Page', [], ['context' => 'DIPAS']),
        '#options' => $this->getPageOptions(),
        '#default_value' => $this->settings['node'] ?? NULL,
        '#states' => [
          'required' => [$requiredSelector => ['checked' => TRUE]],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeStorage() {
    return $this->nodeStorage;
  }

}
