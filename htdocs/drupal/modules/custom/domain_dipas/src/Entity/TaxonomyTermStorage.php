<?php

namespace Drupal\domain_dipas\Entity;

use Drupal\domain_access\DomainAccessManagerInterface;
use Drupal\masterportal\DomainAwareTrait;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;

/**
 * Class TaxonomyTermStorage.
 *
 * Extends the default {@link TaxonomyStorage} to filter the Terms by assigned
 * domains.
 *
 * @package Drupal\domain_dipas\Entity
 */
class TaxonomyTermStorage extends TermStorage {

  use DomainAwareTrait;

  /**
   * {@inheritDoc}
   */
  public function loadTree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
    $tree = parent::loadTree($vid, $parent, $max_depth, $load_entities);

    $activeDomainId = $this->getActiveDomain();

    $filteredTree = array_filter(
      $tree,
      function ($term) use ($activeDomainId) {
        if ($term instanceof TermInterface) {
          if (
            $term->hasField(DomainAccessManagerInterface::DOMAIN_ACCESS_ALL_FIELD) ||
            $term->hasField(DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD)
          ) {
            // Terms of the current vocabulary utilize domain affiliation data,
            // so we need to check their domain affiliation in order to decide
            // wheter this term is accessible from the current domain or not
            if ($term->hasField(DomainAccessManagerInterface::DOMAIN_ACCESS_ALL_FIELD)) {
              $accessAllField = $term->get(DomainAccessManagerInterface::DOMAIN_ACCESS_ALL_FIELD);

              if (
                ($first = $accessAllField->first()) &&
                (bool) $first->getValue()['value']
              ) {
                // The term is marked as "available for all domains".
                return TRUE;
              }
            }

            if ($term->hasField(DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD)) {
              $accessField = $term->get(DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD);

              foreach ($accessField->getValue() as $domainId) {
                if ($domainId['target_id'] === $activeDomainId) {
                  // The term is marked as "available for this domain".
                  return TRUE;
                }
              }

              // No marking for this domain found.
              return FALSE;
            }
          }
          else {
            // This taxonomy vocabulary does not utilize domain affiliation,
            // hence terms in this vocabulary are considered to be available everywhere
            return TRUE;
          }
        }
        else if (isset($term->domain)) {
          return $term->domain === $activeDomainId;
        }

        return TRUE;
      }
    );

    return array_values($filteredTree);
  }

}
