<?php

namespace Drupal\dipas_design\EventSubscriber;

use Drupal\dipas_design\Service\DipasDesignAPIInterface;
use Drupal\masterportal\DomainAwareTrait;
use Drupal\masterportal\Event\MasterportalLayerDataEventInterface;
use Drupal\masterportal\Event\MasterportalLayerDefinitionEventInterface;
use Drupal\masterportal\Event\MasterportalLayerEvents;
use Drupal\masterportal\GeoJSONFeatureInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MasterportalEvents implements EventSubscriberInterface {

  use DomainAwareTrait;

  protected $apiService;

  public function __construct(DipasDesignAPIInterface $api_service) {
    $this->apiService = $api_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MasterportalLayerEvents::LayerGeoJsonPluginLayerdefinitionAlter => 'onGeoJsonLayerdefinitionAlter',
      MasterportalLayerEvents::LayerGeoJsonPluginDataAlter => 'onGeoJsonDataAlter',
    ];
  }

  public function onGeoJsonLayerdefinitionAlter(MasterportalLayerDefinitionEventInterface $event) {
    if ($event->getLayerID() === 'contributions') {
      $layerDefinition = $event->getLayerDefinition();

      if (isset($layerDefinition->gfiAttributes)) {
        $layerDefinition->gfiAttributes->ButtonBackgroundColor = 'ButtonBackgroundColor';
        $layerDefinition->gfiAttributes->ButtonBackgroundHoverColor = 'ButtonBackgroundHoverColor';
        $layerDefinition->gfiAttributes->ButtonTextColor = 'ButtonTextColor';
      }

      $event->setLayerDefinition($layerDefinition);
    }
  }

  public function onGeoJsonDataAlter(MasterportalLayerDataEventInterface $event) {
    if ($event->getLayerID() === 'contributions') {
      $colors = $this->apiService->getConfiguredColors();

      $layerData = $event->getLayerData();

      array_walk(
        $layerData,
        function (GeoJSONFeatureInterface $feature) use ($colors) {
          $feature->addProperty('ButtonBackgroundColor', $colors['primary2']);
          $feature->addProperty('ButtonBackgroundHoverColor', $colors['primaryButtonHover']);
          $feature->addProperty('ButtonTextColor', $colors['primary2TextColor']);
        }
      );

      $event->setCacheTags(['dipas_design.colors', 'dipas_design.colors:' . $this->getActiveDomain()]);
    }
  }

}
