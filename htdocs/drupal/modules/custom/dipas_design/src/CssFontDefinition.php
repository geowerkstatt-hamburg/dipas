<?php

namespace Drupal\dipas_design;

class CssFontDefinition {

  protected $meta = [];

  protected $formats = [];

  public function __construct($familyName, $style, $weight) {
    $this->meta['name'] = $familyName;
    $this->meta['style'] = $style;
    $this->meta['weight'] = $weight;
  }

  public function setFormat($format, $url) {
    $this->formats[$format] = $url;
  }

  public function __toString() {
    $lines = ['@font-face {'];
    $lines[] = sprintf("\tfont-family: '%s';", $this->meta['name']);
    $lines[] = sprintf("\tfont-style: %s;", $this->meta['style']);
    $lines[] = sprintf("\tfont-weight: %s;", $this->meta['weight']);

    $lines[] = sprintf("\tsrc: url('%s'); /* IE9 Compat Modes */", $this->formats['eot']);
    $lines[] = "\tsrc: local(''),";
    $lines[] = sprintf("\t     url('%s?#iefix') format('embedded-opentype'),  /* IE6-IE8 */", $this->formats['eot']);
    $lines[] = sprintf("\t     url('%s') format('woff2'), /* Super Modern Browsers */", $this->formats['woff2']);
    $lines[] = sprintf("\t     url('%s') format('woff'), /* Modern Browsers */", $this->formats['woff']);
    $lines[] = sprintf("\t     url('%s') format('truetype'), /* Safari, Android, iOS */", $this->formats['ttf']);
    $lines[] = sprintf("\t     url('%s#%s') format('svg'); /* Legacy iOS */", $this->formats['svg'], $this->meta['name']);

    $lines[] = '}';

    return implode("\r\n", $lines);
  }

}
