<?php

namespace Drupal\dipas_design\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\DomainAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Images extends DipasDesignFormbase {

  use DomainAwareTrait;

  const FILE_UPLOAD_PATH = 'public://dipas/images';

  /**
 * @var \Drupal\file\FileStorageInterface
 */
  protected $fileStorage;

  public function setAdditionalDependencies(ContainerInterface $container) {
    $this->fileStorage = $container->get('entity_type.manager')->getStorage('file');
  }

  public static function getDefaults() : array {
    return [
      'favicon' => NULL,
      'operatorlogo' => NULL,
      'alignment' => 'left',
      'alttext' => 'powered by DIPAS - Digital participation system',
      'menulinelogo' => NULL,
    ];
  }

  public function uploadConfiguredValue($key) {
    if ($fileID = $this->getConfigValue($key)) {
      return [$fileID];
    }

    return NULL;
  }

  public function getFormElements(array $form, FormStateInterface $form_state) : array {
    return [
      'favicon' => [
        '#type' => 'managed_file',
        '#title' => 'Favicon',
        '#upload_location' => static::FILE_UPLOAD_PATH,
        '#upload_validators' => [
          'file_validate_extensions' => ['ico'],
        ],
        '#default_value' => $this->uploadConfiguredValue('favicon'),
      ],
      'operatorlogo' => [
        '#type' => 'managed_file',
        '#title' => $this->t('Operator logo', [], ['context' => 'dipas_design']),
        '#upload_location' => static::FILE_UPLOAD_PATH,
        '#upload_validators' => [
          'file_validate_extensions' => ['jpg jpeg png gif webp'],
        ],
        '#default_value' => $this->uploadConfiguredValue('operatorlogo'),
      ],
      'alignment' => [
        '#type' => 'radios',
        '#title' => $this->t('Alignment'),
        '#options' => [
          'left' => $this->t('left'),
          'right' => $this->t('right'),
        ],
        '#default_value' => $this->getConfigValue('alignment'),
      ],
      'alttext' => [
        '#type' => 'textfield',
        '#title' => $this->t('Alternative text'),
        '#default_value' => $this->getConfigValue('alttext'),
      ],
      'menulinelogo' => [
        '#type' => 'managed_file',
        '#title' => $this->t('Menuline logo', [], ['context' => 'dipas_design']),
        '#upload_location' => static::FILE_UPLOAD_PATH,
        '#upload_validators' => [
          'file_validate_extensions' => ['jpg jpeg png gif webp'],
        ],
        '#default_value' => $this->uploadConfiguredValue('menulinelogo'),
      ],
    ];
  }

  public function setConfigValues(array &$form, FormStateInterface $form_state) : array {
    $settings = [];

    foreach (['favicon', 'operatorlogo', 'menulinelogo'] as $upload) {
      if ($fileID = $form_state->getValue($upload)[0] ?? NULL) {
        $fileEntity = $this->fileStorage->load($fileID);
        $fileEntity->setPermanent();
        $fileEntity->save();
      }

      $settings[$upload] = $fileID;
    }

    $settings['alignment'] = $form_state->getValue('alignment');
    $settings['alttext'] = $form_state->getValue('alttext');

    return $settings;
  }

  protected function getCacheTagsToInvalidate() {
    return ['dipas_design.images:' . $this->getActiveDomain()];
  }

}
