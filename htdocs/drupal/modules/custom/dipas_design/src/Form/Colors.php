<?php

namespace Drupal\dipas_design\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\DomainAwareTrait;

class Colors extends DipasDesignFormbase {

  use DomainAwareTrait;

  public static function getDefaults() : array {
    return [
      'primary1' => '#003063',
      'primary2' => '#e10019',
      'primary1TextColor' => '#ffffff',
      'primary2TextColor' => '#ffffff',
      'font' => '#212529',
      'fontDisabled' => '#b0b6ba',
      'fontPlaceholder' => '#5f666e',
      'tableHeader' => '#eceef3',
      'formBackground' => '#f7f8fa',
      'border' => '#e0e0e0',
      'focus' => '#007bff',
      'inactiveNavElementHoverColorUnderline' => '#ffb7bf',
      'buttonDisabled' => '#f8f2f2',
      'primaryButtonHover' => '#b4081b',
      'blank' => '#ffffff',
      'stateError' => '#E81C23',
      'stateActive' => '#DAEFDE',
      'stateActiveBorder' => '#005D00',
      'stateCompleted' => '#FFF5E3',
      'stateCompletedBorder' => '#B84430',
    ];
  }

  public function getFormElements(array $form, FormStateInterface $form_state) : array {
    return [
      'colors' => [
        '#type' => 'container',

        'primary1' => [
          '#type' => 'color',
          '#title' => $this->t('Primary1 color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The primary1 color is used for elements like the menu background', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('primary1'),
          '#required' => TRUE,
        ],
        'primary2' => [
          '#type' => 'color',
          '#title' => $this->t('Primary2 color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The primary2 color is used for active primary buttons', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('primary2'),
          '#required' => TRUE,
        ],
        'primary1TextColor' => [
          '#type' => 'color',
          '#title' => $this->t('Primary 1 text contrast color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is being used for text on top of a primary1 color background', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('primary1TextColor'),
          '#required' => TRUE,
        ],
        'primary2TextColor' => [
          '#type' => 'color',
          '#title' => $this->t('Primary 2 text contrast color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is being used for text on top of a primary2 color background', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('primary2TextColor'),
          '#required' => TRUE,
        ],
        'font' => [
          '#type' => 'color',
          '#title' => $this->t('Font color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The font color is used for elements like headings or paragraphs', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('font'),
          '#required' => TRUE,
        ],
        'fontDisabled' => [
          '#type' => 'color',
          '#title' => $this->t('Font color for disabled elements', [], ['context' => 'dipas_design']),
          '#description' => $this->t('This font color is used on disabled elements', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('fontDisabled'),
          '#required' => TRUE,
        ],
        'fontPlaceholder' => [
          '#type' => 'color',
          '#title' => $this->t('Placeholder text color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The font color that is used on placeholder texts', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('fontPlaceholder'),
          '#required' => TRUE,
        ],
        'tableHeader' => [
          '#type' => 'color',
          '#title' => $this->t('Table header / focussed content elements color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for table header cell backgrounds or activated/selected content boxes', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('tableHeader'),
          '#required' => TRUE,
        ],
        'blank' => [
          '#type' => 'color',
          '#title' => $this->t('Page background color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for the page background', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('blank'),
          '#required' => TRUE,
        ],
        'formBackground' => [
          '#type' => 'color',
          '#title' => $this->t('Shaded background color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The shaded background color gets used as the background color on page areas that should get visibly seperated from the rest of the page', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('formBackground'),
          '#required' => TRUE,
        ],
        'border' => [
          '#type' => 'color',
          '#title' => $this->t('Border color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The border color is used for borders', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('border'),
          '#required' => TRUE,
        ],
        'focus' => [
          '#type' => 'color',
          '#title' => $this->t('Focus color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The focus color is used when the user focusses the element', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('focus'),
          '#required' => TRUE,
        ],
        'inactiveNavElementHoverColorUnderline' => [
          '#type' => 'color',
          '#title' => $this->t('Focus color for the mouse hover underline of inactive main navigation elements', [], ['context' => 'dipas_design']),
          '#description' => $this->t('This color is used on the colored stylistic element beneath inactive navigation links of the main menu when a user hovers the mouse', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('inactiveNavElementHoverColorUnderline'),
          '#required' => TRUE,
        ],
        'buttonDisabled' => [
          '#type' => 'color',
          '#title' => $this->t('Button disabled color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The button disabled color is used when an element is disabled', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('buttonDisabled'),
          '#required' => TRUE,
        ],
        'primaryButtonHover' => [
          '#type' => 'color',
          '#title' => $this->t('Primary button hover color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The primary button hover color is used when mouse is above the button', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('primaryButtonHover'),
          '#required' => TRUE,
        ],
        'stateError' => [
          '#type' => 'color',
          '#title' => $this->t('Error color', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used to mark errors in forms', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('stateError'),
          '#required' => TRUE,
        ],
        'stateActive' => [
          '#type' => 'color',
          '#title' => $this->t('Proceeding state color: active', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for active proceedings state badges', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('stateActive'),
          '#required' => TRUE,
        ],
        'stateActiveBorder' => [
          '#type' => 'color',
          '#title' => $this->t('Proceeding state color: active (border)', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for the border of active proceedings state badges', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('stateActiveBorder'),
          '#required' => TRUE,
        ],
        'stateCompleted' => [
          '#type' => 'color',
          '#title' => $this->t('Proceeding state color: completed', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for completed proceedings state badges', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('stateCompleted'),
          '#required' => TRUE,
        ],
        'stateCompletedBorder' => [
          '#type' => 'color',
          '#title' => $this->t('Proceeding state color: completed (border)', [], ['context' => 'dipas_design']),
          '#description' => $this->t('The color that is used for the border of completed proceedings state badges', [], ['context' => 'dipas_design']),
          '#default_value' => $this->getConfigValue('stateCompletedBorder'),
          '#required' => TRUE,
        ],
      ],
      'reset_button' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => ['::resetColors'],
        '#attributes' => [
          'onClick' => sprintf('confirm("%s");', $this->t('Should the configured colors really be set back to factory defaults?', [], ['context' => 'dipas_design'])),
        ],
      ],

      '#attached' => [
        'library' => ['dipas_design/formColumns'],
      ],
    ];
  }

  public function setConfigValues(array &$form, FormStateInterface $form_state) : array {
    $colors = $form_state->getValue('colors');

    return [
      'primary1' => $colors['primary1'],
      'primary2' => $colors['primary2'],
      'primary1TextColor' => $colors['primary1TextColor'],
      'primary2TextColor' => $colors['primary2TextColor'],
      'font' => $colors['font'],
      'fontDisabled' => $colors['fontDisabled'],
      'fontPlaceholder' => $colors['fontPlaceholder'],
      'tableHeader' => $colors['tableHeader'],
      'formBackground' => $colors['formBackground'],
      'border' => $colors['border'],
      'focus' => $colors['focus'],
      'inactiveNavElementHoverColorUnderline' => $colors['inactiveNavElementHoverColorUnderline'],
      'buttonDisabled' => $colors['buttonDisabled'],
      'primaryButtonHover' => $colors['primaryButtonHover'],
      'blank' => $colors['blank'],
      'stateError' => $colors['stateError'],
      'stateActive' => $colors['stateActive'],
      'stateActiveBorder' => $colors['stateActiveBorder'],
      'stateCompleted' => $colors['stateCompleted'],
      'stateCompletedBorder' => $colors['stateCompletedBorder'],
    ];
  }

  public function resetColors(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('colors', self::getDefaults());

    $this->messenger->addStatus($this->t('The colors were reset to their defaults!', [], ['context'=>'dipas_design']));
    parent::submitForm($form, $form_state);
  }

  protected function getCacheTagsToInvalidate() {
    return ['dipas_design.colors:' . $this->getActiveDomain()];
  }

}
