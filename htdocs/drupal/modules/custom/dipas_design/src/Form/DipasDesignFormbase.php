<?php

namespace Drupal\dipas_design\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\masterportal\DomainAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class DipasDesignFormbase extends ConfigFormBase {

  use StringTranslationTrait,
    DomainAwareTrait;

  const CONFIG_NAME = 'dipas_design.settings';

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('current_user'),
      $container->get('logger.channel.dipas_design'),
      $container->get('messenger'),
      $container->get('cache_tags.invalidator')
    );
  }

  public function __construct(
    ContainerInterface $container,
    AccountInterface $user,
    LoggerChannelInterface $logger,
    MessengerInterface $messenger,
    CacheTagsInvalidatorInterface $cache_tags_invalidator
  ) {
    $this->user = $user;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->setAdditionalDependencies($container);
  }

  protected function setAdditionalDependencies(ContainerInterface $container) {}

  final protected function getEditableConfigNames() {
    return sprintf(
      implode('.%s.', explode('.', self::CONFIG_NAME)),
      $this->getActiveDomain()
    );
  }

  final protected function getConfig() {
    $config = $this->configFactory()->getEditable($this->getEditableConfigNames());

    // If there is no pre-existing configuration for this domain, clone and
    // save the default settings first.
    if ($config->isNew()) {
      $defaults = $this->configFactory()->get(
        $this->isDefaultDomain()
          ? self::CONFIG_NAME
          : implode('.default.', explode('.', self::CONFIG_NAME))
      );
      $config->setData($defaults->getRawData());
      $config->save();

      $this->messenger->addStatus($this->t(
        'Since this is the first time configuring design settings for this particular domain, the default settings were cloned and saved as a boilerplate. Future settings in this interface will be for this proceeding only.',
        [],
        ['context' => 'dipas_design']
      ));
    }

    if ($this->isDefaultDomain()) {
      $this->messenger->addWarning($this->t(
        'You are editing the default settings that will get applied to every new proceeding in the future.',
        [],
        ['context' => 'dipas_design']
      ));
    }

    return $config;
  }

  final protected function getConfigNamespace() {
    return (new \ReflectionClass($this))->getShortName();
  }

  final protected function getConfigValue($key) {
    return $this->getConfig()->get($this->getConfigNamespace() . '.' . $key) ?? (static::getDefaults()[$key] ?? NULL);
  }

  final public function getFormId() {
    return 'dipas_design.settings.' . $this->getConfigNamespace();
  }

  final public function buildForm(array $form, FormStateInterface $form_state) {
    return array_merge(
      $this->getFormElements($form, $form_state),
      [
        '#tree' => TRUE,
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Save configuration', [], ['context' => 'dipas_design']),
          '#button_type' => 'primary',
        ],
      ]
    );
  }

  final public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $config->set($this->getConfigNamespace(), $this->setConfigValues($form, $form_state));
    $config->save();

    if ($cacheTagsToInvalidate = $this->getCacheTagsToInvalidate()) {
      $this->cacheTagsInvalidator->invalidateTags($cacheTagsToInvalidate);
    }

    $this->logger->info(
      'DIPAS design settings (section @section) for proceeding "@proceeding" were updated by user @user',
      [
        '@section' => $this->getConfigNamespace(),
        '@proceeding' => $this->getActiveDomain(),
        '@user' => $this->user->getAccountName(),
      ]
    );

    $this->messenger->addStatus('The design settings were successfully updated!');
  }

  protected function getCacheTagsToInvalidate() {
    return [];
  }

  abstract protected function getFormElements(array $form, FormStateInterface $form_state) : array;

  abstract public static function getDefaults() : array;

  abstract protected function setConfigValues(array &$form, FormStateInterface $form_state) : array;

}
