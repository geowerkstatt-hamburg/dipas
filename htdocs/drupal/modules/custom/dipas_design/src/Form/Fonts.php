<?php

namespace Drupal\dipas_design\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\masterportal\DomainAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Fonts extends DipasDesignFormbase {

  use DomainAwareTrait;

  const FILE_UPLOAD_PATH = 'public://dipas/fonts';

  // Weight name => Weight numeric value
  const FONT_WEIGHTS = [
    'regular' => 400,
    'medium' => 500,
    'bold' => 700,
  ];

  const FONT_FORMATS = ['eot', 'svg', 'ttf', 'woff', 'woff2'];

  /**
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  public function setAdditionalDependencies(ContainerInterface $container) {
    $this->fileStorage = $container->get('entity_type.manager')->getStorage('file');
  }

  public static function getDefaults() : array {
    $defaults = [];

    foreach (array_keys(self::FONT_WEIGHTS) as $weight) {
      $defaults[$weight] = [];

      foreach (self::FONT_FORMATS as $format) {
        $defaults[$weight][$format] = NULL;
      }
    }

    return $defaults;
  }

  public function fontConfiguredValue($key) {
    if ($fileID = $this->getConfigValue($key)) {
      return [$fileID];
    }

    return NULL;
  }

  public function getFormElements(array $form, FormStateInterface $form_state) : array {
    $form = [
      '#tree' => TRUE,
      '#attached' => [
        'library' => ['dipas_design/formColumns'],
      ],
      'preamble' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t(
          'Roboto (Google font, stored locally) is the default web font for DIPAS. Uploading Roboto font files is therefore not necessary. All font weights (regular/medium/bold) have a hardcoded fallback to Roboto. If you upload/configure any other font face, make sure to replace all weights. You can use <a href="https://www.fontconverter.org/" target="_blank">Fontconverter</a> to transform files between different formats.',
          [],
          ['context' => 'dipas_design']
        ),
      ],
    ];

    foreach (self::FONT_WEIGHTS as $weight => $numericalWeight) {
      $form[$weight] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Font definition for weight %weight (@numWeight)', ['%weight' => $weight, '@numWeight' => $numericalWeight], ['context' => 'dipas_design']),
        '#description' => $this->t('', [], ['context' => 'dipas_design']),
        '#description_display' => 'before',
        '#open' => TRUE,
      ];

      foreach (self::FONT_FORMATS as $format) {
        $form[$weight][$format] = [
          '#type' => 'managed_file',
          '#title' => strtoupper($format),
          '#upload_location' => static::FILE_UPLOAD_PATH . "/$weight",
          '#upload_validators' => [
            'file_validate_extensions' => [$format],
          ],
          '#default_value' => $this->fontConfiguredValue("$weight.$format"),
          '#states' => [
            'required' => array_map(
              function ($fileFormat) use ($weight) {
                return [
                  [sprintf(':input[type="hidden"][name="%1$s[%2$s][fids]"]', $weight, $fileFormat) => ['!value' => '']],
                ];
              },
              array_filter(
                self::FONT_FORMATS,
                function ($fileFormat) use ($format) {
                  return $fileFormat !== $format;
                }
              )
            ),
          ],
        ];
      }
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach (array_keys(self::FONT_WEIGHTS) as $weight) {
      $uploads = [];

      foreach (self::FONT_FORMATS as $format) {
        $uploads[$format] = ($formValue = $form_state->getValue($weight)[$format]) &&
          ($fileID = $formValue[0]);
      }

      if (in_array(TRUE, $uploads)) {
        foreach ($uploads as $format => $uploaded) {
          if (!$uploaded) {
            $form_state->setError(
              $form[$weight][$format],
              $this->t('Format %format is required for weight %weight', ['%weight' => $weight, '%format' => $format], ['context' => 'dipas_design'])
            );
          }
        }
      }
    }
  }

  public function setConfigValues(array &$form, FormStateInterface $form_state) : array {
    // Prepare the configuration container
    $config = [];

    foreach (array_keys(self::FONT_WEIGHTS) as $weight) {
      foreach (self::FONT_FORMATS as $format) {
        // If a file was uploaded...
        if (
          ($formValue = $form_state->getValue($weight)[$format]) &&
          ($fileID = $formValue[0])
        ) {
          // Make the uploaded file permanent
          $fileEntity = $this->fileStorage->load($fileID);
          $fileEntity->setPermanent();
          $fileEntity->save();

          // And save the file ID of the uploaded file to our configuration
          if (!isset($config[$weight])) {
            $config[$weight] = [];
          }

          $config[$weight][$format] = $fileID;
        }
      }
    }

    return $config;
  }

  protected function getCacheTagsToInvalidate() {
    return ['dipas_design.fonts:' . $this->getActiveDomain()];
  }

}
