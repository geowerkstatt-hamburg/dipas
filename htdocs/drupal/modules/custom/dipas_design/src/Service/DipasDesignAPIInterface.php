<?php

namespace Drupal\dipas_design\Service;

interface DipasDesignAPIInterface {

  /**
   * @param string $key
   *   The key of the desired output, either favicon.ico or styles.css
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function endpoint($key);

  /**
   * Returns an array of configured color values.
   *
   * @return array
   *   The color values in effect
   */
  public function getConfiguredColors();

}
