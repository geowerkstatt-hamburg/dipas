<?php

namespace Drupal\dipas_design\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\dipas_design\CssFontDefinition;
use Drupal\dipas_design\Form\Colors;
use Drupal\dipas_design\Form\DipasDesignFormbase;
use Drupal\dipas_design\Form\Fonts;
use Drupal\masterportal\DomainAwareTrait;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class DipasDesignAPI implements DipasDesignAPIInterface {

  use DomainAwareTrait;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * @var CacheBackendInterface
   */
  protected $cache;

  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ExtensionPathResolver $extension_path_resolver,
    FileUrlGeneratorInterface $file_url_generator,
    CacheBackendInterface $cache
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileStorage = $this->entityTypeManager->getStorage('file');
    $this->cache = $cache;

    $configID = sprintf(
      implode('.%s.', explode('.', DipasDesignFormbase::CONFIG_NAME)),
      $this->getActiveDomain()
    );
    $this->config = $config_factory->get($configID);

    // Fallback for unconfigured proceedings - load the default domain configuration.
    if ($this->config->isNew()) {
      $this->config = $config_factory->get(implode('.default.', explode('.', DipasDesignFormbase::CONFIG_NAME)));

      // Fallback for totally unconfigured environments - load dipas_design defaults
      if ($this->config->isNew()) {
        $this->config = $config_factory->get(DipasDesignFormbase::CONFIG_NAME);
      }
    }
  }

  public function endpoint($key) {
    switch ($key) {
      case 'operatorlogo':
      case 'menulinelogo':
      case 'favicon.ico':
        return $this->dipasBinaryResponse($key);
      case 'settings.js':
        return $this->dipasSettings();
      default:
        return $this->dipasStyles();
    }
  }

  protected function dipasBinaryResponse($file) {
    $cacheID = sprintf(
      'dipas_design.file:%s:%s',
      $file,
      $this->getActiveDomain()
    );

    if ($cache = $this->cache->get($cacheID)) {
      $uri = $cache->data;
    }
    else {
      switch ($file) {
        case 'favicon.ico':
          $configKey = 'Images.favicon';
          $fallback = sprintf(
            '%s/assets/images/favicon.ico',
            $this->extensionPathResolver->getPath('module', 'dipas_design')
          );
          break;

        default:
          $configKey = 'Images.' . $file;
          $fallback = sprintf(
            '%s/assets/images/%s.png',
            $this->extensionPathResolver->getPath('module', 'dipas_design'),
            $file
          );
          break;
      }

      if (
        ($fileID = $this->config->get($configKey)) &&
        ($file = $this->fileStorage->load($fileID)) &&
        ($uri = $file->get('uri')->first()) &&
        file_exists($uri->getString())
      ) {
        $uri = $uri->getString();
      }
      else {
        $uri = $fallback;

        if (!file_exists($uri)) {
          $uri = FALSE;
        }
      }

      if ($uri !== FALSE) {
        $this->cache->set(
          $cacheID,
          $uri,
          Cache::PERMANENT,
          [
            'dipas_design.images:' . $this->getActiveDomain(),
          ]
        );
      }
    }

    return $uri !== FALSE
      ? new BinaryFileResponse($uri, 200)
      : new Response();
  }

  protected function dipasSettings() {
    $response = new Response();
    $response->headers->set('Content-Type', 'application/javascript');

    $cacheID = sprintf('dipas_design.settings:%s', $this->getActiveDomain());

    if ($cache = $this->cache->get($cacheID)) {
      $settings = $cache->data;
    }
    else {
      $settings = [
        'hasMenuLineLogo' => $this->config->get('Images.menulinelogo') ? TRUE : FALSE,
        'operatorLogoAlignment' => $this->config->get('Images.alignment'),
        'operatorLogoAlttext' => $this->config->get('Images.alttext'),
        'colors' => $this->getConfiguredColors(),
      ];

      $this->cache->set(
        $cacheID,
        $settings,
        Cache::PERMANENT,
        [
          'dipas_design.images:' . $this->getActiveDomain(),
        ]
      );
    }

    $response->setContent('const dipasSettings = ' . json_encode($settings, JSON_PRETTY_PRINT) . ';');

    return $response;
  }

  public function getConfiguredColors() {
    $colors = drupal_static('dipas_design_colors', []);

    if (!count($colors) && ($colors = $this->config->get('Colors')) === NULL) {
      $colors = Colors::getDefaults();
    }

    return $colors;
  }

  protected function dipasStyles() {
    $response = new Response();
    $response->headers->set('Content-Type', 'text/css');
    $response->headers->set('Access-Control-Allow-Origin', '*');

    $cacheID = sprintf('dipas_design.styles:%s', $this->getActiveDomain());

    if ($cache = $this->cache->get($cacheID)) {
      $styles = $cache->data;
    }
    else {
      $styles = $this->getConfiguredColors();

      array_walk(
        $styles,
        function (&$value, $key) {
          $value = sprintf("\t--DipasColors%s: %s;", ucfirst($key), $value);
        }
      );
      array_unshift($styles, ':root {');

      $styles[] = '}';

      $moduleAssetsPath = $this->fileUrlGenerator->generateAbsoluteString(
        $this->extensionPathResolver->getPath('module', 'dipas_design') . '/assets/'
      );

      $fonts = [
        'regular' => [
          'eot' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/regular/Roboto-Regular.eot'),
          'svg' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/regular/Roboto-Regular.svg'),
          'ttf' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/regular/Roboto-Regular.ttf'),
          'woff' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/regular/Roboto-Regular.woff'),
          'woff2' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/regular/Roboto-Regular.woff2'),
        ],
        'medium' => [
          'eot' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/medium/Roboto-Medium.eot'),
          'svg' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/medium/Roboto-Medium.svg'),
          'ttf' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/medium/Roboto-Medium.ttf'),
          'woff' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/medium/Roboto-Medium.woff'),
          'woff2' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/medium/Roboto-Medium.woff2'),
        ],
        'bold' => [
          'eot' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/bold/Roboto-Bold.eot'),
          'svg' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/bold/Roboto-Bold.svg'),
          'ttf' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/bold/Roboto-Bold.ttf'),
          'woff' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/bold/Roboto-Bold.woff'),
          'woff2' => sprintf('%s%s', $moduleAssetsPath, 'fonts/Roboto/bold/Roboto-Bold.woff2'),
        ],
      ];

      foreach (array_keys(Fonts::FONT_WEIGHTS) as $weight) {
        if ($fontDefinition = $this->getFontDefinition($weight)) {
          $fonts[$weight] = $fontDefinition;
        }
      }

      foreach ($fonts as $weight => $formats) {
        $font = new CssFontDefinition(
          sprintf('Dipas%s', ucwords($weight)),
          'normal',
          Fonts::FONT_WEIGHTS[$weight]
        );

        foreach ($formats as $format => $url) {
          $font->setFormat($format, $url);
        }

        $styles[] = '';
        $styles[] = $font;
      }

      $this->cache->set(
        $cacheID,
        $styles,
        Cache::PERMANENT,
        [
          'dipas_design.styles',
          'dipas_design.colors:' . $this->getActiveDomain(),
          'dipas_design.fonts:' . $this->getActiveDomain(),
        ]
      );
    }

    $response->setContent(implode("\r\n", $styles));

    return $response;
  }

  private function getFontDefinition($weight) {
    $fontConfig = drupal_static('DIPAS_DESIGN_FONT_DEFINITIONS', NULL);

    if ($fontConfig === NULL) {
      $fontConfig = $this->config->get('Fonts');
    }

    if (isset($fontConfig[$weight])) {
      $definition = [];

      foreach ($fontConfig[$weight] as $format => $fileID) {
        if (
          ($file = $this->fileStorage->load($fileID)) &&
          ($uri = $file->get('uri')->first()) &&
          file_exists(($uri = $uri->getString()))
        ) {
          $definition[$format] = $this->fileUrlGenerator->generateAbsoluteString($uri);
        }
      }

      return $definition;
    }
    else {
      return FALSE;
    }
  }

}
