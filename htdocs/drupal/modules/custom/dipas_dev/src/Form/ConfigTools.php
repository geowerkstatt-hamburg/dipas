<?php

namespace Drupal\dipas_dev\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

class ConfigTools extends DipasDevToolsFormBase {

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var CacheBackendInterface
   */
  protected $cacheConfig;

  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->configFactory = $container->get('config.factory');
    $this->cacheConfig = $container->get('cache.config');
  }

  public function getFormId() {
    return 'dipas_dev.config_tools';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [
      'warning_headline' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Caution', [], ['context' => 'dipas_dev']),
        '#weight' => -10,
      ],
      'warning' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('This tool can SERIOUSLY harm your website or render it useless! Only use it when you know EXACTLY what you are doing! This tool does NOT check the configuration values for proper functioning!', [], ['context' => 'dipas_dev']),
        '#weight' => -9,
      ],
      'configIdentifier' => [
        '#type' => 'textfield',
        '#title' => $this->t('Configuration identifier string', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('You can use * as a wildcard (max 1 allowed). Identifier cannot start with a wildcard.', [], ['context' => 'dipas_dev']),
        '#required' => TRUE,
        '#element_validate' => [[$this, 'validateFieldValue']],
        '#regex' => '~^[a-z][a-z0-9]*?\.(?:[^*]*\*?[^*]*)$~i', // allow just one wildcard
        '#weight' => 10,
      ],
      'configBlacklist' => [
        '#type' => 'textarea',
        '#title' => $this->t('Wildcard blacklist', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('Add values for the wildcard that should get excluded. Single entry per line. Entries can utilize wildcards (*).', [], ['context' => 'dipas_dev']),
        '#cols' => 10,
        '#weight' => 20,
      ],
      'valuePath' => [
        '#type' => 'textfield',
        '#title' => $this->t('Path of configuration value', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('Use . as a path divider. Path must exist within configurations. Case sensitive!', [], ['context' => 'dipas_dev']),
        '#required' => TRUE,
        '#element_validate' => [[$this, 'validateFieldValue']],
        '#regex' => '~^[a-z][a-z0-9_\-]*?(?:\.[a-z0-9][a-z0-9_\-]*)*$~i',
        '#weight' => 30,
      ],
      'actionToTake' => [
        '#type' => 'radios',
        '#title' => $this->t('Action to take on the value', [], ['context' => 'dipas_dev']),
        '#options' => [
          'modify' => $this->t('modify', [], ['context' => 'dipas_dev']),
          'delete' => $this->t('delete', [], ['context' => 'dipas_dev']),
        ],
        '#required' => TRUE,
        '#default_value' => 'modify',
        '#weight' => 35,
      ],
      'configValue' => [
        '#type' => 'textfield',
        '#title' => $this->t('Configuration value to set', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('Any value that should get set for the given configuration path in all matching configuration entities', [], ['context' => 'dipas_dev']),
        '#required' => FALSE,
        '#states' => [
          'visible' => [
            [':input[name="actionToTake"]' => ['value' => 'modify']],
          ],
        ],
        '#weight' => 40,
      ],
      'dryrun' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Dry run', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('Just test how many configuration entities would be affected and if the value to set can be applied.', [], ['context' => 'dipas_dev']),
        '#default_value' => 1,
        '#required' => FALSE,
        '#weight' => 50,
      ],
      'execute' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Execute', [], ['context' => 'dipas_dev']),
        '#description' => $this->t('I completely understand the implications of the settings above and want to proceed', [], ['context' => 'dipas_dev']),
        '#default_value' => 0,
        '#weight' => 60,
        '#states' => [
          'visible' => [
            [':input[name="dryrun"]' => ['checked' => FALSE]],
          ],
          'required' => [
            [':input[name="dryrun"]' => ['checked' => FALSE]],
          ],
        ],
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#button_type' => 'primary',
        '#states' => [
          'visible' => [
            [':input[name="dryrun"]' => ['checked' => TRUE]],
            'or',
            [':input[name="execute"]' => ['checked' => TRUE]],
          ],
        ],
        '#weight' => 70,
      ],
    ];

    if ($configIdentifier = $form_state->getValue('configIdentifier')) {
      $configEntities = $this->getConfigIdentifiers($configIdentifier, $form_state->getValue('configBlacklist'));

      $form['affectedConfigs'] = [
        '#type' => 'details',
        '#title' => $this->t('@count affected configuration entities', ['@count' => count($configEntities)], ['context' => 'dipas_dev']),
        '#open' => FALSE,
        '#weight' => -15,

        'configurationEntityIDs' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => implode('<br/>', $configEntities),
        ],
      ];
    }

    return $form;
  }

  public function validateFieldValue(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!preg_match($element['#regex'], $element['#value'])) {
      $form_state->setError(
        $element,
        $this->t('Please check your inputs.')
      );
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dry_run = (bool) $form_state->getValue('dryrun');

    $configIdentifier = $form_state->getValue('configIdentifier');
    $blacklist = $form_state->getValue('configBlacklist');
    $valuePath = $form_state->getValue('valuePath');
    $actionToTake = $form_state->getValue('actionToTake');
    $value = $form_state->getValue('configValue');

    $configs = $this->getConfigIdentifiers($configIdentifier, $blacklist);

    if ($dry_run) {
      $form_state->setRebuild(TRUE);

      if (count($configs)) {
        $this->messenger->addStatus(count($configs) . ' configuration entities are matching your configuration identifier string.');

        $configentities = $this->configFactory->loadMultiple($configs);
        $pathValid = TRUE;

        foreach ($configentities as $entity) {
          if (!$entity->get($valuePath)) {
            $pathValid = FALSE;
            break;
          }
        }

        if ($actionToTake === 'modify') {
          if ($pathValid) {
            $this->messenger->addStatus('The configuration value path is valid and your changes can be applied.');
          }
          else {
            $this->messenger->addWarning('The path of the configuration value does not exist in some/all configuration entities found!');
          }
        }
      }
      else {
        $this->messenger->addWarning('No configuration entities match your identifier string!');
      }
    }
    else if (count($configs)) {
      if ($actionToTake === 'modify') {
        $this->logger->info(
          'Configuration entity updates applied by user @user on configuration entities %entityidentifier for value path %valuePath with value %value.',
          [
            '@user' => $this->currentUser->getAccountName(),
            '%entityidentifier' => $configIdentifier,
            '%valuePath' => $valuePath,
            '%value' => $value,
          ]
        );
      }
      else {
        $this->logger->info(
          'Configuration entity updates applied by user @user on configuration entities %entityidentifier, deleted value at path %valuePath.',
          [
            '@user' => $this->currentUser->getAccountName(),
            '%entityidentifier' => $configIdentifier,
            '%valuePath' => $valuePath,
          ]
        );
      }

      $operations = array_map(
        function ($configIdentifier) use ($actionToTake, $valuePath, $value) {
          return [
            [$this, 'processConfigurationChange'],
            [$configIdentifier, $actionToTake, $valuePath, $value ?? FALSE]
          ];
        },
        $configs
      );

      $operations[] = [
        [$this, 'finalizeProcessing'],
        []
      ];

      $batch = [
        'title' => $this->t('Applying configuration changes', [], ['context' => 'dipas_dev']),
        'operations' => $operations,
        'finished' => [$this, 'statusReport'],
        'init_message' => 'Starting...',
        'progress_message' => 'Processed @current out of @total',
        'error_message' => 'There was a problem while processing the changes',
      ];

      batch_set($batch);
    }
    else {
      $this->messenger->addWarning('No configuration entities match your identifier string!');
    }
  }

  protected function getConfigIdentifiers($identifierString, $blacklist = NULL) {
    if (preg_match('~\*~', $identifierString)) {
      [$configPrefix, $identifierTail] = explode('*', $identifierString);
    }
    else {
      $configPrefix = $identifierString;
      $identifierTail = FALSE;
    }

    $configs = $this->configFactory->listAll($configPrefix);

    if ($identifierTail) {
      $configs = array_filter(
        $configs,
        function ($element) use ($identifierTail) {
          return preg_match(sprintf('~%s$~', preg_quote($identifierTail, '~')), $element);
        }
      );
    }

    if ($blacklist) {
      $blacklist = explode("\n", preg_replace("~\n+~", "\n", preg_replace("~\r+~", '', $blacklist)));
      $blacklist = array_map(
        function ($entry) {
          if (preg_match('~\*~', $entry)) {
            $entry = preg_replace('~\*~', '.*?', $entry);
          }

          return $entry;
        },
        $blacklist
      );
      $blacklist = sprintf('(?:%s)', implode('|', $blacklist));

      $configs = array_filter(
        $configs,
        function ($element) use ($configPrefix, $identifierTail, $blacklist) {
          return !preg_match(
            sprintf(
              '~^%s%s%s$~',
              preg_quote($configPrefix, '~'),
              $blacklist,
              preg_quote($identifierTail ?? '', '~')
            ),
            $element
          );
        }
      );
    }

    return $configs;
  }

  public function processConfigurationChange($configIdentifier, $actionToTake, $valuePath, $value, &$context) {
    if (!isset($context['results']['processed'])) {
      $context['results']['processed'] = [
        'count' => 0,
        'configurationentities' => [],
      ];
    }

    $config = $this->configFactory->getEditable($configIdentifier);

    if ($actionToTake === 'modify') {
      $config->set($valuePath, $value);
    }
    else {
      $config->clear($valuePath);
    }

    $config->save();

    $context['results']['processed']['count']++;
    $context['results']['processed']['configurationentities'][] = $configIdentifier;
  }

  public function finalizeProcessing() {
    $this->cacheConfig->invalidateAll();
  }

  public function statusReport($success, $results, $operations, $elapsed) {
    $this->logger->info(
      '@count configuration entities changed.',
      [
        '@count' => $results['processed']['count'],
      ]
    );

    $this->messenger->addStatus($this->t(
      'Processed @count configuration entities',
      ['@count' => $results['processed']['count']],
      ['context' => 'dipas_dev']
    ));
  }

}
