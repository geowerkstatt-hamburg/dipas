<?php

namespace Drupal\dipas_dev\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileTools extends DipasDevToolsFormBase {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->database = $container->get('database');
    $this->tempStore = $container->get('tempstore.private')->get('dipas_dev.file_tool');
    $this->fileSystem = $container->get('file_system');
  }

  public function getFormId() {
    return 'dipas_dev.file_tools';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['markup'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t(
        'Scan for missing files, that are nonetheless included in the database and are causing problems when uploading new files with seemingly already taken filenames.',
        [],
        ['context' => 'dipas_dev']
      ),
    ];

    $form['dry_run'] = [
      '#type' => 'checkbox',
      '#title' => 'Dry run',
      '#default_value' => 1,
    ];

    $form['clean_missing_files'] = [
      '#type' => 'checkbox',
      '#title' => 'Clean missing files',
      '#default_value' => 1,
      '#states' => [
        'invisible' => ['input[type="checkbox"][name="dry_run"]' => ['checked' => TRUE]],
      ],
    ];

    $form['clean_corrupted_files'] = [
      '#type' => 'checkbox',
      '#title' => 'Clean corrupted files',
      '#default_value' => NULL,
      '#states' => [
        'invisible' => ['input[type="checkbox"][name="dry_run"]' => ['checked' => TRUE]],
      ],
    ];

    $form['scan_for_missing_files'] = [
      '#type' => 'submit',
      '#value' => 'Scan file database entries',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dry_run = (bool) $form_state->getValue('dry_run');
    $clean_missing = (bool) $form_state->getValue('clean_missing_files');
    $clean_corrupt = (bool) $form_state->getValue('clean_corrupted_files');

    $batch = [
      'title' => 'Scanning database',
      'operations' => array_map(
        function ($row) use ($dry_run, $clean_missing, $clean_corrupt) {
          return [
            [$this, 'processFileEntry'],
            [
              $row,
              $dry_run,
              $clean_missing,
              $clean_corrupt,
            ]
          ];
        },
        $this->database->select('file_managed', 'fm')
          ->fields('fm')
          ->execute()
          ->fetchAll()
      ),
      'finished' => [$this, 'statusReport'],
      'init_message' => 'Starting scan',
      'progress_message' => 'Scanned @current out of @total',
      'error_message' => 'There was a problem while scanning the file system',
    ];

    $batch['operations'][] = [
      [$this, 'finalize'],
      [
        $dry_run,
        $clean_missing,
        $clean_corrupt,
      ]
    ];

    batch_set($batch);
  }

  public function processFileEntry($dbEntry, $dry_run, $clean_missing, $clean_corrupt, &$context) {
    if (!isset($context['results']['cleaned'])) {
      $context['results']['cleaned'] = [
        'missing' => 0,
        'corrupt' => 0,
      ];
    }

    if (!file_exists($dbEntry->uri) || filesize($dbEntry->uri) !== (int) $dbEntry->filesize) {
      $reason = !file_exists($dbEntry->uri) ? 'missing' : 'corrupt';

      if (!isset($context['results'][$reason])) {
        $context['results'][$reason] = [];
      }

      if (!$dry_run && ${'clean_' . $reason}) {
        $this->database->delete('file_usage')
          ->condition('fid', $dbEntry->fid, '=')
          ->execute();

        $this->database->delete('file_managed')
          ->condition('fid', $dbEntry->fid, '=')
          ->execute();

        // If it's a corrupted file that should get cleaned, we need to also clean the file system
        if ($reason === 'corrupt') {
          $this->fileSystem->delete($dbEntry->uri);
        }

        $context['results']['cleaned'][$reason]++;
      }
      else {
        $context['results'][$reason][] = [
          'file' => $dbEntry,
          'usages' => $this->database->select('file_usage', 'fu')
            ->fields('fu')
            ->condition('fu.fid', $dbEntry->fid, '=')
            ->execute()
            ->fetchAll()
        ];
      }
    }
  }

  public function finalize($dry_run, $clean_missing, $clean_corrupt, &$context) {
    $context['results']['executed_operations'] = [
      'dry_run' => $dry_run,
      'clean_missing' => $clean_missing,
      'clean_corrupt' => $clean_corrupt,
    ];

    if (!$dry_run) {
      $orphaned_usage_entries = $this->database->select('file_usage', 'fu');
      $orphaned_usage_entries->fields('fu', ['fid']);
      $orphaned_usage_entries->addJoin('LEFT', 'file_managed', 'fm', 'fu.fid = fm.fid');
      $orphaned_usage_entries->groupBy('fu.fid');
      $orphaned_usage_entries->having('COUNT(fm.fid)=0');
      $ids = array_map(
        function ($row) {
          return $row->fid;
        },
        $orphaned_usage_entries->execute()->fetchAll()
      );

      if (count($ids)) {
        $this->database->delete('file_usage')
          ->condition('fid', join(',', $ids), 'IN')
          ->execute();
      }
    }
  }

  public function statusReport($success, $results, $operations, $elapsed) {
    $allClean = !count($results['missing'] ?? []) && !count($results['corrupt'] ?? []);

    $this->tempStore->set('dipas_dev.files.report', $results);

    if (!$allClean) {
      $report = Link::createFromRoute(
        '(' . $this->t('Details') . ')',
        'dipas_dev.file_tools.report',
        [],
        ['attributes' => ['target' => '_blank']]
      )->toString();
    }

    if ($results['executed_operations']['dry_run']) {
      $this->messenger->{$allClean ? 'addMessage' : 'addWarning'}(
        $this->t(
          "Scan finished, found @missing missing and @corrupt potentionally corrupted files due to a mismatch in filesize. @report",
          [
            '@missing' => count($results['missing'] ?? []),
            '@corrupt' => count($results['corrupt'] ?? []),
            '@report' => !$allClean ? $report : '',
          ]
        )
      );
    }
    else {
      if (
        $results['executed_operations']['clean_missing'] &&
        $results['executed_operations']['clean_corrupt']
      ) {
        $this->messenger->addMessage($this->t(
          "Cleaning finished, cleaned @missing entries related to missing and @corrupt entries related to corrupted files from the database.",
          [
            '@missing' => $results['cleaned']['missing'],
            '@corrupt' => $results['cleaned']['corrupt'],
          ]
        ));
      }
      else if ($results['executed_operations']['clean_missing']) {
        $this->messenger->addMessage($this->t(
          "Cleaning finished, cleaned @missing database entries related to missing files from the database.",
          [
            '@missing' => $results['cleaned']['missing'],
          ]
        ));
      }
      else if ($results['executed_operations']['clean_corrupt']) {
        $this->messenger->addMessage($this->t(
          "Cleaning finished, cleaned @corrupt database entries related to corrupt files from the database.",
          [
            '@corrupt' => $results['cleaned']['corrupt'],
          ]
        ));
      }

      if (count($results['missing'] ?? [])) {
        $this->messenger->addWarning($this->t(
          "@missing database entries related to missing files still remaining in the database.",
          [
            '@missing' => count($results['missing'] ?? []),
          ]
        ));
      }

      if (count($results['corrupt'] ?? [])) {
        $this->messenger->addWarning($this->t(
          "@corrupt database entries related to corrupted files still remaining in the database.",
          [
            '@corrupt' => count($results['corrupt'] ?? []),
          ]
        ));
      }
    }
  }

}
