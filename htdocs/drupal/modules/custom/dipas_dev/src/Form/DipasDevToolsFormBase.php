<?php

namespace Drupal\dipas_dev\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\dipas_dev\Service\DipasDevService;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class DipasDevToolsFormBase extends FormBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\dipas_dev\Service\DipasDevService
   */
  protected $dipasDevService;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var AccountInterface
   */
  protected $currentUser;

  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('dipas_dev.service'),
      $container->get('messenger'),
      $container->get('logger.channel.dipas_dev'),
      $container->get('current_user')
    );
  }

  public function __construct(
    ContainerInterface $container,
    DipasDevService $dipasDevService,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    AccountInterface $user
  ) {
    $this->dipasDevService = $dipasDevService;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->currentUser = $user;

    $this->setAdditionalDependencies($container);
  }

  protected function setAdditionalDependencies(ContainerInterface $container) {}

}
