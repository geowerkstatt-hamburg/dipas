<?php

namespace Drupal\dipas_dev\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UUIDTools extends DipasDevToolsFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $configCache;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface[]
   */
  protected $definitions;

  protected function setAdditionalDependencies(ContainerInterface $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->configStorage = $container->get('config.storage');
    $this->configFactory = $container->get('config.factory');
    $this->database = $container->get('database');
    $this->configCache = $container->get('cache.config');

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type => $definition) {
      if ($definition->entityClassImplements(ConfigEntityInterface::class)) {
        $this->definitions[$entity_type] = $definition;
      }
    }
  }

  public function getFormId() {
    return 'dipas_dev.uuid_tools';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $config_type = NULL, $config_name = NULL) {
    $form['#prefix'] = '<div id="js-config-form-wrapper">';
    $form['#suffix'] = '</div>';

    $entity_types = array_map(
      function (EntityTypeInterface $definition) {
        return $definition->getLabel();
      },
      $this->definitions
    );

    // Sort the entity types by label, then add the simple config to the top.
    uasort($entity_types, 'strnatcasecmp');

    $config_types = ['' => $this->t('Please choose')] + $entity_types;

    $form['config_type'] = [
      '#title' => $this->t('Configuration type'),
      '#type' => 'select',
      '#options' => $config_types,
      '#default_value' => $config_type,
      '#ajax' => [
        'callback' => '::updateConfigurationType',
        'wrapper' => 'js-config-form-wrapper',
      ],
    ];

    $default_type = $form_state->getValue('config_type', $config_type);

    $form['config_name'] = [
      '#title' => $this->t('Configuration name'),
      '#type' => 'select',
      '#options' => $this->findConfiguration($default_type),
      '#default_value' => $config_name,
      '#prefix' => '<div id="edit-config-type-wrapper">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => '::updateUUID',
        'wrapper' => 'edit-uuid-wrapper',
      ],
      '#states' => [
        'visible' => [
          'select[name="config_type"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['uuid'] = [
      '#title' => $this->t('UUID'),
      '#type' => 'textfield',
      '#prefix' => '<div id="edit-uuid-wrapper">',
      '#suffix' => '</div>',
      '#maxlength' => 36,
      '#element_validate' => [[$this, 'validateUUID']],
      '#states' => [
        'visible' => [
          'select[name="config_type"]' => ['!value' => ''],
          'select[name="config_name"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Update UUID',
      '#button_type' => 'primary',
      '#states' => [
        'visible' => [
          'select[name="config_type"]' => ['!value' => ''],
          'select[name="config_name"]' => ['!value' => ''],
        ],
      ],
    ];

    return $form;
  }

  public function validateUUID(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!preg_match('~^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}~', $element['#value'])) {
      $form_state->setError(
        $element,
        $this->t(
          'The UUID entered failed to validate!',
          [],
          ['context' => 'dipas_dev']
        )
      );
    }
    else {
      $configs = $this->database->select('config', 'c')
        ->fields('c')
        ->execute()
        ->fetchAll();

      $definition = $this->entityTypeManager->getDefinition($form_state->getValue('config_type'));
      $configID = $definition->getConfigPrefix() . '.' . $form_state->getValue('config_name');

      $affected = [];
      foreach ($configs as $config) {
        $data = unserialize($config->data);

        if (
          $data &&
          isset($data['uuid']) &&
          $data['uuid'] === strtolower($element['#value']) &&
          $config->name !== $configID
        ) {
          $affected[] = $config;
        }
      }

      if (count($affected)) {
        $form_state->setError(
          $element,
          $this->t(
            'The UUID entered is already in use!',
            [],
            ['context' => 'dipas_dev']
          )
        );
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $definition = $this->entityTypeManager->getDefinition($form_state->getValue('config_type'));
    $configID = $definition->getConfigPrefix() . '.' . $form_state->getValue('config_name');

    $config = $this->configFactory->getEditable($configID);
    $config->set('uuid', strtolower($form_state->getValue('uuid')));
    $config->save();

    $this->configCache->invalidateAll();
    $this->messenger->addStatus($this->t('UUID was successfully updated!'));
  }

  /**
   * Handles switching the configuration type selector.
   */
  public function updateConfigurationType($form, FormStateInterface $form_state) {
    $form['config_name']['#options'] = $this->findConfiguration($form_state->getValue('config_type'));
    return $form;
  }

  public function updateUUID($form, FormStateInterface $form_state) {
    $definition = $this->entityTypeManager->getDefinition($form_state->getValue('config_type'));
    $configID = $definition->getConfigPrefix() . '.' . $form_state->getValue('config_name');

    $config = $this->configFactory->get($configID);

    $form['uuid']['#value'] = $config->get('uuid');

    return $form['uuid'];
  }

  /**
   * Handles switching the configuration type selector.
   */
  protected function findConfiguration($config_type) {
    $names = ['' => $this->t('- Select -')];

    // For a given entity type, load all entities.
    if ($config_type) {
      $entity_storage = $this->entityTypeManager->getStorage($config_type);
      foreach ($entity_storage->loadMultiple() as $entity) {
        $entity_id = $entity->id();
        if ($label = $entity->label()) {
          $names[$entity_id] = new TranslatableMarkup('@label (@id)', ['@label' => $label, '@id' => $entity_id]);
        }
        else {
          $names[$entity_id] = $entity_id;
        }
      }
    }

    return $names;
  }

}

