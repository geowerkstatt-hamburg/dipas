<?php

namespace Drupal\dipas_dev\Form;

use Drupal\Core\Form\FormStateInterface;

class DomainTools extends DipasDevToolsFormBase {

  public function getFormId() {
    return 'DipasDev';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['markup'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t(
        'Use this tool to reconfigure all domain records present to use the host name of the CURRENT system. Use it when the database was imported from another system.',
        [],
        ['context' => 'dipas_dev']
      ),
    ];

    $form['fix_domain_entries'] = [
      '#type' => 'submit',
      '#value' => 'Fix Domain entries',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->dipasDevService->modifyDomainRecords();
  }

}
