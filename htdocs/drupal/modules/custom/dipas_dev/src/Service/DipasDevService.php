<?php

namespace Drupal\dipas_dev\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

class DipasDevService {

  use StringTranslationTrait;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheConfig;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * DipasDevService constructor.
   *
   * @param RequestStack $request_stack
   * @param MessengerInterface $messenger
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param ConfigFactoryInterface $config_factory
   * @param CacheBackendInterface $cache_config
   * @param Connection $database
   * @param PrivateTempStore $temp_store_factory
   */
  public function __construct(
    RequestStack $request_stack,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CacheBackendInterface $cache_config,
    Connection $database,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->cacheConfig = $cache_config;
    $this->database = $database;
    $this->tempStore = $temp_store_factory->get('dipas_dev.file_tool');
  }

  /**
   * Changes defined domain records to use the current http host name.
   *
   * @return void
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function modifyDomainRecords($host = null, $port = null) {
    $httpHost = $host ?? $this->currentRequest->getHttpHost();
    $httpPort = $port ?? $this->currentRequest->getPort();

    if (preg_match('~localhost~i', $httpHost)) {
      $httpHost = sprintf(
        'localhost%s',
        (int) $httpPort !== 80
            ? ':' . $httpPort
            : ''
      );
    }

    /**
     * @var \Drupal\Core\Entity\EntityTypeInterface[]
     */
    $entityTypeDefinitions = $this->entityTypeManager->getDefinitions();
    $definedDomains = $this->entityTypeManager->getStorage('domain')->loadMultiple();

    foreach (array_keys($definedDomains) as $domain) {
      $config = $this->configFactory->getEditable(
        $entityTypeDefinitions['domain']->getConfigPrefix() . '.' . $domain
      );

      $config->set(
        'hostname',
        sprintf(
          '%s%s',
          $domain !== 'default' ? "$domain." : '',
          $httpHost
        )
      );
      $config->save();
    }

    $this->cacheConfig->invalidateAll();
    $this->messenger->addMessage('Domain-Records converted!');
  }

  public function fileScanReportDetails() {
    $entriesPerPage = 10;
    $result = $this->tempStore->get('dipas_dev.files.report');

    $report = [];

    if (count($result['corrupt'] ?? [])) {
      $report['corrupt'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Corrupted files (total: @count)', ['@count' => count($result['corrupt'] ?? [])]),
      ];
    }

    if (count($result['missing'] ?? [])) {
      $report['missing'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Missing files (total: @count)', ['@count' => count($result['missing'] ?? [])]),
      ];
    }

    foreach (['corrupt' => 'cp', 'missing' => 'mp'] as $property => $queryParam) {
      if (isset($result[$property])) {
        $totalPages = ceil(count($result[$property]) / $entriesPerPage);
        $currentPage = $this->currentRequest->query->has($queryParam) ? $this->currentRequest->query->get($queryParam) : 0;
        $displayEntries = array_slice(
          $result[$property],
          $currentPage * $entriesPerPage,
          $entriesPerPage
        );

        $report[$property][] = [
          '#markup' => $this->t('Page @current of @max', ['@current' => $currentPage + 1, '@max' => $totalPages]),
        ];

        if ($totalPages > 1) {
          $pager = [[
            '#markup' => ' | ',
          ]];

          if ($currentPage > 0) {
            $queryParams = $this->currentRequest->query->all();
            $queryParams[$queryParam] = $currentPage - 1;

            $pager[] = [
              '#markup' => Link::createFromRoute(
                'previous',
                'dipas_dev.file_tools.report',
                [],
                ['query' => $queryParams],
              )->toString()
            ];
          }

          if ($currentPage < $totalPages) {
            if ($currentPage > 0) {
              $pager[] = [
                '#markup' => ' | ',
              ];
            }

            $queryParams = $this->currentRequest->query->all();
            $queryParams[$queryParam] = $currentPage + 1;

            $pager[] = [
              '#markup' => Link::createFromRoute(
                'next',
                'dipas_dev.file_tools.report',
                [],
                ['query' => $queryParams],
              )->toString()
            ];
          }

          $report[$property][] = $pager;
        }

        foreach ($displayEntries as $entry) {
          $fileEntry = [
            '#type' => 'details',
            '#title' => $this->t('@filename (@count usages)', ['@filename' => $entry['file']->filename, '@count' => count($entry['usages'])]),

            'usages' => [
              '#type' => 'fieldset',
              '#title' => $this->t('Usages'),
            ],
          ];

          foreach ($entry['usages'] as $usage) {
            $fileEntry['usages'][] = [
              '#markup' => Link::fromTextAndUrl(
                sprintf('%s/%s', $usage->type, $usage->id),
                Url::fromUserInput(sprintf('/%s/%s/edit', $usage->type, $usage->id))
              )->toString(),
              '#prefix' => '<p>',
              '#sufffix' => '</p>',
            ];
          }

          $report[$property][] = $fileEntry;
        }
      }
    }

    return $report;
  }

}
