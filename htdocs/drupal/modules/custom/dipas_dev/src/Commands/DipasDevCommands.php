<?php

namespace Drupal\dipas_dev\Commands;

use Drupal\dipas_dev\Service\DipasDevService;
use Drush\Commands\DrushCommands;

class DipasDevCommands extends DrushCommands {

  /**
   * @var \Drupal\dipas_dev\Service\DipasDevService DipasDevService
   */
  protected DipasDevService $dipasDevService;

  /**
   * DipasDevCommands constructor.
   *
   * @param \Drupal\dipas_dev\Service\DipasDevService $dipasDevService
   */
  public function __construct(DipasDevService $dipasDevService) {
    parent::__construct();

    $this->dipasDevService = $dipasDevService;
  }

  /**
   * Updates domain records to use the current local HTTP host/port for development.
   * This is required because imported DIPAS database dumps contain live or staging URLs.
   *
   * @usage drush dipas-dev:fix-domain-entries
   *   Fix domains using the default values for host "localhost" and port "80"
   *
   * @usage drush dipas-dev:fix-domain-entries --host=localhost --port=80
   *   Fix domains but use custom values of host and port
   *
   * @command dipas-dev:fix-domain-entries
   *
   * @aliases dd:fde
   *
   * @param array $options drush command options for host and port
   * @return void
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function commandFixDomainEntries(array $options = ["host" => "localhost", "port" => 80]): void {
    $this->dipasDevService->modifyDomainRecords($options["host"], $options["port"]);
  }
}
