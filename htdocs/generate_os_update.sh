echo Erzeuge OS-Update-Ordner oder leere ihn
mkdir -p os_update_patches
rm os_update_patches/*
echo Verbinde remote Repository OpenSource
git remote add -f opensource https://bitbucket.org/geowerkstatt-hamburg/dipas.git
git remote update
echo Erzeuge zwei Patches
git diff opensource/dev origin/HEAD :^UPDATE.md :^CHANGELOG.txt :^bitbucket-pipelines.yml :^config/export/ :^config/local/ :^config/sync/dipas.default.configuration.yml :^config/sync/masterportal.config.default.layers.yml :^doc/DrupalRestAPI_de.md :^doc/conventions_de.md :^drupal/modules/custom/masterportal/libraries/masterportal/css/fonts.css :^drupal/modules/custom/masterportal/libraries/masterportal/css/woffs/Hamburg* :^public/HH_Bug.png :^public/HH_Logo.png :^public/themeconfig.json :^src/assets/HH_Bug.png :^src/assets/HH_Logo.png :^public/0-home-np.ico :^public/favicon.ico :^public/dipaslogo.jpg >  os_update_patches/basic_update_patches.patch
git diff opensource/dev origin/HEAD UPDATE.md config/sync/dipas.default.configuration.yml config/sync/masterportal.config.default.layers.yml drupal/modules/custom/masterportal/libraries/masterportal/css/fonts.css public/themeconfig.json >  os_update_patches/patches_to_check.patch
echo Trenne remote Repository OpenSource
git remote rm opensource
echo Erzeugen der Patches erledigt. Jetzt bitte checken und CHANGELOG.md anpassen
