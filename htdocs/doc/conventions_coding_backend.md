# Coding Conventions - DIPAS backend (Drupal / PHP)

The "DIPAS backend" is based on [Drupal 10](https://www.drupal.org/about/10) and [PHP 8.1](https://www.php.net/releases/8.1/en.php).

This document **describes the coding conventions for the "DIPAS backend"**. For the "DIPAS frontend" (Vue3) coding conventions, see the ["DIPAS frontend" coding conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_frontend.md).

> Version v1.0-alpha - Last update: 2023-09-05

- [General conventions](#general-conventions)
- [Drupal & PHP coding conventions](#drupal--php-coding-conventions)
- [Git](#git)
- [Definition of Done](#definition-of-done)

---

## General conventions
- File encoding is UTF-8 (no BOM).
- EOL is UNIX style / line feed only (\n).
- Code indentation is 2 spaces, no tabs.
- All files end with a single \n, to avoid git warnings "No newline at end of file".

> These general conventions are set in the project's `htdocs/.editorconfig` file.

- The language of the code and commends is English.

## Drupal & PHP coding conventions

For code in Drupal, code must follow the Drupal coding standards as described here: https://www.drupal.org/docs/develop/standards

Drupal's PHP coding standards are available here: https://www.drupal.org/docs/develop/standards/php/php-coding-standards

## Git

The Git conventions are described in detail in the ["DIPAS frontend" coding conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_frontend.md) and also apply to the DIPAS backend.

## Definition of Done

The "Definition of Done" is available in the document ["DIPAS frontend" coding conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_frontend.md) and also applies to the DIPAS backend.
