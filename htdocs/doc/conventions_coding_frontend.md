# Coding Conventions - DIPAS frontend (Vue 3)

The "DIPAS frontend" is created with [Vue3](https://vuejs.org/), HTML5 and [SCSS](https://sass-lang.com/guide/).

This document **describes the "DIPAS frontend"** coding conventions. For the "DIPAS backend" coding conventions (Drupal
and PHP), see
the  ["DIPAS backend" coding conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_backend.md).

All code must follow the coding standards as described below.

> Coding Conventions Version v1.0.3 - Last update: 2023-10-25

- [General conventions](#markdown-header-general-conventions)
- [Vue coding conventions](#markdown-header-vue-coding-conventions)
    - [Vue "script" container](#markdown-header-vue-script-container)
    - [Vue "template" container](#markdown-header-vue-template-container)
    - [Vue "style" container](#markdown-header-vue-style-container)
    - [VueX](#markdown-header-vuex)
    - [File structure](#markdown-header-file-structure)
- [Comments on code](#markdown-header-comments-on-code)
- [License hints in the code](#markdown-header-license-hints-in-the-code)
- [Linter](#markdown-header-linter)
- [Unit tests](#markdown-header-unit-tests)
- [Git](#markdown-header-git)
    - [Commits](#markdown-header-commits)
    - [Merge strategy](#markdown-header-merge-strategy)
    - [Branch naming conventions](#markdown-header-branch-naming-conventions)
- [Definition of Done](#markdown-header-definition-of-done)
- [Masterportal - Conventions & Clean coding rules](#markdown-header-masterportal-conventions-clean-coding-rules)

---

## General conventions

- File encoding is UTF-8 (no BOM).
- EOL is UNIX style / line feed only (\n).
- Code indentation is 2 spaces, no tabs.
- All files end with a single \n, to avoid git warnings "No newline at end of file".

> These general conventions are set in the project's `htdocs/.editorconfig` file.

- The language of the code and commends is English.

## Vue coding conventions

- This project uses VueJS' **Options API** syntax.
- The Sequence of containers in .vue files is:
    1. `<script>`
    2. `<template>`
    3. `<style>`

The coding conventions for each container are explained in detail below.

  ```vue

<script>
    export default {}
</script>

<template>
    <div class="DemoComponent">
        <!-- Content here -->
    </div>
</template>

<style lang="scss">
    div.DemoComponent {
        // scss here
    }
</style>
  ```

### Vue "script" container

- Use meaningful variable and method names.
- camelCase for declaration of functions
- Don't set the component name using the Vue component's "name:" attribute.
  ```vue
  export default {
    // name: "CheckboxGroup", <-- Remove, not needed!
    props: [],
    // ...
  }
  ```
- Add a single blank line before a return statement.
  ```js
  // Add empty line before return statement.
  receiveData () {
    const data = this.receiveDataFromEndpoint();
    const filteredData = data.filter(/* Do something here */);

    return filteredData;
  }

  // Skip new line for one-line return statement
  receiveFallbackData () {
    return this.fallbackData();
  }
  ```
- elseif / else on new line. (closing bracket on previous line)
  ```js
  if (dataAvailable) {
    //
  }
  else if (fallbackDataAvailable) {
    //
  }
  else {
    //
  }
  ```
- Prefer switch/case over elseif...elseif...else.
- Conditional blocks are prefixed and suffixed by a blank line.
  ```js
  // ...

  if (data) {
    //
  }

  if (fallbackData) {
    //
  }

  // ...
  ```
- Component properties have to be fully defined. (type/required/default, optional validator)
  ```vue
  <script>
  export default {
    props: {
      modelValue: {
        type: [String, Number],
        required: false,
        default: ""
      },
      label: {
        type: String,
        required: true,
      },
      title: {
        type: [String, Boolean],
        required: false,
        default: false,
      },
    }
  }
  </script>
  ```
- Ternary operator: "?" and ":" after line break only. (except: very short TOs like `myVar = foo ? bar : baz`)
  ```js
  var data = dataAvailable
    ? this.receiveData()
    : this.receiveFallbackData();
  ```
- Single space between function name and argument parenthesis on function definitions.
  ```js
  receiveData () {
    //
  }
  ```
- No space between function name and parenthesis on function calls.
  ```js
  const data = receiveData();
  ```

### Vue "template" container

- Only one root element is allowed, e.g. `<div class="DemoComponent"></div>`.
- A CSS class with the component name on the root element is required. The class name must be in PascalCase,
  e.g. `DemoComponent`. (The class is later used to soft-scope the CSS in the Vue "style" container, see below)
- Set HTML tag attributes on a new line (Except tags with only one attribute). Closing tag on newline. Contents of HTML
  tags are always on dedicated lines.
  ```html
  <div
    id="details"
    @click="..."
    class="..."
  >
    ...
  </div>

  <span class="info">
    ...
  </span>
  ```
- Own component properties should be set in camelCase. (e.g. `:isOpen="true"` instead of `:is-open="true"`)
  ```html
  <SidebarToggle
    :isOpen="true"
  >
  </SidebarToggle>
  ```
- Nodes on the same level are separated with one blank line.
  ```vue
  <template>
    <div class="DemoComponent">
      <foo>....</foo>

      <bar>...</bar>

      <baz>...</baz>
    </div>
  </template>
  ```
- Encapsulated nodes are indented by 2 spaces, but not separated from the parent by a blank line.
  ```vue
  <template>
    <div class="DemoComponent">
      <foo>
        <bar>...</bar>
      </foo>
    </div>
  </template>
  ```
- Nodes without content are self-closing. (e.g. `<br />`)

### Vue "style" container

- SCSS must be used for CSS styling.
- Vue's style scope attribute is not allowed. (NO `<style scoped>`)
- Soft-scoping is in effect - a component has to utilize a CSS class for it's root element and all styles must be
  prefixed with that scope
  e.g. `div.DemoComponent div {...}`.
- Only ONE "root style element" - ALL other styles have to be encapsulated in root. (Exceptions require good and valid
  reasons!)
- All CSS selectors have to specifically utilize the DOM node type (e.g. div.className, not .className only)
- Use `rem` units. (e.g. 1rem === 16px, 0.0625rem === 1px) - `rem` units should not be rounded. (e.g. use 0.0625rem for
  1px, not 0.06rem) [Online rem converter](https://nekocalc.com/de/px-zu-rem-umrechner)

Example:

```vue

<style lang="scss">
    div.DemoComponent {
        someStyle: someValue;

        a.myClass {
            someStyle: someValue;
            anotherStyle: someValue;

            &:hover {
                someStyle: someValue;
            }
        }
    }
</style>
```

### Vuex

- [Vuex](https://vuex.vuejs.org/), a Vue state management library, is used in the project.
- **NO** use of MapState / MapActions / MapMutations / MapGetters. Some IDEs are capable of tracing defined variables
  and those shorthands might interfere with that
- **NO** direct access to `$store.state` in components!

### File structure

For the Vue-framework several best practices are recommended.

In this project the following of these recommendations are chosen to be used:

#### Project folder structure

  ```
  htdocs/
    vue/
      src/
        assets/
          fonts/
          images/
        components/
          StyledTextarea.vue
        locales/
          de.json
          en.json
        pages/
          HomePage/
            HomePage.vue
        router/
        scss/
        store/
        utils/
      tests/
        components/
          StyledTextarea.spec.js
  ```

#### Components

- Single-file component filename casing: Filenames of component files shall be named in PascalCase:

  ```
  components/
  |- MyComponent.vue (PascalCase)
  ```

- One-word component names are allowed. (e.g. Sidebar) If a component is created for an HTML element (e.g. button,
  textarea), a multi-word name must be used to avoid conflicts. (e.g. StyledButton, StyledTextarea)

- Tightly coupled component names: Structure and coupling of components shall be available from the file name. A
  senseful and ordered folder structure shall be used.

  ```
  components/
    |- TodoList.vue
    |- TodoListItem.vue
    |- TodoListItemButton.vue
    |- SearchSidebar.vue
    |- SearchSidebarNavigation.vue
  ```

- Order of words in component names: Order of words in the filename shall mirror the component structure.

```
components/
  |- SearchButtonClear.vue
  |- SearchButtonRun.vue
  |- SearchInputQuery.vue
  |- SearchInputExcludeGlob.vue
  |- SettingsCheckboxTerms.vue
  |- SettingsCheckboxLaunchOnStartup.vue
```

- Full-word component names: The name of components shall consist of full words instead of abbreviations.

```
components/
  |- StudentDashboardSettings.vue
  |- UserProfileOptions.vue
```

## Comments on code

- The written source code should be self-explanatory and written with meaningful variable and function names.
- JSDoc can be used accompanying, but inline comments are preferred on complex code.

## Linter

- [JSLint](https://www.jslint.com/), a code quality tool, is configured in the project to ensure the code style.
- Run `npm run lint` (report linter errors only)
- Run `npm run lint:fix` (auto fix linter errors)

> The project's linter Rules are stored in `htdocs/vue/.eslintrc.json`

## Unit tests

- [Vitest](https://vitest.dev/guide/features.html) is used to run the project's unit tests.
- Tests need to be created or updated as features are developed.
- Run `npm run test:unit` to test the whole DIPAS frontend project.
- Unit test files mirror the directory structure of the file under test.
- Unit test files have the same name as the file under test. The original file extension is replaced by `.spec.js`.
  ```
  File under test: htdocs/vue/src/components/SidebarToggle.vue
  Unit test file: htdocs/vue/test/components/SidebarToggle.spec.js
  ```

## Git

### Commits

- Language of the commits shall be English
- Commit messages shall be prefixed with the ticket number. (e.g. "DPS-1234: A meaningful commit message") If there is
  no Ticket, use "w/o Ticket: A meaningful commit message"

### Merge strategy

- Merge commits are prohibited. Use git rebase instead.

### Branch naming conventions

- No GIT-flow strategy (no "feature/")
- Branches shall be prefixed with the ticket number (if there is one) - See also "Commits"

## Definition of Done

- Code follows these conventions
- Tests have been extended
- Unit tests for new modules have been added
- German and English translations have been added for new UI components
- Function meets the requirements described in the ticket
- Function has been tested in established browsers (Chrome, Edge, Firefox)
- Function has been tested on mobile device or at least in mobile emulation of Chrome
- Function has been tested using a Screen reader (e.g. Microsoft Narrator or NVDA)
- Function and all its elements have been tested on DSGVO compliance
- Function has been tested with "real life" data
- Function has been tested in 1920x1280 and 150% display zoom
- Pull request contains a description how the function of the ticket can be tested and approved

## Masterportal - Conventions & Clean coding rules

The Masterportal repository also contains a set of conventions that apply to the DIPAS frontend.

All conventions described in the current document take precedence for the DIPAS frontend.

Part B of the Masterportal conventions is mandatory in DIPAS to improve code quality.

- [Masterportal - Coding conventions](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/codingConventions.md)
- [Masterportal - Additional information on the coding conventions](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/codingConventionsInfo.md)
