# Coding Conventions - DIPAS

The DIPAS project consists of a **DIPAS backend** based on Drupal and PHP and a **DIPAS frontend** based on Vue 3.

For both there is a set of **coding conventions** that must be followed when developing functions for the projects.

**You can find the code conventions here:**
-
- [DIPAS "frontend" Coding Conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_frontend.md) (Vue 3)
- [DIPAS "backend" Coding Conventions](https://bitbucket.org/geowerkstatt-hamburg/dipas_community/src/dev/htdocs/doc/conventions_coding_backend.md) (Drupal / PHP)

These documents cover the general coding conventions, but also rules for working with the project's Git repository and the "definition of done".
