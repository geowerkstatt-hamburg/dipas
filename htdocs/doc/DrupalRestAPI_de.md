## Dokumentation der durch Drupal bereitgestellten REST-Schnittstelle


#### Aufruf der Endpoints
Grundsätzlich sind die REST-Endpoints unter der URL  
`[server.tld[:port]]/drupal/dipas/[endpoint]`  
aufrufbar. Zuständig für Instantiierung und Aufruf der Plugins ist der
Service  
`Drupal\dipas\Service\RestApi::requestEndpoint`.  
Die Antwort der Endpoints wird contextsensitiv inkl potentieller GET-Parameter gecached
(d.h. `/drupal/dipas/contributionlist?page=1` hat einen anderen Cache als
`/drupal/dipas/contributionlist?page=2`). Soll der Cache umgangen werden,
kann der GET-Parameter `noCache` an die Endpoint-URL angehangen werden
(z.B. `/drupal/dipas/contributionlist?page=2&noCache`)


#### Entwicklung von (neuen) Endpoints
Endpoints werden mittels PHP-Plugins bereitgestellt. Diese finden sich unter
`/drupal/modules/custom/dipas/src/Plugin/ResponseKey`

Plugins müssen das Interface `Drupal\dipas\PluginSystem\ResponseKeyPluginInterface`
implementieren und können auf `Drupal\dipas\Plugin\ResponseKeyBase` aufsetzen. Das
Attribut `id` in der Annotation des Plugins ist für den URL-Pfad entscheidend.

Plugins können die zulässigen Requestarten (Get/Post/...) einschränken. Wird der
Endpoint mit einer nicht zulässigen Requestart aufgrufen, gibt der Server ein
HTTP/404 zurück.


#### Verfügbare Endpoints
Siehe Files in `/drupal/modules/custom/dipas/src/Plugin/ResponseKey`, welche nicht
"Interface", "Base" oder "Trait" im Dateinamen tragen. Die jeweiligen Endpoints
stehen als Attribut `id`in den Annotations (Kommentarbereich über der Klassendefinition).


#### Verfügbare GET-Parameter

- bei allen Endpoints:
  - `noCache`:  
  
    unterdrückt das Laden/Ausliefern eines Caches des Endpoints (Caches werden neu generiert!).
  
- Endpoint `contributionlist`  
  - `itemsPerPage`:  
    Default 10. Wird `-1` oder `0` übergeben, findet keine Paginierung statt. 
  - `page`:  
    Einzelne, spezifische Seite des Pagers. Sind z.B. 23 Beiträge vorhanden,
    und wird `?page=2` aufgerufen, werden die Beiträge 11-20 gezeigt.


#### Erforderliche Parameter

- Endpoint `contributiondetails`:  

  URL: `/drupal/dipas/contributiondetails/[id]`  
  - `id`: Typ `int`  
  
  Beispielaufruf:  
  `/drupal/dipas/contributiondetails/12`  
  
  Antwort des Servers:
  - im Erfolgsfall das Datenobjekt des Beitrags
  - im Fehlerfall ein Array mit status "error" und der Fehlermeldung

- Endpoint `getcomments`:  

  URL: `/drupal/dipas/getcomments/[id]`  
  - `id`: Typ `int`  
  
  Beispielaufruf:  
  `/drupal/dipas/getcomments/12`  
  
  Antwort des Servers:  
  - im Erfolgsfall das Datenobjekt mit den Kommentaren des Nodes  
  - im Fehlerfall ein Array mit status "error" und der Fehlermeldung  

- Endpoint `addcontribution`:  

  Erforderliche POST-Felder:  
  `title`: Typ `string`  
  `text`: Typ `string`  
  `category`: Typ `int`  
  `rubric`: Typ `int`  
  `lat`: Typ `float` (ob Pflichtfeld abhängig von Konfiguration)  
  `lon`: Typ `float` (ob Pflichtfeld abhängig von Konfiguration)  
  `user`: Typ `int` (ob Pflichtfeld abhängig von Konfiguration)  
  
  Antwort des Servers bei Erfolg:  
  `nid`: Typ `int`, Node-ID des neu angelegten Nodes 

- Endpoint `addcomment`:  

  Erforderliche POST-Felder:  
  `rootEntityID`: Typ `int` (ID des Contribution-Nodes)  
  `commentedEntityType`: Typ `string` (`node` oder `comment`)  
  `commentedEntityID`: Typ `int`  
  `subject`: Typ `string` (mindestens leerer String)  
  `comment`: Typ `string`
  
  Antwort des Servers:
  - im Erfolgsfall das komplette Kommentararray wie im Endpoint `getcomments`  
  - im Fehlerfall ein Array mit status "error" und der Fehlermeldung  

- Endpoint `rate`:  

  URL: `/drupal/dipas/rate/[id]`  
  - `id`: Typ `int`  
  
  Beispielaufruf:  
  `/drupal/dipas/rate/12`  
  
  Erforderliche POST-Felder:  
  - `rating`: Typ `int` (1 für upvote, -1 für downvote)  
  
  Verfügbare POST-Felder:  
  - `entity_type`: Typ `string` (Entity-Typ der zu bewertenden Entity (Standard: "node"))  
  
  Antwort des Servers:  
  - im Erfolgsfall ein Datenobjekt mit der aktuellen Bewertungswerten  
  - im Fehlerfall ein Array mit status "error" und der Fehlermeldung  

- Endpoint `confirmcookies`:  

  Erforderliche POST-Felder:  
  - `confirmCookies`: Typ `bool`  
  
  Antwort des Servers:  
  - im Erfolgsfall ein Datenobjekt mit der Nachricht, dass Cookies aktiviert wurrden (status: success)
  - im Fehlerfall ein Datenobjekt mit der Nachricht, dass Cookies nicht aktiviert wurden (status: error)
  
   
