# Update Guide
This document contains a general guide how to perform updates of the project as well as a guide per version with extra steps
needed to go from version to version. Please note that updates are only supported between minor versions, or the latest minor
to the next major version!

*You will find instructions for the upgrade from version 1.x to version 2.x [below](#upgrade-from-1.x-to-2.x).*

## General
The general procedure works as follows:

0. **IMPORTANT:** Backup all files and the database to prevent data loss
1. Navigate to `YOURDOMAIN.TLD/drupal/de/admin/config/development/maintenance`, enable maintenance mode and save the configuration
2. Copy the following files & directories to a temporary location for later restoring:
   1. `config/drupal.database-settings.php`
   2. `config/drupal.salt.inc.php`
   3. `config/drupal.reverse-proxy-settings.php`
   4. `config/drupal.services.yml`
   5. `htdocs/drupal/sites/default/files`
3. Delete the folders "config", "htdocs" and "tmp" containing the project files
4. Extract the contents of the new ZIP into the location of the previously deleted folders. This will recreate all folders.
5. Ensure the web server has write permissions on `tmp` and `htdocs/drupal/sites/default/files`
6. Copy the files from step #2 back to their original locations
7. Navigate to `YOURDOMAIN.TLD/drupal/admin/config/development/configuration`
8. Scroll to the bottom and click *Import all*. (There might be no changes, then there is no button. In these cases, skip ahead to step #10)
9. Repeat step #8 a second time to make sure potential changes to the config ignore list get respected.
10. Navigate to `YOURDOMAIN.TLD/drupal/update.php` & follow the wizard and execute all available update steps. There might be no available update steps.
11. Navigate to `YOURDOMAIN.TLD/drupal/de/admin/config/development/maintenance`, disable maintenance mode and save the configuration


## Versions with domain-module

### Update to version 3.0.0
IMPORTANT: Starting with this release the included services definition of the Masterportal module is reduced to publicly
available services. If you currently are using the included services definitions file, then please make sure to place a
copy of your current version on your web server and configure the path to that file AFTER the update was run. The included
services.json is found under the following path:
htdocs/drupal/modules/custom/masterportal/libraries/masterportal/services.json
The configuration interface is found under this URL: /admin/config/user-interface/masterportal/basics - if this interface
reads "{{library_path}}/services.json" in the configuration field for services, you are using the included file.
Additionally, Cesium (the 3D library used in the Masterportal) had to be updated to version 1.113.
The path to this library has to be updated in the Masterportal basic settings --> html-structure in the src to Cesium.js
(in the lower part of the configuration value):

<script type="text/javascript" src="{{base_path}}modules/custom/masterportal/libraries/cesium/1_113/Cesium.js"></script>

### Update to version 2.9.7
Prior to the update, please log on to the Drupal administrative backend as a site administrator. Then, please navigate to
`/drupal/de/admin/config/development/configuration/ignore`, remove the line `dipas_statistics.settings` and save the
configuration. After that, the code base can be updated and the update procedure follows normal paths. Adjust the settings
in `config/drupal.services.yml` to your needs.

### Update to version 2.9.6
Beginning from version 2.9.6, the config folder holding all Drupal configuration files was moved outside of the document
root directory of the webserver in order to improve on application security. It now resides on the same file system path
besides htdocs. Please make sure that potential remnants  of the config folder in the old location (htdocs/config) are
properly deleted.
In the update instructions above ignore the mentioning of the folder "htdocs/" when creating the file copies and treat
the files/locations named as if there was no "htdocs/" prefix to the paths stated (i.e., save "drupal/sites/default/files"
instead of "htdocs/drupal/sites/default/files"). When restoring the files, move them to the appropriate locations within
"htdocs/".
This update includes an update of Drupal core to version 10. With this update, a core theme used by DIPAS was moved outside
of Drupal core, which moved the theme's code files to a different location within the code base. This requires a hard
reset of database cache tables, since otherwise library files of this theme will be found (a "cache rebuilt" by either
the Drupal backend or drush will not suffice, since the error will be triggered before these measures can take effect).
In order to truncate the cache tables, log on to your database and run the following queries (replace "dipas" with the
actual name of your database):
TRUNCATE TABLE dipas.public.cache_entity;
TRUNCATE TABLE dipas.public.cache_bootstrap;
TRUNCATE TABLE dipas.public.cache_config;
TRUNCATE TABLE dipas.public.cache_container;
TRUNCATE TABLE dipas.public.cache_data;
TRUNCATE TABLE dipas.public.cache_default;
TRUNCATE TABLE dipas.public.cache_discovery;
TRUNCATE TABLE dipas.public.cache_dynamic_page_cache;
TRUNCATE TABLE dipas.public.cache_file_mdm;
TRUNCATE TABLE dipas.public.cache_menu;
TRUNCATE TABLE dipas.public.cache_page;
TRUNCATE TABLE dipas.public.cache_render;
TRUNCATE TABLE dipas.public.cache_toolbar;

There is a new Masterportal Addon for the DIPAS navigator which enables opening the GFI of a proceeding from outside the Masterportal.
This Addon needs to be added to the basic settings of the Masterportal Basic Javascript configuration.
In the Masterportal Basic Settings -> Basic JavaScript Configurations in the [] of the addons add "openGfiRemotely".

### Update to version 2.9.5
After updating, navigate to /drupal/admin/config/user-interface/dipas-design/colors to configure your desired color scheme, logo images and fonts.
If you are implementing a reverse proxy / load balancer infrastructure, adapt and enable the related settings in `config/drupal.reverse-proxy-settings.php`.

### Update to version 2.9.2
1. In the Masterportal Basic Settings -> Basic JavaScript Configurations in the [] of the addons "dipasAddons-dipasStorySelector", "dipasAddons-storyTellingTool" should be removed again as those are set programmatically now.

### Update to version 2.9.1
There is a new Masterportal instance for the DIPAS Story Telling Tool. Since the import of Masterportal instances from the configuration is disabled by default,
this new instance was set as an exeption from this ignore-rule and therefore this time there is a need to import the configuration twice (first import run will
update the config_ignore settings, second run will then create a new Masterportal instance named "default.dipas_story_telling").

### Update to version 2.9.0

1. In the Masterportal Basic Settings -> Basic JavaScript Configurations add ?{{query_params}} at the end of the URLs of the portalConf, layerConf, restConf and styleConf

### Update to version 2.8.0

There is a new optional Parameter in the Masterportal Instance configurations that only allows the map to be panned using two fingers.
By default this option is ticked off.
For the DIPAS-Navigator this Parameter needs to be checked.
This needs to be done in the Masterportal Instance "Cockpit Map" under 'Map settings' -> Use two finger pan
The tickbox 'Use map scale options' underneath can also be ticked off in the 'Cockpit Map' Map settings.

### Update to version 2.6.5

**To update to this version some important steps are neccessary:**

1. before you follow step 6. in the update procedure description navigate to `YOURDOMAIN.TLD/drupal/admin/config/development/configuration/full/import`
  1.1. open tab *"single item"*
  1.2. choose Configuration type *"Simple configuration"*
  1.3. type Configuration name *"config_ignore.settings"*
  1.4. paste the complete content of the file `config\sync\config_ignore.settings.yml` in the text field
  1.5. import the single configuration by pressing the button von bottom of the page
2. then go on with the update procedure as described [above](#general)
3. after having finished the update procedure some settings in Masterportal configuration need to be edited manually
  3.1. for each proceeding the Masterportal instance *"Create Appointment"* needs to be edited
  3.2. go to *"Portal Settings"* and remove the checkmarks for *"Use setMarker"* and *"Center the map around a marker set"*
  3.3. go to *"Tool plugins"* and enable *"Draw"*-tool, then uncheck *"Draw-Tool is visible in Masterportal menu"*
4. flush all caches

### Update to version 2.6.2

**To update to this version some important steps are neccessary:**

To update to this version please import configuration before you navigate to update.php (***swap step 6 and 7 in update procedure description***)


### Upgrade from 1.x to 2.x

This update will update from drupal 8 to drupal 9. Therefore some special steps are necessary!
Additionally have in mind the changed [requirements](https://www.drupal.org/docs/system-requirements) for installation of drupal 9.

- in the DIPAS 1.x-version
    - navigate to `YOURDOMAIN.TLD/drupal/admin/modules/uninstall` and uninstall module "nimbus"
    - uninstall module "domain_migrate" as well if it is in use
    - flush all caches
    - create a dump of the database
- import the dump to the new PostgreSQL database installed for usage with drupal 9
- then follow steps 0 to 5 of the generel update guide [above](#general)
- asure write rights for everyone in folder `drupal/sites/default/files`
- open `/drupal/sites/default/settings.php` in a texteditor and set parameter `$settings['update_free_access']` to TRUE
- follow step 6 to 8 of the general update guide [above](#general)

### From 1.0.2 to 1.2.0

No manual changes are necessary.

### From 1.0.0 to 1.0.2

No manual changes are necessary.

### From 1.0.0 to 1.0.1

Take care of the config-ignore-ignore-bug!
Configs will be overwritten, please do not use 1.0.1 to update a running system!


## Versions without domain-module

### From 0.5.3 to 0.5.5.2

No manual changes are necessary.

### From 0.5.3 to 0.5.5.1

Take care of the config-ignore-ignore-bug!
Configs will be overwritten, please do not use 0.5.5.1 to update a running system!

### From 0.4.1 to 0.5.3

No manual changes are neccessary.

### From 0.3.0 to 0.4.1

No manual changes are neccessary.

### From 0.2.1 to 0.3.0

No manual changes are neccessary.

### From 0.1.0 to 0.2.1

No manual changes are neccessary.

### From unversioned to 0.1.0

All custom settings made to masterportal configuration will be lost and need to be reapplied by hand after the update.
This includes the basic settings, custom layers and custom styles. The masterportal instances will remain unchanged!
