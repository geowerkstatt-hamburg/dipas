<?php

/**
 * If you are using a reverse proxy / load balancer, adjust and enable the
 * settings below to your needs.
 *
 * IMPORTANT:
 * You have to make sure that your reverse proxy sets the X_FORWARDED_PROTO
 * header on the request reaching your web server to ensure the proper
 * generation of links!
 */
# $settings['reverse_proxy'] = TRUE;
# $settings['reverse_proxy_addresses'] = ['1.2.3.4', ...];
# $settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_ALL
