# Installation of DIPAS

## Requirements
- php >= 8.1.16
- apache
- postgres

## Procedure
1. Unpack the zip to a location on your webserver
2. Enter the database credentials in `config/drupal.database-settings.php`
3. Adjust/enable the settings contained in `config/drupal.reverse-proxy-settings.php` (optional / if needed)
4. Configure your webserver to use the htdocs directory within the extracted zip contents as the document root
5. Make sure the webserver has write permissions on the extracted "tmp" folder
6. Navigate to `YOURDOMAIN.TLD/drupal/install.php`
7. Choose your language (currently only used for the wizard because the config defaults to german)
8. Choose `Installation from existing configuration`
9. Finish the wizard
10. Enter account details for the main administration account
11. After the installation completes login into drupal and navigate to `YOURDOMAIN.TLD/drupal/admin/config/development/configuration/ignore`
12. Clear the textarea and save
13. Navigate to `YOURDOMAIN.TLD/drupal/admin/config/development/configuration`
14. Scroll to the bottom and click *Import all*
15. Flush all caches
16. Create the default domain by navigating to `YOURDOMAIN.TLD/drupal/de/admin/config/user-interface/proceedings/add`
17. Configure DIPAS to your needs (see [Step by Step guide (german language)](https://wiki.dipas.org/index.php/Verfahrensvorlage_erstellen))
